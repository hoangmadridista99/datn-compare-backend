import { Logger, ValidationPipe, VersioningType } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';
import { AppModule } from './app.module';
import { ENV_CONFIG } from './shared/constants/env.constant';
import { useContainer } from 'class-validator';
import { getSwaggerBuilderConfig } from './shared/constants/swagger/swagger-config';

const logContent = (port: number) => {
  return `Server listening on http://localhost:${port}`;
};
const appDisableOption = 'x-powered-by';

async function bootstrap() {
  const { port, apiVersion } = ENV_CONFIG.system;
  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  useContainer(app.select(AppModule), { fallbackOnErrors: true });
  app.enableCors();
  app.enableVersioning({
    type: VersioningType.URI,
    defaultVersion: [apiVersion],
  });
  app.disable(appDisableOption);

  // Setup Swagger
  await getSwaggerBuilderConfig(app);

  // Setup auto-validations
  app.useGlobalPipes(new ValidationPipe({ transform: true }));

  await app.listen(port);
  Logger.log(logContent(port));
}
bootstrap();
