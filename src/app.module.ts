import { Module } from '@nestjs/common';
import { RouterModule } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ClientModule } from './module/client/client.module';
import { OperatorModule } from './module/operator/operator.module';
import { VendorModule } from './module/vendor/vendor.module';
import { dataSourceOption } from './data-source';
import { registerRouterModule } from './shared/constants/modules.constant';
import { importAllProviders } from './shared/constants/providers.constant';

@Module({
  imports: [
    TypeOrmModule.forRoot(dataSourceOption),

    RouterModule.register(registerRouterModule()),

    OperatorModule,
    ClientModule,
    VendorModule,
  ],
  providers: [...importAllProviders()],
})
export class AppModule {}
