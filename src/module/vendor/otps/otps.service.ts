import { Injectable } from '@nestjs/common/decorators/core/injectable.decorator';
import {
  getError,
  LIST_ERROR_CODES,
} from '../../../shared/constants/error/base.error';
import { NO_CONTENT_SUCCESS_RESPONSE } from '../../../shared/lib/responses/response';
import { OtpsService } from '../../core/otps/otps.service';
import { HttpStatus } from '@nestjs/common';
import { UsersService } from '../../core/users/users.service';
import {
  IOtpDTO,
  IOtpLoginDto,
} from '../../core/otps/interfaces/otp.interface';
import { DestinationTypes, OtpTypes } from '../../core/otps/enums/otp.enum';
import { UserRole } from '../../core/users/entities/user.entity';

@Injectable()
export class VendorOtpsService {
  constructor(
    private readonly otpsService: OtpsService,
    private readonly usersService: UsersService,
  ) {}

  async sendOtpForVendorRegister(body: IOtpDTO) {
    try {
      const {
        destination_type,
        type,
        phone,
        email,
        numberOfSendingOtp,
        destination,
      } = body;

      const validateBody = await this.usersService.validateEmailAndPhoneInBody(
        email,
        phone,
      );
      if (typeof validateBody === 'string')
        return getError(validateBody, HttpStatus.UNPROCESSABLE_ENTITY);

      const code = await this.otpsService.sendOtp(
        destination,
        destination_type,
      );
      if (!code)
        return getError(LIST_ERROR_CODES.SS24501, HttpStatus.BAD_REQUEST);

      await this.otpsService.createOtp(
        code,
        destination,
        type,
        destination_type,
        numberOfSendingOtp,
      );

      return NO_CONTENT_SUCCESS_RESPONSE;
    } catch (error) {
      console.log(
        '🚀 ~ file: otps.service.ts:61 ~ VendorOtpsService ~ sendOtpForVendor ~ error:',
        error,
      );
      return getError(LIST_ERROR_CODES.SS24001);
    }
  }

  async sendOtpForVendorLogin(body: IOtpLoginDto) {
    const { destination_type, destination, numberOfSendingOtp } = body;
    const vendorAccount =
      destination_type === DestinationTypes.EMAIL
        ? await this.usersService.findOneByUsersQuery({ email: destination })
        : await this.usersService.findOneByUsersQuery({ phone: destination });
    if (!vendorAccount)
      return getError(
        LIST_ERROR_CODES.SS24211,
        HttpStatus.UNPROCESSABLE_ENTITY,
      );
    if (vendorAccount.role !== UserRole.VENDOR)
      return getError(LIST_ERROR_CODES.SS24003, HttpStatus.FORBIDDEN);
    const code = await this.otpsService.sendOtp(destination, destination_type);
    if (!code)
      return getError(LIST_ERROR_CODES.SS24501, HttpStatus.BAD_REQUEST);
    const otpType = OtpTypes.LOGIN;
    await this.otpsService.createOtp(
      code,
      destination,
      otpType,
      destination_type,
      numberOfSendingOtp,
    );
    return NO_CONTENT_SUCCESS_RESPONSE;
  }
}
