import { Body, Controller, HttpCode, Post } from '@nestjs/common';
import { ApiBody, ApiTags } from '@nestjs/swagger';
import {
  getError,
  LIST_ERROR_CODES,
} from '../../../shared/constants/error/base.error';
import { VendorOtpsService } from './otps.service';
import {
  IOtpDTO,
  IOtpLoginDto,
} from '../../core/otps/interfaces/otp.interface';
import {
  SendOtpDto,
  SendOtpLoginForVendorDto,
} from 'src/module/core/otps/dto/otp.dto';

@ApiTags('Otps')
@Controller('otps')
export class VendorOtpsController {
  constructor(private readonly clientOtpsService: VendorOtpsService) {}

  @Post('register/send-otp')
  @ApiBody({ type: SendOtpDto })
  @HttpCode(204)
  async sendOtpForRegister(@Body() body: IOtpDTO) {
    try {
      return await this.clientOtpsService.sendOtpForVendorRegister(body);
    } catch (error) {
      console.log('error', error);
      throw getError(LIST_ERROR_CODES.SS24001);
    }
  }

  @Post('login/send-otp')
  @ApiBody({ type: SendOtpLoginForVendorDto })
  @HttpCode(204)
  async sendOtpForLogin(@Body() body: IOtpLoginDto) {
    try {
      return await this.clientOtpsService.sendOtpForVendorLogin(body);
    } catch (error) {
      console.log('error', error);
      throw getError(LIST_ERROR_CODES.SS24001);
    }
  }
}
