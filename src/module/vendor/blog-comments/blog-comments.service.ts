import { Injectable } from '@nestjs/common';
import { BlogCommentsService } from '../../core/blog-reviews/blog-comments.service';
import { PaginateQuery } from 'nestjs-paginate';

@Injectable()
export class VendorBlogCommentsService {
  constructor(private readonly blogCommentsService: BlogCommentsService) {}

  async getListCommentsByBlogId(id: string, query: PaginateQuery) {
    return await this.blogCommentsService.getAllCommentsByBlogId(id, query);
  }
}
