import { Controller, Get, Param, UseGuards } from '@nestjs/common';
import {
  ApiTags,
  ApiBearerAuth,
  ApiOperation,
  ApiQuery,
} from '@nestjs/swagger';
import { VendorRoles } from '../../../shared/decorators/role.decorator';
import { JwtUserAuthGuard } from '../../core/auth/guards/jwt-auth.guard';
import { VendorRolesGuard } from '../../core/auth/guards/vendor/vendor-role.guard';
import { UserRole } from '../../core/users/entities/user.entity';
import { VendorBlogCommentsService } from './blog-comments.service';
import { UUIDPipe } from '../../../shared/pipes/uuid.pipe';
import { Paginate, PaginateQuery } from 'nestjs-paginate';
import {
  LIST_ERROR_CODES,
  getError,
} from '../../../shared/constants/error/base.error';
import {
  PAGE_QUERY,
  LIMIT_QUERY,
} from '../../../shared/constants/swagger/constant';

@ApiTags('Blog-Comments')
@Controller('blog-comments')
@UseGuards(JwtUserAuthGuard, VendorRolesGuard)
@VendorRoles(UserRole.VENDOR)
@ApiBearerAuth()
export class VendorBlogCommentsController {
  constructor(
    private readonly vendorBlogCommentsService: VendorBlogCommentsService,
  ) {}

  @Get('blogs/:blog_id')
  @ApiQuery(PAGE_QUERY)
  @ApiQuery(LIMIT_QUERY)
  @ApiOperation({ summary: 'Get list comments by blog id' })
  async getListCommentsByBlogId(
    @Param('blog_id', new UUIDPipe()) id: string,
    @Paginate() query: PaginateQuery,
  ) {
    try {
      return await this.vendorBlogCommentsService.getListCommentsByBlogId(
        id,
        query,
      );
    } catch (error) {
      console.log(
        '🚀 ~ file: blog-comments.controller.ts:30 ~ VendorBlogCommentsController ~ error:',
        error,
      );
      return getError(LIST_ERROR_CODES.SS24001);
    }
  }
}
