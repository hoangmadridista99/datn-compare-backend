import {
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  Param,
  Patch,
  Post,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiQuery,
  ApiTags,
} from '@nestjs/swagger';
import { VendorRoles } from '../../../shared/decorators/role.decorator';
import { JwtUserAuthGuard } from '../../core/auth/guards/jwt-auth.guard';
import { VendorRolesGuard } from '../../core/auth/guards/vendor/vendor-role.guard';
import { User, UserRole } from '../../core/users/entities/user.entity';
import { VendorBlogsService } from './blogs.service';
import { AuthResponse } from '../../../shared/decorators/response-auth/auth.decorator';
import {
  LIST_ERROR_CODES,
  getError,
} from '../../../shared/constants/error/base.error';
import {
  CreateBlogDto,
  VendorUpdateBlogDto,
} from '../../core/blogs/dto/blog.dto';
import { BlogStatusesTypes } from '../../core/blogs/enums/blogs.enum';
import { UUIDPipe } from '../../../shared/pipes/uuid.pipe';
import { Paginate, PaginateQuery } from 'nestjs-paginate';
import { VendorFilterBlogs } from '../../../shared/decorators/blogs/vendor-filter.blogs';
import { IVendorBlogsFilter } from '../../core/blogs/interfaces/blogs.interface';
import {
  PAGE_QUERY,
  LIMIT_QUERY,
  CREATED_AT_QUERY,
} from '../../../shared/constants/swagger/constant';
import {
  TITLE_QUERY,
  STATUS_QUERY,
  BLOG_CATEGORY_ID_QUERY,
} from '../../../shared/constants/swagger/blogs/blogs.constant';

@ApiTags('Blogs')
@Controller('blogs')
@UseGuards(JwtUserAuthGuard, VendorRolesGuard)
@VendorRoles(UserRole.VENDOR)
@ApiBearerAuth()
export class VendorBlogsController {
  constructor(private readonly vendorBlogsService: VendorBlogsService) {}

  @Post()
  @ApiOperation({ summary: 'Create new blog for vendor' })
  async createBlog(@Body() body: CreateBlogDto, @AuthResponse() { id }: User) {
    try {
      return await this.vendorBlogsService.createNewBlog(
        { ...body, status: BlogStatusesTypes.Pending },
        id,
      );
    } catch (error) {
      console.log(
        '🚀 ~ file: blogs.controller.ts:28 ~ VendorBlogsController ~ error:',
        error,
      );
      return getError(
        LIST_ERROR_CODES.SS24001,
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  @Get('user')
  @ApiOperation({ summary: 'Get list blogs of vendor ' })
  @ApiQuery(PAGE_QUERY)
  @ApiQuery(LIMIT_QUERY)
  @ApiQuery(TITLE_QUERY)
  @ApiQuery(STATUS_QUERY)
  @ApiQuery(BLOG_CATEGORY_ID_QUERY)
  @ApiQuery(CREATED_AT_QUERY)
  async getListBlogs(
    @AuthResponse() { id }: User,
    @Paginate() query: PaginateQuery,
    @VendorFilterBlogs() filter: IVendorBlogsFilter,
  ) {
    try {
      if (!filter)
        return getError(LIST_ERROR_CODES.SS24429, HttpStatus.BAD_REQUEST);
      return await this.vendorBlogsService.getAllBlogsByVendor(
        query,
        id,
        filter,
      );
    } catch (error) {
      console.log(
        '🚀 ~ file: blogs.controller.ts:77 ~ VendorBlogsController ~ getListBlogs ~ error:',
        error,
      );
      return getError(LIST_ERROR_CODES.SS24001);
    }
  }

  @Get(':id')
  @ApiOperation({ summary: 'Get detail blog' })
  async getDetail(@Param('id', new UUIDPipe()) id: string) {
    try {
      return await this.vendorBlogsService.getOneById(id);
    } catch (error) {
      console.log(
        '🚀 ~ file: blogs.controller.ts:81 ~ ClientBlogsController ~ getDetail ~ error:',
        error,
      );
      return getError(LIST_ERROR_CODES.SS24001);
    }
  }

  @Patch(':id')
  @ApiOperation({ summary: 'Update blog for vendor when blog unverified' })
  async updateBlog(
    @Param('id', new UUIDPipe()) id: string,
    @Body() body: VendorUpdateBlogDto,
    @AuthResponse() user: User,
  ) {
    try {
      return await this.vendorBlogsService.updateBlogWhenUnverified(
        id,
        body,
        user.id,
      );
    } catch (error) {
      console.log(
        '🚀 ~ file: blogs.controller.ts:70 ~ VendorBlogsController ~ error:',
        error,
      );
      return getError(
        LIST_ERROR_CODES.SS24001,
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  @Delete(':id')
  @ApiOperation({ summary: 'Delete blog' })
  async deleteBlog(
    @Param('id', new UUIDPipe()) id: string,
    @AuthResponse() user: User,
  ) {
    try {
      return await this.vendorBlogsService.deleteBlogById(id, user.id);
    } catch (error) {
      console.log(
        '🚀 ~ file: blogs.controller.ts:152 ~ ClientBlogsController ~ deleteBlog ~ error:',
        error,
      );
      return getError(LIST_ERROR_CODES.SS24001);
    }
  }
}
