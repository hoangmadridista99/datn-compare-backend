import { Module } from '@nestjs/common';
import { BlogsModule } from '../../core/blogs/blogs.module';

@Module({
  imports: [BlogsModule],
  providers: [BlogsModule],
  exports: [BlogsModule],
})
export class VendorBlogsModule {}
