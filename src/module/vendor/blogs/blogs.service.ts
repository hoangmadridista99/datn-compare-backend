import { HttpStatus, Injectable } from '@nestjs/common';
import { BlogsService } from '../../core/blogs/blogs.service';
import {
  ICreateBlogDto,
  IVendorBlogsFilter,
  IVendorUpdateBlogDto,
} from '../../core/blogs/interfaces/blogs.interface';
import {
  getError,
  LIST_ERROR_CODES,
} from '../../../shared/constants/error/base.error';
import { NO_CONTENT_SUCCESS_RESPONSE } from '../../../shared/lib/responses/response';
import { BlogStatusesTypes } from '../../core/blogs/enums/blogs.enum';
import { PaginateQuery } from 'nestjs-paginate';

@Injectable()
export class VendorBlogsService {
  constructor(private readonly blogsService: BlogsService) {}

  async createNewBlog(body: ICreateBlogDto, userId: string) {
    const result = await this.blogsService.create(body, userId);
    return !result
      ? getError(LIST_ERROR_CODES.SS24103, HttpStatus.INTERNAL_SERVER_ERROR)
      : NO_CONTENT_SUCCESS_RESPONSE;
  }

  async deleteBlogById(id: string, userId: string) {
    const blogNeedToDelete = await this.blogsService.findOneById(id);
    if (!blogNeedToDelete)
      return getError(LIST_ERROR_CODES.SS24101, HttpStatus.NOT_FOUND);
    if (blogNeedToDelete.user_id !== userId)
      return getError(LIST_ERROR_CODES.SS24104, HttpStatus.NOT_FOUND);
    const result = await this.blogsService.deleteBlog(id);
    return !result
      ? getError(LIST_ERROR_CODES.SS24104, HttpStatus.NOT_FOUND)
      : NO_CONTENT_SUCCESS_RESPONSE;
  }

  async updateBlogWhenUnverified(
    id: string,
    body: IVendorUpdateBlogDto,
    userId: string,
  ) {
    const checkRequest = await this.verifyUpdateInsuranceRequest(id, userId);
    if (checkRequest?.error_code) return checkRequest;
    const bodyUpdate = { ...body, status: BlogStatusesTypes.Pending };
    const result = await this.blogsService.update(id, bodyUpdate);
    return !result.affected
      ? getError(LIST_ERROR_CODES.SS24103, HttpStatus.NOT_FOUND)
      : NO_CONTENT_SUCCESS_RESPONSE;
  }

  async verifyUpdateInsuranceRequest(id: string, userId: string) {
    const isBlogExist = await this.blogsService.findOneById(id);
    switch (true) {
      case !isBlogExist:
      case isBlogExist.user_id !== userId:
        return getError(LIST_ERROR_CODES.SS24108, HttpStatus.NOT_FOUND);
      // case isBlogExist.status !== BlogStatusesTypes.Pending:
      //   return getError(LIST_ERROR_CODES.SS24809, HttpStatus.BAD_REQUEST);
      default:
        return;
    }
  }

  async getAllBlogsByVendor(
    query: PaginateQuery,
    userId: string,
    filter: IVendorBlogsFilter,
  ) {
    return await this.blogsService.filterBlogsForVendor(query, userId, filter);
  }

  async getOneById(id: string) {
    return (
      (await this.blogsService.findOneById(id)) ??
      getError(LIST_ERROR_CODES.SS24101, HttpStatus.NOT_FOUND)
    );
  }
}
