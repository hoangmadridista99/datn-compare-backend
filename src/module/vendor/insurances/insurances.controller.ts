import {
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  Param,
  Patch,
  Post,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiBody,
  ApiOperation,
  ApiQuery,
  ApiTags,
} from '@nestjs/swagger';
import { JwtUserAuthGuard } from '../../core/auth/guards/jwt-auth.guard';
import { VendorRolesGuard } from '../../core/auth/guards/vendor/vendor-role.guard';
import { User, UserRole } from '../../core/users/entities/user.entity';
import { VendorInsurancesService } from './insurances.service';
import { VendorRoles } from '../../../shared/decorators/role.decorator';
import { CreateHealthInsuranceDto } from '../../core/insurances/dto/insurance.dto';
import { UUIDPipe } from '../../../shared/pipes/uuid.pipe';
import {
  getError,
  LIST_ERROR_CODES,
} from '../../../shared/constants/error/base.error';
import { Paginate, PaginateQuery } from 'nestjs-paginate';
import { AuthResponse } from '../../../shared/decorators/response-auth/auth.decorator';

import { VendorInsurancesFilter } from '../../../shared/decorators/insurances/vendor/filter-insurances-vendor.decorator';
import {
  ICreateHealthInsurance,
  ICreateLifeInsurance,
  IUpdateHealthInsurance,
  IUpdateLifeInsurance,
  IVendorInsurancesFilter,
} from '../../core/insurances/interfaces/insurance.interface';
import {
  PAGE_QUERY,
  LIMIT_QUERY,
  CREATED_AT_QUERY,
} from '../../../shared/constants/swagger/constant';
import { InsuranceCategoryNames } from '../../core/insurance-categories/enums/insurance-category.enum';
import {
  STATUS_INSURANCES_QUERY,
  INSURANCE_NAME_QUERY,
  INSURANCE_CATEGORY_LABEL_QUERY,
  MONTHLY_FEE_QUERY_QUERY,
} from '../../../shared/constants/swagger/insurances/insurances.constant';

@ApiTags('Insurances')
@Controller('insurances')
export class VendorInsurancesController {
  constructor(
    private readonly vendorInsurancesService: VendorInsurancesService,
  ) {}

  @Post()
  @ApiOperation({ summary: 'Create insurance for vendor' })
  @ApiBody({ type: CreateHealthInsuranceDto })
  async createInsurance(
    @Body() body: ICreateHealthInsurance | ICreateLifeInsurance,
    @AuthResponse() user: User,
  ) {
    try {
      const bodyCreate = {
        ...body,
        company_id: user.company_id,
      };
      return body.insurance_category.label === InsuranceCategoryNames.Life
        ? await this.vendorInsurancesService.createLifeInsurance(
            bodyCreate as ICreateLifeInsurance,
            user,
          )
        : await this.vendorInsurancesService.createHealthInsurance(
            bodyCreate as ICreateHealthInsurance,
            user,
          );
    } catch (error) {
      console.log(
        '🚀 ~ file: insurances.controller.ts:31 ~ VendorInsurancesController ~ createInsurance ~ error:',
        error,
      );
      throw getError(LIST_ERROR_CODES.SS24001);
    }
  }

  @Get(':id')
  @UseGuards(JwtUserAuthGuard, VendorRolesGuard)
  @VendorRoles(UserRole.VENDOR)
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Get detail a insurance' })
  async getDetailInsurance(@Param('id', new UUIDPipe()) id: string) {
    try {
      return await this.vendorInsurancesService.getDetailInsurance(id);
    } catch (error) {
      console.log(
        '🚀 ~ file: insurances.controller.ts:43 ~ VendorInsurancesController ~ getDetailInsurance ~ error:',
        error,
      );
      throw getError(LIST_ERROR_CODES.SS24001);
    }
  }

  @Get()
  @UseGuards(JwtUserAuthGuard, VendorRolesGuard)
  @VendorRoles(UserRole.VENDOR)
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Get all insurances' })
  @ApiQuery(PAGE_QUERY)
  @ApiQuery(LIMIT_QUERY)
  @ApiQuery(STATUS_INSURANCES_QUERY)
  @ApiQuery(INSURANCE_NAME_QUERY)
  @ApiQuery(CREATED_AT_QUERY)
  @ApiQuery(INSURANCE_CATEGORY_LABEL_QUERY)
  @ApiQuery(MONTHLY_FEE_QUERY_QUERY)
  async getAll(
    @Paginate() query: PaginateQuery,
    @VendorInsurancesFilter() filter: IVendorInsurancesFilter,
    @AuthResponse() user: User,
  ) {
    try {
      if (!filter)
        return getError(LIST_ERROR_CODES.SS24429, HttpStatus.BAD_REQUEST);
      return await this.vendorInsurancesService.findAllInsurances(
        query,
        filter,
        user.id,
      );
    } catch (error) {
      console.log(
        '🚀 ~ file: insurances.controller.ts:57 ~ VendorInsurancesController ~ getAll ~ error:',
        error,
      );
      throw getError(LIST_ERROR_CODES.SS24001);
    }
  }

  @Patch(':id')
  @ApiOperation({ summary: 'Update insurance' })
  async update(
    @Param('id', new UUIDPipe()) id: string,
    @Body() body: IUpdateHealthInsurance | IUpdateLifeInsurance,
    @AuthResponse() user: User,
  ) {
    try {
      const dto = { ...body, company_id: user.company_id };
      return await this.vendorInsurancesService.updateInsuranceById(
        id,
        dto,
        user.id,
      );
    } catch (error) {
      console.log(
        '🚀 ~ file: insurances.controller.ts:135 ~ VendorInsurancesController ~ error:',
        error,
      );
      return getError(
        LIST_ERROR_CODES.SS24001,
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  @Delete(':id')
  @UseGuards(JwtUserAuthGuard, VendorRolesGuard)
  @VendorRoles(UserRole.VENDOR)
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Remove insurance' })
  async remove(
    @Param('id', new UUIDPipe()) id: string,
    @AuthResponse() user: User,
  ) {
    try {
      return await this.vendorInsurancesService.removeInsuranceById(
        id,
        user.id,
      );
    } catch (error) {
      return getError(
        LIST_ERROR_CODES.SS24001,
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }
}
