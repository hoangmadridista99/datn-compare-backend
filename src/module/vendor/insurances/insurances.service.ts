import { HttpStatus, Injectable } from '@nestjs/common';
import { InsurancesService } from '../../core/insurances/insurances.service';
import {
  getError,
  LIST_ERROR_CODES,
} from '../../../shared/constants/error/base.error';
import { NO_CONTENT_SUCCESS_RESPONSE } from '../../../shared/lib/responses/response';
import {
  ICreateHealthInsurance,
  ICreateLifeInsurance,
  IUpdateHealthInsurance,
  IUpdateLifeInsurance,
  IVendorInsurancesFilter,
} from '../../core/insurances/interfaces/insurance.interface';
import { PaginateQuery } from 'nestjs-paginate';
import { User } from '../../core/users/entities/user.entity';
import { InsuranceStatusesTypes } from '../../core/insurances/enums/insurances.enum';
import { getPaginate } from 'src/shared/lib/paginate/paginate.lib';
import { InsuranceRepositoryService } from 'src/module/core/insurances/repository';
import { InsuranceCategoriesService } from '../../core/insurance-categories/insurance-categories.service';
import { OperationSign } from '../../../shared/enums/operation.enum';
import { InsuranceCategoryNames } from '../../core/insurance-categories/enums/insurance-category.enum';

@Injectable()
export class VendorInsurancesService {
  constructor(
    private readonly insurancesService: InsurancesService,
    private readonly insuranceRepositoryService: InsuranceRepositoryService,
    private readonly insuranceCategoriesService: InsuranceCategoriesService,
  ) {}

  async createHealthInsurance(body: ICreateHealthInsurance, user: User) {
    let result = {};
    if (body.hospitals && body.hospitals.length > 0) {
      result = this.insurancesService.handleSeparateHospital(body.hospitals);
    }
    const data = {
      ...body,
      ...result,
      status: InsuranceStatusesTypes.Pending,
    };
    await this.insuranceRepositoryService.create(data, user);
    return NO_CONTENT_SUCCESS_RESPONSE;
  }

  async createLifeInsurance(body: ICreateLifeInsurance, user: User) {
    const data = { ...body, status: InsuranceStatusesTypes.Pending };
    await this.insuranceRepositoryService.create(data, user);
    return NO_CONTENT_SUCCESS_RESPONSE;
  }

  async getDetailInsurance(id: string) {
    return (
      (await this.insurancesService.findOneByQuery({ id })) ??
      getError(LIST_ERROR_CODES.SS24101, HttpStatus.NOT_FOUND)
    );
  }

  async findAllInsurances(
    query: PaginateQuery,
    filter: IVendorInsurancesFilter,
    userId: string,
  ) {
    const queryBuilder = this.insurancesService.getListInsurancesForVendor(
      filter,
      userId,
    );
    return await getPaginate(query, queryBuilder);
  }

  async updateInsuranceById(
    id: string,
    body: IUpdateLifeInsurance | IUpdateHealthInsurance,
    userId: string,
  ) {
    const insuranceById = await this.insurancesService.findOneByQuery({ id });
    if (!insuranceById)
      return getError(LIST_ERROR_CODES.SS24101, HttpStatus.NOT_FOUND);
    if (insuranceById.user.id !== userId)
      return getError(LIST_ERROR_CODES.SS24108, HttpStatus.BAD_REQUEST);
    const categoryId = insuranceById.insurance_category.id;
    let updateBody: IUpdateLifeInsurance | IUpdateHealthInsurance = {
      ...body,
      status: InsuranceStatusesTypes.Pending,
    };

    switch (body.insurance_category.label) {
      case InsuranceCategoryNames.Life:
        break;
      case InsuranceCategoryNames.Health:
        const healthInsuranceBody = body as IUpdateHealthInsurance;
        const { hospitals, hospitals_pending } =
          this.insurancesService.handleSeparateHospital(
            healthInsuranceBody.hospitals,
          );
        updateBody = {
          ...updateBody,
          hospitals,
          hospitals_pending,
        };
        break;
    }
    const result = await this.insuranceRepositoryService.updateInsurance(
      insuranceById,
      updateBody,
    );
    if (!result) {
      return getError(LIST_ERROR_CODES.SS24103, HttpStatus.BAD_REQUEST);
    }
    if (insuranceById.status === InsuranceStatusesTypes.Approved) {
      await this.insuranceCategoriesService.updateQuantityOfCategoryByInsurance(
        categoryId,
        OperationSign.SUBTRACT,
      );
    }
    return NO_CONTENT_SUCCESS_RESPONSE;
  }

  async removeInsuranceById(id: string, userId: string) {
    const insurance = await this.insurancesService.findOneByQuery({ id: id });
    if (insurance.user.id !== userId)
      return getError(LIST_ERROR_CODES.SS24102, HttpStatus.BAD_REQUEST);
    const result = await this.insuranceRepositoryService.deleteById(id);
    if (!result.affected)
      return getError(LIST_ERROR_CODES.SS24102, HttpStatus.BAD_REQUEST);
    if (insurance.status === InsuranceStatusesTypes.Approved)
      await this.insuranceCategoriesService.updateQuantityOfCategoryByInsurance(
        insurance.insurance_category.id,
        OperationSign.SUBTRACT,
      );
    return NO_CONTENT_SUCCESS_RESPONSE;
  }
}
