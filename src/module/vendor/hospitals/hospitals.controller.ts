import { Controller, Get, Query, UseGuards } from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiQuery,
  ApiTags,
} from '@nestjs/swagger';
import { JwtUserAuthGuard } from '../../core/auth/guards/jwt-auth.guard';
import { VendorRolesGuard } from '../../core/auth/guards/vendor/vendor-role.guard';
import { UserRole } from '../../core/users/entities/user.entity';
import { VendorRoles } from '../../../shared/decorators/role.decorator';
import { VendorHospitalsService } from './hospitals.service';
import {
  LIST_ERROR_CODES,
  getError,
} from 'src/shared/constants/error/base.error';

@ApiTags('Hospitals')
@Controller('hospitals')
@UseGuards(JwtUserAuthGuard, VendorRolesGuard)
@VendorRoles(UserRole.VENDOR)
@ApiBearerAuth()
export class VendorHospitalsController {
  constructor(
    private readonly vendorHospitalsService: VendorHospitalsService,
  ) {}

  @Get()
  @ApiQuery({
    name: 'name',
    required: false,
  })
  @ApiOperation({ summary: 'Get hospitals' })
  async findAll(@Query('name') name?: string) {
    try {
      return await this.vendorHospitalsService.findAll(name);
    } catch (error) {
      console.log(
        '🚀 ~ file: hospitals.controller.ts:41 ~ VendorHospitalsController ~ findAll ~ error:',
        error,
      );
      return getError(LIST_ERROR_CODES.SS24001);
    }
  }

  // @Post()
  // @ApiOperation({ summary: 'Create hospital' })
  // async createHospital(
  //   @Body() body: CreateHospitalDto,
  //   @AuthResponse() user: User,
  // ) {
  //   try {
  //     return await this.vendorHospitalsService.createHospital(user, body);
  //   } catch (error) {
  //     console.log(
  //       '🚀 ~ file: hospitals.controller.ts:61 ~ VendorHospitalsController ~ createHospital ~ error:',
  //       error,
  //     );
  //     return getError(LIST_ERROR_CODES.SS24001);
  //   }
  // }

  // @Patch(':id')
  // @ApiOperation({ summary: 'Update hospital' })
  // async updateHospital(
  //   @Param('id') id: string,
  //   @AuthResponse() user: User,
  //   @Body() body: UpdateHospitalDto,
  // ) {
  //   try {
  //     return await this.vendorHospitalsService.updateHospital(
  //       user.id,
  //       id,
  //       body,
  //     );
  //   } catch (error) {
  //     console.log(
  //       '🚀 ~ file: hospitals.controller.ts:78 ~ VendorHospitalsController ~ updateHospital ~ error:',
  //       error,
  //     );
  //     return getError(LIST_ERROR_CODES.SS24001);
  //   }
  // }

  // @Delete(':id')
  // @ApiOperation({ summary: 'Remove hospital' })
  // async removeHospital(@Param('id') id: string, @AuthResponse() user: User) {
  //   try {
  //     return this.vendorHospitalsService.removeHospital(user.id, id);
  //   } catch (error) {
  //     console.log(
  //       '🚀 ~ file: hospitals.controller.ts:103 ~ VendorHospitalsController ~ removeHospital ~ error:',
  //       error,
  //     );
  //     return getError(LIST_ERROR_CODES.SS24001);
  //   }
  // }
}
