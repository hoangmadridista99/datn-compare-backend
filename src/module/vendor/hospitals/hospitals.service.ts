import { Injectable } from '@nestjs/common';
import { HospitalsService } from 'src/module/core/hospitals/hospitals.service';

@Injectable()
export class VendorHospitalsService {
  constructor(private readonly hospitalsService: HospitalsService) {}

  async findAll(name?: string) {
    return await this.hospitalsService.findAll(name);
  }

  // async createHospital(user: User, body: ICreateHospital) {
  //   return (
  //     (await this.hospitalsService.createHospital(user, body)) ??
  //     getError(LIST_ERROR_CODES.SS24104, HttpStatusCode.BadRequest)
  //   );
  // }

  // async updateHospital(
  //   userId: string,
  //   hospitalId: string,
  //   body: IUpdateHospital,
  // ) {
  //   const isCreatedByUser = await this.hospitalsService.isHospitalCreatedByUser(
  //     userId,
  //     hospitalId,
  //   );
  //   if (!isCreatedByUser)
  //     return getError(LIST_ERROR_CODES.SS24005, HttpStatusCode.BadRequest);
  //   const result = await this.hospitalsService.updateHospital(hospitalId, body);
  //   return result
  //     ? NO_CONTENT_SUCCESS_RESPONSE
  //     : getError(LIST_ERROR_CODES.SS24103, HttpStatusCode.BadRequest);
  // }

  // async removeHospital(userId: string, hospitalId: string) {
  //   const isCreatedByUser = await this.hospitalsService.isHospitalCreatedByUser(
  //     userId,
  //     hospitalId,
  //   );
  //   if (!isCreatedByUser)
  //     return getError(LIST_ERROR_CODES.SS24005, HttpStatusCode.BadRequest);
  //   const result = await this.hospitalsService.removeHospital(hospitalId);
  //   return result
  //     ? NO_CONTENT_SUCCESS_RESPONSE
  //     : getError(LIST_ERROR_CODES.SS24102, HttpStatusCode.BadRequest);
  // }
}
