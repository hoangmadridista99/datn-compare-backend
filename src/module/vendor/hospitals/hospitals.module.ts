import { Module } from '@nestjs/common';
import { HospitalsModule } from 'src/module/core/hospitals/hospitals.module';

@Module({
  imports: [HospitalsModule],
  providers: [HospitalsModule],
  exports: [HospitalsModule],
})
export class VendorHospitalsModule {}
