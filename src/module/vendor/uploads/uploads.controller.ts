import {
  Controller,
  Post,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { VendorUploadsService } from './uploads.service';
import {
  ApiBearerAuth,
  ApiBody,
  ApiConsumes,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { JwtUserAuthGuard } from 'src/module/core/auth/guards/jwt-auth.guard';
import { UserRolesGuard } from 'src/module/core/auth/guards/local/role.guard';
import { UserRole } from 'src/module/core/users/entities/user.entity';
import { AdminRoles } from 'src/shared/decorators/role.decorator';
import { BaseUploadFileDto } from 'src/shared/dto/file.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { FilePdfValidationPipe } from 'src/shared/pipes/file-pdf.pipe';
import { FileImageValidationPipe } from '../../../shared/pipes/file-image.pipe';
import {
  LIST_ERROR_CODES,
  getError,
} from '../../../shared/constants/error/base.error';
import { UploadBannerDto } from '../../core/blogs/dto/blog.dto';

@Controller('uploads')
@ApiTags('Uploads')
@ApiBearerAuth()
@UseGuards(JwtUserAuthGuard, UserRolesGuard)
@AdminRoles(UserRole.VENDOR)
export class VendorUploadsController {
  constructor(private readonly vendorUploadsService: VendorUploadsService) {}

  @Post('/insurance/pdf')
  @ApiOperation({ summary: 'Upload Insurance Pdf' })
  @ApiConsumes('multipart/form-data')
  @ApiBody({ type: BaseUploadFileDto })
  @ApiResponse({
    status: 204,
    schema: {
      type: 'string',
    },
  })
  @UseInterceptors(FileInterceptor('file'))
  async uploadInsurancePdf(
    @UploadedFile(new FilePdfValidationPipe()) file: Express.Multer.File,
  ) {
    try {
      return this.vendorUploadsService.uploadInsurancePdf(file);
    } catch (error) {
      console.log(
        '🚀 ~ file: upload.controller.ts:27 ~ OperatorUploadController ~ uploadInsurancePdf:',
        error,
      );
      throw error;
    }
  }

  @Post('blogs/banner')
  @ApiConsumes('multipart/form-data')
  @ApiOperation({ summary: 'Upload banner blog' })
  @UseInterceptors(FileInterceptor('file'))
  @ApiBody({ type: UploadBannerDto })
  async uploadAvatar(
    @UploadedFile(new FileImageValidationPipe()) file: Express.Multer.File,
  ) {
    try {
      const image_url =
        (await this.vendorUploadsService.uploadBanner(file)) ?? null;
      return { image_url };
    } catch (error) {
      console.log(
        '🚀 ~ file: blogs.controller.ts:66 ~ ClientBlogsController ~ uploadAvatar ~ error:',
        error,
      );
      return getError(LIST_ERROR_CODES.SS24001);
    }
  }
}
