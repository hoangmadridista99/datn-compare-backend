import { Module } from '@nestjs/common';
import { MinioModule } from 'src/module/core/minio/minio.module';
import { VendorUploadsService } from './uploads.service';

@Module({
  imports: [MinioModule],
  providers: [VendorUploadsService],
  exports: [VendorUploadsService],
})
export class VendorUploadsModule {}
