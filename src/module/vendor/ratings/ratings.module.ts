import { Module } from '@nestjs/common';
import { RatingsModule } from '../../core/ratings/ratings.module';

@Module({
  imports: [RatingsModule],
  providers: [RatingsModule],
  exports: [RatingsModule],
})
export class VendorRatingsModule {}
