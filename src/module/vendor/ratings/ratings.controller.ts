import { Controller, Get, Param, UseGuards } from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiQuery,
  ApiTags,
} from '@nestjs/swagger';
import { VendorRoles } from '../../../shared/decorators/role.decorator';
import { JwtUserAuthGuard } from '../../core/auth/guards/jwt-auth.guard';
import { VendorRolesGuard } from '../../core/auth/guards/vendor/vendor-role.guard';
import { UserRole } from '../../core/users/entities/user.entity';
import { VendorRatingsService } from './ratings.service';
import { Paginate, PaginateQuery } from 'nestjs-paginate';
import {
  LIST_ERROR_CODES,
  getError,
} from '../../../shared/constants/error/base.error';
import { UUIDPipe } from '../../../shared/pipes/uuid.pipe';
import {
  PAGE_QUERY,
  LIMIT_QUERY,
} from '../../../shared/constants/swagger/constant';

@ApiTags('Ratings')
@Controller('ratings')
@UseGuards(JwtUserAuthGuard, VendorRolesGuard)
@VendorRoles(UserRole.VENDOR)
@ApiBearerAuth()
export class VendorRatingsController {
  constructor(private readonly vendorRatingsService: VendorRatingsService) {}

  @Get(':insurance_id')
  @ApiOperation({ summary: 'Get list ratings by insurance id' })
  @ApiQuery(PAGE_QUERY)
  @ApiQuery(LIMIT_QUERY)
  async getListRatings(
    @Paginate() query: PaginateQuery,
    @Param('id', new UUIDPipe()) id: string,
  ) {
    try {
      return await this.vendorRatingsService.getListRatingsByInsuranceId(
        query,
        id,
      );
    } catch (error) {
      console.log(
        '🚀 ~ file: ratings.controller.ts:38 ~ VendorRatingsController ~ error:',
        error,
      );
      return getError(LIST_ERROR_CODES.SS24001);
    }
  }
}
