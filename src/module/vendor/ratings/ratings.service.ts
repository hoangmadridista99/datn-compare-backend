import { Injectable } from '@nestjs/common';
import { RatingsService } from '../../core/ratings/ratings.service';
import { PaginateQuery } from 'nestjs-paginate';

@Injectable()
export class VendorRatingsService {
  constructor(private readonly ratingsService: RatingsService) {}

  async getListRatingsByInsuranceId(query: PaginateQuery, blogId: string) {
    return await this.ratingsService.findAllByInsurance(query, blogId);
  }
}
