import {
  Body,
  Controller,
  Get,
  Patch,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiTags,
  ApiBearerAuth,
  ApiOperation,
  ApiBody,
  ApiConsumes,
} from '@nestjs/swagger';
import {
  getError,
  LIST_ERROR_CODES,
} from '../../../shared/constants/error/base.error';
import { AuthResponse } from '../../../shared/decorators/response-auth/auth.decorator';
import { VendorRoles } from '../../../shared/decorators/role.decorator';
import { JwtUserAuthGuard } from '../../core/auth/guards/jwt-auth.guard';
import { VendorRolesGuard } from '../../core/auth/guards/vendor/vendor-role.guard';
import {
  BodyUpdateVendorDto,
  UpdateUserByVendorDto,
} from '../../core/users/dto/update-user.dto';
import { User, UserRole } from '../../core/users/entities/user.entity';
import { VendorUsersService } from './users.service';
import { FileInterceptor } from '@nestjs/platform-express';
import { FileImageValidationPipe } from '../../../shared/pipes/file-image.pipe';

@ApiTags('Users')
@Controller('users')
@UseGuards(JwtUserAuthGuard, VendorRolesGuard)
@VendorRoles(UserRole.VENDOR)
@ApiBearerAuth()
export class VendorUsersController {
  constructor(private readonly vendorUsersService: VendorUsersService) {}
  @Get('/profile')
  @ApiOperation({ summary: 'Get vendor profile' })
  async getProfile(@AuthResponse() user: User) {
    return user;
  }

  @Patch('profile')
  @ApiOperation({ summary: 'Update vendor profile' })
  @ApiConsumes('multipart/form-data')
  @UseInterceptors(FileInterceptor('file'))
  @ApiBody({ type: BodyUpdateVendorDto })
  async updateProfile(
    @AuthResponse() user: User,
    @Body() body: UpdateUserByVendorDto,
    @UploadedFile(new FileImageValidationPipe()) file: Express.Multer.File,
  ) {
    try {
      return await this.vendorUsersService.updateProfile(user, body, file);
    } catch (error) {
      console.log(
        '🚀 ~ file: users.controller.ts:30 ~ VendorUsersController ~ updateProfile ~ error:',
        error,
      );
      throw getError(LIST_ERROR_CODES.SS24001);
    }
  }
}
