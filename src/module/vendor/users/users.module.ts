import { Module } from '@nestjs/common/decorators/modules/module.decorator';
import { UsersModule } from '../../core/users/users.module';

@Module({
  imports: [UsersModule],
  providers: [UsersModule],
  exports: [UsersModule],
})
export class VendorUsersModule {}
