import { HttpStatus, Injectable } from '@nestjs/common';
import {
  getError,
  LIST_ERROR_CODES,
} from '../../../shared/constants/error/base.error';
import { NO_CONTENT_SUCCESS_RESPONSE } from '../../../shared/lib/responses/response';
import { UpdateUserByVendorDto } from '../../core/users/dto/update-user.dto';
import { UsersService } from '../../core/users/users.service';
import { User } from '../../core/users/entities/user.entity';

@Injectable()
export class VendorUsersService {
  constructor(private readonly usersService: UsersService) {}

  async updateProfile(
    user: User,
    body: UpdateUserByVendorDto,
    file: Express.Multer.File,
  ) {
    const avatarUrl = !file
      ? undefined
      : await this.usersService.uploadAvatarProfile(file);
    body.avatar_profile_url = avatarUrl;

    const result = await this.usersService.updateUserById(user.id, body);
    if (!result.affected)
      return getError(LIST_ERROR_CODES.SS24103, HttpStatus.NOT_FOUND);
    return !avatarUrl
      ? NO_CONTENT_SUCCESS_RESPONSE
      : { avatar_profile_url: avatarUrl };
  }
}
