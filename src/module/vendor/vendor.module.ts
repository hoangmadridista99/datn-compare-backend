import { Module } from '@nestjs/common';
import { VendorAuthController } from './auth/auth.controller';
import { VendorAuthModule } from './auth/auth.module';
import { VendorInsurancesController } from './insurances/insurances.controller';
import { VendorInsurancesModule } from './insurances/insurances.module';
import { VendorUsersController } from './users/users.controller';
import { VendorUsersModule } from './users/users.module';
import { VendorOtpsModule } from './otps/opts.module';
import { VendorOtpsController } from './otps/otps.controller';
import { VendorUploadsModule } from './uploads/uploads.module';
import { VendorUploadsController } from './uploads/uploads.controller';
import { VendorBlogsModule } from './blogs/blogs.module';
import { VendorBlogsController } from './blogs/blogs.controller';
import { VendorBlogCommentsModule } from './blog-comments/blog-comments.module';
import { VendorBlogCommentsController } from './blog-comments/blog-comments.controller';
import { VendorRatingsController } from './ratings/ratings.controller';
import { VendorRatingsModule } from './ratings/ratings.module';
import { VendorHospitalsController } from './hospitals/hospitals.controller';
import { VendorHospitalsModule } from './hospitals/hospitals.module';

@Module({
  imports: [
    VendorInsurancesModule,
    VendorAuthModule,
    VendorUsersModule,
    VendorOtpsModule,
    VendorUploadsModule,
    VendorBlogsModule,
    VendorBlogCommentsModule,
    VendorRatingsModule,
    VendorHospitalsModule,
  ],
  controllers: [
    VendorInsurancesController,
    VendorAuthController,
    VendorUsersController,
    VendorOtpsController,
    VendorUploadsController,
    VendorBlogsController,
    VendorBlogCommentsController,
    VendorRatingsController,
    VendorHospitalsController,
  ],
})
export class VendorModule {}
