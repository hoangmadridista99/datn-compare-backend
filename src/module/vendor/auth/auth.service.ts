import { HttpStatus, Injectable } from '@nestjs/common';
import { BasicResponse } from '../../../shared/basic.response';
import {
  getError,
  LIST_ERROR_CODES,
} from '../../../shared/constants/error/base.error';
import { AuthService } from '../../core/auth/auth.service';
import { RegisterVendorDto } from '../../core/auth/dto/auth.dto';
import { ResponseAuthUser } from '../../core/auth/models/auth.model';
import { OtpsService } from '../../core/otps/otps.service';
import { User, UserRole } from '../../core/users/entities/user.entity';
import { VendorStatusTypes } from '../../core/users/enums/users.enum';
import { CompaniesService } from 'src/module/core/companies/companies.service';
import { IUpdatePasswordDto } from '../../core/users/interfaces/users.interface';
import { UsersService } from '../../core/users/users.service';
import { NO_CONTENT_SUCCESS_RESPONSE } from '../../../shared/lib/responses/response';
import { IOtpDestinationType } from '../../core/otps/interfaces/otp.interface';

@Injectable()
export class VendorAuthService {
  constructor(
    private readonly authService: AuthService,
    private readonly otpsService: OtpsService,
    private readonly companiesService: CompaniesService,
    private readonly usersService: UsersService,
  ) {}

  async validateLoginVendor(
    destination: string,
    password: string,
    otpCode: string,
    destinationType: IOtpDestinationType,
  ): Promise<BasicResponse | ResponseAuthUser> {
    try {
      const user = await this.authService.validateUser(destination, password);
      if (typeof user === 'string')
        return getError(user, HttpStatus.UNPROCESSABLE_ENTITY);
      if (user.role !== UserRole.VENDOR)
        return getError(LIST_ERROR_CODES.SS24003, HttpStatus.FORBIDDEN);
      if (user.vendor_status !== VendorStatusTypes.ACCEPTED)
        return getError(LIST_ERROR_CODES.SS24002, HttpStatus.FORBIDDEN);
      const isValidOtp = await this.otpsService.checkOtpBeforeLogin(
        otpCode,
        destinationType,
        destination,
      );
      if (!isValidOtp)
        return getError(
          LIST_ERROR_CODES.SS24505,
          HttpStatus.UNPROCESSABLE_ENTITY,
        );
      return await this.authService.getResponseLogin(user);
    } catch (error) {
      console.log('🚀 ~ file: auth.service.ts:54 ~ CliAuthService ', error);
      throw error;
    }
  }

  async registerVendor(body: RegisterVendorDto, file: Express.Multer.File) {
    const { company_id } = body;
    if (company_id) {
      const company = await this.companiesService.findById(company_id);
      if (!company)
        return getError(LIST_ERROR_CODES.SS24703, HttpStatus.BAD_REQUEST);
    }
    const checkOtp = await this.otpsService.checkOtpBeforeRegister(body);
    if (!checkOtp)
      return getError(
        LIST_ERROR_CODES.SS24505,
        HttpStatus.UNPROCESSABLE_ENTITY,
      );
    const checkBodyRegister = await this.authService.checkBodyRegister(
      body.email,
      body.phone,
    );
    if (typeof checkBodyRegister === 'string')
      return getError(checkBodyRegister, HttpStatus.UNPROCESSABLE_ENTITY);

    return await this.authService.handleRegister(body, file, true);
  }

  async changePassword(user: User, body: IUpdatePasswordDto) {
    const isValidCurrentPassword = await this.authService.checkPassword(
      body.current_password,
      user.password,
    );
    if (!isValidCurrentPassword)
      return getError(LIST_ERROR_CODES.SS24321, HttpStatus.BAD_REQUEST);
    const newHashPassword = await this.authService.hashPassword(
      body.new_password,
    );
    const result = await this.usersService.updateUserById(user.id, {
      password: newHashPassword,
    });
    return !result.affected
      ? getError(LIST_ERROR_CODES.SS24322, HttpStatus.INTERNAL_SERVER_ERROR)
      : NO_CONTENT_SUCCESS_RESPONSE;
  }
}
