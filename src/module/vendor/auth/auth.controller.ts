import {
  Body,
  Controller,
  HttpStatus,
  Post,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiBody,
  ApiConsumes,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import {
  getError,
  LIST_ERROR_CODES,
} from '../../../shared/constants/error/base.error';
import { AuthResponse } from '../../../shared/decorators/response-auth/auth.decorator';
import { Public } from '../../../shared/decorators/public.decorator';
import {
  CreateRegisterVendorDto,
  LoginVendorDto,
  RegisterVendorDto,
} from '../../core/auth/dto/auth.dto';
import { VendorAuthGuard } from '../../core/auth/guards/vendor/vendor.guard';
import { ResponseAuthUser } from '../../core/auth/models/auth.model';
import { User, UserRole } from '../../core/users/entities/user.entity';
import { VendorAuthService } from './auth.service';
import { RegisterVendorValidateCompanyPipe } from 'src/shared/pipes/register-vendor.pipe';
import { FileImageValidationPipe } from '../../../shared/pipes/file-image.pipe';
import { FileInterceptor } from '@nestjs/platform-express';
import { ChangePasswordVendorDto } from '../../core/users/dto/update-user.dto';
import { VendorRoles } from '../../../shared/decorators/role.decorator';
import { JwtUserAuthGuard } from '../../core/auth/guards/jwt-auth.guard';
import { VendorRolesGuard } from '../../core/auth/guards/vendor/vendor-role.guard';

@ApiTags('Authentication')
@Controller('auth')
export class VendorAuthController {
  constructor(private readonly vendorAuthService: VendorAuthService) {}

  @Post('/login')
  @UseGuards(VendorAuthGuard)
  @ApiBody({ type: LoginVendorDto })
  @ApiOperation({ summary: 'Login for vendor' })
  @ApiResponse({ status: 401, description: 'Unauthorized.' })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  @ApiResponse({ status: 200, description: 'OK', type: ResponseAuthUser })
  async login(@AuthResponse() user: User): Promise<User> {
    return user;
  }

  @Public()
  @Post('/register')
  @ApiConsumes('multipart/form-data')
  @UseInterceptors(FileInterceptor('file'))
  @ApiBody({ type: CreateRegisterVendorDto })
  @ApiOperation({ summary: 'Register for vendor' })
  @ApiResponse({ status: 401, description: 'Unauthorized.' })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  @ApiResponse({ status: 200, description: 'OK' })
  async registerForVendor(
    @UploadedFile(new FileImageValidationPipe()) file: Express.Multer.File,
    @Body(new RegisterVendorValidateCompanyPipe()) body: RegisterVendorDto,
  ) {
    try {
      return await this.vendorAuthService.registerVendor(body, file);
    } catch (error) {
      console.log(
        '🚀 ~ file: auth.controller.ts:38 ~ VendorAuthController ~ registerForVendor ~ error:',
        error,
      );
      throw getError(LIST_ERROR_CODES.SS24001);
    }
  }

  @Post('change-password')
  @UseGuards(JwtUserAuthGuard, VendorRolesGuard)
  @VendorRoles(UserRole.VENDOR)
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Change password for vendor' })
  async changePassword(
    @AuthResponse() user: User,
    @Body() { confirmation_password, ...body }: ChangePasswordVendorDto,
  ) {
    try {
      if (confirmation_password !== body.new_password)
        return getError(LIST_ERROR_CODES.SS24108, HttpStatus.BAD_REQUEST);
      return await this.vendorAuthService.changePassword(user, body);
    } catch (error) {
      console.log(
        '🚀 ~ file: auth.controller.ts:85 ~ VendorAuthController ~ error:',
        error,
      );

      throw getError(LIST_ERROR_CODES.SS24001);
    }
  }
}
