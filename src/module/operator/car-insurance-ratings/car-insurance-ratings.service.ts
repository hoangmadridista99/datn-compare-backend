import { HttpStatus, Injectable } from '@nestjs/common';
import { CarInsuranceRatingsService } from '../../core/car-insurance-ratings/car-insurance-ratings.service';
import {
  LIST_ERROR_CODES,
  getError,
} from '../../../shared/constants/error/base.error';
import { PaginateQuery } from 'nestjs-paginate';
import {
  IFilterCarInsuranceRating,
  IVerifyCarInsuranceRatingDto,
} from '../../core/car-insurance-ratings/interfaces/car-insurance-rating.interface';
import { NO_CONTENT_SUCCESS_RESPONSE } from '../../../shared/lib/responses/response';

@Injectable()
export class OperatorCarInsuranceRatingsService {
  constructor(
    private readonly carInsuranceRatingsService: CarInsuranceRatingsService,
  ) {}

  async verifyRating(id: string, body: IVerifyCarInsuranceRatingDto) {
    const currentRating = await this.carInsuranceRatingsService.findRatingById(
      id,
    );
    if (!currentRating)
      return getError(LIST_ERROR_CODES.SS24101, HttpStatus.NOT_FOUND);
    await this.carInsuranceRatingsService.verifyRatingForAdmin(id, body);
    const result =
      await this.carInsuranceRatingsService.handleCalculateRatingAfterVerify(
        currentRating.car_insurance_id,
      );
    return !result
      ? getError(LIST_ERROR_CODES.SS24605, HttpStatus.BAD_REQUEST)
      : NO_CONTENT_SUCCESS_RESPONSE;
  }

  async getCarInsuranceRatingsList(
    query: PaginateQuery,
    filter: IFilterCarInsuranceRating,
  ) {
    return await this.carInsuranceRatingsService.findAllByOperator(
      query,
      filter,
    );
  }

  async findAllRatingsByCarInsurance(query: PaginateQuery, id: string) {
    return await this.carInsuranceRatingsService.findAllRatingsByCarInsurance(
      query,
      id,
    );
  }

  async rejectCarInsuranceRating(id: string) {
    const result = await this.carInsuranceRatingsService.deleteRatingById(id);
    return !result
      ? getError(LIST_ERROR_CODES.SS24102, HttpStatus.BAD_REQUEST)
      : NO_CONTENT_SUCCESS_RESPONSE;
  }
}
