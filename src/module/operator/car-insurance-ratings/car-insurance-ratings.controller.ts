import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Query,
  UseGuards,
} from '@nestjs/common';
import {
  ApiTags,
  ApiBearerAuth,
  ApiQuery,
  ApiOperation,
} from '@nestjs/swagger';
import { AdminRoles } from '../../../shared/decorators/role.decorator';
import { JwtUserAuthGuard } from '../../core/auth/guards/jwt-auth.guard';
import { UserRolesGuard } from '../../core/auth/guards/local/role.guard';
import { UserRole } from '../../core/users/entities/user.entity';
import { OperatorCarInsuranceRatingsService } from './car-insurance-ratings.service';
import {
  LIMIT_QUERY,
  PAGE_QUERY,
} from '../../../shared/constants/swagger/constant';
import { Paginate, PaginateQuery } from 'nestjs-paginate';
import {
  LIST_ERROR_CODES,
  getError,
} from '../../../shared/constants/error/base.error';
import { UUIDPipe } from '../../../shared/pipes/uuid.pipe';
import { VerifyRatingForAdminDto } from '../../core/ratings/dto/ratings.dto';
import { FilterCarInsuranceRatingDto } from '../../core/car-insurance-ratings/dto/car-insurance-rating.dto';

@ApiTags('Car-Insurance-Ratings')
@Controller('car-insurance-ratings')
@UseGuards(JwtUserAuthGuard, UserRolesGuard)
@ApiBearerAuth()
@AdminRoles(UserRole.ADMIN)
export class OperatorCarInsuranceRatingsController {
  constructor(
    private readonly operatorCarInsuranceRatingsService: OperatorCarInsuranceRatingsService,
  ) {}

  @Get('unverified')
  @ApiQuery(PAGE_QUERY)
  @ApiQuery(LIMIT_QUERY)
  @ApiQuery({
    name: 'insurance_name',
    required: false,
  })
  @ApiQuery({
    name: 'rating_score',
    required: false,
  })
  @ApiOperation({ summary: 'Get all ratings unverified' })
  async getListUnverifiedRatings(
    @Paginate() query: PaginateQuery,
    @Query() filter: FilterCarInsuranceRatingDto,
  ) {
    try {
      return this.operatorCarInsuranceRatingsService.getCarInsuranceRatingsList(
        query,
        filter,
      );
    } catch (error) {
      console.log(
        '🚀 ~ file: car-insurance-ratings.controller.ts:52 ~ OperatorCarInsuranceRatingsController ~ error:',
        error,
      );
      return getError(LIST_ERROR_CODES.SS24001);
    }
  }

  @Patch(':id/verify')
  @ApiOperation({ summary: 'Verify rating for admin' })
  async verifyRating(
    @Param('id', new UUIDPipe()) id: string,
    @Body() body: VerifyRatingForAdminDto,
  ) {
    try {
      return await this.operatorCarInsuranceRatingsService.verifyRating(
        id,
        body,
      );
    } catch (error) {
      console.log(
        '🚀 ~ file: car-insurance-ratings.controller.ts:78 ~ OperatorCarInsuranceRatingsController ~ error:',
        error,
      );
      return getError(LIST_ERROR_CODES.SS24001);
    }
  }

  @Get('/car-insurance/:id')
  @ApiQuery(PAGE_QUERY)
  @ApiQuery(LIMIT_QUERY)
  @ApiOperation({ summary: 'Get all rating by car insurance' })
  async findAllByInsurance(
    @Paginate() query: PaginateQuery,
    @Param('id', new UUIDPipe()) id: string,
  ) {
    try {
      return this.operatorCarInsuranceRatingsService.findAllRatingsByCarInsurance(
        query,
        id,
      );
    } catch (error) {
      console.log(
        '🚀 ~ file: ratings.controller.ts:73 ~ OperatorRatingsController ~ findAllByInsurance:',
        error,
      );
      return getError(LIST_ERROR_CODES.SS24001);
    }
  }

  @Delete(':id')
  @ApiOperation({ summary: 'Remove a car insurance rating ' })
  async removeCarInsuranceRating(@Param('id', new UUIDPipe()) id: string) {
    try {
      return this.operatorCarInsuranceRatingsService.rejectCarInsuranceRating(
        id,
      );
    } catch (error) {
      console.log(
        '🚀 ~ file: car-insurance-ratings.controller.ts:123 ~ OperatorCarInsuranceRatingsController ~ removeCarInsuranceRating ~ error:',
        error,
      );
      return getError(LIST_ERROR_CODES.SS24001);
    }
  }
}
