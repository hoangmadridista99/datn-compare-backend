import { Module } from '@nestjs/common';
import { CarInsuranceRatingsModule } from '../../core/car-insurance-ratings/car-insurance-ratings.module';

@Module({
  imports: [CarInsuranceRatingsModule],
  providers: [CarInsuranceRatingsModule],
  exports: [CarInsuranceRatingsModule],
})
export class OperatorCarInsuranceRatingsModule {}
