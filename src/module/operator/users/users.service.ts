import { HttpStatus, Injectable } from '@nestjs/common';
import { PaginateQuery } from 'nestjs-paginate';
import { UsersService } from 'src/module/core/users/users.service';
import {
  getError,
  LIST_ERROR_CODES,
} from '../../../shared/constants/error/base.error';
import { NO_CONTENT_SUCCESS_RESPONSE } from '../../../shared/lib/responses/response';
import { UpdateUserByAdminDto } from '../../core/users/dto/update-user.dto';
import { UserFilter } from '../../core/users/models/user.model';
import { getPaginate } from 'src/shared/lib/paginate/paginate.lib';
import {
  IUpdateOperatorProfile,
  IUpdatePasswordDto,
} from '../../core/users/interfaces/users.interface';
import { User, UserRole } from '../../core/users/entities/user.entity';
import { AuthService } from '../../core/auth/auth.service';

@Injectable()
export class OperatorUsersService {
  constructor(
    private readonly usersService: UsersService,
    private readonly authService: AuthService,
  ) {}

  async getOne(id: string) {
    try {
      return (
        (await this.usersService.findOneByUsersQuery({ id })) ??
        getError(LIST_ERROR_CODES.SS24101, HttpStatus.NOT_FOUND)
      );
    } catch (error) {
      console.log('🚀 ~ file: users.service.ts:25 ~  ', error);
      throw error;
    }
  }

  async getListVerifiedVendor(query: PaginateQuery, filter: UserFilter) {
    const queryBuilder = await this.usersService.findVendors(filter);
    return await getPaginate(query, queryBuilder);
  }

  async updatePersonalProfile(
    userId: string,
    body: IUpdateOperatorProfile,
    file: Express.Multer.File,
  ) {
    const avatarUrl = !file
      ? undefined
      : await this.usersService.uploadAvatarProfile(file);

    const result = await this.usersService.updateUserById(userId, {
      ...body,
      avatar_profile_url: avatarUrl,
    });
    if (!result.affected)
      getError(LIST_ERROR_CODES.SS24103, HttpStatus.NOT_FOUND);
    return !!avatarUrl
      ? { avatar_profile_url: avatarUrl }
      : NO_CONTENT_SUCCESS_RESPONSE;
  }

  async updateUser(id: string, body: UpdateUserByAdminDto) {
    const checkExist = await this.usersService.findOneByUsersQuery({ id });
    if (!checkExist) return getError(LIST_ERROR_CODES.SS24101);
    if (checkExist.role !== UserRole.VENDOR)
      return getError(LIST_ERROR_CODES.SS24009, HttpStatus.FORBIDDEN);
    const result = await this.usersService.updateUserById(id, body);
    return !result.affected
      ? getError(LIST_ERROR_CODES.SS24103, HttpStatus.NOT_FOUND)
      : NO_CONTENT_SUCCESS_RESPONSE;
  }

  async changePasswordForAdmin(user: User, body: IUpdatePasswordDto) {
    const isValidCurrentPassword = await this.authService.checkPassword(
      body.current_password,
      user.password,
    );
    if (!isValidCurrentPassword)
      return getError(LIST_ERROR_CODES.SS24321, HttpStatus.BAD_REQUEST);
    const newHashPassword = await this.authService.hashPassword(
      body.new_password,
    );
    const result = await this.usersService.updateUserById(user.id, {
      password: newHashPassword,
    });
    return !result.affected
      ? getError(LIST_ERROR_CODES.SS24322, HttpStatus.INTERNAL_SERVER_ERROR)
      : NO_CONTENT_SUCCESS_RESPONSE;
  }
}
