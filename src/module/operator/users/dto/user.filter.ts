import { IsEnum, IsNumberString, IsOptional, IsString } from 'class-validator';
import { VendorStatusTypes } from '../../../core/users/enums/users.enum';

export class OperatorFilterVendor {
  @IsOptional()
  @IsString()
  company_name?: string;

  @IsOptional()
  @IsEnum(VendorStatusTypes)
  vendor_status?: `${VendorStatusTypes}`;

  @IsNumberString()
  page: number;

  @IsNumberString()
  limit: number;
}
