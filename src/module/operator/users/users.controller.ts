import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Get,
  HttpStatus,
  Param,
  Patch,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiBody,
  ApiConsumes,
  ApiOperation,
  ApiQuery,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { OperatorUsersService } from './users.service';
import { Paginate, PaginateQuery } from 'nestjs-paginate';
import { UUIDPipe } from '../../../shared/pipes/uuid.pipe';
import { AuthResponse } from '../../../shared/decorators/response-auth/auth.decorator';
import {
  ChangePasswordAdminDto,
  UpdateOperatorProfileDto,
  UpdateUserByAdminDto,
} from '../../core/users/dto/update-user.dto';
import { AdminRoles } from '../../../shared/decorators/role.decorator';
import { JwtUserAuthGuard } from '../../core/auth/guards/jwt-auth.guard';
import { User, UserRole } from '../../core/users/entities/user.entity';
import {
  getError,
  LIST_ERROR_CODES,
} from '../../../shared/constants/error/base.error';
import { UserFilter } from '../../core/users/models/user.model';
import { VendorStatusTypes } from '../../core/users/enums/users.enum';
import { UserRolesGuard } from '../../core/auth/guards/local/role.guard';
import { FileImageValidationPipe } from '../../../shared/pipes/file-image.pipe';
import { FileInterceptor } from '@nestjs/platform-express';
import { IUpdateOperatorProfile } from '../../core/users/interfaces/users.interface';
import { FilterVendorByOperatorDecorator } from 'src/shared/decorators/users/operator/vendor.decorator';

@ApiTags('Users')
@Controller('users')
@UseInterceptors(ClassSerializerInterceptor)
@UseGuards(JwtUserAuthGuard, UserRolesGuard)
@AdminRoles(UserRole.ADMIN)
@ApiBearerAuth()
export class OperatorUsersController {
  constructor(private readonly operatorUsersService: OperatorUsersService) {}

  @ApiQuery({
    example: 1,
    name: 'page',
  })
  @ApiQuery({
    example: 10,
    name: 'limit',
  })
  @ApiQuery({
    name: 'vendor_status',
    example: VendorStatusTypes.PENDING,
    required: false,
  })
  @ApiQuery({
    name: 'company_name',
    example: 'manulife',
    required: false,
  })
  @ApiOperation({ summary: 'Get all verified vendor' })
  @ApiResponse({ status: 401, description: 'Unauthorized.' })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  @ApiResponse({ status: 200, description: 'OK' })
  @Get('/vendors')
  async getAllVerifiedVendors(
    @Paginate() query: PaginateQuery,
    @FilterVendorByOperatorDecorator() filter: UserFilter,
  ) {
    try {
      if (!filter)
        return getError(LIST_ERROR_CODES.SS24429, HttpStatus.BAD_REQUEST);
      return await this.operatorUsersService.getListVerifiedVendor(
        query,
        filter,
      );
    } catch (error) {
      console.log(
        '🚀 ~ file: users.controller.ts:137 ~ OperatorUsersController ~ getAllVerifiedVendors ~ error:',
        error,
      );
      throw getError(LIST_ERROR_CODES.SS24001);
    }
  }

  @Get('/profile')
  async getProfile(@AuthResponse() user: User) {
    return user;
  }

  @Get(':id')
  @ApiOperation({ summary: 'Get an user' })
  @ApiResponse({ status: 401, description: 'Unauthorized.' })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  @ApiResponse({ status: 200, description: 'OK' })
  async findOne(@Param('id', new UUIDPipe()) id: string) {
    try {
      return await this.operatorUsersService.getOne(id);
    } catch (error) {
      console.log(
        '🚀 ~ file: users.controller.ts:97 ~ OpUsersController ~ findOne ~ error:',
        error,
      );
      throw getError(LIST_ERROR_CODES.SS24001);
    }
  }

  @Patch('/profile/update')
  @ApiOperation({ summary: 'Update profile' })
  @ApiBody({ type: UpdateOperatorProfileDto })
  @ApiConsumes('multipart/form-data')
  @UseInterceptors(FileInterceptor('file'))
  async updatePersonalProfile(
    @AuthResponse() user: User,
    @Body() body: IUpdateOperatorProfile,
    @UploadedFile(new FileImageValidationPipe()) file: Express.Multer.File,
  ) {
    try {
      return await this.operatorUsersService.updatePersonalProfile(
        user.id,
        body,
        file,
      );
    } catch (error) {
      console.log(
        '🚀 ~ file: users.controller.ts:108 ~ OperatorUsersController ~ updatePersonalProfile ~ error:',
        error,
      );
      return getError(LIST_ERROR_CODES.SS24001);
    }
  }

  @Patch('change-password')
  @ApiOperation({ summary: 'Change password' })
  async changePassword(
    @AuthResponse() user: User,
    @Body() { confirmation_password, ...body }: ChangePasswordAdminDto,
  ) {
    try {
      if (confirmation_password !== body.new_password)
        return getError(LIST_ERROR_CODES.SS24108, HttpStatus.BAD_REQUEST);
      return await this.operatorUsersService.changePasswordForAdmin(user, body);
    } catch (error) {
      console.log(
        '🚀 ~ file: users.controller.ts:132 ~ OperatorUsersController ~ changePassword ~ error:',
        error,
      );
      return getError(LIST_ERROR_CODES.SS24001);
    }
  }

  @Patch(':id')
  @ApiOperation({ summary: 'Update an user' })
  async updateUserByAdmin(
    @Param('id') id: string,
    @Body() body: UpdateUserByAdminDto,
  ) {
    try {
      return await this.operatorUsersService.updateUser(id, body);
    } catch (error) {
      console.log(
        '🚀 ~ file: users.controller.ts:112 ~ OpUsersController ~ error:',
        error,
      );
      throw getError(LIST_ERROR_CODES.SS24001);
    }
  }
}
