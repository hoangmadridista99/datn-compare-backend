import { Injectable } from '@nestjs/common';
import { PaginateQuery } from 'nestjs-paginate';
import { CompaniesService } from 'src/module/core/companies/companies.service';
import {
  CreateCompanyDto,
  UpdateCompanyDto,
} from 'src/module/core/companies/dto/companies.dto';
import { IUpdateCompany } from 'src/module/core/companies/interfaces/companies.interface';
import { MinioTypes } from 'src/module/core/minio/enums/minio.enum';
import { MinioService } from 'src/module/core/minio/minio.service';
import {
  LIST_ERROR_CODES,
  getError,
} from 'src/shared/constants/error/base.error';
import {
  getFileName,
  getFileKey,
  getFileUrl,
} from 'src/shared/helper/system.helper';
import { IGetError } from 'src/shared/interfaces/error.interface';
import { getPaginate } from 'src/shared/lib/paginate/paginate.lib';
import { NO_CONTENT_SUCCESS_RESPONSE } from 'src/shared/lib/responses/response';
import { CompanyTypes } from '../../core/companies/entities/company.entity';

@Injectable()
export class OperatorCompaniesService {
  constructor(
    private readonly companiesService: CompaniesService,
    private readonly minioService: MinioService,
  ) {}

  async uploadLogo(file: Express.Multer.File): Promise<string | IGetError> {
    try {
      const fileName = getFileName(file.originalname);
      const imageKey = getFileKey(MinioTypes.COMPANY_LOGO, fileName);
      const isUploadFile = await this.minioService.uploadFile(imageKey, file);
      if (!isUploadFile) return getError(LIST_ERROR_CODES.SS24105, 400);
      return getFileUrl(MinioTypes.COMPANY_LOGO, fileName);
    } catch (error) {
      console.log(
        '🚀 ~ file: companies.service.ts:28 ~ OperatorCompaniesService ~ uploadLogo:',
        error,
      );

      return getError(LIST_ERROR_CODES.SS24001, 500);
    }
  }

  async findAllVehicleInsuranceCompanies(query: PaginateQuery) {
    try {
      const queryBuilder = this.companiesService.queryBuilder();
      queryBuilder.where({ type: CompanyTypes.Vehicle });
      return await getPaginate(query, queryBuilder);
    } catch (error) {
      console.log(
        '🚀 ~ file: companies.service.ts:54 ~ OperatorCompaniesService ~ findAllVehicleInsuranceCompanies ~ error:',
        error,
      );
      throw error;
    }
  }

  async findAll(query: PaginateQuery) {
    try {
      const queryBuilder = this.companiesService.queryBuilder();
      return await getPaginate(query, queryBuilder);
    } catch (error) {
      console.log(
        '🚀 ~ file: companies.service.ts:56 ~ OperatorCompaniesService ~ findAll:',
        error,
      );

      return getError(LIST_ERROR_CODES.SS24001, 500);
    }
  }

  async createCompany(body: CreateCompanyDto, file: Express.Multer.File) {
    try {
      const logo = await this.uploadLogo(file);
      if (typeof logo !== 'string') return logo;
      const company = await this.companiesService.createCompany({
        ...body,
        logo,
      });
      if (!company) return getError(LIST_ERROR_CODES.SS24104, 400);
      return company;
    } catch (error) {
      console.log(
        '🚀 ~ file: companies.service.ts:14 ~ OperatorCompaniesService ~ createCompany:',
        error,
      );

      return getError(LIST_ERROR_CODES.SS24001, 500);
    }
  }

  async updateCompany(
    id: string,
    data: UpdateCompanyDto,
    file?: Express.Multer.File,
  ) {
    try {
      let updateData: IUpdateCompany = { ...data };
      if (file) {
        const logo = await this.uploadLogo(file);

        if (typeof logo !== 'string') return logo;

        updateData = { ...updateData, logo };
      }
      const isUpdate = await this.companiesService.updateCompany(
        id,
        updateData,
      );
      return !isUpdate
        ? getError(LIST_ERROR_CODES.SS24103, 400)
        : NO_CONTENT_SUCCESS_RESPONSE;
    } catch (error) {
      console.log(
        '🚀 ~ file: companies.service.ts:77 ~ OperatorCompaniesService:',
        error,
      );

      return getError(LIST_ERROR_CODES.SS24001, 500);
    }
  }

  async deleteCompany(id: string) {
    try {
      const isDeleted = await this.companiesService.removeCompany(id);
      return isDeleted
        ? NO_CONTENT_SUCCESS_RESPONSE
        : getError(LIST_ERROR_CODES.SS24102, 400);
    } catch (error) {
      console.log(
        '🚀 ~ file: companies.service.ts:109 ~ OperatorCompaniesService ~ deleteCompany:',
        error,
      );

      return getError(LIST_ERROR_CODES.SS24001, 500);
    }
  }
}
