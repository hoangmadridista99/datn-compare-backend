import { Module } from '@nestjs/common';
import { CompaniesModule } from 'src/module/core/companies/companies.module';

@Module({
  imports: [CompaniesModule],
  exports: [CompaniesModule],
  providers: [CompaniesModule],
})
export class OperatorCompaniesModule {}
