import {
  Controller,
  Post,
  Get,
  Patch,
  Delete,
  UseGuards,
  UseInterceptors,
  Body,
  UploadedFile,
  Param,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiTags,
  ApiConsumes,
  ApiBody,
  ApiOperation,
  ApiQuery,
} from '@nestjs/swagger';
import { JwtUserAuthGuard } from 'src/module/core/auth/guards/jwt-auth.guard';
import { UserRolesGuard } from 'src/module/core/auth/guards/local/role.guard';
import { UserRole } from 'src/module/core/users/entities/user.entity';
import { AdminRoles } from 'src/shared/decorators/role.decorator';
import { OperatorCompaniesService } from './companies.service';
import {
  CreateCompanyDto,
  CreateCompanyHaveFileDto,
  UpdateCompanyDto,
  UpdateCompanyHaveFileDto,
} from 'src/module/core/companies/dto/companies.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { UUIDPipe } from 'src/shared/pipes/uuid.pipe';
import { Paginate, PaginateQuery } from 'nestjs-paginate';
import { FileImageValidationPipe } from 'src/shared/pipes/file-image.pipe';

@Controller('companies')
@ApiTags('Companies')
@ApiBearerAuth()
@UseGuards(JwtUserAuthGuard, UserRolesGuard)
@AdminRoles(UserRole.ADMIN)
export class OperatorCompaniesController {
  constructor(
    private readonly operatorCompanyService: OperatorCompaniesService,
  ) {}

  @Get()
  @ApiQuery({
    example: 1,
    name: 'page',
  })
  @ApiQuery({
    example: 10,
    name: 'limit',
  })
  @ApiOperation({ summary: 'Get list companies' })
  async findAll(@Paginate() query: PaginateQuery) {
    try {
      return this.operatorCompanyService.findAll(query);
    } catch (error) {
      console.log(
        '🚀 ~ file: companies.controller.ts:60 ~ OperatorCompaniesController ~ findAll:',
        error,
      );
      throw error;
    }
  }

  @Get('vehicle')
  @ApiQuery({
    example: 1,
    name: 'page',
  })
  @ApiQuery({
    example: 10,
    name: 'limit',
  })
  async findAllVehicleInsuranceCompanies(@Paginate() query: PaginateQuery) {
    try {
      return this.operatorCompanyService.findAllVehicleInsuranceCompanies(
        query,
      );
    } catch (error) {
      console.log(
        '🚀 ~ file: companies.controller.ts:60 ~ OperatorCompaniesController ~ findAll:',
        error,
      );
      throw error;
    }
  }

  @Post()
  @ApiOperation({ summary: 'Create company' })
  @ApiConsumes('multipart/form-data')
  @ApiBody({ type: CreateCompanyHaveFileDto })
  @UseInterceptors(FileInterceptor('file'))
  async createCompany(
    @UploadedFile(new FileImageValidationPipe()) file: Express.Multer.File,
    @Body() body: CreateCompanyDto,
  ) {
    try {
      return this.operatorCompanyService.createCompany(body, file);
    } catch (error) {
      console.log(
        '🚀 ~ file: companies.controller.ts:38 ~ OperatorCompaniesController ~ createCompany:',
        error,
      );
      throw error;
    }
  }

  @Patch(':id')
  @ApiOperation({ summary: 'Update company' })
  @ApiConsumes('multipart/form-data')
  @ApiBody({ type: UpdateCompanyHaveFileDto })
  @UseInterceptors(FileInterceptor('file'))
  async updateCompany(
    @Param('id', new UUIDPipe()) id: string,
    @Body() body: UpdateCompanyDto,
    @UploadedFile(new FileImageValidationPipe())
    file?: Express.Multer.File,
  ) {
    try {
      return this.operatorCompanyService.updateCompany(id, body, file);
    } catch (error) {
      console.log(
        '🚀 ~ file: companies.controller.ts:62 ~ OperatorCompaniesController ~ updateCompany:',
        error,
      );
      throw error;
    }
  }

  @Delete(':id')
  @ApiOperation({ summary: 'Delete company' })
  async deleteCompany(@Param('id', new UUIDPipe()) id: string) {
    try {
      return this.operatorCompanyService.deleteCompany(id);
    } catch (error) {
      console.log(
        '🚀 ~ file: companies.controller.ts:90 ~ OperatorCompaniesController ~ deleteCompany:',
        error,
      );
      throw error;
    }
  }
}
