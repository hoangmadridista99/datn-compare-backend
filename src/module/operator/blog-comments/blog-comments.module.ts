import { Module } from '@nestjs/common';
import { BlogCommentsModule } from '../../core/blog-reviews/blog-comments.module';

@Module({
  imports: [BlogCommentsModule],
  providers: [BlogCommentsModule],
  exports: [BlogCommentsModule],
})
export class OperatorBlogCommentsModule {}
