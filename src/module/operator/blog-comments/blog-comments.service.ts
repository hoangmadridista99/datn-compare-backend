import { HttpStatus, Injectable } from '@nestjs/common';
import { BlogCommentsService } from '../../core/blog-reviews/blog-comments.service';
import {
  IClientUpdateBlogComment,
  IFilterBlogComment,
} from '../../core/blog-reviews/interfaces/blog-comment.interface';
import { PaginateQuery } from 'nestjs-paginate';
import {
  LIST_ERROR_CODES,
  getError,
} from '../../../shared/constants/error/base.error';
import { NO_CONTENT_SUCCESS_RESPONSE } from '../../../shared/lib/responses/response';

@Injectable()
export class OperatorBlogCommentsService {
  constructor(private readonly blogCommentsService: BlogCommentsService) {}

  async getListComments(query: PaginateQuery, filter: IFilterBlogComment) {
    return await this.blogCommentsService.getListCommentsByBlogId(
      query,
      filter,
      true,
    );
  }

  async deleteCommentById(id: string) {
    const result = await this.blogCommentsService.deleteComment(id);
    return !result.affected
      ? getError(LIST_ERROR_CODES.SS24102, HttpStatus.NOT_FOUND)
      : NO_CONTENT_SUCCESS_RESPONSE;
  }

  async hideTheCommentByAd(id: string, body: IClientUpdateBlogComment) {
    const result = await this.blogCommentsService.updateComment(id, body);
    return !result.affected
      ? getError(LIST_ERROR_CODES.SS24103, HttpStatus.NOT_FOUND)
      : NO_CONTENT_SUCCESS_RESPONSE;
  }
}
