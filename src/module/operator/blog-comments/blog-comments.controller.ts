import {
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  Param,
  Patch,
  UseGuards,
} from '@nestjs/common';
import {
  ApiTags,
  ApiBearerAuth,
  ApiOperation,
  ApiQuery,
} from '@nestjs/swagger';
import { AdminRoles } from '../../../shared/decorators/role.decorator';
import { JwtUserAuthGuard } from '../../core/auth/guards/jwt-auth.guard';
import { UserRolesGuard } from '../../core/auth/guards/local/role.guard';
import { UserRole } from '../../core/users/entities/user.entity';
import { OperatorBlogCommentsService } from './blog-comments.service';
import { BlogCommentsFilter } from '../../../shared/decorators/blog-comments/filter-blog-comments.decorator';
import { IFilterBlogComment } from '../../core/blog-reviews/interfaces/blog-comment.interface';
import { Paginate, PaginateQuery } from 'nestjs-paginate';

import {
  BLOG_ID_QUERY,
  USER_ID_QUERY,
} from '../../client/blog-comments/blog-comments.controller';
import {
  LIST_ERROR_CODES,
  getError,
} from '../../../shared/constants/error/base.error';
import { UUIDPipe } from '../../../shared/pipes/uuid.pipe';
import { UpdateBlogCommentDto } from '../../core/blog-reviews/dto/blog-comment.dto';
import {
  PAGE_QUERY,
  LIMIT_QUERY,
} from '../../../shared/constants/swagger/constant';

@ApiTags('Blog-Comments')
@Controller('blog-comments')
@UseGuards(JwtUserAuthGuard, UserRolesGuard)
@AdminRoles(UserRole.ADMIN)
@ApiBearerAuth()
export class OperatorBlogCommentsController {
  constructor(
    private readonly operatorBlogCommentsService: OperatorBlogCommentsService,
  ) {}

  @Get()
  @ApiQuery(PAGE_QUERY)
  @ApiQuery(LIMIT_QUERY)
  @ApiQuery(USER_ID_QUERY)
  @ApiQuery(BLOG_ID_QUERY)
  @ApiOperation({ summary: 'Get list comments by blog id' })
  async getListComment(
    @BlogCommentsFilter() filter: IFilterBlogComment,
    @Paginate() query: PaginateQuery,
  ) {
    try {
      return !filter
        ? getError(LIST_ERROR_CODES.SS24108, HttpStatus.BAD_REQUEST)
        : await this.operatorBlogCommentsService.getListComments(query, filter);
    } catch (error) {
      console.log(
        '🚀 ~ file: blog-comments.controller.ts:34 ~ OperatorBlogCommentsController ~ getListComment ~ error:',
        error,
      );
      return getError(LIST_ERROR_CODES.SS24001);
    }
  }

  @Patch(':id')
  @ApiOperation({ summary: 'update comment by ad' })
  async updateComment(
    @Param('id', new UUIDPipe()) id: string,
    @Body() body: UpdateBlogCommentDto,
  ) {
    try {
      return await this.operatorBlogCommentsService.hideTheCommentByAd(
        id,
        body,
      );
    } catch (error) {
      console.log(
        '🚀 ~ file: blog-comments.controller.ts:81 ~ OperatorBlogCommentsController ~ error:',
        error,
      );
      return getError(LIST_ERROR_CODES.SS24001);
    }
  }

  @Delete(':id')
  @ApiOperation({ summary: 'delete comment by ad' })
  async deleteComment(@Param('id', new UUIDPipe()) id: string) {
    try {
      return await this.operatorBlogCommentsService.deleteCommentById(id);
    } catch (error) {
      console.log(
        '🚀 ~ file: blog-comments.controller.ts:99 ~ OperatorBlogCommentsController ~ deleteComment ~ error:',
        error,
      );
      return getError(LIST_ERROR_CODES.SS24001);
    }
  }
}
