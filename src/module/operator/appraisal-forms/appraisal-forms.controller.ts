import {
  Body,
  Controller,
  Get,
  HttpStatus,
  Param,
  Patch,
  Query,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiQuery,
  ApiTags,
} from '@nestjs/swagger';
import { OperatorAppraisalFormsService } from './appraisal-forms.service';
import { AdminRoles } from '../../../shared/decorators/role.decorator';
import { JwtUserAuthGuard } from '../../core/auth/guards/jwt-auth.guard';
import { UserRolesGuard } from '../../core/auth/guards/local/role.guard';
import { UserRole } from '../../core/users/entities/user.entity';
import { Paginate, PaginateQuery } from 'nestjs-paginate';
import {
  PAGE_QUERY,
  LIMIT_QUERY,
} from '../../../shared/constants/swagger/constant';
import {
  LIST_ERROR_CODES,
  getError,
} from '../../../shared/constants/error/base.error';
import { UUIDPipe } from '../../../shared/pipes/uuid.pipe';
import { VerifyAppraisalFormDto } from '../../core/appraisal-form/dto/appraisal-form.dto';
import { AppraisalFormStatuses } from '../../core/appraisal-form/enum/appraisal-form.enum';
import { AdminFilterAppraisalFormsListDto } from '../../core/appraisal-form/dto/admin-filter.dto';
import {
  APPRAISAL_FORM_CODE_QUERY,
  APPRAISAL_FORM_STATUS_QUERY,
} from '../../../shared/constants/swagger/appraisal-forms/appraisal-forms.constant';

@ApiTags('Appraisal-Forms')
@Controller('appraisal-forms')
@UseGuards(JwtUserAuthGuard, UserRolesGuard)
@ApiBearerAuth()
@AdminRoles(UserRole.ADMIN)
export class OperatorAppraisalFormsController {
  constructor(
    private readonly operatorAppraisalFormsService: OperatorAppraisalFormsService,
  ) {}

  @Get()
  @ApiQuery(PAGE_QUERY)
  @ApiQuery(LIMIT_QUERY)
  @ApiQuery(APPRAISAL_FORM_STATUS_QUERY)
  @ApiQuery(APPRAISAL_FORM_CODE_QUERY)
  @ApiOperation({ summary: 'Get appraisal-forms for admin' })
  async getListFormsForAdmin(
    @Paginate() query: PaginateQuery,
    @Query() filter: AdminFilterAppraisalFormsListDto,
  ) {
    try {
      return this.operatorAppraisalFormsService.getFormsListByClient(
        query,
        filter,
      );
    } catch (error) {
      console.log(
        '🚀 ~ file: appraisal-forms.controller.ts:29 ~ OperatorAppraisalFormsController ~ getListFormsForAdmin ~ error:',
        error,
      );
      return getError(LIST_ERROR_CODES.SS24001);
    }
  }

  @Get(':id')
  @ApiOperation({ summary: 'Get appraisal form details' })
  async getDetailsById(@Param('id', new UUIDPipe()) id: string) {
    try {
      return this.operatorAppraisalFormsService.getFormDetailsById(id);
    } catch (error) {
      console.log(
        '🚀 ~ file: appraisal-forms.controller.ts:76 ~ OperatorAppraisalFormsController ~ getDetailsById ~ error:',
        error,
      );
      return getError(LIST_ERROR_CODES.SS24001);
    }
  }

  @Patch(':id/verify')
  @ApiOperation({ summary: 'Verify appraisal form for admin ' })
  async verifyAppraisalForm(
    @Param('id', new UUIDPipe()) id: string,
    @Body() body: VerifyAppraisalFormDto,
  ) {
    try {
      if (body.form_status === AppraisalFormStatuses.Paid)
        return getError(LIST_ERROR_CODES.SS24103, HttpStatus.BAD_REQUEST);
      return await this.operatorAppraisalFormsService.updateAppraisalFormStatus(
        id,
        body,
      );
    } catch (error) {
      console.log(
        '🚀 ~ file: appraisal-forms.controller.ts:60 ~ OperatorAppraisalFormsController ~ error:',
        error,
      );
      return getError(LIST_ERROR_CODES.SS24001);
    }
  }
}
