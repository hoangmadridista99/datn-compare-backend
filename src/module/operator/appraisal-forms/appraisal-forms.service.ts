import { HttpStatus, Injectable } from '@nestjs/common';
import { AppraisalFormsService } from '../../core/appraisal-form/appraisal-forms.service';
import { PaginateQuery } from 'nestjs-paginate';
import {
  IAppraisalFormsFilter,
  IVerifyAppraisalFormDto,
} from '../../core/appraisal-form/interfaces/appraisal-form.interface';
import {
  getError,
  LIST_ERROR_CODES,
} from '../../../shared/constants/error/base.error';
import { NO_CONTENT_SUCCESS_RESPONSE } from '../../../shared/lib/responses/response';
import { CarInsurancesService } from '../../core/car-insurances/car-insurances.service';
import { IAppraisalData } from '../../core/appraisals/entities/appraisal.entity';
import { AppraisalFormStatuses } from '../../core/appraisal-form/enum/appraisal-form.enum';
import { AppraisalsService } from '../../core/appraisals/appraisals.service';

@Injectable()
export class OperatorAppraisalFormsService {
  constructor(
    private readonly appraisalFormsService: AppraisalFormsService,
    private readonly carInsurancesService: CarInsurancesService,
    private readonly appraisalsService: AppraisalsService,
  ) {}

  private async handleAppraisalDataAfter(appraisalData: IAppraisalData[]) {
    const result = appraisalData.map(async (data: IAppraisalData) => {
      const carInsurance = await this.carInsurancesService.findOneByQuery({
        id: data.physical_setting_id,
      });
      if (!carInsurance) return;

      return {
        ...data,
        company_logo_url: carInsurance.company_logo_url,
        company_name: carInsurance.company_name,
      };
    });
    return (await Promise.all(result)).filter((element) => !!element);
  }

  async getFormsListByClient(
    query: PaginateQuery,
    filter: IAppraisalFormsFilter,
  ) {
    return this.appraisalFormsService.getFormsListForAdmin(query, filter);
  }

  async getFormDetailsById(id: string) {
    const result = await this.appraisalFormsService.findOneFormByQuery({ id });
    if (!result)
      return getError(LIST_ERROR_CODES.SS24101, HttpStatus.NOT_FOUND);
    if (
      (result.form_status !== AppraisalFormStatuses.Approved &&
        result.form_status !== AppraisalFormStatuses.Paid) ||
      !result?.appraisal?.appraisal_data
    )
      return result;

    const appraisalData = await this.handleAppraisalDataAfter(
      result.appraisal.appraisal_data,
    );
    result.appraisal.appraisal_data = appraisalData;

    if (appraisalData.length !== result.appraisal.appraisal_data.length) {
      const updateBody = {
        ...result.appraisal,
        appraisal: result.appraisal,
      };
      await this.appraisalsService.saveAppraisal(updateBody);
    }
    return result;
  }

  async updateAppraisalFormStatus(id: string, body: IVerifyAppraisalFormDto) {
    const result = await this.appraisalFormsService.updateAppraisalFormById(
      id,
      body,
    );
    if (!result)
      return getError(LIST_ERROR_CODES.SS24103, HttpStatus.BAD_REQUEST);
    return NO_CONTENT_SUCCESS_RESPONSE;
  }
}
