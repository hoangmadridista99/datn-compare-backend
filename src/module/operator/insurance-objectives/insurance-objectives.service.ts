import { HttpStatus, Injectable } from '@nestjs/common';
import { InsuranceObjectivesService } from '../../core/insurance-objectives/insurance-objectives.service';
import {
  CreateInsuranceObjectiveDto,
  UpdateInsuranceObjectiveDto,
} from '../../core/insurance-objectives/dto/insurance-objective.dto';
import {
  LIST_ERROR_CODES,
  getError,
} from '../../../shared/constants/error/base.error';
import { NO_CONTENT_SUCCESS_RESPONSE } from '../../../shared/lib/responses/response';

@Injectable()
export class OperatorInsuranceObjectivesService {
  constructor(
    private readonly insuranceObjectivesService: InsuranceObjectivesService,
  ) {}

  async createNewObjective(body: CreateInsuranceObjectiveDto) {
    const checkTypeExist =
      await this.insuranceObjectivesService.findOneByObjectiveType(
        body.objective_type,
      );
    if (checkTypeExist)
      return getError(LIST_ERROR_CODES.SS24404, HttpStatus.BAD_REQUEST);
    return await this.insuranceObjectivesService.createInsuranceObjective(body);
  }

  async getAllObjectives() {
    return await this.insuranceObjectivesService.findAllInsuranceObjectives();
  }

  async updateById(id: string, body: UpdateInsuranceObjectiveDto) {
    const result = await this.insuranceObjectivesService.updateById(id, body);
    return !result.affected
      ? getError(LIST_ERROR_CODES.SS24103, HttpStatus.NOT_FOUND)
      : NO_CONTENT_SUCCESS_RESPONSE;
  }

  async deleteById(id: string) {
    const result = await this.insuranceObjectivesService.deleteById(id);
    return !result.affected
      ? getError(LIST_ERROR_CODES.SS24102, HttpStatus.NOT_FOUND)
      : NO_CONTENT_SUCCESS_RESPONSE;
  }
}
