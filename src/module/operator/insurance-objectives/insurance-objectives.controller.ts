import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  UseGuards,
} from '@nestjs/common';
import { OperatorInsuranceObjectivesService } from './insurance-objectives.service';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { AdminRoles } from '../../../shared/decorators/role.decorator';
import { JwtUserAuthGuard } from '../../core/auth/guards/jwt-auth.guard';
import { UserRolesGuard } from '../../core/auth/guards/local/role.guard';
import { UserRole } from '../../core/users/entities/user.entity';
import {
  CreateInsuranceObjectiveDto,
  UpdateInsuranceObjectiveDto,
} from '../../core/insurance-objectives/dto/insurance-objective.dto';
import {
  LIST_ERROR_CODES,
  getError,
} from '../../../shared/constants/error/base.error';
import { UUIDPipe } from '../../../shared/pipes/uuid.pipe';

@ApiTags('Insurance-Objectives')
@Controller('insurance-objectives')
@UseGuards(JwtUserAuthGuard, UserRolesGuard)
@AdminRoles(UserRole.ADMIN)
@ApiBearerAuth()
export class OperatorInsuranceObjectivesController {
  constructor(
    private readonly operatorInsuranceObjectivesService: OperatorInsuranceObjectivesService,
  ) {}

  @Post()
  @ApiOperation({ summary: 'Create a new objective of insurance' })
  async createNewObjective(@Body() body: CreateInsuranceObjectiveDto) {
    try {
      return await this.operatorInsuranceObjectivesService.createNewObjective(
        body,
      );
    } catch (error) {
      console.log(
        '🚀 ~ file: insurance-objectives.controller.ts:26 ~ OperatorInsuranceObjectivesController ~ createNewObjective ~ error:',
        error,
      );
      throw getError(LIST_ERROR_CODES.SS24001);
    }
  }

  @Get()
  @ApiOperation({ summary: 'List all objectives of insurance' })
  async getAllObjectives() {
    try {
      return await this.operatorInsuranceObjectivesService.getAllObjectives();
    } catch (error) {
      console.log(
        '🚀 ~ file: insurance-objectives.controller.ts:46 ~ OperatorInsuranceObjectivesController ~ getAllObjectives ~ error:',
        error,
      );
      throw getError(LIST_ERROR_CODES.SS24001);
    }
  }

  @Patch(':id')
  @ApiOperation({ summary: 'Update  objective of insurance' })
  async updateById(
    @Param('id', new UUIDPipe()) id: string,
    @Body() body: UpdateInsuranceObjectiveDto,
  ) {
    try {
      return await this.operatorInsuranceObjectivesService.updateById(id, body);
    } catch (error) {
      console.log(
        '🚀 ~ file: insurance-objectives.controller.ts:61 ~ OperatorInsuranceObjectivesController ~ updateById ~ error:',
        error,
      );

      throw getError(LIST_ERROR_CODES.SS24001);
    }
  }

  @Delete(':id')
  @ApiOperation({ summary: 'Delete a objective of insurance ' })
  async deleteById(@Param('id', new UUIDPipe()) id: string) {
    try {
      return await this.operatorInsuranceObjectivesService.deleteById(id);
    } catch (error) {
      console.log(
        '🚀 ~ file: insurance-objectives.controller.ts:91 ~ OperatorInsuranceObjectivesController ~ deleteById ~ error:',
        error,
      );
      throw getError(LIST_ERROR_CODES.SS24001);
    }
  }
}
