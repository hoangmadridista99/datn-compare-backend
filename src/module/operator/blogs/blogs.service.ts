import { HttpStatus, Injectable } from '@nestjs/common';
import { BlogsService } from '../../core/blogs/blogs.service';
import { PaginateQuery } from 'nestjs-paginate';
import {
  getError,
  LIST_ERROR_CODES,
} from '../../../shared/constants/error/base.error';
import { NO_CONTENT_SUCCESS_RESPONSE } from '../../../shared/lib/responses/response';
import {
  IAdminBlogsFilter,
  IAdminUpdateBlogDto,
  IAdminVerifyBlogDto,
  ICreateBlogDto,
} from '../../core/blogs/interfaces/blogs.interface';
import { BlogStatusesTypes } from '../../core/blogs/enums/blogs.enum';

@Injectable()
export class OperatorBlogsService {
  constructor(private readonly blogsService: BlogsService) {}

  async createNewBlog(body: ICreateBlogDto, userId: string) {
    const result = await this.blogsService.create(body, userId);
    return !result
      ? getError(LIST_ERROR_CODES.SS24103, HttpStatus.INTERNAL_SERVER_ERROR)
      : NO_CONTENT_SUCCESS_RESPONSE;
  }

  async getAllBlogs(query: PaginateQuery, filter: IAdminBlogsFilter) {
    return await this.blogsService.filterBlogsForAdmin(query, filter);
  }

  async getOneById(id: string) {
    return (
      (await this.blogsService.findOneById(id)) ??
      getError(LIST_ERROR_CODES.SS24101, HttpStatus.NOT_FOUND)
    );
  }

  async uploadBanner(file: Express.Multer.File) {
    return await this.blogsService.uploadBannerImage(file);
  }

  async handleVerifyBlog(id: string, body: IAdminVerifyBlogDto) {
    const blog = await this.blogsService.findOneById(id);
    if (!blog) return getError(LIST_ERROR_CODES.SS24101, HttpStatus.NOT_FOUND);
    await this.blogsService.update(id, body);

    const isApproveBlog =
      body.status === BlogStatusesTypes.Approved &&
      blog.status !== BlogStatusesTypes.Approved;
    const isRejectBlogWhenItsApprovedAfter =
      body.status !== BlogStatusesTypes.Approved &&
      blog.status === BlogStatusesTypes.Approved;
    let newQuantity: number;
    switch (true) {
      case isApproveBlog:
        newQuantity = blog.blog_category.quantity_of_blogs + 1;
        await this.blogsService.calculateQuantityOfBlogAfterVerified(
          blog.blog_category_id,
          newQuantity,
        );
        break;
      case isRejectBlogWhenItsApprovedAfter:
        newQuantity = blog.blog_category.quantity_of_blogs - 1;
        await this.blogsService.calculateQuantityOfBlogAfterVerified(
          blog.blog_category_id,
          newQuantity,
        );
        break;
    }

    return NO_CONTENT_SUCCESS_RESPONSE;
  }

  async updateBlogById(id: string, body: IAdminUpdateBlogDto) {
    const result = await this.blogsService.update(id, body);
    return !result.affected
      ? getError(LIST_ERROR_CODES.SS24103, HttpStatus.NOT_FOUND)
      : NO_CONTENT_SUCCESS_RESPONSE;
  }

  async deleteBlogById(id: string) {
    const result = await this.blogsService.deleteBlogForAdmin(id);
    return !result
      ? getError(LIST_ERROR_CODES.SS24104, HttpStatus.NOT_FOUND)
      : NO_CONTENT_SUCCESS_RESPONSE;
  }
}
