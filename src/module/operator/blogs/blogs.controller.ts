import {
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  Param,
  Patch,
  Post,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiTags,
  ApiBearerAuth,
  ApiOperation,
  ApiBody,
  ApiConsumes,
  ApiQuery,
} from '@nestjs/swagger';
import { JwtUserAuthGuard } from '../../core/auth/guards/jwt-auth.guard';
import { UserRolesGuard } from '../../core/auth/guards/local/role.guard';
import { OperatorBlogsService } from './blogs.service';
import { AdminRoles } from '../../../shared/decorators/role.decorator';
import { User, UserRole } from '../../core/users/entities/user.entity';
import {
  getError,
  LIST_ERROR_CODES,
} from '../../../shared/constants/error/base.error';
import { AuthResponse } from '../../../shared/decorators/response-auth/auth.decorator';
import {
  AdminUpdateBlogDto,
  AdminVerifyBlogDto,
  CreateBlogDto,
  UploadBannerDto,
} from '../../core/blogs/dto/blog.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { FileImageValidationPipe } from '../../../shared/pipes/file-image.pipe';
import { UUIDPipe } from '../../../shared/pipes/uuid.pipe';
import { Paginate, PaginateQuery } from 'nestjs-paginate';

import { BlogStatusesTypes } from '../../core/blogs/enums/blogs.enum';
import { AdminFilterBlogs } from '../../../shared/decorators/blogs/admin-filter.blogs';
import { IAdminBlogsFilter } from '../../core/blogs/interfaces/blogs.interface';

import {
  PAGE_QUERY,
  LIMIT_QUERY,
  CREATED_AT_QUERY,
  ROLE_USER_QUERY,
} from '../../../shared/constants/swagger/constant';

import {
  BLOG_CATEGORY_ID_QUERY,
  STATUS_QUERY,
  TITLE_QUERY,
} from '../../../shared/constants/swagger/blogs/blogs.constant';

@ApiTags('Blogs')
@Controller('blogs')
@UseGuards(JwtUserAuthGuard, UserRolesGuard)
@ApiBearerAuth()
@AdminRoles(UserRole.ADMIN)
export class OperatorBlogsController {
  constructor(private operatorBlogsService: OperatorBlogsService) {}

  @Post()
  @ApiOperation({ summary: 'Create new blog' })
  async createNewBlog(@Body() body: CreateBlogDto, @AuthResponse() user: User) {
    try {
      return await this.operatorBlogsService.createNewBlog(
        { ...body, status: BlogStatusesTypes.Approved },
        user.id,
      );
    } catch (error) {
      console.log(
        '🚀 ~ file: blogs.controller.ts:41 ~ ClientBlogsController ~ error:',
        error,
      );
      return getError(LIST_ERROR_CODES.SS24001);
    }
  }

  @Post('upload/banner')
  @ApiConsumes('multipart/form-data')
  @ApiOperation({ summary: 'Upload banner blog' })
  @UseInterceptors(FileInterceptor('file'))
  @ApiBody({ type: UploadBannerDto })
  async uploadAvatar(
    @UploadedFile(new FileImageValidationPipe()) file: Express.Multer.File,
  ) {
    try {
      const image_url =
        (await this.operatorBlogsService.uploadBanner(file)) ?? null;
      return { image_url };
    } catch (error) {
      console.log(
        '🚀 ~ file: blogs.controller.ts:66 ~ ClientBlogsController ~ uploadAvatar ~ error:',
        error,
      );
      return getError(LIST_ERROR_CODES.SS24001);
    }
  }

  @Get(':id')
  @ApiOperation({ summary: 'Get detail blog' })
  async getDetail(@Param('id', new UUIDPipe()) id: string) {
    try {
      return await this.operatorBlogsService.getOneById(id);
    } catch (error) {
      console.log(
        '🚀 ~ file: blogs.controller.ts:81 ~ ClientBlogsController ~ getDetail ~ error:',
        error,
      );
      return getError(LIST_ERROR_CODES.SS24001);
    }
  }

  @Patch(':id')
  @ApiOperation({ summary: 'Update blog' })
  async updateBlogById(
    @Param('id', new UUIDPipe()) id: string,
    @Body() body: AdminUpdateBlogDto,
  ) {
    try {
      return await this.operatorBlogsService.updateBlogById(id, body);
    } catch (error) {
      console.log(
        '🚀 ~ file: blogs.controller.ts:99 ~ ClientBlogsController ~ error:',
        error,
      );
      return getError(LIST_ERROR_CODES.SS24001);
    }
  }

  @Patch(':id/verify')
  @ApiOperation({ summary: 'Verify blog' })
  async verifyBlog(
    @Param('id', new UUIDPipe()) id: string,
    @Body() body: AdminVerifyBlogDto,
  ) {
    try {
      return await this.operatorBlogsService.handleVerifyBlog(id, body);
    } catch (error) {
      console.log(
        '🚀 ~ file: blogs.controller.ts:143 ~ OperatorBlogsController ~ error:',
        error,
      );
      return getError(LIST_ERROR_CODES.SS24001);
    }
  }

  @Get()
  @ApiQuery(PAGE_QUERY)
  @ApiQuery(LIMIT_QUERY)
  @ApiQuery(BLOG_CATEGORY_ID_QUERY)
  @ApiQuery(CREATED_AT_QUERY)
  @ApiQuery(TITLE_QUERY)
  @ApiQuery(STATUS_QUERY)
  @ApiQuery(ROLE_USER_QUERY)
  @ApiOperation({ summary: 'Get list blogs' })
  async getListBlogs(
    @Paginate() query: PaginateQuery,
    @AdminFilterBlogs() filter: IAdminBlogsFilter,
  ) {
    try {
      if (!filter)
        return getError(LIST_ERROR_CODES.SS24429, HttpStatus.BAD_REQUEST);
      return await this.operatorBlogsService.getAllBlogs(query, filter);
    } catch (error) {
      console.log(
        '🚀 ~ file: blogs.controller.ts:63 ~ ClientBlogsController ~ getListBlogs ~ error:',
        error,
      );
      return getError(LIST_ERROR_CODES.SS24001);
    }
  }

  @Delete(':id')
  @ApiOperation({ summary: 'Delete blog' })
  async deleteBlog(@Param('id', new UUIDPipe()) id: string) {
    try {
      return await this.operatorBlogsService.deleteBlogById(id);
    } catch (error) {
      console.log(
        '🚀 ~ file: blogs.controller.ts:152 ~ ClientBlogsController ~ deleteBlog ~ error:',
        error,
      );
      return getError(LIST_ERROR_CODES.SS24001);
    }
  }
}
