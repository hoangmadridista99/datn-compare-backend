import { HttpStatus, Injectable } from '@nestjs/common';
import { InsurancesService } from '../../core/insurances/insurances.service';
import type {
  ICreateHealthInsurance,
  ICreateLifeInsurance,
  IInsuranceFilterAdmin,
  IInsuranceStatuses,
  IUpdateHealthInsurance,
  IUpdateLifeInsurance,
} from '../../core/insurances/interfaces/insurance.interface';
import { NO_CONTENT_SUCCESS_RESPONSE } from 'src/shared/lib/responses/response';
import {
  getError,
  LIST_ERROR_CODES,
} from '../../../shared/constants/error/base.error';
import { PaginateQuery } from 'nestjs-paginate';
import { User } from '../../core/users/entities/user.entity';
import { InsuranceStatusesTypes } from '../../core/insurances/enums/insurances.enum';
import { InsuranceRepositoryService } from 'src/module/core/insurances/repository';
import { HospitalsService } from 'src/module/core/hospitals/hospitals.service';
import { Hospital } from 'src/module/core/hospitals/entities/hospital.entity';
import { InsuranceCategoriesService } from '../../core/insurance-categories/insurance-categories.service';
import { OperationSign } from '../../../shared/enums/operation.enum';
@Injectable()
export class OperatorInsurancesService {
  constructor(
    private insurancesService: InsurancesService,
    private readonly insuranceRepositoryService: InsuranceRepositoryService,
    private readonly hospitalsService: HospitalsService,
    private readonly insuranceCategoriesService: InsuranceCategoriesService,
  ) {}

  async findAll(query: PaginateQuery, filter: IInsuranceFilterAdmin) {
    return await this.insurancesService.getFilterByAdminQueryBuilder(
      query,
      filter,
    );
  }

  async findById(id: string) {
    const result = await this.insurancesService.findOneByQuery({ id });
    return result ?? getError(LIST_ERROR_CODES.SS24101, HttpStatus.NOT_FOUND);
  }

  async createHealthInsurance(body: ICreateHealthInsurance, user: User) {
    let hospitals = [];
    if (body.hospitals && body.hospitals.length > 0) {
      const dataHospitals = [...body.hospitals].map((item) => {
        if (item.id) return item;
        return {
          ...item,
          user,
        };
      }) as Hospital[];
      hospitals = await this.hospitalsService.saveHospitals(dataHospitals);
    }
    const data = {
      ...body,
      status: InsuranceStatusesTypes.Approved,
      hospitals,
    };
    await this.insuranceRepositoryService.create(data, user);
    await this.insuranceCategoriesService.updateQuantityOfCategoryByInsurance(
      body.insurance_category.id,
      OperationSign.ADD,
    );
    return NO_CONTENT_SUCCESS_RESPONSE;
  }

  async createLifeInsurance(body: ICreateLifeInsurance, user: User) {
    const data = {
      ...body,
      status: InsuranceStatusesTypes.Approved,
    };
    await this.insuranceRepositoryService.create(data, user);
    return NO_CONTENT_SUCCESS_RESPONSE;
  }

  async removeById(id: string) {
    const insurance = await this.insurancesService.findOneByQuery({ id });
    if (!insurance)
      return getError(LIST_ERROR_CODES.SS24102, HttpStatus.BAD_REQUEST);
    const result = await this.insuranceRepositoryService.deleteById(id);
    if (!!result.affected) {
      await this.insuranceCategoriesService.updateQuantityOfCategoryByInsurance(
        insurance.insurance_category.id,
        OperationSign.SUBTRACT,
      );
    }
    return !result.affected
      ? getError(LIST_ERROR_CODES.SS24102, HttpStatus.BAD_REQUEST)
      : NO_CONTENT_SUCCESS_RESPONSE;
  }

  async verifyInsuranceForAdmin(
    id: string,
    status: IInsuranceStatuses,
    reason: string,
  ) {
    const insurance = await this.insurancesService.findOneByQuery({ id });
    if (!insurance)
      return getError(LIST_ERROR_CODES.SS24101, HttpStatus.NOT_FOUND);
    if (status === insurance.status)
      return getError(LIST_ERROR_CODES.SS24108, HttpStatus.BAD_REQUEST);
    const operationType =
      insurance.status === InsuranceStatusesTypes.Approved
        ? OperationSign.SUBTRACT
        : OperationSign.ADD;

    switch (status) {
      case InsuranceStatusesTypes.Approved:
        if (insurance.hospitals_pending) {
          const updateHospitals = [...insurance.hospitals_pending].map(
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            ({ id, ...result }) => ({
              ...result,
              user: insurance.user,
            }),
          ) as Hospital[];
          const hospitals = await this.hospitalsService.saveHospitals(
            updateHospitals,
          );
          insurance.hospitals = [...insurance.hospitals, ...hospitals];
          insurance.hospitals_pending = null;
        }

        break;
      case InsuranceStatusesTypes.Rejected:
        insurance.reason = reason;
        break;
    }

    insurance.status = status;
    const result =
      this.insuranceRepositoryService.updateInsuranceStatusForAdmin(insurance);
    if (!result)
      return getError(LIST_ERROR_CODES.SS24103, HttpStatus.BAD_REQUEST);
    if (status !== InsuranceStatusesTypes.Rejected)
      await this.insuranceCategoriesService.updateQuantityOfCategoryByInsurance(
        insurance.insurance_category.id,
        operationType,
      );
    return NO_CONTENT_SUCCESS_RESPONSE;
  }

  async updateById(
    id: string,
    body: IUpdateHealthInsurance | IUpdateLifeInsurance,
  ) {
    const insuranceById = await this.insurancesService.findOneByQuery({ id });
    if (!insuranceById)
      return getError(LIST_ERROR_CODES.SS24101, HttpStatus.NOT_FOUND);
    const result = await this.insuranceRepositoryService.updateInsurance(
      insuranceById,
      body,
    );
    return !result
      ? getError(LIST_ERROR_CODES.SS24103, HttpStatus.BAD_REQUEST)
      : NO_CONTENT_SUCCESS_RESPONSE;
  }
}
