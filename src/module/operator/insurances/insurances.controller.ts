import {
  Controller,
  Get,
  Delete,
  Post,
  UseGuards,
  Param,
  Body,
  Patch,
  HttpStatus,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiTags,
  ApiBody,
  ApiQuery,
} from '@nestjs/swagger';
import { OperatorInsurancesService } from './insurances.service';
import {
  AdminVerifyInsuranceDto,
  CreateHealthInsuranceDto,
} from '../../core/insurances/dto/insurance.dto';

import { UUIDPipe } from '../../../shared/pipes/uuid.pipe';
import { AdminRoles } from 'src/shared/decorators/role.decorator';
import { UserRolesGuard } from 'src/module/core/auth/guards/local/role.guard';
import { JwtUserAuthGuard } from 'src/module/core/auth/guards/jwt-auth.guard';
import { User, UserRole } from 'src/module/core/users/entities/user.entity';
import { Paginate, PaginateQuery } from 'nestjs-paginate';
import {
  IInsuranceFilterAdmin,
  ICreateHealthInsurance,
  ICreateLifeInsurance,
  IUpdateHealthInsurance,
  IUpdateLifeInsurance,
} from '../../core/insurances/interfaces/insurance.interface';
import { InsurancesFilterAdmin } from '../../../shared/decorators/insurances/admin/filter-insurances-admin.decorator';
import {
  getError,
  LIST_ERROR_CODES,
} from '../../../shared/constants/error/base.error';
import { AuthResponse } from '../../../shared/decorators/response-auth/auth.decorator';
import { InsuranceCategoryNames } from '../../core/insurance-categories/enums/insurance-category.enum';
import {
  CREATED_AT_QUERY,
  LIMIT_QUERY,
  PAGE_QUERY,
  ROLE_USER_QUERY,
} from '../../../shared/constants/swagger/constant';
import {
  INSURANCE_TYPE_QUERY,
  NAME_QUERY,
  STATUS_INSURANCES_QUERY,
  TOTAL_SUM_INSURED_QUERY,
} from '../../../shared/constants/swagger/insurances/insurances.constant';

@ApiTags('Insurances')
@Controller('insurances')
export class OperatorInsurancesController {
  constructor(private insurancesService: OperatorInsurancesService) {}

  @Get()
  @UseGuards(JwtUserAuthGuard, UserRolesGuard)
  @AdminRoles(UserRole.ADMIN)
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Get all insurances' })
  @ApiQuery(PAGE_QUERY)
  /** Trạng thái */
  @ApiQuery(STATUS_INSURANCES_QUERY)
  @ApiQuery(LIMIT_QUERY)
  /** Khoảng giá */
  @ApiQuery(TOTAL_SUM_INSURED_QUERY)
  /** Tên */
  @ApiQuery(NAME_QUERY)
  /**Ngày tạo */
  @ApiQuery(CREATED_AT_QUERY)
  /** Loại bảo hiểm */
  @ApiQuery(INSURANCE_TYPE_QUERY)
  /** Role User */
  @ApiQuery(ROLE_USER_QUERY)
  /** Insurance Category Type */
  @ApiQuery(INSURANCE_TYPE_QUERY)
  async findAll(
    @Paginate() query: PaginateQuery,
    @InsurancesFilterAdmin() filter: IInsuranceFilterAdmin,
  ) {
    try {
      if (!filter)
        return getError(LIST_ERROR_CODES.SS24429, HttpStatus.BAD_REQUEST);
      return await this.insurancesService.findAll(query, filter);
    } catch (error) {
      console.log(
        '🚀 ~ file: insurances.controller.ts:35 ~ OperatorInsurancesController ~ findAll ~ error:',
        error,
      );
      return getError(
        LIST_ERROR_CODES.SS24001,
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  @Get(':id')
  @UseGuards(JwtUserAuthGuard, UserRolesGuard)
  @AdminRoles(UserRole.ADMIN)
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Get insurance' })
  async findOne(@Param('id', new UUIDPipe()) id: string) {
    try {
      return await this.insurancesService.findById(id);
    } catch (error) {
      console.log(
        '🚀 ~ file: insurances.controller.ts:46 ~ OperatorInsurancesController ~ findOne ~ error:',
        error,
      );
      return getError(
        LIST_ERROR_CODES.SS24001,
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  @Post()
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Create insurance' })
  @ApiBody({ type: CreateHealthInsuranceDto })
  async create(
    @Body() body: ICreateHealthInsurance | ICreateLifeInsurance,
    @AuthResponse() user: User,
  ) {
    try {
      if (!body.company_id) return getError(LIST_ERROR_CODES.SS24108);
      switch (body.insurance_category.label) {
        case InsuranceCategoryNames.Life:
          return await this.insurancesService.createLifeInsurance(
            body as ICreateLifeInsurance,
            user,
          );
        case InsuranceCategoryNames.Health:
          return await this.insurancesService.createHealthInsurance(
            body as ICreateHealthInsurance,
            user,
          );
      }
    } catch (error) {
      console.log(
        '🚀 ~ file: insurances.controller.ts:58 ~ OperatorInsurancesController ~ create ~ error:',
        error,
      );
      return getError(
        LIST_ERROR_CODES.SS24001,
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  @Delete(':id')
  @ApiOperation({ summary: 'Remove insurance' })
  @UseGuards(JwtUserAuthGuard, UserRolesGuard)
  @AdminRoles(UserRole.ADMIN)
  @ApiBearerAuth()
  async remove(@Param('id', new UUIDPipe()) id: string) {
    try {
      return await this.insurancesService.removeById(id);
    } catch (error) {
      return getError(
        LIST_ERROR_CODES.SS24001,
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  @Patch(':id/verify')
  @UseGuards(JwtUserAuthGuard, UserRolesGuard)
  @AdminRoles(UserRole.ADMIN)
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Verify insurance for admin' })
  async verifyInsurance(
    @Param('id', new UUIDPipe()) id: string,
    @Body() { status, reason }: AdminVerifyInsuranceDto,
  ) {
    try {
      return await this.insurancesService.verifyInsuranceForAdmin(
        id,
        status,
        reason,
      );
    } catch (error) {
      console.log(
        '🚀 ~ file: insurances.controller.ts:165 ~ OperatorInsurancesController ~ error:',
        error,
      );
      return getError(
        LIST_ERROR_CODES.SS24001,
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  @Patch(':id')
  @ApiOperation({ summary: 'Update insurance' })
  async update(
    @Param('id', new UUIDPipe()) id: string,
    @Body() body: IUpdateHealthInsurance | IUpdateLifeInsurance,
  ) {
    try {
      return await this.insurancesService.updateById(id, body);
    } catch (error) {
      console.log(
        '🚀 ~ file: insurances.controller.ts:82 ~ OperatorInsurancesController ~ error:',
        error,
      );
      return getError(
        LIST_ERROR_CODES.SS24001,
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }
}
