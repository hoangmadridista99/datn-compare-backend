import { Module } from '@nestjs/common';
import { InsurancesModule } from '../../core/insurances/insurances.module';

@Module({
  imports: [InsurancesModule],
  providers: [InsurancesModule],
  exports: [InsurancesModule],
})
export class OperatorInsurancesModule {}
