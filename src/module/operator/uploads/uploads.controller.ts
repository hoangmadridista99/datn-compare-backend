import {
  Controller,
  Post,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { OperatorUploadsService } from './uploads.service';
import {
  ApiBearerAuth,
  ApiBody,
  ApiConsumes,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { JwtUserAuthGuard } from 'src/module/core/auth/guards/jwt-auth.guard';
import { UserRolesGuard } from 'src/module/core/auth/guards/local/role.guard';
import { UserRole } from 'src/module/core/users/entities/user.entity';
import { AdminRoles } from 'src/shared/decorators/role.decorator';
import { BaseUploadFileDto } from 'src/shared/dto/file.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { FilePdfValidationPipe } from 'src/shared/pipes/file-pdf.pipe';

@Controller('uploads')
@ApiTags('Uploads')
@ApiBearerAuth()
@UseGuards(JwtUserAuthGuard, UserRolesGuard)
@AdminRoles(UserRole.ADMIN)
export class OperatorUploadsController {
  constructor(
    private readonly operatorUploadsService: OperatorUploadsService,
  ) {}

  @Post('/insurance/pdf')
  @ApiOperation({ summary: 'Upload Insurance Pdf' })
  @ApiConsumes('multipart/form-data')
  @ApiBody({ type: BaseUploadFileDto })
  @ApiResponse({
    status: 204,
    schema: {
      type: 'string',
    },
  })
  @UseInterceptors(FileInterceptor('file'))
  async uploadInsurancePdf(
    @UploadedFile(new FilePdfValidationPipe()) file: Express.Multer.File,
  ) {
    try {
      return this.operatorUploadsService.uploadInsurancePdf(file);
    } catch (error) {
      console.log(
        '🚀 ~ file: upload.controller.ts:27 ~ OperatorUploadController ~ uploadInsurancePdf:',
        error,
      );
      throw error;
    }
  }
}
