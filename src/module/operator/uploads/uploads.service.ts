import { Injectable } from '@nestjs/common';
import { MinioTypes } from 'src/module/core/minio/enums/minio.enum';
import { MinioService } from 'src/module/core/minio/minio.service';
import {
  LIST_ERROR_CODES,
  getError,
} from 'src/shared/constants/error/base.error';
import {
  getFileKey,
  getFileName,
  getFileUrl,
} from 'src/shared/helper/system.helper';

@Injectable()
export class OperatorUploadsService {
  constructor(private readonly minioService: MinioService) {}

  async uploadInsurancePdf(file: Express.Multer.File) {
    try {
      const filename = getFileName(file.originalname);
      const fileKey = getFileKey(MinioTypes.INSURANCE_PDF, filename);
      const isUploadFile = await this.minioService.uploadFile(fileKey, file);
      if (!isUploadFile) return getError(LIST_ERROR_CODES.SS24105, 400);

      return getFileUrl(MinioTypes.INSURANCE_PDF, filename);
    } catch (error) {
      console.log(
        '🚀 ~ file: upload.service.ts:12 ~ OperatorUploadService ~ uploadPdf:',
        error,
      );

      return getError(LIST_ERROR_CODES.SS24001, error);
    }
  }
}
