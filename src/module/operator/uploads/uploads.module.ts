import { Module } from '@nestjs/common';
import { MinioModule } from 'src/module/core/minio/minio.module';
import { OperatorUploadsService } from './uploads.service';

@Module({
  imports: [MinioModule],
  providers: [OperatorUploadsService],
  exports: [OperatorUploadsService],
})
export class OperatorUploadsModule {}
