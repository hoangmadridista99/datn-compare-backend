import { HttpStatus, Injectable } from '@nestjs/common';
import { InsuranceCategoriesService } from '../../core/insurance-categories/insurance-categories.service';
import {
  ICreateInsuranceCategory,
  IUpdateInsuranceCategory,
} from '../../core/insurance-categories/interfaces/insurance-categories.interface';
import {
  LIST_ERROR_CODES,
  getError,
} from '../../../shared/constants/error/base.error';
import { NO_CONTENT_SUCCESS_RESPONSE } from '../../../shared/lib/responses/response';

@Injectable()
export class OperatorInsuranceCategoriesService {
  constructor(
    private readonly insuranceCategoriesService: InsuranceCategoriesService,
  ) {}

  async createCategory(body: ICreateInsuranceCategory) {
    return this.insuranceCategoriesService.createInsuranceCategory(body);
  }

  async getListCategories() {
    const isAdmin = true;
    return this.insuranceCategoriesService.getListCategories(isAdmin);
  }

  async updateCategory(id: string, body: IUpdateInsuranceCategory) {
    const result =
      await this.insuranceCategoriesService.updateInsuranceCategory(id, body);
    return !result
      ? getError(LIST_ERROR_CODES.SS24101, HttpStatus.BAD_REQUEST)
      : NO_CONTENT_SUCCESS_RESPONSE;
  }

  async deleteCategory(id: string) {
    const result =
      await this.insuranceCategoriesService.deleteInsuranceCategory(id);
    return !result
      ? getError(LIST_ERROR_CODES.SS24102, HttpStatus.BAD_REQUEST)
      : NO_CONTENT_SUCCESS_RESPONSE;
  }

  async getOneById(id: string) {
    return this.insuranceCategoriesService.getOneCategoryById(id);
  }
}
