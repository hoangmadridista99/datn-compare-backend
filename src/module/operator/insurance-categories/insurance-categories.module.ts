import { Module } from '@nestjs/common';
import { InsuranceCategoriesModule } from '../../core/insurance-categories/insurance-categories.module';

@Module({
  imports: [InsuranceCategoriesModule],
  providers: [InsuranceCategoriesModule],
  exports: [InsuranceCategoriesModule],
})
export class OperatorInsuranceCategoriesModule {}
