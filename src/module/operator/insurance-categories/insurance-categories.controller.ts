import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  UseGuards,
} from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { AdminRoles } from '../../../shared/decorators/role.decorator';
import { JwtUserAuthGuard } from '../../core/auth/guards/jwt-auth.guard';
import { UserRolesGuard } from '../../core/auth/guards/local/role.guard';
import { UserRole } from '../../core/users/entities/user.entity';
import { OperatorInsuranceCategoriesService } from './insurance-categories.service';
import {
  CreateInsuranceCategoryDto,
  UpdateInsuranceCategoryDto,
} from '../../core/insurance-categories/dto/insurance-categories.dto';
import {
  LIST_ERROR_CODES,
  getError,
} from '../../../shared/constants/error/base.error';
import { UUIDPipe } from '../../../shared/pipes/uuid.pipe';

@ApiTags('Insurance-Categories')
@Controller('insurance-categories')
@UseGuards(JwtUserAuthGuard, UserRolesGuard)
@ApiBearerAuth()
@AdminRoles(UserRole.ADMIN)
export class OperatorInsuranceCategoriesController {
  constructor(
    private readonly operatorInsuranceCategoriesService: OperatorInsuranceCategoriesService,
  ) {}

  @Post()
  @ApiOperation({ summary: 'Create new category of insurances' })
  async createNewCategory(@Body() body: CreateInsuranceCategoryDto) {
    try {
      return await this.operatorInsuranceCategoriesService.createCategory(body);
    } catch (error) {
      console.log(
        '🚀 ~ file: insurance-categories.controller.ts:26 ~ OperatorInsuranceCategoriesController ~ createNewCategory ~ error:',
        error,
      );
      return getError(LIST_ERROR_CODES.SS24001);
    }
  }

  @Get(':id')
  @ApiOperation({ summary: 'Get detail category' })
  async getDetailCategory(@Param('id', new UUIDPipe()) id: string) {
    try {
      return await this.operatorInsuranceCategoriesService.getOneById(id);
    } catch (error) {
      console.log(
        '🚀 ~ file: insurance-categories.controller.ts:57 ~ OperatorInsuranceCategoriesController ~ getDetailCategory ~ error:',
        error,
      );
      return getError(LIST_ERROR_CODES.SS24001);
    }
  }

  @Get()
  @ApiOperation({ summary: 'Get list insurances for admin' })
  async getListCategories() {
    try {
      return await this.operatorInsuranceCategoriesService.getListCategories();
    } catch (error) {
      console.log(
        '🚀 ~ file: insurance-categories.controller.ts:44 ~ OperatorInsuranceCategoriesController ~ getListCategories ~ error:',
        error,
      );
      return getError(LIST_ERROR_CODES.SS24001);
    }
  }

  @Patch(':id')
  @ApiOperation({ summary: 'Update category for admin' })
  async updateCategory(
    @Param('id', new UUIDPipe()) id: string,
    @Body() body: UpdateInsuranceCategoryDto,
  ) {
    try {
      return this.operatorInsuranceCategoriesService.updateCategory(id, body);
    } catch (error) {
      console.log(
        '🚀 ~ file: insurance-categories.controller.ts:65 ~ OperatorInsuranceCategoriesController ~ error:',
        error,
      );
      return getError(LIST_ERROR_CODES.SS24001);
    }
  }

  @Delete(':id')
  @ApiOperation({ summary: 'Delete a category for admin' })
  async deleteCategory(@Param('id', new UUIDPipe()) id: string) {
    try {
      return this.operatorInsuranceCategoriesService.deleteCategory(id);
    } catch (error) {
      console.log(
        '🚀 ~ file: insurance-categories.controller.ts:91 ~ OperatorInsuranceCategoriesController ~ deleteCategory ~ error:',
        error,
      );
      return getError(LIST_ERROR_CODES.SS24001);
    }
  }
}
