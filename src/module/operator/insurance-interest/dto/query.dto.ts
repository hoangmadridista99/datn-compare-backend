import { IsEnum, IsOptional } from 'class-validator';
import { SortRatingTypes } from 'src/module/core/ratings/enums/ratings.enum';
import { ISortByType } from 'src/module/core/ratings/interfaces/ratings.interface';

export class QueryOperatorDto {
  @IsOptional()
  @IsEnum(SortRatingTypes)
  sort_created_at?: ISortByType;
}
