import {
  Controller,
  Get,
  HttpStatus,
  Param,
  Query,
  UseGuards,
} from '@nestjs/common';
import { OperatorInsuranceInterestService } from './insurance-interest.service';
import { JwtUserAuthGuard } from 'src/module/core/auth/guards/jwt-auth.guard';
import { UserRolesGuard } from 'src/module/core/auth/guards/local/role.guard';
import { AdminRoles } from 'src/shared/decorators/role.decorator';
import { UserRole } from 'src/module/core/users/entities/user.entity';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiParam,
  ApiQuery,
  ApiTags,
} from '@nestjs/swagger';
import { Paginate, PaginateQuery } from 'nestjs-paginate';
import {
  LIST_ERROR_CODES,
  getError,
} from 'src/shared/constants/error/base.error';
import { UUIDPipe } from 'src/shared/pipes/uuid.pipe';
import { InsuranceInterestFilterByOperatorDecorator } from 'src/shared/decorators/insurances/admin/filter-insurance-interest.decorator';
import { IUserInsurancesQueryFilter } from 'src/module/core/user-insurances/interfaces/user-insurance.interface';
import {
  LIMIT_QUERY,
  PAGE_QUERY,
} from '../../../shared/constants/swagger/constant';
import { InsuranceSort } from 'src/module/core/insurances/enums/insurances.enum';
import { QueryOperatorDto } from './dto/query.dto';
import { SortRatingTypes } from 'src/module/core/ratings/enums/ratings.enum';

@Controller('insurance-interest')
@ApiTags('Insurance-Interest')
@UseGuards(JwtUserAuthGuard, UserRolesGuard)
@AdminRoles(UserRole.ADMIN)
@ApiBearerAuth()
export class OperatorInsuranceInterestController {
  constructor(
    private readonly operatorInsuranceInterestService: OperatorInsuranceInterestService,
  ) {}

  @Get()
  @ApiOperation({ summary: 'Get List Insurance Interest' })
  @ApiQuery(PAGE_QUERY)
  @ApiQuery(LIMIT_QUERY)
  @ApiQuery({
    name: 'insurance_name',
    required: false,
  })
  @ApiQuery({
    name: 'created_at',
    required: false,
  })
  @ApiQuery({
    name: 'insurance_category_label',
    required: false,
  })
  @ApiQuery({
    name: 'total_interest',
    required: false,
    enum: InsuranceSort,
  })
  async findAll(
    @Paginate() query: PaginateQuery,
    @InsuranceInterestFilterByOperatorDecorator()
    filter: IUserInsurancesQueryFilter,
  ) {
    try {
      if (!filter)
        return getError(LIST_ERROR_CODES.SS24429, HttpStatus.BAD_REQUEST);
      return await this.operatorInsuranceInterestService.findAll(query, filter);
    } catch (error) {
      console.log(
        '🚀 ~ file: insurance-interest.controller.ts:27 ~ OperatorInsuranceInterestController ~ findAll ~ error:',
        error,
      );
      return getError(LIST_ERROR_CODES.SS24001);
    }
  }

  @Get(':insuranceId')
  @ApiOperation({ summary: 'Get List User Interest By Insurance Id' })
  @ApiQuery({
    name: 'page',
    required: true,
    example: 1,
  })
  @ApiQuery({
    name: 'limit',
    required: true,
    example: 10,
  })
  @ApiQuery({
    name: 'sort_created_at',
    required: false,
    enum: SortRatingTypes,
    example: SortRatingTypes.Asc,
  })
  @ApiParam({ name: 'insuranceId' })
  async getUsersByInsuranceId(
    @Paginate() paginateQuery: PaginateQuery,
    @Param('insuranceId', new UUIDPipe()) insuranceId: string,
    @Query() query: QueryOperatorDto,
  ) {
    try {
      return this.operatorInsuranceInterestService.getUsersByInsurancesId(
        paginateQuery,
        insuranceId,
        query,
      );
    } catch (error) {
      console.log(
        '🚀 ~ file: insurance-interest.controller.ts:91 ~ OperatorInsuranceInterestController ~ error:',
        error,
      );
      return getError(LIST_ERROR_CODES.SS24001);
    }
  }
}
