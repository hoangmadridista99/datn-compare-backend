import { HttpStatus, Injectable } from '@nestjs/common';
import { PaginateQuery, paginate } from 'nestjs-paginate';
import { InsurancesService } from 'src/module/core/insurances/insurances.service';
import { IUserInsurancesQueryFilter } from 'src/module/core/user-insurances/interfaces/user-insurance.interface';
import { UserInsurancesService } from 'src/module/core/user-insurances/user-insurances.service';
import {
  LIST_ERROR_CODES,
  getError,
} from 'src/shared/constants/error/base.error';
import { QueryOperatorDto } from './dto/query.dto';

@Injectable()
export class OperatorInsuranceInterestService {
  constructor(
    private readonly insurancesService: InsurancesService,
    private readonly userInsurancesService: UserInsurancesService,
  ) {}

  async findAll(query: PaginateQuery, filter: IUserInsurancesQueryFilter) {
    try {
      const queryBuilder =
        this.insurancesService.getInterestedInsurances(filter);
      if (!queryBuilder)
        return getError(
          LIST_ERROR_CODES.SS24001,
          HttpStatus.INTERNAL_SERVER_ERROR,
        );

      const result = await paginate(query, queryBuilder, {
        maxLimit: query.limit,
        defaultLimit: query.page,
        sortableColumns: ['total_interest'],
        defaultSortBy: [['total_interest', filter.total_interest ?? 'DESC']],
      });

      const data = result.data.map(({ user_insurances, ...result }) => {
        let user_latest = user_insurances[0];
        if (user_insurances.length > 1) {
          user_latest = user_insurances.sort(
            (a, b) =>
              Date.parse(b.created_at.toString()) -
              Date.parse(a.created_at.toString()),
          )[0];
        }
        return {
          ...result,
          user_latest,
        };
      });

      return {
        ...result,
        data,
      };
    } catch (error) {
      console.log(
        '🚀 ~ file: insurance-interest.service.ts:13 ~ OperatorInsuranceInterestService ~ findAll ~ error:',
        error,
      );

      throw getError(LIST_ERROR_CODES.SS24001);
    }
  }

  async getUsersByInsurancesId(
    paginateQuery: PaginateQuery,
    insuranceId: string,
    query: QueryOperatorDto,
  ) {
    try {
      const result = await this.userInsurancesService.getListByInsuranceId(
        paginateQuery,
        insuranceId,
        query,
      );
      return result ?? getError(LIST_ERROR_CODES.SS24101);
    } catch (error) {
      console.log(
        '🚀 ~ file: insurance-interest.service.ts:44 ~ OperatorInsuranceInterestService ~ getUsersByInsurancesId ~ error:',
        error,
      );

      return getError(LIST_ERROR_CODES.SS24001);
    }
  }
}
