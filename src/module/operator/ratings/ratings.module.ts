import { Module } from '@nestjs/common';
import { RatingsModule } from 'src/module/core/ratings/ratings.module';

@Module({
  imports: [RatingsModule],
  providers: [RatingsModule],
  exports: [RatingsModule],
})
export class OperatorRatingsModule {}
