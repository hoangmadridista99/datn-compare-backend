import {
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  Param,
  Patch,
  Query,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiQuery,
  ApiTags,
  ApiOperation,
} from '@nestjs/swagger';
import { OperatorRatingsService } from './ratings.service';
import { JwtUserAuthGuard } from 'src/module/core/auth/guards/jwt-auth.guard';
import { UserRolesGuard } from 'src/module/core/auth/guards/local/role.guard';
import { UserRole } from 'src/module/core/users/entities/user.entity';
import { AdminRoles } from 'src/shared/decorators/role.decorator';
import { Paginate, PaginateQuery } from 'nestjs-paginate';
import {
  LIST_ERROR_CODES,
  getError,
} from 'src/shared/constants/error/base.error';
import { UUIDPipe } from 'src/shared/pipes/uuid.pipe';
import { VerifyRatingForAdminDto } from 'src/module/core/ratings/dto/ratings.dto';

@Controller('ratings')
@ApiTags('Ratings')
@UseGuards(JwtUserAuthGuard, UserRolesGuard)
@ApiBearerAuth()
@AdminRoles(UserRole.ADMIN)
export class OperatorRatingsController {
  constructor(
    private readonly operatorRatingsService: OperatorRatingsService,
  ) {}

  @Get('unverified')
  @ApiQuery({
    name: 'page',
    example: 1,
  })
  @ApiQuery({
    example: 10,
    name: 'limit',
  })
  @ApiQuery({
    name: 'insurance_category_label',
    required: false,
  })
  @ApiQuery({
    name: 'insurance_name',
    required: false,
  })
  @ApiOperation({ summary: 'Get all ratings unverified' })
  async findAll(
    @Paginate() query: PaginateQuery,
    @Query('insurance_category_label') insurance_category_label?: string,
    @Query('insurance_name') insurance_name?: string,
  ) {
    try {
      return this.operatorRatingsService.findAll(
        query,
        insurance_category_label,
        insurance_name,
      );
    } catch (error) {
      console.log(
        '🚀 ~ file: ratings.controller.ts:29 ~ OperatorRatingsController ~ findAll:',
        error,
      );

      return getError(LIST_ERROR_CODES.SS24001);
    }
  }

  @Get('/insurance/:id')
  @ApiQuery({
    name: 'page',
    example: 1,
  })
  @ApiQuery({
    example: 10,
    name: 'limit',
  })
  @ApiOperation({ summary: 'Get all rating by insurance' })
  async findAllByInsurance(
    @Paginate() query: PaginateQuery,
    @Param('id', new UUIDPipe()) id: string,
  ) {
    try {
      return this.operatorRatingsService.findAllByInsurance(query, id);
    } catch (error) {
      console.log(
        '🚀 ~ file: ratings.controller.ts:73 ~ OperatorRatingsController ~ findAllByInsurance:',
        error,
      );

      return getError(LIST_ERROR_CODES.SS24001);
    }
  }

  @Patch(':id/verify')
  @ApiOperation({ summary: 'Update rating' })
  async updateRating(
    @Param('id', new UUIDPipe()) id: string,
    @Body()
    { is_verified_by_admin, is_hide, ...validData }: VerifyRatingForAdminDto,
  ) {
    if (Object.values(validData).length > 0)
      return getError(LIST_ERROR_CODES.SS24108, HttpStatus.BAD_REQUEST);
    try {
      return await this.operatorRatingsService.verifyRating(id, {
        is_verified_by_admin,
        is_hide,
      });
    } catch (error) {
      console.log(
        '🚀 ~ file: ratings.controller.ts:91 ~ OperatorRatingsController ~ updateRating:',
        error,
      );

      return getError(LIST_ERROR_CODES.SS24001);
    }
  }

  @Delete(':id')
  @ApiOperation({ summary: 'Delete rating' })
  async deleteRating(@Param('id', new UUIDPipe()) id: string) {
    try {
      return await this.operatorRatingsService.deleteRating(id);
    } catch (error) {
      console.log(
        '🚀 ~ file: ratings.controller.ts:108 ~ OperatorRatingsController ~ deleteRating:',
        error,
      );

      return getError(LIST_ERROR_CODES.SS24001);
    }
  }
}
