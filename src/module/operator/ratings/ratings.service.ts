import { HttpStatus, Injectable } from '@nestjs/common';
import { PaginateQuery } from 'nestjs-paginate';
import { IVerifyRatingByAdmin } from 'src/module/core/ratings/interfaces/ratings.interface';
import { RatingsService } from 'src/module/core/ratings/ratings.service';
import {
  LIST_ERROR_CODES,
  getError,
} from 'src/shared/constants/error/base.error';
import { NO_CONTENT_SUCCESS_RESPONSE } from 'src/shared/lib/responses/response';

@Injectable()
export class OperatorRatingsService {
  constructor(private readonly ratingsService: RatingsService) {}

  async findAll(
    query: PaginateQuery,
    insurance_category?: string,
    insurance_name?: string,
  ) {
    const result = await this.ratingsService.findAllByOperator(
      query,
      insurance_category,
      insurance_name,
    );
    return result ?? getError(LIST_ERROR_CODES.SS24001);
  }

  async findAllByInsurance(query: PaginateQuery, id: string) {
    const result = await this.ratingsService.findAllByInsurance(query, id);
    return result ?? getError(LIST_ERROR_CODES.SS24001);
  }

  async verifyRating(id: string, data: IVerifyRatingByAdmin) {
    const checkExistRating = await this.ratingsService.findById(id);
    if (!checkExistRating)
      return getError(LIST_ERROR_CODES.SS24101, HttpStatus.NOT_FOUND);

    await this.ratingsService.updateVerifiedByAdmin(id, data);
    const result = await this.ratingsService.handleCalculateAfterVerify(
      checkExistRating.insurance.id,
    );
    return !result
      ? getError(LIST_ERROR_CODES.SS24605, HttpStatus.BAD_REQUEST)
      : NO_CONTENT_SUCCESS_RESPONSE;
  }

  async deleteRating(id: string) {
    const result = await this.ratingsService.deleteRating(id);
    return result
      ? NO_CONTENT_SUCCESS_RESPONSE
      : getError(LIST_ERROR_CODES.SS24604);
  }
}
