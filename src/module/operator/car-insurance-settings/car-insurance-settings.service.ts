import { HttpStatus, Injectable } from '@nestjs/common';
import { CarInsuranceSettingsService } from '../../core/car-insurance-settings/car-insurance-settings.service';
import { NO_CONTENT_SUCCESS_RESPONSE } from '../../../shared/lib/responses/response';
import { User } from '../../core/users/entities/user.entity';
import {
  LIST_ERROR_CODES,
  getError,
} from '../../../shared/constants/error/base.error';
import { CarInsurancesService } from '../../core/car-insurances/car-insurances.service';
import { ICreateMandatorySettingDto } from '../../core/car-insurance-settings/interfaces/mandatory-setting.interface';

@Injectable()
export class OperatorCarInsuranceSettingsService {
  constructor(
    private readonly carInsuranceSettingsService: CarInsuranceSettingsService,
    private readonly carInsurancesService: CarInsurancesService,
  ) {}

  /** Mandatory */
  async createNewMandatorySetting(
    body: ICreateMandatorySettingDto,
    user: User,
  ) {
    await this.carInsuranceSettingsService.createNewMandatorySetting(
      body,
      user,
    );

    return NO_CONTENT_SUCCESS_RESPONSE;
  }

  async getMandatorySettingDetails() {
    return (
      (
        await this.carInsuranceSettingsService.getMandatorySettingDetails()
      )[0] ?? getError(LIST_ERROR_CODES.SS24101, HttpStatus.NOT_FOUND)
    );
  }
}
