import { Module } from '@nestjs/common';
import { CarInsuranceSettingsModule } from '../../core/car-insurance-settings/car-insurance.settings.module';

@Module({
  imports: [CarInsuranceSettingsModule],
  providers: [CarInsuranceSettingsModule],
  exports: [CarInsuranceSettingsModule],
})
export class OperatorCarInsuranceSettingsModule {}
