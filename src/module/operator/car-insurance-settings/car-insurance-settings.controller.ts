import { Body, Controller, Get, Post, UseGuards } from '@nestjs/common';
import { ApiTags, ApiBearerAuth, ApiOperation } from '@nestjs/swagger';
import { AdminRoles } from '../../../shared/decorators/role.decorator';
import { JwtUserAuthGuard } from '../../core/auth/guards/jwt-auth.guard';
import { UserRolesGuard } from '../../core/auth/guards/local/role.guard';
import { User, UserRole } from '../../core/users/entities/user.entity';
import { OperatorCarInsuranceSettingsService } from './car-insurance-settings.service';
import {
  LIST_ERROR_CODES,
  getError,
} from '../../../shared/constants/error/base.error';
import { AuthResponse } from '../../../shared/decorators/response-auth/auth.decorator';
import { CreateMandatorySettingDto } from '../../core/car-insurance-settings/dto/mandatory-setting.dto';

@ApiTags('Car-Insurance-Settings')
@Controller('car-insurance-settings')
@UseGuards(JwtUserAuthGuard, UserRolesGuard)
@AdminRoles(UserRole.ADMIN)
@ApiBearerAuth()
export class OperatorCarInsuranceSettingsController {
  constructor(
    private readonly operatorCarInsuranceSettingsService: OperatorCarInsuranceSettingsService,
  ) {}
  /** Mandatory */
  @Post('mandatory')
  @ApiOperation({ summary: 'Create mandatory setting for car insurance' })
  async createMandatorySetting(
    @Body() body: CreateMandatorySettingDto,
    @AuthResponse() user: User,
  ) {
    try {
      return await this.operatorCarInsuranceSettingsService.createNewMandatorySetting(
        body,
        user,
      );
    } catch (error) {
      console.log(
        '🚀 ~ file: car-insurance-settings.controller.ts:47 ~ OperatorCarInsuranceSettingsController ~ error:',
        error,
      );
      return getError(LIST_ERROR_CODES.SS24001);
    }
  }

  @Get('mandatory/details')
  @ApiOperation({ summary: 'Get mandatory setting details' })
  async getMandatorySettingDetails() {
    try {
      return await this.operatorCarInsuranceSettingsService.getMandatorySettingDetails();
    } catch (error) {
      console.log(
        '🚀 ~ file: car-insurance-settings.controller.ts:64 ~ OperatorCarInsuranceSettingsController ~ getMandatorySettingDetails ~ error:',
        error,
      );
      return getError(LIST_ERROR_CODES.SS24001);
    }
  }
}
