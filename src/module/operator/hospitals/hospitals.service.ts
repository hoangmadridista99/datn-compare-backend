import { HttpStatus, Injectable } from '@nestjs/common';
import { HospitalsService } from 'src/module/core/hospitals/hospitals.service';
import {
  ICreateHospital,
  IHospital,
  IUpdateHospital,
} from 'src/module/core/hospitals/interfaces/hospital.interface';
import { User } from 'src/module/core/users/entities/user.entity';
import {
  LIST_ERROR_CODES,
  getError,
} from 'src/shared/constants/error/base.error';
import { NO_CONTENT_SUCCESS_RESPONSE } from 'src/shared/lib/responses/response';

@Injectable()
export class OperatorHospitalsService {
  constructor(private readonly hospitalsService: HospitalsService) {}

  async findAll(name?: string): Promise<IHospital[]> {
    return this.hospitalsService.findAll(name);
  }

  async createHospital(user: User, body: ICreateHospital) {
    const result = await this.hospitalsService.createHospital(user, body);
    return result ?? getError(LIST_ERROR_CODES.SS24104, HttpStatus.BAD_REQUEST);
  }

  async updateHospital(id: string, body: IUpdateHospital) {
    const result = await this.hospitalsService.updateHospital(id, body);
    return !result
      ? getError(LIST_ERROR_CODES.SS24103, HttpStatus.BAD_REQUEST)
      : NO_CONTENT_SUCCESS_RESPONSE;
  }

  async removeHospital(id: string) {
    const result = await this.hospitalsService.removeHospital(id);
    return !result
      ? getError(LIST_ERROR_CODES.SS24102, HttpStatus.BAD_REQUEST)
      : NO_CONTENT_SUCCESS_RESPONSE;
  }
}
