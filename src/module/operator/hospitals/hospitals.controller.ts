import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiQuery,
  ApiTags,
} from '@nestjs/swagger';
import { AdminRoles } from '../../../shared/decorators/role.decorator';
import { JwtUserAuthGuard } from '../../core/auth/guards/jwt-auth.guard';
import { UserRolesGuard } from '../../core/auth/guards/local/role.guard';
import { User, UserRole } from '../../core/users/entities/user.entity';
import { OperatorHospitalsService } from './hospitals.service';
import {
  CreateHospitalDto,
  UpdateHospitalDto,
} from 'src/module/core/hospitals/dto/hospital.dto';
import {
  LIST_ERROR_CODES,
  getError,
} from 'src/shared/constants/error/base.error';
import { UUIDPipe } from 'src/shared/pipes/uuid.pipe';
import { AuthResponse } from 'src/shared/decorators/response-auth/auth.decorator';

@ApiTags('Hospitals')
@Controller('hospitals')
@UseGuards(JwtUserAuthGuard, UserRolesGuard)
@AdminRoles(UserRole.ADMIN)
@ApiBearerAuth()
export class OperatorHospitalsController {
  constructor(
    private readonly operatorHospitalsService: OperatorHospitalsService,
  ) {}

  @Get()
  @ApiQuery({
    name: 'name',
    required: false,
  })
  @ApiOperation({ summary: 'Get hospitals' })
  async findAll(@Query('name') name?: string) {
    try {
      return await this.operatorHospitalsService.findAll(name);
    } catch (error) {
      console.log(
        '🚀 ~ file: hospitals.controller.ts:39 ~ OperatorInsuranceObjectivesController ~ findAll ~ error:',
        error,
      );
      return getError(LIST_ERROR_CODES.SS24001);
    }
  }

  @Post()
  @ApiOperation({ summary: 'Create Hospital' })
  async createHospital(
    @Body() body: CreateHospitalDto,
    @AuthResponse() user: User,
  ) {
    try {
      return await this.operatorHospitalsService.createHospital(user, body);
    } catch (error) {
      console.log(
        '🚀 ~ file: hospitals.controller.ts:57 ~ OperatorInsuranceObjectivesController ~ createHospital ~ error:',
        error,
      );
      return getError(LIST_ERROR_CODES.SS24001);
    }
  }

  @Patch(':id')
  @ApiOperation({ summary: 'Update Hospital' })
  async updateHospital(
    @Param('id', new UUIDPipe()) id: string,
    @Body() body: UpdateHospitalDto,
  ) {
    try {
      return await this.operatorHospitalsService.updateHospital(id, body);
    } catch (error) {
      console.log(
        '🚀 ~ file: hospitals.controller.ts:75 ~ OperatorInsuranceObjectivesController ~ error:',
        error,
      );
      return getError(LIST_ERROR_CODES.SS24001);
    }
  }

  @Delete(':id')
  @ApiOperation({ summary: 'Remove Hospital' })
  async removeHospital(@Param('id', new UUIDPipe()) id: string) {
    try {
      return await this.operatorHospitalsService.removeHospital(id);
    } catch (error) {
      console.log(
        '🚀 ~ file: hospitals.controller.ts:89 ~ OperatorInsuranceObjectivesController ~ removeHospital ~ error:',
        error,
      );
      return getError(LIST_ERROR_CODES.SS24001);
    }
  }
}
