import { Controller, Post, UseGuards } from '@nestjs/common';
import { ApiBody, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { AuthResponse } from '../../../shared/decorators/response-auth/auth.decorator';
import { LoginAdminDto } from '../../core/auth/dto/auth.dto';
import { AdminAuthGuard } from '../../core/auth/guards/admin/admin.guard';
import { ResponseAuthUser } from '../../core/auth/models/auth.model';
import { User } from '../../core/users/entities/user.entity';
import { OperatorAuthService } from './auth.service';

@ApiTags('Authentication')
@Controller('auth')
export class OperatorAuthController {
  constructor(private readonly operatorAuthService: OperatorAuthService) {}
  @Post('/login')
  @UseGuards(AdminAuthGuard)
  @ApiBody({ type: LoginAdminDto })
  @ApiOperation({ summary: 'Login for admin' })
  @ApiResponse({ status: 401, description: 'Unauthorized.' })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  @ApiResponse({ status: 200, description: 'OK', type: ResponseAuthUser })
  async loginForAdmin(@AuthResponse() user: User) {
    return user;
  }
}
