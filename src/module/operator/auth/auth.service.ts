import { HttpStatus, Injectable } from '@nestjs/common';
import { BasicResponse } from '../../../shared/basic.response';
import {
  getError,
  LIST_ERROR_CODES,
} from '../../../shared/constants/error/base.error';
import { AuthService } from '../../core/auth/auth.service';
import { ResponseAuthUser } from '../../core/auth/models/auth.model';
import { UserRole } from '../../core/users/entities/user.entity';

@Injectable()
export class OperatorAuthService {
  constructor(private readonly authService: AuthService) {}

  async validateAdmin(
    account: string,
    password: string,
  ): Promise<BasicResponse | ResponseAuthUser> {
    const user = await this.authService.validateAdmin(account, password);

    if (typeof user === 'string')
      return getError(user, HttpStatus.UNPROCESSABLE_ENTITY);
    if (user.role !== UserRole.ADMIN && user.role !== UserRole.SUPER_ADMIN)
      return getError(LIST_ERROR_CODES.SS24004, HttpStatus.FORBIDDEN);
    if (!!user.is_blocked)
      return getError(LIST_ERROR_CODES.SS24008, HttpStatus.FORBIDDEN);
    return await this.authService.getResponseLogin(user);
  }
}
