import { Module } from '@nestjs/common';
import { OperatorUsersController } from './users/users.controller';
import { OperatorUsersModule } from './users/users.module';
import { OperatorInsurancesController } from './insurances/insurances.controller';
import { OperatorInsurancesModule } from './insurances/insurances.module';
import { OperatorAuthModule } from './auth/auth.module';
import { OperatorAuthController } from './auth/auth.controller';
import { OperatorRatingsModule } from './ratings/ratings.module';
import { OperatorRatingsController } from './ratings/ratings.controller';
import { OperatorInsuranceObjectivesModule } from './insurance-objectives/insurance-objectives.module';
import { OperatorInsuranceObjectivesController } from './insurance-objectives/insurance-objectives.controller';
import { OperatorCompaniesModule } from './companies/companies.module';
import { OperatorCompaniesController } from './companies/companies.controller';
import { OperatorUploadsModule } from './uploads/uploads.module';
import { OperatorUploadsController } from './uploads/uploads.controller';
import { OperatorBlogsModule } from './blogs/blogs.module';
import { OperatorBlogsController } from './blogs/blogs.controller';
import { OperatorBlogCommentsModule } from './blog-comments/blog-comments.module';
import { OperatorBlogCommentsController } from './blog-comments/blog-comments.controller';
import { OperatorBlogCategoriesController } from './blog-categories/blog-categories.controller';
import { OperatorBlogCategoriesModule } from './blog-categories/blog-categories.module';
import { SuperAdminUsersModule } from '../super-admin/users/users.module';
import { SuperAdminUsersController } from '../super-admin/users/users.controller';
import { OperatorInsuranceInterestModule } from './insurance-interest/insurance-interest.module';
import { OperatorInsuranceInterestController } from './insurance-interest/insurance-interest.controller';
import { OperatorInsuranceCategoriesModule } from './insurance-categories/insurance-categories.module';
import { OperatorInsuranceCategoriesController } from './insurance-categories/insurance-categories.controller';
import { OperatorHospitalModule } from './hospitals/hospitals.module';
import { OperatorHospitalsController } from './hospitals/hospitals.controller';
import { OperatorAppraisalFormsController } from './appraisal-forms/appraisal-forms.controller';
import { OperatorAppraisalFormsModule } from './appraisal-forms/appraisal-forms.module';
import { OperatorCarInsuranceSettingsController } from './car-insurance-settings/car-insurance-settings.controller';
import { OperatorCarInsuranceSettingsModule } from './car-insurance-settings/car-insurance-settings.module';
import { OperatorAppraisalsController } from './appraisals/appraisals.controller';
import { OperatorAppraisalsModule } from './appraisals/appraisals.module';
import { OperatorCarInsurancesController } from './car-insurances/car-insurances.controller';
import { OperatorCarInsurancesModule } from './car-insurances/car-insurances.module';
import { OperatorCarInsuranceRatingsController } from './car-insurance-ratings/car-insurance-ratings.controller';
import { OperatorCarInsuranceRatingsModule } from './car-insurance-ratings/car-insurance-ratings.module';

@Module({
  imports: [
    OperatorUsersModule,
    OperatorInsurancesModule,
    OperatorAuthModule,
    OperatorRatingsModule,
    OperatorInsuranceObjectivesModule,
    OperatorCompaniesModule,
    OperatorUploadsModule,
    OperatorBlogsModule,
    OperatorBlogCommentsModule,
    OperatorBlogCategoriesModule,
    SuperAdminUsersModule,
    OperatorInsuranceInterestModule,
    OperatorInsuranceCategoriesModule,
    OperatorHospitalModule,
    OperatorAppraisalFormsModule,
    OperatorCarInsuranceSettingsModule,
    OperatorAppraisalsModule,
    OperatorCarInsurancesModule,
    OperatorCarInsuranceRatingsModule,
  ],
  controllers: [
    OperatorUsersController,
    OperatorInsurancesController,
    OperatorAuthController,
    OperatorRatingsController,
    OperatorInsuranceObjectivesController,
    OperatorCompaniesController,
    OperatorUploadsController,
    OperatorBlogsController,
    OperatorBlogCommentsController,
    OperatorBlogCategoriesController,
    SuperAdminUsersController,
    OperatorInsuranceInterestController,
    OperatorInsuranceCategoriesController,
    OperatorHospitalsController,
    OperatorAppraisalFormsController,
    OperatorCarInsuranceSettingsController,
    OperatorAppraisalsController,
    OperatorCarInsurancesController,
    OperatorCarInsuranceRatingsController,
  ],
})
export class OperatorModule {}
