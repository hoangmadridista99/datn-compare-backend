import {
  Body,
  Controller,
  Get,
  Param,
  Patch,
  Post,
  UseGuards,
} from '@nestjs/common';
import {
  ApiTags,
  ApiBearerAuth,
  ApiOperation,
  ApiQuery,
} from '@nestjs/swagger';
import { AdminRoles } from '../../../shared/decorators/role.decorator';
import { JwtUserAuthGuard } from '../../core/auth/guards/jwt-auth.guard';
import { UserRolesGuard } from '../../core/auth/guards/local/role.guard';
import { UserRole } from '../../core/users/entities/user.entity';
import { OperatorAppraisalsService } from './appraisals.service';
import {
  CreateAppraisalDto,
  UpdateAppraisalDto,
} from '../../core/appraisals/dto/appraisal.dto';
import {
  LIST_ERROR_CODES,
  getError,
} from '../../../shared/constants/error/base.error';
import { UUIDPipe } from '../../../shared/pipes/uuid.pipe';
import { Paginate, PaginateQuery } from 'nestjs-paginate';
import {
  PAGE_QUERY,
  LIMIT_QUERY,
} from '../../../shared/constants/swagger/constant';

@ApiTags('Appraisal')
@Controller('appraisals')
@UseGuards(JwtUserAuthGuard, UserRolesGuard)
@AdminRoles(UserRole.ADMIN)
@ApiBearerAuth()
export class OperatorAppraisalsController {
  constructor(
    private readonly operatorAppraisalsService: OperatorAppraisalsService,
  ) {}

  @Post('car-cost')
  @ApiOperation({ summary: 'Submit car cost for admin' })
  async calculatorByCarCost(@Body() body: CreateAppraisalDto) {
    try {
      return await this.operatorAppraisalsService.calculatorByCarCost(body);
    } catch (error) {
      console.log(
        '🚀 ~ file: appraisals.controller.ts:26 ~ OperatorAppraisalsController ~ handleAppraisal ~ error:',
        error,
      );
      return getError(LIST_ERROR_CODES.SS24001);
    }
  }

  @Patch(':id/approve')
  @ApiOperation({ summary: 'approve form for admin' })
  async approveForm(
    @Param('id', new UUIDPipe()) id: string,
    @Body() body: UpdateAppraisalDto,
  ) {
    try {
      return await this.operatorAppraisalsService.approveTheForm(id, body);
    } catch (error) {
      console.log(
        '🚀 ~ file: appraisals.controller.ts:67 ~ OperatorAppraisalsController ~ error:',
        error,
      );
      return getError(LIST_ERROR_CODES.SS24001);
    }
  }

  @Get(':id')
  @ApiOperation({ summary: 'Get appraisal details' })
  async getOneById(@Param('id', new UUIDPipe()) id: string) {
    try {
      return this.operatorAppraisalsService.getDetailsById(id);
    } catch (error) {
      console.log(
        '🚀 ~ file: appraisals.controller.ts:45 ~ OperatorAppraisalsController ~ getOneById ~ error:',
        error,
      );

      return getError(LIST_ERROR_CODES.SS24001);
    }
  }

  @Get()
  @ApiQuery(PAGE_QUERY)
  @ApiQuery(LIMIT_QUERY)
  @ApiOperation({ summary: 'Get list forms for each user' })
  async getListFormsByUserId(@Paginate() query: PaginateQuery) {
    try {
      return await this.operatorAppraisalsService.getAppraisalsList(query);
    } catch (error) {
      console.log(
        '🚀 ~ file: appraisals.controller.ts:80 ~ OperatorAppraisalsController ~ getListFormsByUserId ~ error:',
        error,
      );
      return getError(LIST_ERROR_CODES.SS24001);
    }
  }
}
