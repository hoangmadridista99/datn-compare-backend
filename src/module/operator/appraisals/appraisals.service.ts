import { Injectable } from '@nestjs/common';
import { AppraisalsService } from '../../core/appraisals/appraisals.service';
import {
  IApproveFormDto,
  ICreateAppraisalDto,
} from '../../core/appraisals/interfaces/appraisal.interface';
import { PaginateQuery } from 'nestjs-paginate';

@Injectable()
export class OperatorAppraisalsService {
  constructor(private readonly appraisalsService: AppraisalsService) {}

  async calculatorByCarCost(body: ICreateAppraisalDto) {
    return await this.appraisalsService.handleAppraisal(body);
  }

  async getDetailsById(id: string) {
    return await this.appraisalsService.getOneByQuery({ id });
  }

  async getAppraisalsList(query: PaginateQuery) {
    return await this.appraisalsService.getAppraisalsList(query);
  }

  async approveTheForm(id: string, body: IApproveFormDto) {
    return await this.appraisalsService.updateAppraisalDataById(id, body);
  }
}
