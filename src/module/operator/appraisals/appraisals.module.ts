import { Module } from '@nestjs/common';
import { AppraisalsModule } from '../../core/appraisals/appraisals.module';

@Module({
  imports: [AppraisalsModule],
  providers: [AppraisalsModule],
  exports: [AppraisalsModule],
})
export class OperatorAppraisalsModule {}
