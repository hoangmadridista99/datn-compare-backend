import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiQuery,
  ApiTags,
} from '@nestjs/swagger';
import { AdminRoles } from '../../../shared/decorators/role.decorator';
import { JwtUserAuthGuard } from '../../core/auth/guards/jwt-auth.guard';
import { UserRolesGuard } from '../../core/auth/guards/local/role.guard';
import { UserRole } from '../../core/users/entities/user.entity';
import { OperatorBlogCategoriesService } from './blog-categories.service';
import { CreateBlogCategoryDto } from '../../core/blog-categories/dto/blog-categories.dto';
import {
  LIST_ERROR_CODES,
  getError,
} from '../../../shared/constants/error/base.error';
import { UpdateBlogCategoryDto } from '../../core/blog-categories/dto/blog-categories.dto';
import { UUIDPipe } from '../../../shared/pipes/uuid.pipe';
import { Paginate, PaginateQuery } from 'nestjs-paginate';

@ApiTags('Blog-Categories')
@Controller('blog-categories')
@UseGuards(JwtUserAuthGuard, UserRolesGuard)
@ApiBearerAuth()
@AdminRoles(UserRole.ADMIN)
export class OperatorBlogCategoriesController {
  constructor(
    private readonly operatorBlogCategoriesService: OperatorBlogCategoriesService,
  ) {}

  @Post()
  @ApiOperation({ summary: 'Create new blog category' })
  async createNewCategory(@Body() body: CreateBlogCategoryDto) {
    try {
      return await this.operatorBlogCategoriesService.createNewCategory(body);
    } catch (error) {
      console.log(
        '🚀 ~ file: blog-categories.controller.ts:26 ~ OperatorBlogCategoriesController ~ createNewCategory ~ error:',
        error,
      );
      return getError(LIST_ERROR_CODES.SS24001);
    }
  }

  @Get()
  @ApiOperation({ summary: 'Get list blog categories for admin' })
  @ApiQuery({
    name: 'page',
    example: 1,
    required: true,
  })
  @ApiQuery({
    name: 'limit',
    example: 10,
    required: true,
  })
  async getListCategories(@Paginate() query: PaginateQuery) {
    try {
      return await this.operatorBlogCategoriesService.getListCategoriesForAdmin(
        query,
      );
    } catch (error) {
      console.log(
        '🚀 ~ file: blog-categories.controller.ts:44 ~ OperatorBlogCategoriesController ~ getListCategories ~ error:',
        error,
      );
      return getError(LIST_ERROR_CODES.SS24001);
    }
  }

  @Patch(':id')
  @ApiOperation({ summary: 'Update blog category for admin' })
  async updateBlogCategory(
    @Param('id', new UUIDPipe()) id: string,
    @Body() body: UpdateBlogCategoryDto,
  ) {
    try {
      return await this.operatorBlogCategoriesService.updateCategory(id, body);
    } catch (error) {
      console.log(
        '🚀 ~ file: blog-categories.controller.ts:71 ~ OperatorBlogCategoriesController ~ error:',
        error,
      );
      return getError(LIST_ERROR_CODES.SS24001);
    }
  }

  @Delete(':id')
  @ApiOperation({ summary: 'Delete blog category for admin' })
  async deleteBlogCategory(@Param('id', new UUIDPipe()) id: string) {
    try {
      return await this.operatorBlogCategoriesService.deleteCategory(id);
    } catch (error) {
      console.log(
        '🚀 ~ file: blog-categories.controller.ts:86 ~ OperatorBlogCategoriesController ~ deleteBlogCategory ~ error:',
        error,
      );
      return getError(LIST_ERROR_CODES.SS24001);
    }
  }
}
