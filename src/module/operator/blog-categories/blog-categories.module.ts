import { Module } from '@nestjs/common';
import { BlogCategoriesModule } from '../../core/blog-categories/blog-categories.module';

@Module({
  imports: [BlogCategoriesModule],
  providers: [BlogCategoriesModule],
  exports: [BlogCategoriesModule],
})
export class OperatorBlogCategoriesModule {}
