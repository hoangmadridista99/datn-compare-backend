import { HttpStatus, Injectable } from '@nestjs/common';
import { BlogCategoriesService } from '../../core/blog-categories/blog-categories.service';
import {
  ICreateBlogCategory,
  IUpdateBlogCategory,
} from '../../core/blog-categories/interfaces/blog-categories.interface';
import {
  LIST_ERROR_CODES,
  getError,
} from '../../../shared/constants/error/base.error';
import { NO_CONTENT_SUCCESS_RESPONSE } from '../../../shared/lib/responses/response';
import { PaginateQuery } from 'nestjs-paginate';

@Injectable()
export class OperatorBlogCategoriesService {
  constructor(private readonly blogCategoriesService: BlogCategoriesService) {}

  async createNewCategory(body: ICreateBlogCategory) {
    const result = await this.blogCategoriesService.createBlogCategory(body);
    return !result
      ? getError(LIST_ERROR_CODES.SS24104, HttpStatus.INTERNAL_SERVER_ERROR)
      : NO_CONTENT_SUCCESS_RESPONSE;
  }

  async updateCategory(id: string, body: IUpdateBlogCategory) {
    const result = await this.blogCategoriesService.updateCategoryById(
      id,
      body,
    );
    return !result
      ? getError(LIST_ERROR_CODES.SS24103, HttpStatus.NOT_FOUND)
      : NO_CONTENT_SUCCESS_RESPONSE;
  }

  async deleteCategory(id: string) {
    const result = await this.blogCategoriesService.deleteCategoryById(id);
    return !result
      ? getError(LIST_ERROR_CODES.SS24102, HttpStatus.NOT_FOUND)
      : NO_CONTENT_SUCCESS_RESPONSE;
  }

  async getListCategoriesForAdmin(query: PaginateQuery) {
    return await this.blogCategoriesService.getAllCategoryByOperator(query);
  }
}
