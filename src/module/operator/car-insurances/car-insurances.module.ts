import { Module } from '@nestjs/common';
import { CarInsurancesModule } from '../../core/car-insurances/car-insurances.module';

@Module({
  imports: [CarInsurancesModule],
  providers: [CarInsurancesModule],
  exports: [CarInsurancesModule],
})
export class OperatorCarInsurancesModule {}
