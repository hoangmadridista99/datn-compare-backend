import { HttpStatus, Injectable } from '@nestjs/common';
import { CarInsurancesService } from '../../core/car-insurances/car-insurances.service';
import { User } from '../../core/users/entities/user.entity';
import {
  ICarInsurancesFilter,
  ICreateCarInsuranceDto,
  IUpdateCarInsuranceDto,
} from '../../core/car-insurances/interfaces/car-insurance.interface';
import { PaginateQuery } from 'nestjs-paginate';
import {
  LIST_ERROR_CODES,
  getError,
} from '../../../shared/constants/error/base.error';
import { NO_CONTENT_SUCCESS_RESPONSE } from '../../../shared/lib/responses/response';
import { CarInsuranceSettingsService } from '../../core/car-insurance-settings/car-insurance-settings.service';
import { CarInsuranceTypes } from '../../core/car-insurances/enums/car-insurance.enum';

@Injectable()
export class OperatorCarInsurancesService {
  constructor(
    private readonly carInsurancesService: CarInsurancesService,
    private readonly carInsuranceSettingsService: CarInsuranceSettingsService,
  ) {}

  async createCarInsurance(
    user: User,
    body: ICreateCarInsuranceDto,
    file: Express.Multer.File,
  ) {
    return await this.carInsurancesService.createCarInsurance(user, body, file);
  }

  async getCarInsuranceDetails(id: string) {
    return (
      (await this.carInsurancesService.findOneByQuery({ id })) ??
      getError(LIST_ERROR_CODES.SS24101, HttpStatus.NOT_FOUND)
    );
  }

  async getCarInsurancesList(
    query: PaginateQuery,
    filter: ICarInsurancesFilter,
  ) {
    const mandatorySettingDetails =
      await this.carInsuranceSettingsService.getMandatorySettingDetails();
    const result = await this.carInsurancesService.getCarInsurancesList(
      query,
      filter,
    );
    return {
      ...result,
      is_mandatory:
        filter.insurance_type !== CarInsuranceTypes.Physical
          ? !!mandatorySettingDetails[0]
          : undefined,
    };
  }

  async updateCarInsuranceById(
    id: string,
    body: IUpdateCarInsuranceDto,
    file: Express.Multer.File,
  ) {
    await this.carInsurancesService.updateCarInsuranceById(id, body, file);
    return NO_CONTENT_SUCCESS_RESPONSE;
  }

  async deleteCarInsuranceById(id: string) {
    const result = await this.carInsurancesService.deleteCarInsuranceById(id);
    return !result
      ? getError(LIST_ERROR_CODES.SS24101, HttpStatus.NOT_FOUND)
      : NO_CONTENT_SUCCESS_RESPONSE;
  }
}
