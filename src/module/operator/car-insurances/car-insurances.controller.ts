import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Query,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiTags,
  ApiBearerAuth,
  ApiOperation,
  ApiBody,
  ApiConsumes,
  ApiQuery,
} from '@nestjs/swagger';
import { AdminRoles } from '../../../shared/decorators/role.decorator';
import { JwtUserAuthGuard } from '../../core/auth/guards/jwt-auth.guard';
import { UserRolesGuard } from '../../core/auth/guards/local/role.guard';
import { User, UserRole } from '../../core/users/entities/user.entity';
import { OperatorCarInsurancesService } from './car-insurances.service';
import { AuthResponse } from '../../../shared/decorators/response-auth/auth.decorator';
import {
  ApiBodyCreateCarInsurance,
  ApiBodyUpdateCarInsurance,
  CarInsuranceFilterDto,
} from '../../core/car-insurances/dto/car-insurance.dto';
import {
  LIST_ERROR_CODES,
  getError,
} from '../../../shared/constants/error/base.error';
import { FileInterceptor } from '@nestjs/platform-express';
import { FileImageValidationPipe } from '../../../shared/pipes/file-image.pipe';
import { Paginate, PaginateQuery } from 'nestjs-paginate';
import {
  PAGE_QUERY,
  LIMIT_QUERY,
} from '../../../shared/constants/swagger/constant';
import { CAR_INSURANCE_TYPE_QUERY } from '../../../shared/constants/swagger/car-insurances/car-insurances.constant';
import { UUIDPipe } from '../../../shared/pipes/uuid.pipe';
import {
  ICreateCarInsuranceDto,
  IUpdateCarInsuranceDto,
} from '../../core/car-insurances/interfaces/car-insurance.interface';
import {
  CreateCarInsuranceDtoPipe,
  UpdateCarInsuranceDtoPipe,
} from '../../../shared/pipes/car-insurance.pipe';

@ApiTags('Car-Insurances')
@Controller('car-insurances')
@UseGuards(JwtUserAuthGuard, UserRolesGuard)
@ApiBearerAuth()
@AdminRoles(UserRole.ADMIN)
export class OperatorCarInsurancesController {
  constructor(
    private readonly operatorCarInsurancesService: OperatorCarInsurancesService,
  ) {}

  @Post()
  @ApiBody({ type: ApiBodyCreateCarInsurance })
  @UseInterceptors(FileInterceptor('file'))
  @ApiConsumes('multipart/form-data')
  @ApiOperation({ summary: 'Create car insurance' })
  async createCarInsurance(
    @AuthResponse() user: User,
    @UploadedFile(new FileImageValidationPipe()) file: Express.Multer.File,
    @Body(new CreateCarInsuranceDtoPipe()) body: ICreateCarInsuranceDto,
  ) {
    try {
      return await this.operatorCarInsurancesService.createCarInsurance(
        user,
        body,
        file,
      );
    } catch (error) {
      console.log(
        '🚀 ~ file: car-insurances.controller.ts:30 ~ OperatorCarInsurancesController ~ error:',
        error,
      );
      return getError(LIST_ERROR_CODES.SS24001);
    }
  }

  @Get()
  @ApiQuery(PAGE_QUERY)
  @ApiQuery(LIMIT_QUERY)
  @ApiQuery(CAR_INSURANCE_TYPE_QUERY)
  @ApiOperation({ summary: 'Filter car insurances list' })
  async getCarInsurancesList(
    @Paginate() query: PaginateQuery,
    @Query() filter: CarInsuranceFilterDto,
  ) {
    try {
      return await this.operatorCarInsurancesService.getCarInsurancesList(
        query,
        filter,
      );
    } catch (error) {
      console.log(
        '🚀 ~ file: car-insurances.controller.ts:77 ~ OperatorCarInsurancesController ~ getCarInsurancesList ~ error:',
        error,
      );
      return getError(LIST_ERROR_CODES.SS24001);
    }
  }

  @Get(':id')
  @ApiOperation({ summary: 'Get car insurance details' })
  async getCarInsuranceById(@Param('id', new UUIDPipe()) id: string) {
    try {
      return await this.operatorCarInsurancesService.getCarInsuranceDetails(id);
    } catch (error) {
      console.log(
        '🚀 ~ file: car-insurances.controller.ts:108 ~ OperatorCarInsurancesController ~ getCarInsuranceById ~ error:',
        error,
      );
      return getError(LIST_ERROR_CODES.SS24001);
    }
  }

  @Patch(':id')
  @ApiOperation({ summary: 'Update car insurance details' })
  @ApiBody({ type: ApiBodyUpdateCarInsurance })
  @UseInterceptors(FileInterceptor('file'))
  @ApiConsumes('multipart/form-data')
  async updateCarInsurance(
    @Param('id', new UUIDPipe()) id: string,
    @Body(new UpdateCarInsuranceDtoPipe()) body: IUpdateCarInsuranceDto,
    @UploadedFile(new FileImageValidationPipe()) file: Express.Multer.File,
  ) {
    try {
      return await this.operatorCarInsurancesService.updateCarInsuranceById(
        id,
        body,
        file,
      );
    } catch (error) {
      console.log(
        '🚀 ~ file: car-insurances.controller.ts:125 ~ OperatorCarInsurancesController ~ updateCarInsurance ~ error:',
        error,
      );
      return getError(LIST_ERROR_CODES.SS24001);
    }
  }

  @Delete(':id')
  @ApiOperation({ summary: 'Delete car insurance' })
  async deleteCarInsurance(@Param('id', new UUIDPipe()) id: string) {
    try {
      return await this.operatorCarInsurancesService.deleteCarInsuranceById(id);
    } catch (error) {
      console.log(
        '🚀 ~ file: car-insurances.controller.ts:158 ~ OperatorCarInsurancesController ~ deleteCarInsurance ~ error:',
        error,
      );
      return getError(LIST_ERROR_CODES.SS24001);
    }
  }
}
