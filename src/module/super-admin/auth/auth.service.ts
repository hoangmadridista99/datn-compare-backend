// import { HttpStatus, Injectable } from '@nestjs/common';
// import { AuthService } from '../../core/auth/auth.service';
// import {
//   LIST_ERROR_CODES,
//   getError,
// } from '../../../shared/constants/error/base.error';
// import { UserRole } from '../../core/users/entities/user.entity';

// @Injectable()
// export class SuperAdminAuthService {
//   constructor(private readonly authService: AuthService) {}

//   async validateSuperAdmin(account: string, password: string) {
//     const superAdmin = await this.authService.validateAdmin(account, password);
//     if (typeof superAdmin === 'string')
//       return getError(superAdmin, HttpStatus.UNPROCESSABLE_ENTITY);
//     if (superAdmin.role !== UserRole.SUPER_ADMIN)
//       return getError(
//         LIST_ERROR_CODES.SS24212,
//         HttpStatus.UNPROCESSABLE_ENTITY,
//       );
//     return await this.authService.getResponseLogin(superAdmin);
//   }
// }
