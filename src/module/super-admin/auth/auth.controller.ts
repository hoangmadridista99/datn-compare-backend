// import { Controller, Post, UseGuards } from '@nestjs/common';
// import { ApiBody, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
// import { AuthResponse } from '../../../shared/decorators/response-auth/auth.decorator';
// import { LoginAdminDto } from '../../core/auth/dto/auth.dto';
// import { ResponseAuthUser } from '../../core/auth/models/auth.model';
// import { User } from '../../core/users/entities/user.entity';
// import { SuperAdminAuthService } from './auth.service';
// import { SuperAdminAuthGuard } from '../../core/auth/guards/super-admin/super-admin.guard';

// @ApiTags('Authentication')
// @Controller('auth')
// export class OperatorAuthController {
//   constructor(private readonly superAdminAuthService: SuperAdminAuthService) {}
//   @Post('/login')
//   @UseGuards(SuperAdminAuthGuard)
//   @ApiBody({ type: LoginAdminDto })
//   @ApiOperation({ summary: 'Login for admin' })
//   @ApiResponse({ status: 401, description: 'Unauthorized.' })
//   @ApiResponse({ status: 403, description: 'Forbidden.' })
//   @ApiResponse({ status: 200, description: 'OK', type: ResponseAuthUser })
//   async loginForAdmin(@AuthResponse() user: User) {
//     return user;
//   }
// }
