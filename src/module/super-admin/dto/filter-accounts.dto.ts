import {
  IsBooleanString,
  IsNumberString,
  IsOptional,
  IsString,
} from 'class-validator';

export class FilterAccountsForSuperAdmin {
  @IsNumberString()
  page: number;

  @IsNumberString()
  limit: number;

  @IsBooleanString()
  @IsOptional()
  is_blocked: boolean;

  @IsString()
  @IsOptional()
  account: string;
}
