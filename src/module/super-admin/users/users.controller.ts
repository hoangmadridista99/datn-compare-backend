import {
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  Param,
  Patch,
  Post,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiQuery,
  ApiTags,
} from '@nestjs/swagger';
import { SuperAdminRoles } from '../../../shared/decorators/role.decorator';
import { JwtUserAuthGuard } from '../../core/auth/guards/jwt-auth.guard';
import { UserRole } from '../../core/users/entities/user.entity';
import {
  CreateAdminDto,
  UpdateAdminDto,
} from '../../core/users/dto/create.dto';
import {
  LIST_ERROR_CODES,
  getError,
} from '../../../shared/constants/error/base.error';
import { SuperAdminUsersService } from './users.service';
import { UUIDPipe } from '../../../shared/pipes/uuid.pipe';
import { Paginate, PaginateQuery } from 'nestjs-paginate';
import { SuperAdminRolesGuard } from '../../core/auth/guards/super-admin/super-admin.guard';
import { AdminAccountFilter } from '../../../shared/decorators/users/super-admin/filter-admin.decorator';
import { IFilterAdmin } from '../../core/users/interfaces/users.interface';
import {
  PAGE_QUERY,
  LIMIT_QUERY,
} from '../../../shared/constants/swagger/constant';
const STATUS_ADMIN_QUERY = {
  name: 'is_blocked',
  type: 'boolean',
  example: false,
  require: false,
};
const ACCOUNT_ADMIN_QUERY = {
  name: 'account',
  type: 'string',
  example: 'admin',
  require: false,
};

@ApiTags('SuperAdmin-Users')
@Controller('super-admin/users')
@UseGuards(JwtUserAuthGuard, SuperAdminRolesGuard)
@SuperAdminRoles(UserRole.SUPER_ADMIN)
@ApiBearerAuth()
export class SuperAdminUsersController {
  constructor(
    private readonly superAdminUsersService: SuperAdminUsersService,
  ) {}
  @Post()
  @ApiOperation({ summary: 'Create admin by super admin' })
  async createAdmin(@Body() body: CreateAdminDto) {
    try {
      return await this.superAdminUsersService.createNewAdminAccount(body);
    } catch (error) {
      console.log(
        '🚀 ~ file: users.controller.ts:21 ~ SuperAdminUsersController ~ createAdmin ~ error:',
        error,
      );
      return getError(
        LIST_ERROR_CODES.SS24001,
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  @Get(':id')
  @ApiOperation({ summary: 'Get detail admin account' })
  async getOneAdminById(@Param('id', new UUIDPipe()) id: string) {
    try {
      return await this.superAdminUsersService.findOneAdmin(id);
    } catch (error) {
      console.log(
        '🚀 ~ file: users.controller.ts:55 ~ SuperAdminUsersController ~ getOneAdminById ~ error:',
        error,
      );
      return getError(
        LIST_ERROR_CODES.SS24001,
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  @Get()
  @ApiQuery(PAGE_QUERY)
  @ApiQuery(LIMIT_QUERY)
  @ApiQuery(STATUS_ADMIN_QUERY)
  @ApiQuery(ACCOUNT_ADMIN_QUERY)
  @ApiOperation({ summary: 'Get list admin' })
  async getListAdmin(
    @Paginate() query: PaginateQuery,
    @AdminAccountFilter() filter: IFilterAdmin,
  ) {
    try {
      if (!filter)
        return getError(LIST_ERROR_CODES.SS24429, HttpStatus.BAD_REQUEST);
      return await this.superAdminUsersService.findAllAdminBySuperAdmin(
        query,
        filter,
      );
    } catch (error) {
      console.log(
        '🚀 ~ file: users.controller.ts:72 ~ SuperAdminUsersController ~ getListAdmin ~ error:',
        error,
      );
      return getError(
        LIST_ERROR_CODES.SS24001,
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  @Patch(':id')
  @ApiOperation({ summary: 'Update profile of admin by super admin' })
  async updateProfileAdmin(
    @Param('id', new UUIDPipe()) id: string,
    @Body() body: UpdateAdminDto,
  ) {
    try {
      return await this.superAdminUsersService.updateAdminBySuperAdmin(
        id,
        body,
      );
    } catch (error) {
      console.log(
        '🚀 ~ file: users.controller.ts:97 ~ SuperAdminUsersController ~ error:',
        error,
      );
      return getError(
        LIST_ERROR_CODES.SS24001,
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  @Delete(':id')
  @ApiOperation({ summary: 'Delete account admin' })
  async deleteAdminAccount(@Param('id', new UUIDPipe()) id: string) {
    try {
      return await this.superAdminUsersService.deleteAdminById(id);
    } catch (error) {
      console.log(
        '🚀 ~ file: users.controller.ts:118 ~ SuperAdminUsersController ~ deleteAdminAccount ~ error:',
        error,
      );
      return getError(
        LIST_ERROR_CODES.SS24001,
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }
}
