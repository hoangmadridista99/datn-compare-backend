import { HttpStatus, Injectable } from '@nestjs/common';
import { UsersService } from '../../core/users/users.service';
import {
  ICreateAdminDto,
  IFilterAdmin,
  IUpdateAdminDto,
} from '../../core/users/interfaces/users.interface';
import { PaginateQuery } from 'nestjs-paginate';
import { getPaginate } from '../../../shared/lib/paginate/paginate.lib';
import {
  getError,
  LIST_ERROR_CODES,
} from '../../../shared/constants/error/base.error';
import { NO_CONTENT_SUCCESS_RESPONSE } from '../../../shared/lib/responses/response';
import { AuthService } from '../../core/auth/auth.service';

@Injectable()
export class SuperAdminUsersService {
  constructor(
    private readonly usersService: UsersService,
    private readonly authService: AuthService,
  ) {}

  async createNewAdminAccount(body: ICreateAdminDto) {
    const checkExistAccount = await this.usersService.findOneByUsersQuery({
      account: body.account,
    });
    if (checkExistAccount)
      return getError(
        LIST_ERROR_CODES.SS24206,
        HttpStatus.UNPROCESSABLE_ENTITY,
      );
    const hashPassword = await this.authService.hashPassword(body.password);
    body.password = hashPassword;
    const result = await this.usersService.createAdmin(body);
    return !result
      ? getError(LIST_ERROR_CODES.SS24103, HttpStatus.INTERNAL_SERVER_ERROR)
      : NO_CONTENT_SUCCESS_RESPONSE;
  }

  async findAllAdminBySuperAdmin(query: PaginateQuery, filter: IFilterAdmin) {
    const queryBuilder = await this.usersService.findAllAdminBySuperAdmin(
      filter,
    );
    return await getPaginate(query, queryBuilder);
  }

  async findOneAdmin(id: string) {
    return (
      (await this.usersService.findOneByUsersQuery({ id })) ??
      getError(LIST_ERROR_CODES.SS24101, HttpStatus.NOT_FOUND)
    );
  }

  async deleteAdminById(id: string) {
    const result = await this.usersService.deleteById(id);
    return !result
      ? getError(LIST_ERROR_CODES.SS24104, HttpStatus.INTERNAL_SERVER_ERROR)
      : NO_CONTENT_SUCCESS_RESPONSE;
  }

  async updateAdminBySuperAdmin(id: string, body: IUpdateAdminDto) {
    if (!!body.password) {
      const newHashPassword = await this.authService.hashPassword(
        body.password,
      );
      body.password = newHashPassword;
    }
    const result = await this.usersService.updateUserById(id, body);
    return !result.affected
      ? getError(LIST_ERROR_CODES.SS24103, HttpStatus.NOT_FOUND)
      : NO_CONTENT_SUCCESS_RESPONSE;
  }
}
