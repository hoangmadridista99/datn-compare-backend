import { IsUUID } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateUserInsuranceDto {
  @ApiProperty({ description: 'Insurance Id' })
  @IsUUID()
  insuranceId: string;
}
