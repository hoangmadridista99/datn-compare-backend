import { InsuranceSort } from '../../insurances/enums/insurances.enum';

export interface IUserInsurancesQueryFilter {
  insurance_name?: string;
  insurance_category_label?: string;
  total_interest?: `${InsuranceSort}`;
}
