import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';
import { BaseEntity } from 'src/shared/entities/base.entity';

import { User } from '../../users/entities/user.entity';
import { Insurance } from '../../insurances/entities/insurance.entity';

@Entity('user-insurances')
export class UserInsurances extends BaseEntity {
  @Column()
  user_id: string;

  @ManyToOne(() => User, (user) => user.user_insurances)
  @JoinColumn({ name: 'user_id', referencedColumnName: 'id' })
  user: User;

  @ManyToOne(() => Insurance, (insurance) => insurance.id, {
    eager: true,
  })
  insurance: Insurance;
}
