import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

import { UserInsurances } from './entities/user-insurance.entity';
import { User } from '../users/entities/user.entity';
import { Insurance } from '../insurances/entities/insurance.entity';
import { PaginateQuery, Paginated } from 'nestjs-paginate';
import { getPaginate } from 'src/shared/lib/paginate/paginate.lib';
import { OperationSign } from 'src/shared/enums/operation.enum';
import {
  IInsurance,
  IInsuranceHaveIsSaved,
} from '../insurances/interfaces/insurance.interface';
import { IInsuranceCategoriesTypes } from '../insurance-categories/interfaces/insurance-categories.interface';
import {
  COLUMNS_FIND_ALL_HEALTH_BY_FILTER,
  COLUMNS_NEED_TO_SELECT_OF_CLIENT,
} from '../insurances/constants/query.constant';
import { QueryOperatorDto } from 'src/module/operator/insurance-interest/dto/query.dto';
import { SortRatingTypes } from '../ratings/enums/ratings.enum';

export class UserInsurancesService {
  constructor(
    @InjectRepository(UserInsurances)
    private readonly userInsuranceRepository: Repository<UserInsurances>,
  ) {}

  private async createUserInsurance(user: User, insurance: Insurance) {
    try {
      const data = this.userInsuranceRepository.create({ user, insurance });
      return await this.userInsuranceRepository.save(data);
    } catch (error) {
      console.log(
        '🚀 ~ file: insurance-favorites.service.ts:16 ~ InsuranceFavoritesService ~ createInsuranceFavorite ~ error:',
        error,
      );
      throw error;
    }
  }

  private createQueryBuilder() {
    try {
      return this.userInsuranceRepository
        .createQueryBuilder('user-insurances')
        .leftJoinAndSelect('user-insurances.insurance', 'insurance');
    } catch (error) {
      console.log(
        '🚀 ~ file: user-insurances.service.ts:31 ~ UserInsurancesService ~ createQueryBuilder ~ error:',
        error,
      );
      throw error;
    }
  }

  async getListByUserId(
    query: PaginateQuery,
    userId: string,
    name?: string,
    categoryType?: IInsuranceCategoriesTypes,
  ): Promise<Paginated<Insurance | UserInsurances> | null> {
    try {
      const columnSelect = [
        ...new Set([
          ...COLUMNS_NEED_TO_SELECT_OF_CLIENT,
          ...COLUMNS_FIND_ALL_HEALTH_BY_FILTER,
        ]),
      ].map((item) => {
        const [parent, key] = item.split('.');
        if (parent === 'insurances') return `insurance.${key}`;
        return item;
      });
      const queryBuilder = this.userInsuranceRepository
        .createQueryBuilder('user-insurances')
        .select([
          'user-insurances.id',
          'user-insurances.created_at',
          'insurance_category.label',
          ...columnSelect,
        ])
        .where({ user: { id: userId } })
        .leftJoin('user-insurances.insurance', 'insurance')
        .leftJoin('insurance.terms', 'terms')
        .leftJoin('insurance.customer_orientation', 'customer_orientation')
        .leftJoin('insurance.inpatient', 'inpatient')
        .leftJoin('insurance.obstetric', 'obstetric')
        .leftJoin('insurance.dental', 'dental')
        .leftJoin('insurance.company', 'company')
        .leftJoin('insurance.insurance_category', 'insurance_category');

      if (name) {
        queryBuilder.andWhere('insurance.name ilike :name', {
          name: `%${name}%`,
        });
      }
      if (categoryType) {
        queryBuilder.andWhere('insurance_category.label = :categoryType', {
          categoryType,
        });
      }
      const result = await getPaginate(query, queryBuilder);
      const data = [...result.data].map((item) => ({
        ...item.insurance,
        isSaved: true,
      }));
      return {
        ...result,
        data,
      };
    } catch (error) {
      console.log(
        '🚀 ~ file: user-insurances.service.ts:31 ~ UserInsurancesService ~ findByUserId ~ error:',
        error,
      );
      return null;
    }
  }

  async getListByInsuranceId(
    paginateQuery: PaginateQuery,
    insuranceId: string,
    query: QueryOperatorDto,
  ) {
    try {
      const queryBuilder = this.createQueryBuilder();
      queryBuilder
        .leftJoin('user-insurances.user', 'user')
        .select([
          'user-insurances.id',
          'user-insurances.created_at',
          'user.first_name',
          'user.last_name',
          'user.phone',
        ])
        .andWhere('insurance.id = :insuranceId', { insuranceId });
      return getPaginate(
        {
          ...paginateQuery,
          sortBy: [
            ['created_at', query.sort_created_at ?? SortRatingTypes.Desc],
          ],
        },
        queryBuilder,
      );
    } catch (error) {
      console.log(
        '🚀 ~ file: user-insurances.service.ts:125 ~ UserInsurancesService ~ getListByInsuranceId ~ error:',
        error,
      );

      return null;
    }
  }

  async actionSaveByInsuranceId(
    user: User,
    insurance: Insurance,
  ): Promise<`${OperationSign}`> {
    try {
      const userInsurance = await this.userInsuranceRepository.findOneBy({
        user: { id: user.id },
        insurance: { id: insurance.id },
      });
      if (userInsurance) {
        await this.userInsuranceRepository.delete(userInsurance.id);
        return OperationSign.SUBTRACT;
      }
      await this.createUserInsurance(user, insurance);
      return OperationSign.ADD;
    } catch (error) {
      console.log(
        '🚀 ~ file: user-insurances.service.ts:18 ~ UserInsurancesService ~ actionSaveByInsuranceId ~ error:',
        error,
      );
      throw error;
    }
  }

  async addIsSavedFields(
    list: IInsurance[],
    userId?: string,
  ): Promise<IInsuranceHaveIsSaved[] | null> {
    try {
      let data = [];

      if (userId) {
        data = (
          await this.userInsuranceRepository.find({
            where: { user: { id: userId } },
            relations: ['insurance'],
            select: ['insurance'],
          })
        ).map((item) => item.insurance.id);
      }

      return list.map((item) => ({
        ...item,
        isSaved: !userId ? false : data.includes(item.id),
      }));
    } catch (error) {
      console.log(
        '🚀 ~ file: user-insurances.service.ts:47 ~ UserInsurancesService ~ addIsSavedFields ~ error:',
        error,
      );
      return null;
    }
  }
}
