import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserInsurances } from './entities/user-insurance.entity';
import { UserInsurancesService } from './user-insurances.service';
import { ClientUserInsurancesService } from 'src/module/client/user-insurances/user-insurances.service';
import { InsurancesService } from '../insurances/insurances.service';
import { Insurance } from '../insurances/entities/insurance.entity';
import { OperatorInsuranceInterestService } from 'src/module/operator/insurance-interest/insurance-interest.service';
import { InsuranceRepositoryService } from '../insurances/repository';

@Module({
  imports: [TypeOrmModule.forFeature([UserInsurances, Insurance])],
  providers: [
    UserInsurancesService,
    InsurancesService,
    ClientUserInsurancesService,
    OperatorInsuranceInterestService,
    InsuranceRepositoryService,
  ],
  exports: [
    UserInsurancesService,
    ClientUserInsurancesService,
    OperatorInsuranceInterestService,
  ],
})
export class UserInsurancesModule {}
