import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { InsuranceObjective } from './entities/insurance-objective.entity';
import { InsuranceObjectivesService } from './insurance-objectives.service';
import { OperatorInsuranceObjectivesService } from '../../operator/insurance-objectives/insurance-objectives.service';
import { ClientInsuranceObjectivesService } from '../../client/insurance-objectives/insurance-objectives.service';

@Module({
  imports: [TypeOrmModule.forFeature([InsuranceObjective])],
  providers: [
    InsuranceObjectivesService,
    OperatorInsuranceObjectivesService,
    ClientInsuranceObjectivesService,
  ],
  exports: [
    InsuranceObjectivesService,
    OperatorInsuranceObjectivesService,
    ClientInsuranceObjectivesService,
  ],
})
export class InsuranceObjectivesModule {}
