import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { InsuranceObjective } from './entities/insurance-objective.entity';
import {
  CreateInsuranceObjectiveDto,
  UpdateInsuranceObjectiveDto,
} from './dto/insurance-objective.dto';

export class InsuranceObjectivesService {
  constructor(
    @InjectRepository(InsuranceObjective)
    private readonly insuranceObjectivesRepository: Repository<InsuranceObjective>,
  ) {}

  async createInsuranceObjective(body: CreateInsuranceObjectiveDto) {
    try {
      const result = this.insuranceObjectivesRepository.create(body);
      return await this.insuranceObjectivesRepository.save(result);
    } catch (error) {
      console.log(
        '🚀 ~ file: insurance-objectives.service.ts:16 ~ InsuranceObjectivesService ~ createInsuranceObjective ~ error:',
        error,
      );
      throw error;
    }
  }

  async findOneByObjectiveType(objective_type: string) {
    try {
      return await this.insuranceObjectivesRepository.findOne({
        where: { objective_type },
      });
    } catch (error) {
      console.log(
        '🚀 ~ file: insurance-objectives.service.ts:29 ~ InsuranceObjectivesService ~ findOneByObjectiveType ~ error:',
        error,
      );
    }
  }

  async findAllInsuranceObjectives() {
    try {
      return await this.insuranceObjectivesRepository.find();
    } catch (error) {
      console.log(
        '🚀 ~ file: insurance-objectives.service.ts:29 ~ InsuranceObjectivesService ~ findInsuranceObjectivesByQuery ~ error:',
        error,
      );
      throw error;
    }
  }

  async updateById(id: string, body: UpdateInsuranceObjectiveDto) {
    try {
      return await this.insuranceObjectivesRepository.update(id, body);
    } catch (error) {
      console.log(
        '🚀 ~ file: insurance-objectives.service.ts:57 ~ InsuranceObjectivesService ~ updateById ~ error:',
        error,
      );
      throw error;
    }
  }

  async deleteById(id: string) {
    try {
      return await this.insuranceObjectivesRepository.delete(id);
    } catch (error) {
      console.log(
        '🚀 ~ file: insurance-objectives.service.ts:72 ~ InsuranceObjectivesService ~ deleteById ~ error:',
        error,
      );
      throw error;
    }
  }
}
