import { IBaseEntity } from '../../../../shared/interfaces/base-entity.interface';
import { Insurance } from '../../insurances/entities/insurance.entity';
import { InsuranceObjectiveTypes } from '../enums/insurance-objective.enum';

export interface IInsuranceObjective extends IBaseEntity {
  objective_type: string;
  label: string;
  insurance: Insurance[];
}

export type IInsuranceObjectiveType = `${InsuranceObjectiveTypes}`;
