export enum InsuranceObjectiveTypes {
  /** Bảo hiểm mang tính bảo vệ */
  SECURITY = 'security',
  /** Bảo hiểm mang tính đầu tư */
  INVESTMENT = 'investment',
  /** Bảo hiểm mang tính tích lũy */
  SAVINGS = 'savings',
  /** Bảo hiểm mang tính giáo dục */
  RETIREMENT = 'retirement',
  /** Bảo hiểm doanh nghiệp */
  BUSINESS = 'business',
  /** Bảo hiểm giáo dục */
  EDUCATION = 'education',
}
