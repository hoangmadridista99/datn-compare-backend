import { Column, Entity, ManyToMany } from 'typeorm';
import { BaseEntity } from '../../../../shared/entities/base.entity';
import { Insurance } from '../../insurances/entities/insurance.entity';

@Entity('insurance-objectives')
export class InsuranceObjective extends BaseEntity {
  @Column()
  objective_type: string;

  @ManyToMany(() => Insurance, (insurance) => insurance.id)
  insurance: Insurance[];

  @Column({ nullable: true })
  label: string;
}
