import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';
import { LIST_ERROR_CODES } from '../../../../shared/constants/error/base.error';

export class CreateInsuranceObjectiveDto {
  @ApiProperty()
  @IsString({ message: LIST_ERROR_CODES.SS24403 })
  objective_type: string;

  @ApiProperty()
  @IsString()
  label: string;
}

export class UpdateInsuranceObjectiveDto extends CreateInsuranceObjectiveDto {}
