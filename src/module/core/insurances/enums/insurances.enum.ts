export enum InsuredPersonTypes {
  ONE_SELF = 'one-self',
  OTHER_PERSON = 'other-person',
}

export enum ValueType {
  old = 'old',
  year = 'year',
}

export enum PriorityLevelTypes {
  HIGH = 'high',
  MEDIUM = 'medium',
  NOT_SUPPORT = 'not-support',
  NONE = 'none',
}

export enum ProfessionTypes {
  OTHER_PROFESSION = 'OTHER_PROFESSION',
  STUDENT = 'STUDENT',
  EMPLOYER = 'EMPLOYER',
  EMPLOYEE = 'EMPLOYEE',
}

export enum CustomerSegmentTypes {
  Individual = 'INDIVIDUAL',
  Family = 'FAMILY',
  Organization = 'ORGANIZATION',
  Business = 'BUSINESS',
}

export enum InsuranceStatusesTypes {
  Pending = 'pending',
  Approved = 'approved',
  Rejected = 'rejected',
}

export enum AdditionalBenefitsProperties {
  death_or_disability = 'death_or_disability',
  serious_illnesses = 'serious_illnesses',
  health_care = 'health_care',
  investment_benefit = 'investment_benefit',
  increasing_value_bonus = 'increasing_value_bonus',
  for_child = 'for_child',
  flexible_and_diverse = 'flexible_and_diverse',
  termination_benefits = 'termination_benefits',
  expiration_benefits = 'expiration_benefits',
  fee_exemption = 'fee_exemption',
}

export enum CustomerOrientationProperties {
  acceptance_rate = 'acceptance_rate',
  completion_time_deal = 'completion_time_deal',
  end_of_process = 'end_of_process',
  withdrawal_time = 'withdrawal_time',
  reception_and_processing_time = 'reception_and_processing_time',
}

export enum PaymentPolicyTypes {
  MONTHLY = 'monthly',
  YEARLY = 'yearly',
  FLEXIBLE = 'flexible',
  ONCE = 'once',
}

/** Điều kiện tham gia */
export enum TermProperties {
  age_eligibility = 'age_eligibility',
  deadline_for_deal = 'deadline_for_deal',
  insurance_minimum_fee = 'insurance_minimum_fee',
  deadline_for_payment = 'deadline_for_payment',
  insured_person = 'insured_person',
  profession = 'profession',
  age_of_contract_termination = 'age_of_contract_termination',
  termination_conditions = 'termination_conditions',
  total_sum_insured = 'total_sum_insured',
  monthly_fee = 'monthly_fee',
}

export enum InsuranceSortByClient {
  ALPHA_ASC = 'alpha_asc',
  ALPHA_DESC = 'alpha_desc',
  MIN_PRICE = 'min_price',
}

export enum InsuranceSort {
  ASC = 'ASC',
  DESC = 'DESC',
}

export enum RoomTypes {
  OneBed = 'one-bed',
  TwoBeds = 'two-beds',
  MultiBeds = 'multi-beds',
  VipRoom = 'vip-room',
  SpecialRoom = 'special-room',
}

export enum TimerTypes {
  ByDay = 'by-day',
  ByYear = 'by-year',
}

export enum INSURANCE_SCOPE_LIST {
  Death = 'death',
  Inpatient = 'inpatient',
  Outpatient = 'outpatient',
  Dental = 'dental',
  Obstetric = 'obstetric',
}

export enum InsuranceHealthAdditionalBenefit {
  DENTAL = 'dental',
  OBSTETRIC = 'obstetric',
}
