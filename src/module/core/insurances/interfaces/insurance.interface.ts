import type { IBaseEntity } from 'src/shared/interfaces/base-entity.interface';
import {
  CustomerSegmentTypes,
  InsuranceHealthAdditionalBenefit,
  InsuranceSortByClient,
  InsuranceStatusesTypes,
  PriorityLevelTypes,
  ProfessionTypes,
} from '../enums/insurances.enum';
import { IInsuranceObjective } from '../../insurance-objectives/interfaces/insurance-objectives.interface';
import type {
  ICreateTermsByLifeInsurance,
  IInsuredPersonType,
  ITermsByLifeInsurance,
  IDeadlineForDealType,
  ITermsByHealthInsurance,
  ICreateTermsByHealthInsurance,
} from './terms.interface';
import type {
  IAdditionalBenefits,
  ICreateAdditionalBenefit,
  ICreateLifeAdditionalBenefit,
} from './additional-benefits.interface';
import type {
  ICreateCustomerOrientationByHealthInsurance,
  ICreateCustomerOrientationByLifeInsurance,
} from './customer-orientation.interface';
import { UserRolesQuery } from '../../users/entities/user.entity';
import {
  ICategoryOfInsurance,
  IInsuranceCategoriesTypes,
} from '../../insurance-categories/interfaces/insurance-categories.interface';
import {
  ICreateInpatientDto,
  IInpatient,
  IRoomType,
} from './Inpatient.interface';
import { ICreateDentalDto, IDental } from './dental.interface';
import { ICreateObstetricDto, IObstetric } from './obstetric.interface';
import { ICreateOutpatientDto, IOutpatient } from './outpatient.interface';
import {
  ICreateHospital,
  IHospital,
  IHospitalBody,
  IHospitalsPending,
} from '../../hospitals/interfaces/hospital.interface';

type Join<K, P> = K extends string | number
  ? P extends string | number
    ? `${K}${'' extends P ? '' : '.'}${P}`
    : never
  : never;
type Prev = [never, 0];
type DeepKey<T, D extends number = 1> = [D] extends [never]
  ? never
  : T extends object
  ? {
      [K in keyof T]-?: K extends string | number
        ? T[K] extends string
          ? `insurances.${K}`
          : `${K}` | Join<K, DeepKey<T[K], Prev[D]>>
        : never;
    }[keyof T]
  : '';
type DeepKeyObject<T> = T extends object
  ? {
      [K in keyof T]-?: K extends string
        ? T[K] extends string | number
          ? never
          : `${K}`
        : never;
    }[keyof T]
  : '';
interface Company {
  logo: string;
  long_name: string;
  short_name: string;
  homepage: string;
}
interface Insurance {
  id: string;
  created_at: Date;
  name: string;
  insurance_category: ICategoryOfInsurance;
  company: Company;
  description_insurance: string;
  key_benefits: string;
  documentation_url: string;
  benefits_illustration_table: string;
  objective_of_insurance: Omit<IInsuranceObjective, keyof IBaseEntity>[];
  additional_benefits: ICreateAdditionalBenefit;
  customer_orientation: ICreateCustomerOrientationByHealthInsurance &
    ICreateCustomerOrientationByLifeInsurance;
  terms: ICreateTermsByHealthInsurance & ICreateTermsByLifeInsurance;
  dental: ICreateDentalDto;
  inpatient: ICreateInpatientDto;
  obstetric: ICreateObstetricDto;
  outpatient: ICreateOutpatientDto;
  hospitals: ICreateHospital[];
}
interface CreateInsuranceBase {
  name: string;
  insurance_category: ICategoryOfInsurance;
  company_id: string;
  description_insurance: string;
}

export interface ICreateLifeInsurance extends CreateInsuranceBase {
  key_benefits: string;
  documentation_url: string;
  benefits_illustration_table: string;
  objective_of_insurance: IInsuranceObjective[];
  additional_benefits: ICreateLifeAdditionalBenefit;
  customer_orientation: ICreateCustomerOrientationByLifeInsurance;
  terms: ICreateTermsByLifeInsurance;
}

export interface ICreateHealthInsurance extends CreateInsuranceBase {
  customer_orientation: ICreateCustomerOrientationByHealthInsurance;
  terms: ICreateTermsByHealthInsurance;
  inpatient: IInpatient;
  obstetric: IObstetric;
  dental: IDental;
  outpatient: IOutpatient;
  hospitals: IHospitalBody[];
  additional_benefit: IInsuranceHealthAdditionalBenefit[];
}

export type IInsuranceSortByClient = `${InsuranceSortByClient}`;
export type IInsuranceHealthAdditionalBenefit =
  `${InsuranceHealthAdditionalBenefit}`;
export type IInsuranceDetailsTypeLevel = `${PriorityLevelTypes}`;
export type IInsuranceDetailsType = {
  level: IInsuranceDetailsTypeLevel;
  text: string;
};
export type IDeepKeyRelation = DeepKeyObject<Insurance>;
export type IDeepKeyColumn<T extends Array<DeepKeyObject<Insurance>> = null> =
  T extends null
    ? DeepKey<keyof Omit<Insurance, DeepKeyObject<Insurance>>>
    : DeepKey<
        Pick<
          Insurance,
          T[number] | keyof Omit<Insurance, DeepKeyObject<Insurance>>
        >
      >;

export type ITextOfProfessionType = `${ProfessionTypes}`;
export type ITextCustomerSegmentType = `${CustomerSegmentTypes}`;
export interface IUpdateLifeInsurance
  extends Omit<ICreateLifeInsurance, 'company_id'> {
  status?: InsuranceStatusesTypes;
}
export interface IUpdateHealthInsurance
  extends Omit<ICreateHealthInsurance, 'company_id'> {
  status?: InsuranceStatusesTypes;
  hospitals_pending: IHospitalBody[];
}

export interface IProfessionType {
  level: IInsuranceDetailsTypeLevel;
  text: ITextOfProfessionType;
}

export type IInsuranceStatuses = `${InsuranceStatusesTypes}`;

export interface IInsurance extends IBaseEntity {
  name: string;
  insurance_category: ICategoryOfInsurance;
  company_id: string;
  description_insurance: string;
  key_benefits: string;
  documentation_url: string;
  benefits_illustration_table: string;
  objective_of_insurance: IInsuranceObjective[];
  additional_benefits: IAdditionalBenefits;
  customer_orientation:
    | ICreateCustomerOrientationByHealthInsurance
    | ICreateCustomerOrientationByLifeInsurance;
  terms: ITermsByLifeInsurance | ITermsByHealthInsurance;
  status: IInsuranceStatuses;
  reason: string | null;
  hospitals: IHospital[];
  hospitals_pending: IHospitalsPending[];
}
export interface IInsuranceHaveIsSaved extends IInsurance {
  isSaved: boolean;
}
export interface IFilterByObjectiveOfInsurance {
  type: string;
}

export interface ICheckPermissionFilter {
  is_admin: boolean;
}

export interface IInsuranceFilterClient extends ICheckPermissionFilter {
  /** mục đích tham gia */
  objective: IFilterByObjectiveOfInsurance[];
  /** tổng tiền mong muốn ( khoảng giá) */
  total_sum_insured: number;
  /** bạn muốn bảo hiểm cho ai */
  insured_person: IInsuredPersonType[];
  /** năm sinh */
  year_of_birth: number;
  /** nghề nghiệp */
  profession: string;
  /** thời hạn bảo hiểm */
  deadline_for_deal: number;
  /** <<<=  QUYỀN LỢI BỔ SUNG =>>> */
  sub: ISubInsuranceFilterClient;

  order_by: IInsuranceSortByClient;
  is_suggestion: boolean;
  page: number;
  limit: number;
}

export interface ISubInsuranceFilterClient {
  /** Quyền lợi bảo vệ trước tử vong/ thương tật */
  death_or_disability?: boolean;
  /** Quyền lợi bảo vệ bệnh ung thư, hiểm nghèo */
  serious_illnesses?: boolean;
  /** Quyền lợi chăm sóc y tế */
  health_care?: boolean;
  /** Quyền lợi đầu tư của sản phẩm liên kết chung */
  investment_benefit?: boolean;
  /** Quyền lợi thưởng gia tăng giá trị hợp đồng */
  increasing_value_bonus?: boolean;
  /** Quyền lợi cho con */
  for_child?: boolean;
  /** Quyền lợi linh hoạt, đa dạng */
  flexible_and_diverse?: boolean;
  /** Quyền lợi kết thúc hợp đồng */
  termination_benefits?: boolean;
  /** Quyền lợi đáo hạn */
  expiration_benefits?: boolean;
  /** Quyền lợi miễn đóng phí */
  fee_exemption?: boolean;
}

export interface IInsuranceFilterAdmin {
  status?: IInsuranceStatuses;
  created_at?: Date;
  insurance_type?: string;
  insurance_name?: string;
  total_sum_insured?: number;
  role: `${UserRolesQuery}`;
}

export interface IHealthInsuranceFilterClient
  extends Pick<
    IInsuranceFilterClient,
    | 'insured_person'
    | 'year_of_birth'
    | 'profession'
    | 'page'
    | 'limit'
    | 'order_by'
  > {
  /** Có hỗ trợ nha khoa không */
  is_dental: boolean;
  /** Có hỗ trợ thai sản không */
  is_obstetric: boolean;
  /** Loại phòng */
  room_type: IRoomType[];
}

export interface IVendorInsurancesFilter {
  status?: IInsuranceStatuses;
  insurance_name?: string;
  created_at?: Date;
  insurance_category_label?: IInsuranceCategoriesTypes;
  monthly_fee?: number;
}
