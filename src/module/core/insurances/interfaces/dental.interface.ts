import { IBaseEntity } from '../../../../shared/interfaces/base-entity.interface';
import { IInsuranceDetailsType } from './insurance.interface';

export interface IDentalType extends Pick<IInsuranceDetailsType, 'level'> {
  value: number;
  description: string;
}

export interface IDental extends IBaseEntity {
  /** Khám và chẩn đoán */
  examination_and_diagnosis: IDentalType;
  /** Viêm lợi(nướu) /viêm nha chu */
  gingivitis: IDentalType;
  /** Chụp X-Quang và cận lâm sàng */
  xray_and_diagnostic_imaging: IDentalType;
  /** Trám răng bằng chất liệu thông thường */
  filling_teeth_basic: IDentalType;
  /** Điều trị tủy */
  root_canal_treatment: IDentalType;
  /** Nhổ răng bệnh lý (bao gồm tiểu phẫu) , phẫu thuật cắt chóp răng, lấy u vôi răng */
  dental_pathology: IDentalType;
  /** Lấy cao răng */
  dental_calculus: IDentalType;
}

export type ICreateDentalDto = Omit<IDental, keyof IBaseEntity>;
