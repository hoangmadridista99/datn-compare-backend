import { IBaseEntity } from '../../../../shared/interfaces/base-entity.interface';
import { RoomTypes, TimerTypes } from '../enums/insurances.enum';
import {
  IInsuranceDetailsType,
  IInsuranceDetailsTypeLevel,
} from './insurance.interface';
import { IInsuranceMinimumFeeType } from './terms.interface';

export interface IInpatientType extends Pick<IInsuranceDetailsType, 'level'> {
  value: number;
  description: string;
}

export interface IInpatient extends IBaseEntity {
  /** Loại phòng */
  room_type: IRoomType;
  /** Điều trị ung thư */
  for_cancer: IInpatientType;
  /** Điều trị bệnh tật */
  for_illnesses: IInpatientType;
  /** Điều trị tai nạn */
  for_accidents: IInpatientType;
  /** Chi phí phẫu thuật */
  for_surgical: IInpatientType;
  /** Chi phí nằm viện */
  for_hospitalization: IForHospitalization;
  /** Hồi sức tích cực */
  for_intensive_care: IInpatientType;
  /** Chi phí hành chính */
  for_administrative: IInpatientType;
  /** Cấy ghép bộ phận */
  for_organ_transplant: IInpatientType;
}

export interface IRoomType {
  level: IInsuranceDetailsTypeLevel;
  values: RoomTypes[];
  description: string;
}

export interface IForHospitalization
  extends Omit<IInsuranceMinimumFeeType, 'description'> {
  type: `${TimerTypes}`;
}

export type ICreateInpatientDto = Omit<IInpatient, keyof IBaseEntity>;
