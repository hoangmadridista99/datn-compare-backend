import type { IBaseEntity } from 'src/shared/interfaces/base-entity.interface';
import { IInsuranceDetailsTypeLevel } from './insurance.interface';
import {
  CustomerSegmentTypes,
  InsuredPersonTypes,
  PaymentPolicyTypes,
  ProfessionTypes,
  ValueType,
} from '../enums/insurances.enum';

interface TermsDefault {
  level: IInsuranceDetailsTypeLevel;
  description: string | null;
}
interface Terms {
  age_eligibility: IAgeEligibilityType;
  deadline_for_deal: IDeadlineForDealType;
  insurance_minimum_fee: IInsuranceMinimumFeeType;
  deadline_for_payment: IDeadlineForPaymentType;
  insured_person: IInsuredPersonType;
  profession: IProfessionType;
  age_of_contract_termination: IAgeOfContractTerminationType;
  termination_conditions: ITerminationConditionsType;
  total_sum_insured: ITotalSumInsuredType;
  monthly_fee: IMonthlyFeeType;
  customer_segment: ICustomerSegmentType;
}

export type IPaymentPolicyValue = `${PaymentPolicyTypes}`;
export type IInsuredPersonValue = `${InsuredPersonTypes}`;
export type IValueType = `${ValueType}`;
export type IProfessionValue = `${ProfessionTypes}`;
export type ICustomerSegmentValue = `${CustomerSegmentTypes}`;
export type IAgeEligibilityType = IDeadlineForDealType;
export type IAgeOfContractTerminationType = Omit<IDeadlineForDealType, 'value'>;
export type ITotalSumInsuredType = IMonthlyFeeType;
export type ICreateTermsByLifeInsurance = Omit<Terms, 'customer_segment'>;
export type ICreateTermsByHealthInsurance = Omit<
  Terms,
  'termination_conditions'
>;
export type ITermsByLifeInsurance = IBaseEntity & ICreateTermsByLifeInsurance;
export type ITermsByHealthInsurance = IBaseEntity &
  ICreateTermsByHealthInsurance;

export interface IMonthlyFeeType extends TermsDefault {
  from: number | null;
  to: number | null;
}
export interface IDeadlineForDealType extends IMonthlyFeeType {
  type: IValueType | null;
  value: number | null;
}
export interface IInsuranceMinimumFeeType extends TermsDefault {
  value: number | null;
}
export interface IDeadlineForPaymentType extends TermsDefault {
  value: IPaymentPolicyValue[] | null;
}
export interface IInsuredPersonType extends TermsDefault {
  value: IInsuredPersonValue[] | null;
}
export interface IProfessionType extends Omit<TermsDefault, 'description'> {
  text: IProfessionValue[] | null;
}
export interface ITerminationConditionsType
  extends Omit<TermsDefault, 'description'> {
  text: string | null;
}
export interface ICustomerSegmentType
  extends Omit<TermsDefault, 'description'> {
  text: ICustomerSegmentValue[] | null;
}
