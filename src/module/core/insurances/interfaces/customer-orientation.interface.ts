import type { IBaseEntity } from 'src/shared/interfaces/base-entity.interface';
import type { IInsuranceDetailsType } from './insurance.interface';
import { INSURANCE_SCOPE_LIST } from '../enums/insurances.enum';

type CreateCustomerOrientationKeyByLifeInsurance =
  | 'reception_and_processing_time'
  | 'acceptance_rate'
  | 'completion_time_deal'
  | 'end_of_process'
  | 'withdrawal_time';
type CreateCustomerOrientationKeyByHealthInsurance =
  | 'reception_and_processing_time'
  | 'insurance_scope'
  | 'waiting_period'
  | 'compensation_process';

interface CustomerOrientation {
  reception_and_processing_time: IInsuranceDetailsType;
  acceptance_rate: IInsuranceDetailsType;
  completion_time_deal: IInsuranceDetailsType;
  end_of_process: IInsuranceDetailsType;
  withdrawal_time: IInsuranceDetailsType;
  insurance_scope: IInsuranceScope;
  waiting_period: IWaitingPeriodType;
  compensation_process: IInsuranceDetailsType;
}

export type ICustomerOrientationByLifeInsurance =
  ICreateCustomerOrientationByLifeInsurance & IBaseEntity;
export type ICustomerOrientationByHealthInsurance =
  ICreateCustomerOrientationByHealthInsurance & IBaseEntity;
export type ICreateCustomerOrientationByLifeInsurance = Pick<
  CustomerOrientation,
  CreateCustomerOrientationKeyByLifeInsurance
>;
export type ICreateCustomerOrientationByHealthInsurance = Pick<
  CustomerOrientation,
  CreateCustomerOrientationKeyByHealthInsurance
>;
export type IUpdateCustomerOrientationByLifeInsurance = Partial<
  Record<
    CreateCustomerOrientationKeyByLifeInsurance,
    Partial<IInsuranceDetailsType>
  >
>;
export type IUpdateCustomerOrientationByHealthInsurance = Partial<
  Record<
    CreateCustomerOrientationKeyByLifeInsurance,
    Partial<IInsuranceDetailsType>
  >
>;

export interface IInsuranceScope extends Pick<IInsuranceDetailsType, 'level'> {
  values: INSURANCE_SCOPE_LIST[];
}

export interface IWaitingPeriodType
  extends Pick<IInsuranceDetailsType, 'level'> {
  from: number;
  to: number;
  descriptions: string;
}

export type ICompensationProcess = Pick<
  IWaitingPeriodType,
  'level' | 'descriptions'
>;
