import { IBaseEntity } from '../../../../shared/interfaces/base-entity.interface';
import { IInsuranceDetailsType } from './insurance.interface';

export interface IObstetricType extends Pick<IInsuranceDetailsType, 'level'> {
  value: number;
  description: string;
}
export interface IObstetric extends IBaseEntity {
  /** Sinh thường */
  give_birth_normally: IObstetricType;
  /** Sinh mổ */
  caesarean_section: IObstetricType;
  /** Tai biến sản khoa */
  obstetric_complication: IObstetricType;
  /** Bất thường trong quá trình mang thai và các bệnh ý phát sinh nguyên nhân do thai kỳ */
  give_birth_abnormality: IObstetricType;
  /** Chi phí khám trước khi sinh */
  after_give_birth_fee: IObstetricType;
  /** Chi phí điều trị ngay sau khi xuất viện hoặc 1 lần tái khám*/
  before_discharged: IObstetricType;
  /** Chi phí chăm sóc trẻ sau sinh */
  postpartum_childcare_cost: IObstetricType;
}

export type ICreateObstetricDto = Omit<IObstetric, keyof IBaseEntity>;
