import { IBaseEntity } from '../../../../shared/interfaces/base-entity.interface';
import { IInsuranceDetailsType } from './insurance.interface';

export interface IOutpatientType extends Pick<IInsuranceDetailsType, 'level'> {
  value: number;
  description: string;
}

export interface IOutpatient extends IBaseEntity {
  /** Khám và điều trị */
  examination_and_treatment: IOutpatientType;
  /** Xét nghiệm và chẩn đoán */
  testing_and_diagnosis: IOutpatientType;
  /** Chi phí y tá chăm sóc tại nhà sau khi xuất viện */
  home_care: IOutpatientType;
  /** Điều trị ngoại trú do tai nạn */
  due_to_accident: IOutpatientType;
  /** Điều trị ngoại trú do bệnh tật */
  due_to_illness: IOutpatientType;
  /** Điều trị ngoại trú do ung thư */
  due_to_cancer: IOutpatientType;
  /** Điều trị phục hồi chức năng */
  restore_functionality: IOutpatientType;
}

export type ICreateOutpatientDto = Omit<IOutpatient, keyof IBaseEntity>;
