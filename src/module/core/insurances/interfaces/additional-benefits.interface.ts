import { AdditionalBenefits } from '../entities/additional-benefits/additional-benefits.entity';
import type { IBaseEntity } from 'src/shared/interfaces/base-entity.interface';
import type { IInsuranceDetailsType } from './insurance.interface';

type AdditionalBenefitKey = Exclude<
  keyof AdditionalBenefits,
  keyof IBaseEntity
>;

export type ICreateAdditionalBenefit = Record<
  AdditionalBenefitKey,
  IInsuranceDetailsType
>;

export type ICreateLifeAdditionalBenefit = ICreateAdditionalBenefit;
export type IAdditionalBenefits = ICreateAdditionalBenefit & IBaseEntity;
