import {
  Column,
  Entity,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
} from 'typeorm';
import { BaseEntity } from 'src/shared/entities/base.entity';
import { Rating } from '../../ratings/entities/rating.entity';
import { InsuranceObjective } from '../../insurance-objectives/entities/insurance-objective.entity';
import { AdditionalBenefits } from './additional-benefits/additional-benefits.entity';
import { Company } from '../../companies/entities/company.entity';
import { CustomerOrientation } from './customer-orientation/customer-orientation.entity';
import { Term } from './terms/term.entity';
import { User } from '../../users/entities/user.entity';
import { InsuranceStatusesTypes } from '../enums/insurances.enum';
import { IInsuranceStatuses } from '../interfaces/insurance.interface';
import { UserInsurances } from '../../user-insurances/entities/user-insurance.entity';
import { InsuranceCategory } from '../../insurance-categories/entities/insurance-category.entity';
import { Hospital } from '../../hospitals/entities/hospital.entity';
import { Inpatient } from './inpatient/inpatient.entity';
import { Dental } from './dental/dental.entity';
import { Obstetric } from './obstetric/obstetric.entity';
import { Outpatient } from './outpatient/outpatient.entity';
import { IHospitalsPending } from '../../hospitals/interfaces/hospital.interface';

/** THÔNG TIN CƠ BẢN CỦA BẢO HIỂM */
@Entity('insurances')
export class Insurance extends BaseEntity {
  /** Tên bảo hiểm */
  @Column()
  name: string;

  /** Id công ti */
  @Column({ nullable: true })
  company_id: string;

  /** Mô tả bảo hiểm */
  @Column()
  description_insurance: string;

  @Column({ nullable: true, default: null })
  reason: string;

  /** Tổng số lượng đánh giá */
  @Column({ type: 'int', default: 0 })
  total_rating: number;

  @Column({ default: 0 })
  total_interest: number;

  /** Điểm đánh giá trung bình của bảo hiểm */
  @Column({ type: 'float', default: 0 })
  rating_scores: number;

  /** === Quyền lợi chính ===*/
  @Column({ nullable: true })
  key_benefits: string;

  @Column({ default: InsuranceStatusesTypes.Pending })
  status: IInsuranceStatuses;

  @Column({
    type: 'jsonb',
    nullable: true,
  })
  hospitals_pending: IHospitalsPending[];

  /** === Tài liệu giới thiệu sản phẩm === */
  @Column({ nullable: true })
  benefits_illustration_table: string;

  @Column({ nullable: true })
  documentation_url: string;

  /** === Điều kiện tham gia === */
  @OneToOne(() => Term, { cascade: true, nullable: true })
  @JoinColumn()
  terms: Term;

  /** === Định hướng khách hàng === */
  @OneToOne(() => CustomerOrientation, { cascade: true, nullable: true })
  @JoinColumn()
  customer_orientation: CustomerOrientation;

  /** === Quyền lợi bổ sung === */
  @OneToOne(() => AdditionalBenefits, { cascade: true, nullable: true })
  @JoinColumn()
  additional_benefits: AdditionalBenefits;

  /** === Dịch vụ nội trú === */
  @OneToOne(() => Inpatient, {
    nullable: true,
    cascade: true,
  })
  @JoinColumn()
  inpatient: Inpatient;

  /** === Dịch vụ ngoại trú === */
  @OneToOne(() => Outpatient, (outpatient) => outpatient.insurance, {
    nullable: true,
    cascade: true,
  })
  @JoinColumn()
  outpatient: Outpatient;

  /** === Dịch vụ nha khoa === */
  @OneToOne(() => Dental, {
    nullable: true,
    cascade: true,
  })
  @JoinColumn()
  dental: Dental;

  /** === Dịch vụ thai sản === */
  @OneToOne(() => Obstetric, {
    nullable: true,
    cascade: true,
  })
  @JoinColumn()
  obstetric: Obstetric;

  @ManyToOne(() => User, (user) => user.id)
  user: User;

  @ManyToOne(() => Company, (company) => company.id)
  @JoinColumn({ name: 'company_id', referencedColumnName: 'id' })
  company: Company;

  @OneToMany(() => Rating, (rating) => rating.insurance, { cascade: true })
  ratings: Rating[];

  @OneToMany(() => UserInsurances, (userInsurance) => userInsurance.insurance, {
    cascade: true,
  })
  user_insurances: UserInsurances[];

  @JoinColumn({ name: 'insurance_category_id', referencedColumnName: 'id' })
  @ManyToOne(
    () => InsuranceCategory,
    (insuranceCategory) => insuranceCategory.id,
    {
      onDelete: 'CASCADE',
    },
  )
  insurance_category: InsuranceCategory;

  /** Mục tiêu của bảo hiểm */
  @JoinTable()
  @ManyToMany(
    () => InsuranceObjective,
    (insuranceObjective) => insuranceObjective.insurance,
    { nullable: true },
  )
  objective_of_insurance: InsuranceObjective[];

  @ManyToMany(() => Hospital, (hospital) => hospital.insurances)
  @JoinTable({ name: 'insurances-hospitals' })
  hospitals: Hospital[];
}
