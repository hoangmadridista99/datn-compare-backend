import { Column, Entity } from 'typeorm';
import { BaseEntity } from '../../../../../shared/entities/base.entity';
import {
  IRoomType,
  IForHospitalization,
  IInpatientType,
} from '../../interfaces/Inpatient.interface';

/** Dịch vụ nội trú */
@Entity('inpatient-services')
export class Inpatient extends BaseEntity {
  /** Loại phòng */
  @Column({ type: 'jsonb' })
  room_type: IRoomType;

  /** Điều trị ung thư */
  @Column({ type: 'jsonb' })
  for_cancer: IInpatientType;

  /** Điều trị bệnh tật */
  @Column({ type: 'jsonb' })
  for_illnesses: IInpatientType;

  /** Điều trị tai nạn */
  @Column({ type: 'jsonb' })
  for_accidents: IInpatientType;

  /** Chi phí phẫu thuật */
  @Column({ type: 'jsonb' })
  for_surgical: IInpatientType;

  /** Chi phí nằm viện */
  @Column({ type: 'jsonb' })
  for_hospitalization: IForHospitalization;

  /** Hồi sức tích cực */
  @Column({ type: 'jsonb' })
  for_intensive_care: IInpatientType;

  /** Chi phí hành chính */
  @Column({ type: 'jsonb' })
  for_administrative: IInpatientType;

  /** Cấy ghép bộ phận */
  @Column({ type: 'jsonb' })
  for_organ_transplant: IInpatientType;
}
