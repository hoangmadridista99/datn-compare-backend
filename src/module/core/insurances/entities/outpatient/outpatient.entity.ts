import { Column, Entity, OneToOne } from 'typeorm';
import { BaseEntity } from '../../../../../shared/entities/base.entity';
import { IInsuranceDetailsType } from '../../interfaces/insurance.interface';
import { Insurance } from '../insurance.entity';

@Entity('outpatient-services')
export class Outpatient extends BaseEntity {
  @Column({ type: 'jsonb' })
  examination_and_treatment: IInsuranceDetailsType;

  @Column({ type: 'jsonb' })
  testing_and_diagnosis: IInsuranceDetailsType;

  @Column({ type: 'jsonb' })
  home_care: IInsuranceDetailsType;

  @Column({ type: 'jsonb' })
  due_to_accident: IInsuranceDetailsType;

  @Column({ type: 'jsonb' })
  due_to_illness: IInsuranceDetailsType;

  @Column({ type: 'jsonb' })
  due_to_cancer: IInsuranceDetailsType;

  @Column({ type: 'jsonb' })
  restore_functionality: IInsuranceDetailsType;

  @OneToOne(() => Insurance, (insurance) => insurance.obstetric)
  insurance: Insurance;
}
