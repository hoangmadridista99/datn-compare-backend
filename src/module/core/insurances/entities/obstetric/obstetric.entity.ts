import { Column, Entity } from 'typeorm';
import { BaseEntity } from '../../../../../shared/entities/base.entity';
import { IInsuranceDetailsType } from '../../interfaces/insurance.interface';

@Entity('obstetric-service')
export class Obstetric extends BaseEntity {
  /** Sinh thường */
  @Column({ type: 'jsonb' })
  give_birth_normally: IInsuranceDetailsType;

  /** Sinh mổ */
  @Column({ type: 'jsonb' })
  caesarean_section: IInsuranceDetailsType;

  /** Tai biến sản khoa */
  @Column({ type: 'jsonb' })
  obstetric_complication: IInsuranceDetailsType;

  /** Bất thường trong quá trình mang thai và các bệnh ý phát sinh nguyên nhân do thai kỳ */
  @Column({ type: 'jsonb' })
  give_birth_abnormality: IInsuranceDetailsType;

  /** Chi phí khám trước khi sinh */
  @Column({ type: 'jsonb' })
  after_give_birth_fee: IInsuranceDetailsType;

  /** Chi phí điều trị ngay sau khi xuất viện hoặc 1 lần tái khám*/
  @Column({ type: 'jsonb' })
  before_discharged: IInsuranceDetailsType;

  /** Chi phí chăm sóc trẻ sau sinh */
  @Column({ type: 'jsonb' })
  postpartum_childcare_cost: IInsuranceDetailsType;
}
