import { Column, Entity } from 'typeorm';
import { BaseEntity } from '../../../../../shared/entities/base.entity';
import { IInsuranceDetailsType } from '../../interfaces/insurance.interface';
import { IInsuranceScope } from '../../interfaces/customer-orientation.interface';

/** ĐỊNH HƯỚNG KHÁCH HÀNG */
@Entity('customer-orientation')
export class CustomerOrientation extends BaseEntity {
  /** Thời gian tiếp nhận và xử lý hợp đồng */
  @Column({ type: 'jsonb' })
  reception_and_processing_time: IInsuranceDetailsType;

  /** Tỉ lệ ký hợp đồng thành công */
  @Column({ type: 'jsonb', nullable: true })
  acceptance_rate: IInsuranceDetailsType;

  /** Thời gian hoàn thành hợp đồng */
  @Column({ type: 'jsonb', nullable: true })
  completion_time_deal: IInsuranceDetailsType;

  /** Quy trình kết thúc */
  @Column({ type: 'jsonb', nullable: true })
  end_of_process: IInsuranceDetailsType;

  /** Thời gian rút tiền */
  @Column({ type: 'jsonb', nullable: true })
  withdrawal_time: IInsuranceDetailsType;

  /** Phạm vi bảo hiểm */
  @Column({ type: 'jsonb', nullable: true })
  insurance_scope: IInsuranceScope;

  /** Thời gian chờ */
  @Column({ type: 'jsonb', nullable: true })
  waiting_period: IInsuranceDetailsType;

  /** Quy trình bồi thường */
  @Column({ type: 'jsonb', nullable: true })
  compensation_process: IInsuranceDetailsType;
}
