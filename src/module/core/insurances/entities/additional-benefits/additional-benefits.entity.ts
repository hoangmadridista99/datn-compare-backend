import { Column, Entity } from 'typeorm';
import { BaseEntity } from '../../../../../shared/entities/base.entity';
import { IInsuranceDetailsType } from '../../interfaces/insurance.interface';

/** QUYỀN LỢI BỔ SUNG */
@Entity('additional-benefits')
export class AdditionalBenefits extends BaseEntity {
  /** Quyền lợi bảo vệ trước [tử vong]/[thương tật] */
  @Column({ type: 'jsonb' })
  death_or_disability: IInsuranceDetailsType;

  /** Quyền lợi bảo vệ trước bệnh ung thư, hiểm nghèo */
  @Column({ type: 'jsonb' })
  serious_illnesses: IInsuranceDetailsType;

  /** Quyền lợi chăm sóc y tế */
  @Column({ type: 'jsonb' })
  health_care: IInsuranceDetailsType;

  /** Quyền lợi đầu tư của sản phẩm liên kết chung */
  @Column({ type: 'jsonb' })
  investment_benefit: IInsuranceDetailsType;

  /** Quyền lợi thưởng gia tăng giá trị hợp đồng */
  @Column({ type: 'jsonb' })
  increasing_value_bonus: IInsuranceDetailsType;

  /** Quyền lợi cho con */
  @Column({ type: 'jsonb' })
  for_child: IInsuranceDetailsType;

  /** Quyền lợi linh hoạt, đa dạng */
  @Column({ type: 'jsonb' })
  flexible_and_diverse: IInsuranceDetailsType;

  /** Quyền lợi kết thúc hợp đồng */
  @Column({ type: 'jsonb' })
  termination_benefits: IInsuranceDetailsType;

  /** Quyền lợi đáo hạn */
  @Column({ type: 'jsonb' })
  expiration_benefits: IInsuranceDetailsType;

  /** Quyền lợi miễn đóng phí */
  @Column({ type: 'jsonb' })
  fee_exemption: IInsuranceDetailsType;
}
