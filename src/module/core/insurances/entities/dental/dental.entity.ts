import { Column, Entity } from 'typeorm';
import { BaseEntity } from '../../../../../shared/entities/base.entity';
import { IInsuranceDetailsType } from '../../interfaces/insurance.interface';

@Entity('dental-services')
export class Dental extends BaseEntity {
  /** Khám và chẩn đoán */
  @Column({ type: 'jsonb' })
  examination_and_diagnosis: IInsuranceDetailsType;

  /** Viêm lợi(nướu) /viêm nha chu */
  @Column({ type: 'jsonb' })
  gingivitis: IInsuranceDetailsType;

  /** Chụp X-Quang và cận lâm sàng */

  @Column({ type: 'jsonb' })
  xray_and_diagnostic_imaging: IInsuranceDetailsType;

  /** Trám răng bằng chất liệu thông thường */
  @Column({ type: 'jsonb' })
  filling_teeth_basic: IInsuranceDetailsType;

  /** Điều trị tủy */
  @Column({ type: 'jsonb' })
  root_canal_treatment: IInsuranceDetailsType;

  /** Nhổ răng bệnh lý (bao gồm tiểu phẫu) , phẫu thuật cắt chóp răng, lấy u vôi răng */
  @Column({ type: 'jsonb' })
  dental_pathology: IInsuranceDetailsType;

  /** Lấy cao răng */
  @Column({ type: 'jsonb' })
  dental_calculus: IInsuranceDetailsType;
}
