import { Column, Entity } from 'typeorm';
import { BaseEntity } from '../../../../../shared/entities/base.entity';
import type {
  IInsuredPersonType,
  IAgeOfContractTerminationType,
  IAgeEligibilityType,
  IMonthlyFeeType,
  ITotalSumInsuredType,
  ICustomerSegmentType,
  IInsuranceMinimumFeeType,
  IDeadlineForPaymentType,
  IDeadlineForDealType,
  IProfessionType,
  ITerminationConditionsType,
} from '../../interfaces/terms.interface';

/** Điều kiện tham gia */
@Entity('terms')
export class Term extends BaseEntity {
  /** Độ tuổi tham gia  */
  @Column({ type: 'jsonb' })
  age_eligibility: IAgeEligibilityType;

  /** Thời hạn hợp đồng bảo hiểm */
  @Column({ type: 'jsonb' })
  deadline_for_deal: IDeadlineForDealType;

  /** Phí bảo hiểm tôi thiểu */
  @Column({ type: 'jsonb' })
  insurance_minimum_fee: IInsuranceMinimumFeeType;

  /** Thời hạn đóng phí */
  @Column({ type: 'jsonb' })
  deadline_for_payment: IDeadlineForPaymentType;

  /** Đối tượng được bảo hiểm */
  @Column({ type: 'jsonb' })
  insured_person: IInsuredPersonType;

  /** Nghề nghiệp */
  @Column({ type: 'jsonb' })
  profession: IProfessionType;

  /** Tuổi kết thúc hợp đồng */
  @Column({ type: 'jsonb' })
  age_of_contract_termination: IAgeOfContractTerminationType;

  /** Tổng tiền được bảo hiểm */
  @Column({ type: 'jsonb' })
  total_sum_insured: ITotalSumInsuredType;

  /** Số tiền phải bỏ ra hàng tháng */
  @Column({ type: 'jsonb' })
  monthly_fee: IMonthlyFeeType;

  /** Điều kiện kết thúc hợp đồng */
  @Column({ type: 'jsonb', nullable: true })
  termination_conditions: ITerminationConditionsType;

  /** Đối tượng khách hàng */
  @Column({ type: 'jsonb', nullable: true })
  customer_segment: ICustomerSegmentType;
}
