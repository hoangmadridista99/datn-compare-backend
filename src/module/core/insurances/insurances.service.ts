import { Injectable } from '@nestjs/common';
import { Between, FindOptionsWhere, SelectQueryBuilder } from 'typeorm';
import {
  getError,
  LIST_ERROR_CODES,
} from '../../../shared/constants/error/base.error';
import { Insurance } from './entities/insurance.entity';
import type {
  IInsuranceFilterAdmin,
  IInsuranceSortByClient,
  IVendorInsurancesFilter,
} from './interfaces/insurance.interface';
import { UserRole } from '../users/entities/user.entity';
import { setStartAndEndOfDateByTime } from '../../../shared/helper/system.helper';
import {
  NAME_QUERY,
  ADMIN_MONTHLY_FEE_QUERY,
  TABLE_GET_INTEREST_INSURANCES,
  COLUMNS_GET_INTEREST_INSURANCES,
  TABLE_GET_FILTER_INSURANCES_BY_OPERATOR,
  COLUMN_GET_FILTER_INSURANCE_BY_OPERATOR,
  TABLE_GET_FILTER_INSURANCE_BY_VENDOR,
  COLUMN_GET_FILTER_INSURANCE_BY_VENDOR,
} from './constants/query.constant';
import {
  InsuranceSortByClient,
  InsuranceStatusesTypes,
} from './enums/insurances.enum';
import { InsuranceRepositoryService } from './repository';
import { getPaginate } from '../../../shared/lib/paginate/paginate.lib';
import { PaginateQuery } from 'nestjs-paginate';
import { IUserInsurancesQueryFilter } from '../user-insurances/interfaces/user-insurance.interface';
import { IHospitalBody } from '../hospitals/interfaces/hospital.interface';
import { v4 as uuidV4 } from 'uuid';

@Injectable()
export class InsurancesService {
  constructor(private insuranceRepositoryService: InsuranceRepositoryService) {}

  getQueryBuilderBySortFilterClient(
    queryBuilder: SelectQueryBuilder<Insurance>,
    order_by?: IInsuranceSortByClient,
  ) {
    try {
      switch (order_by) {
        case InsuranceSortByClient.ALPHA_ASC:
        case InsuranceSortByClient.ALPHA_DESC:
          queryBuilder.orderBy(
            'name',
            order_by === InsuranceSortByClient.ALPHA_ASC ? 'ASC' : 'DESC',
          );
          break;
        case InsuranceSortByClient.MIN_PRICE:
          queryBuilder.orderBy("(terms.monthly_fee->>'from')::numeric", 'ASC');
          break;
        default:
          queryBuilder
            .orderBy('count', 'DESC')
            .orderBy("(terms.total_sum_insured->>'to')::numeric", 'DESC');
          break;
      }
    } catch (error) {
      console.log(
        '🚀 ~ file: insurances.service.ts:219 ~ InsurancesService ~ getQueryBuilderBySortFilterClient ~ error:',
        error,
      );
      throw error;
    }
  }

  handleSeparateHospital(data: IHospitalBody[]) {
    return [...data].reduce(
      (result, item) => {
        if (item.id) {
          result.hospitals.push(item);
        } else {
          result.hospitals_pending.push({
            id: uuidV4(),
            ...item,
          });
        }
        return result;
      },
      { hospitals: [], hospitals_pending: [] },
    );
  }

  getListInsurancesForVendor(filter: IVendorInsurancesFilter, userId: string) {
    try {
      const queryBuilder =
        this.insuranceRepositoryService.getSelectQueryBuilder(
          TABLE_GET_FILTER_INSURANCE_BY_VENDOR,
          COLUMN_GET_FILTER_INSURANCE_BY_VENDOR,
        );
      queryBuilder.where({ user: { id: userId } });
      if (filter.insurance_category_label) {
        queryBuilder.andWhere({
          insurance_category: {
            label: filter.insurance_category_label,
          },
        });
      }
      if (filter.status) {
        queryBuilder.andWhere({ status: filter.status });
      }
      if (filter.insurance_name) {
        queryBuilder.andWhere('insurances.name ilike :name', {
          name: `%${filter.insurance_name}%`,
        });
      }
      if (filter.created_at) {
        const { startOfDay, endOfDay } = setStartAndEndOfDateByTime(
          filter.created_at,
        );
        queryBuilder.andWhere({
          created_at: Between(startOfDay, endOfDay),
        });
      }
      if (filter.monthly_fee !== undefined) {
        queryBuilder.andWhere(ADMIN_MONTHLY_FEE_QUERY, {
          query: filter.monthly_fee,
        });
      }
      return queryBuilder;
    } catch (error) {
      throw error;
    }
  }

  async getFilterByAdminQueryBuilder(
    query: PaginateQuery,
    filter: IInsuranceFilterAdmin,
  ) {
    try {
      const queryBuilder =
        this.insuranceRepositoryService.getSelectQueryBuilder(
          TABLE_GET_FILTER_INSURANCES_BY_OPERATOR,
          COLUMN_GET_FILTER_INSURANCE_BY_OPERATOR,
        );
      const conditionRole =
        filter.role === UserRole.VENDOR
          ? { user: { role: UserRole.VENDOR } }
          : [
              { user: { role: UserRole.ADMIN } },
              { user: { role: UserRole.SUPER_ADMIN } },
            ];
      queryBuilder.andWhere(conditionRole);
      if (filter.status) {
        queryBuilder.andWhere({ status: filter.status });
      }
      if (filter.insurance_name) {
        queryBuilder.andWhere(NAME_QUERY, {
          name: `%${filter?.insurance_name}%`,
        });
      }
      if (filter.created_at) {
        const { startOfDay, endOfDay } = setStartAndEndOfDateByTime(
          filter.created_at,
        );
        queryBuilder.andWhere({
          created_at: Between(startOfDay, endOfDay),
        });
      }
      if (filter.total_sum_insured) {
        queryBuilder.andWhere(ADMIN_MONTHLY_FEE_QUERY, {
          query: filter.total_sum_insured,
        });
      }
      if (filter.insurance_type) {
        queryBuilder.andWhere({
          insurance_category: {
            label: filter.insurance_type,
          },
        });
      }
      return await getPaginate(query, queryBuilder);
    } catch (error) {
      throw error;
    }
  }

  async updateInsurance(body: Insurance) {
    return await this.insuranceRepositoryService.updateInsuranceStatusForAdmin(
      body,
    );
  }

  getInterestedInsurances(filter: IUserInsurancesQueryFilter) {
    try {
      const queryBuilder =
        this.insuranceRepositoryService.getSelectQueryBuilder(
          TABLE_GET_INTEREST_INSURANCES,
          COLUMNS_GET_INTEREST_INSURANCES,
        );
      queryBuilder
        .leftJoin('user_insurances.user', 'user')
        .where('insurances.total_interest > 0')
        .andWhere('insurances.status = :status', {
          status: InsuranceStatusesTypes.Approved,
        });
      if (filter.insurance_name) {
        queryBuilder.andWhere('insurances.name ilike :name', {
          name: `%${filter.insurance_name}%`,
        });
      }
      if (filter.insurance_category_label) {
        queryBuilder.andWhere({
          insurance_category: { label: filter.insurance_category_label },
        });
      }
      return queryBuilder;
    } catch (error) {
      console.log(
        '🚀 ~ file: insurances.service.ts:163 ~ InsurancesService ~ getInterestedInsurances ~ error:',
        error,
      );

      return null;
    }
  }

  async findOneByQuery(
    query: FindOptionsWhere<Insurance>[] | FindOptionsWhere<Insurance>,
  ): Promise<Insurance> {
    try {
      const queryBuilder =
        this.insuranceRepositoryService.getSelectQueryBuilder();
      return await queryBuilder
        .leftJoinAndSelect('insurances.user', 'user')
        .andWhere(query)
        .getOne();
    } catch (error) {
      console.log(
        '🚀 ~ file: insurances.service.ts:36 ~ InsurancesService ~ findOne ~ error:',
        error,
      );
      throw getError(LIST_ERROR_CODES.SS24001);
    }
  }
}
