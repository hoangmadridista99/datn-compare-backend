import { ApiProperty } from '@nestjs/swagger';
import { IInsuranceDetailsType } from '../../interfaces/insurance.interface';
import { EXAMPLE_API_PROPERTY } from '../../constants/insurances.constant';
import { IsNotEmpty, ValidateNested } from 'class-validator';
import { InsuranceDetailsTypeDto } from '../../validation/type-insurance.validation';
import { Type } from 'class-transformer';
import { LIST_ERROR_CODES } from '../../../../../shared/constants/error/base.error';

class CreateAdditionalBenefitsDto {
  @ApiProperty(EXAMPLE_API_PROPERTY)
  @IsNotEmpty({ message: LIST_ERROR_CODES.SS24405.split('.')[1] })
  @ValidateNested()
  @Type(() => InsuranceDetailsTypeDto)
  death_or_disability: IInsuranceDetailsType;

  @ApiProperty(EXAMPLE_API_PROPERTY)
  @IsNotEmpty({ message: LIST_ERROR_CODES.SS24406.split('.')[1] })
  @ValidateNested()
  @Type(() => InsuranceDetailsTypeDto)
  serious_illnesses: IInsuranceDetailsType;

  @ApiProperty(EXAMPLE_API_PROPERTY)
  @IsNotEmpty({ message: LIST_ERROR_CODES.SS24407.split('.')[1] })
  @ValidateNested()
  @Type(() => InsuranceDetailsTypeDto)
  health_care: IInsuranceDetailsType;

  @ApiProperty(EXAMPLE_API_PROPERTY)
  @IsNotEmpty({ message: LIST_ERROR_CODES.SS24408.split('.')[1] })
  @ValidateNested()
  @Type(() => InsuranceDetailsTypeDto)
  investment_benefit: IInsuranceDetailsType;

  @ApiProperty(EXAMPLE_API_PROPERTY)
  @IsNotEmpty({ message: LIST_ERROR_CODES.SS24409.split('.')[1] })
  @ValidateNested()
  @Type(() => InsuranceDetailsTypeDto)
  increasing_value_bonus: IInsuranceDetailsType;

  @ApiProperty(EXAMPLE_API_PROPERTY)
  @IsNotEmpty({ message: LIST_ERROR_CODES.SS24410.split('.')[1] })
  @ValidateNested()
  @Type(() => InsuranceDetailsTypeDto)
  for_child: IInsuranceDetailsType;

  @ApiProperty(EXAMPLE_API_PROPERTY)
  @IsNotEmpty({ message: LIST_ERROR_CODES.SS24411.split('.')[1] })
  @ValidateNested()
  @Type(() => InsuranceDetailsTypeDto)
  flexible_and_diverse: IInsuranceDetailsType;

  @ApiProperty(EXAMPLE_API_PROPERTY)
  @IsNotEmpty({ message: LIST_ERROR_CODES.SS24412.split('.')[1] })
  @ValidateNested()
  @Type(() => InsuranceDetailsTypeDto)
  termination_benefits: IInsuranceDetailsType;

  @ApiProperty(EXAMPLE_API_PROPERTY)
  @IsNotEmpty({ message: LIST_ERROR_CODES.SS24413.split('.')[1] })
  @ValidateNested()
  @Type(() => InsuranceDetailsTypeDto)
  expiration_benefits: IInsuranceDetailsType;

  @ApiProperty(EXAMPLE_API_PROPERTY)
  @IsNotEmpty({ message: LIST_ERROR_CODES.SS24416.split('.')[1] })
  @ValidateNested()
  @Type(() => InsuranceDetailsTypeDto)
  fee_exemption: IInsuranceDetailsType;
}

export class CreateLifeAdditionalBenefitsDto extends CreateAdditionalBenefitsDto {}
