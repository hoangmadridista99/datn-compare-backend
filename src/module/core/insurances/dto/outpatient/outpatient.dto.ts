import { ApiProperty } from '@nestjs/swagger';
import { EXAMPLE_HEALTH_INSURANCE_DTO } from '../../constants/insurances.constant';
import { HealthInsuranceDefaultType } from '../../validation/type-insurance.validation';
import { ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';
import { IOutpatientType } from '../../interfaces/outpatient.interface';

export class CreateOutpatientDto {
  @ApiProperty(EXAMPLE_HEALTH_INSURANCE_DTO)
  @ValidateNested()
  @Type(() => HealthInsuranceDefaultType)
  examination_and_treatment: IOutpatientType;

  @ApiProperty(EXAMPLE_HEALTH_INSURANCE_DTO)
  @ValidateNested()
  @Type(() => HealthInsuranceDefaultType)
  testing_and_diagnosis: IOutpatientType;

  @ApiProperty(EXAMPLE_HEALTH_INSURANCE_DTO)
  @ValidateNested()
  @Type(() => HealthInsuranceDefaultType)
  home_care: IOutpatientType;

  @ApiProperty(EXAMPLE_HEALTH_INSURANCE_DTO)
  @ValidateNested()
  @Type(() => HealthInsuranceDefaultType)
  due_to_accident: IOutpatientType;

  @ApiProperty(EXAMPLE_HEALTH_INSURANCE_DTO)
  @ValidateNested()
  @Type(() => HealthInsuranceDefaultType)
  due_to_illness: IOutpatientType;

  @ApiProperty(EXAMPLE_HEALTH_INSURANCE_DTO)
  @ValidateNested()
  @Type(() => HealthInsuranceDefaultType)
  due_to_cancer: IOutpatientType;

  @ApiProperty(EXAMPLE_HEALTH_INSURANCE_DTO)
  @ValidateNested()
  @Type(() => HealthInsuranceDefaultType)
  restore_functionality: IOutpatientType;
}
