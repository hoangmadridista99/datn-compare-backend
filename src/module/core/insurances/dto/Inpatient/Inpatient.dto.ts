import { ApiProperty, OmitType, PickType } from '@nestjs/swagger';
import {
  IRoomType,
  IForHospitalization,
  IInpatientType,
} from '../../interfaces/Inpatient.interface';
import { Type } from 'class-transformer';
import {
  IsArray,
  IsEnum,
  IsNotEmpty,
  ValidateIf,
  ValidateNested,
} from 'class-validator';
import {
  HealthInsuranceDefaultType,
  TermInsuranceTypeDefault,
} from '../../validation/type-insurance.validation';
import { RoomTypes, TimerTypes } from '../../enums/insurances.enum';
import { EXAMPLE_HEALTH_INSURANCE_DTO } from '../../constants/insurances.constant';

export class CreateInpatientDto {
  @ApiProperty({
    example: {
      level: 'high',
      type: 'one-bed',
      description: 'description',
    },
  })
  @ValidateNested()
  @Type(() => RoomTypeDto)
  room_type: IRoomType;

  @ApiProperty(EXAMPLE_HEALTH_INSURANCE_DTO)
  @ValidateNested()
  @Type(() => HealthInsuranceDefaultType)
  for_cancer: IInpatientType;

  @ApiProperty(EXAMPLE_HEALTH_INSURANCE_DTO)
  @ValidateNested()
  @Type(() => HealthInsuranceDefaultType)
  for_illnesses: IInpatientType;

  @ApiProperty(EXAMPLE_HEALTH_INSURANCE_DTO)
  @ValidateNested()
  @Type(() => HealthInsuranceDefaultType)
  for_accidents: IInpatientType;

  @ApiProperty(EXAMPLE_HEALTH_INSURANCE_DTO)
  @ValidateNested()
  @Type(() => HealthInsuranceDefaultType)
  for_surgical: IInpatientType;

  @ApiProperty({
    example: {
      level: 'high',
      value: 60,
      type: TimerTypes.ByDay,
    },
  })
  @ValidateNested()
  @Type(() => ForHospitalizationDto)
  for_hospitalization: IForHospitalization;

  @ApiProperty(EXAMPLE_HEALTH_INSURANCE_DTO)
  @ValidateNested()
  @Type(() => HealthInsuranceDefaultType)
  for_intensive_care: IInpatientType;

  @ApiProperty(EXAMPLE_HEALTH_INSURANCE_DTO)
  @ValidateNested()
  @Type(() => HealthInsuranceDefaultType)
  for_administrative: IInpatientType;

  @ApiProperty(EXAMPLE_HEALTH_INSURANCE_DTO)
  @ValidateNested()
  @Type(() => HealthInsuranceDefaultType)
  for_organ_transplant: IInpatientType;
}

class RoomTypeDto extends PickType(TermInsuranceTypeDefault, [
  'level',
  'description',
]) {
  @ValidateIf((_, value) => value !== null)
  @IsArray()
  @IsEnum(RoomTypes, { each: true })
  values: RoomTypes[];
}

class ForHospitalizationDto extends OmitType(TermInsuranceTypeDefault, [
  'from',
  'to',
  'type',
  'description',
]) {
  @IsEnum(TimerTypes)
  @IsNotEmpty()
  @ValidateIf((_object, value) => value !== null)
  type: `${TimerTypes}`;
}
