import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { ValidateNested } from 'class-validator';
import { EXAMPLE_HEALTH_INSURANCE_DTO } from '../../constants/insurances.constant';
import { HealthInsuranceDefaultType } from '../../validation/type-insurance.validation';
import { IDentalType } from '../../interfaces/dental.interface';

export class CreateDentalDto {
  @ApiProperty(EXAMPLE_HEALTH_INSURANCE_DTO)
  @ValidateNested()
  @Type(() => HealthInsuranceDefaultType)
  examination_and_diagnosis: IDentalType;

  @ApiProperty(EXAMPLE_HEALTH_INSURANCE_DTO)
  @ValidateNested()
  @Type(() => HealthInsuranceDefaultType)
  gingivitis: IDentalType;

  @ApiProperty(EXAMPLE_HEALTH_INSURANCE_DTO)
  @ValidateNested()
  @Type(() => HealthInsuranceDefaultType)
  xray_and_diagnostic_imaging: IDentalType;

  @ApiProperty(EXAMPLE_HEALTH_INSURANCE_DTO)
  @ValidateNested()
  @Type(() => HealthInsuranceDefaultType)
  filling_teeth_basic: IDentalType;

  @ApiProperty(EXAMPLE_HEALTH_INSURANCE_DTO)
  @ValidateNested()
  @Type(() => HealthInsuranceDefaultType)
  root_canal_treatment: IDentalType;

  @ApiProperty(EXAMPLE_HEALTH_INSURANCE_DTO)
  @ValidateNested()
  @Type(() => HealthInsuranceDefaultType)
  dental_pathology: IDentalType;

  @ApiProperty(EXAMPLE_HEALTH_INSURANCE_DTO)
  @ValidateNested()
  @Type(() => HealthInsuranceDefaultType)
  dental_calculus: IDentalType;
}
