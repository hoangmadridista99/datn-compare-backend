import { ApiProperty, OmitType } from '@nestjs/swagger';
import {
  IInsuranceDetailsTypeLevel,
  IProfessionType,
} from '../../interfaces/insurance.interface';
import { EXAMPLE_API_PROPERTY } from '../../constants/insurances.constant';
import {
  IAgeEligibilityType,
  IAgeOfContractTerminationType,
  IInsuredPersonType,
  IMonthlyFeeType,
  ITotalSumInsuredType,
  ICustomerSegmentType,
  IInsuranceMinimumFeeType,
  IDeadlineForPaymentType,
  ITerminationConditionsType,
  IDeadlineForDealType,
  IValueType,
  IPaymentPolicyValue,
  IInsuredPersonValue,
  IProfessionValue,
  ICustomerSegmentValue,
} from '../../interfaces/terms.interface';
import {
  IsArray,
  IsEnum,
  IsNotEmpty,
  IsNumber,
  IsString,
  ValidateIf,
  ValidateNested,
} from 'class-validator';
import { Type } from 'class-transformer';
import { LIST_ERROR_CODES } from '../../../../../shared/constants/error/base.error';
import {
  CustomerSegmentTypes,
  InsuredPersonTypes,
  PaymentPolicyTypes,
  PriorityLevelTypes,
  ProfessionTypes,
  ValueType,
} from '../../enums/insurances.enum';

class TermsTypeDefault {
  @IsEnum(PriorityLevelTypes)
  @IsNotEmpty()
  level: IInsuranceDetailsTypeLevel;
}
class TermsTypeDefaultHaveFieldDescription extends TermsTypeDefault {
  @ValidateIf((_, value) => value !== null)
  @IsString()
  description: string;
}
class TermsAgeEligibilityTypeDto extends TermsTypeDefaultHaveFieldDescription {
  @ValidateIf((_, value) => value !== null)
  @IsNumber()
  from: number;

  @ValidateIf((_, value) => value !== null)
  @IsNumber()
  to: number;
}
class TermsAgeOfContractTerminationTypeDto extends TermsAgeEligibilityTypeDto {
  @IsEnum(ValueType)
  @ValidateIf((_, value) => value !== null)
  type: IValueType;
}
class TermsDeadlineForDealTypeDto extends TermsAgeOfContractTerminationTypeDto {
  @IsNumber()
  @ValidateIf((_, value) => value !== null)
  value: number;
}
class TermsDeadlineForPaymentTypeDto extends TermsTypeDefaultHaveFieldDescription {
  @IsEnum(PaymentPolicyTypes, { each: true })
  @IsArray()
  @ValidateIf((_, value) => value !== null)
  value: IPaymentPolicyValue[];
}
class TermsInsuranceMinimumFeeDto extends TermsTypeDefaultHaveFieldDescription {
  @IsNumber()
  @ValidateIf((_, value) => value !== null)
  value: number;
}
class TermsInsuredPersonDto extends TermsTypeDefaultHaveFieldDescription {
  @IsArray()
  @IsEnum(InsuredPersonTypes, { each: true })
  @ValidateIf((_, value) => value !== null)
  value: IInsuredPersonValue[];
}
class TermsMonthlyFeeDto extends TermsAgeEligibilityTypeDto {}
class TermsProfessionDto extends TermsTypeDefault {
  @IsArray()
  @IsEnum(ProfessionTypes, { each: true })
  @ValidateIf((_, value) => value !== null)
  text: IProfessionValue[];
}
class TermsTerminationConditionsDto extends TermsTypeDefault {
  @IsString()
  @ValidateIf((_, value) => value !== null)
  text: string;
}
class TermsTotalSumInsuredDto extends TermsAgeEligibilityTypeDto {}
class TermsCustomerSegmentDto extends TermsTypeDefault {
  @IsEnum(CustomerSegmentTypes, { each: true })
  @ValidateIf((_, value) => value !== null)
  text: ICustomerSegmentValue[];
}

class CreateTermDto {
  @ApiProperty({
    example: {
      level: 'high',
      from: 10,
      to: 100,
      description: 'example',
    },
  })
  @IsNotEmpty({ message: LIST_ERROR_CODES.SS24423.split('.')[1] })
  @ValidateNested()
  @Type(() => TermsAgeEligibilityTypeDto)
  age_eligibility: IAgeEligibilityType;

  @ApiProperty({
    example: {
      level: 'high',
      type: 'old',
      description: 'example',
      value: null,
      from: 10,
      to: 60,
    },
  })
  @ValidateNested()
  @Type(() => TermsDeadlineForDealTypeDto)
  deadline_for_deal: IDeadlineForDealType;

  @ApiProperty({
    example: {
      level: 'high',
      value: 100,
      description: 'example',
    },
  })
  @IsNotEmpty({ message: LIST_ERROR_CODES.SS24424.split('.')[1] })
  @ValidateNested()
  @Type(() => TermsInsuranceMinimumFeeDto)
  insurance_minimum_fee: IInsuranceMinimumFeeType;

  @ApiProperty({
    example: {
      level: 'high',
      value: [InsuredPersonTypes.OTHER_PERSON],
      description: 'example',
    },
  })
  @IsNotEmpty({ message: LIST_ERROR_CODES.SS24425.split('.')[1] })
  @ValidateNested()
  @Type(() => TermsInsuredPersonDto)
  insured_person: IInsuredPersonType;

  @ApiProperty({
    example: {
      level: 'high',
      text: ['STUDENT'],
    },
  })
  @IsNotEmpty({ message: LIST_ERROR_CODES.SS24430.split('.')[1] })
  @ValidateNested()
  @Type(() => TermsProfessionDto)
  profession: IProfessionType;

  @ApiProperty({
    example: {
      level: 'high',
      description: 'example',
      value: ['monthly'],
    },
  })
  @IsNotEmpty({ message: LIST_ERROR_CODES.SS24431.split('.')[1] })
  @ValidateNested()
  @Type(() => TermsDeadlineForPaymentTypeDto)
  deadline_for_payment: IDeadlineForPaymentType;

  @ApiProperty({
    example: {
      level: 'high',
      type: 'year',
      description: 'example',
      from: 10,
      to: 100,
    },
  })
  @IsNotEmpty({ message: LIST_ERROR_CODES.SS24432.split('.')[1] })
  @ValidateNested()
  @Type(() => TermsAgeOfContractTerminationTypeDto)
  age_of_contract_termination: IAgeOfContractTerminationType;

  @ApiProperty(EXAMPLE_API_PROPERTY)
  @IsNotEmpty({ message: LIST_ERROR_CODES.SS24433.split('.')[1] })
  @ValidateNested()
  @Type(() => TermsTerminationConditionsDto)
  termination_conditions: ITerminationConditionsType;

  @ApiProperty({
    example: {
      level: 'high',
      description: 'example',
      from: 10,
      to: 100,
    },
  })
  @IsNotEmpty({ message: LIST_ERROR_CODES.SS24434.split('.')[1] })
  @ValidateNested()
  @Type(() => TermsTotalSumInsuredDto)
  total_sum_insured: ITotalSumInsuredType;

  @ApiProperty({
    example: {
      level: 'high',
      description: 'example',
      from: 10,
      to: 100,
    },
  })
  @IsNotEmpty({ message: LIST_ERROR_CODES.SS24435.split('.')[1] })
  @ValidateNested()
  @Type(() => TermsMonthlyFeeDto)
  monthly_fee: IMonthlyFeeType;

  @ApiProperty({
    example: {
      level: 'high',
      text: ['FAMILY'],
    },
  })
  @IsNotEmpty({ message: LIST_ERROR_CODES.SS24430.split('.')[1] })
  @ValidateNested()
  @Type(() => TermsCustomerSegmentDto)
  customer_segment: ICustomerSegmentType;
}

export class CreateTermsByLifeInsuranceDto extends OmitType(CreateTermDto, [
  'customer_segment',
]) {}
export class CreateTermsByHealthInsuranceDto extends OmitType(CreateTermDto, [
  'termination_conditions',
]) {}
