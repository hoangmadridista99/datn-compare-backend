import { ApiProperty, PickType } from '@nestjs/swagger';
import { IInsuranceDetailsType } from '../../interfaces/insurance.interface';
import { EXAMPLE_API_PROPERTY } from '../../constants/insurances.constant';
import { Type } from 'class-transformer';
import { IsNotEmpty, ValidateNested } from 'class-validator';
import {
  InsuranceScopeTypeDto,
  InsuranceDetailsTypeDto,
  WaitingPeriodTypeDto,
  CompensationProcessDto,
} from '../../validation/type-insurance.validation';
import { LIST_ERROR_CODES } from '../../../../../shared/constants/error/base.error';
import {
  ICompensationProcess,
  IInsuranceScope,
  IWaitingPeriodType,
} from '../../interfaces/customer-orientation.interface';

const CreateCustomerOrientationByLifeInsuranceKey: (keyof CustomerOrientationDto)[] =
  [
    'acceptance_rate',
    'completion_time_deal',
    'end_of_process',
    'withdrawal_time',
    'reception_and_processing_time',
  ];
const CreateCustomerOrientationByHealthInsuranceKey: (keyof CustomerOrientationDto)[] =
  [
    'reception_and_processing_time',
    'insurance_scope',
    'waiting_period',
    'compensation_process',
  ];

class CustomerOrientationDto {
  @ApiProperty(EXAMPLE_API_PROPERTY)
  @IsNotEmpty({ message: LIST_ERROR_CODES.SS24418.split('.')[1] })
  @ValidateNested()
  @Type(() => InsuranceDetailsTypeDto)
  acceptance_rate: IInsuranceDetailsType;

  @ApiProperty(EXAMPLE_API_PROPERTY)
  @IsNotEmpty({ message: LIST_ERROR_CODES.SS24419.split('.')[1] })
  @ValidateNested()
  @Type(() => InsuranceDetailsTypeDto)
  completion_time_deal: IInsuranceDetailsType;

  @ApiProperty(EXAMPLE_API_PROPERTY)
  @IsNotEmpty({ message: LIST_ERROR_CODES.SS24420.split('.')[1] })
  @ValidateNested()
  @Type(() => InsuranceDetailsTypeDto)
  end_of_process: IInsuranceDetailsType;

  @ApiProperty(EXAMPLE_API_PROPERTY)
  @IsNotEmpty({ message: LIST_ERROR_CODES.SS24421.split('.')[1] })
  @ValidateNested()
  @Type(() => InsuranceDetailsTypeDto)
  withdrawal_time: IInsuranceDetailsType;

  @ApiProperty(EXAMPLE_API_PROPERTY)
  @IsNotEmpty({ message: LIST_ERROR_CODES.SS24422.split('.')[1] })
  @ValidateNested()
  @Type(() => InsuranceDetailsTypeDto)
  reception_and_processing_time: IInsuranceDetailsType;

  @ApiProperty({
    example: {
      level: 'none',
      values: ['death', 'inpatient', 'outpatient', 'dental', 'obstetric'],
    },
  })
  @IsNotEmpty({ message: LIST_ERROR_CODES.SS24422.split('.')[1] })
  @ValidateNested()
  @Type(() => InsuranceScopeTypeDto)
  insurance_scope: IInsuranceScope;

  @ApiProperty({
    example: {
      level: 'high',
      from: 10,
      to: 100,
      description: 'description',
    },
  })
  @IsNotEmpty({ message: LIST_ERROR_CODES.SS24422.split('.')[1] })
  @ValidateNested()
  @Type(() => WaitingPeriodTypeDto)
  waiting_period: IWaitingPeriodType;

  @ApiProperty({
    example: {
      level: 'none',
      description: 'description',
    },
  })
  @IsNotEmpty({ message: LIST_ERROR_CODES.SS24422.split('.')[1] })
  @ValidateNested()
  @Type(() => CompensationProcessDto)
  compensation_process: ICompensationProcess;
}

export class CreateCustomerOrientationByLifeInsuranceDto extends PickType(
  CustomerOrientationDto,
  CreateCustomerOrientationByLifeInsuranceKey,
) {}
export class CreateCustomerOrientationByHealthInsuranceDto extends PickType(
  CustomerOrientationDto,
  CreateCustomerOrientationByHealthInsuranceKey,
) {}
