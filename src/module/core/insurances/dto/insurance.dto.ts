import { ApiProperty, OmitType } from '@nestjs/swagger';
import {
  IsArray,
  IsEnum,
  IsNotEmpty,
  IsNumberString,
  IsOptional,
  IsString,
  IsUUID,
  Validate,
  ValidateIf,
  ValidateNested,
} from 'class-validator';
import { LIST_ERROR_CODES } from '../../../../shared/constants/error/base.error';
import { IInsuranceObjective } from '../../insurance-objectives/interfaces/insurance-objectives.interface';
import {
  CreateCustomerOrientationByLifeInsuranceDto,
  CreateCustomerOrientationByHealthInsuranceDto,
} from './customer-orientation/customer-orientation.dto';
import { Type } from 'class-transformer';
import { CreateLifeAdditionalBenefitsDto } from './additional-benefits/additional-benefits.dto';
import {
  CreateTermsByLifeInsuranceDto,
  CreateTermsByHealthInsuranceDto,
} from './terms/term.dto';
import type { ICreateLifeAdditionalBenefit } from '../interfaces/additional-benefits.interface';
import {
  ICreateTermsByHealthInsurance,
  ICreateTermsByLifeInsurance,
} from '../interfaces/terms.interface';
import {
  ICreateCustomerOrientationByHealthInsurance,
  ICreateCustomerOrientationByLifeInsurance,
} from '../interfaces/customer-orientation.interface';
import {
  IInsuranceHealthAdditionalBenefit,
  IInsuranceStatuses,
} from '../interfaces/insurance.interface';
import {
  InsuranceHealthAdditionalBenefit,
  InsuranceSort,
  InsuranceStatusesTypes,
} from '../enums/insurances.enum';
import { CompanyExistRule } from '../../../../shared/decorators/company-id-validation.decorator';
import {
  ICategoryOfInsurance,
  IInsuranceCategoriesTypes,
} from '../../insurance-categories/interfaces/insurance-categories.interface';
import { CreateDentalDto } from './dental/dental.dto';
import { CreateInpatientDto } from './Inpatient/Inpatient.dto';
import { CreateObstetricDto } from './obstetric/obstetric.dto';
import { CreateOutpatientDto } from './outpatient/outpatient.dto';
import { IDental } from '../interfaces/dental.interface';
import { IInpatient } from '../interfaces/Inpatient.interface';
import { IObstetric } from '../interfaces/obstetric.interface';
import { IOutpatient } from '../interfaces/outpatient.interface';
import { IHospitalBody } from '../../hospitals/interfaces/hospital.interface';
import { InsuranceCategoryNames } from '../../insurance-categories/enums/insurance-category.enum';
import { InsuranceHospitalDto } from '../../hospitals/dto/hospital.dto';

const CategoryExample: ICategoryOfInsurance = {
  id: '1fd8b863-9b7b-4586-aead-191499a44b7b',
  created_at: new Date(),
  label: InsuranceCategoryNames.Life,
};
const PdfExample =
  'https://dev.api.insurance.just.engineer//v1/api/client/file?key=pdf%2F1684824060.327_Giao%20trinh%20dai%20ly%20bao%20hiem%20-%20co%20ban.pdf';

class CreateBasicInsuranceDto {
  @ApiProperty({ example: 'An Thịnh Đầu Tư' })
  @IsString({ message: LIST_ERROR_CODES.SS24402 })
  @IsNotEmpty({ message: LIST_ERROR_CODES.SS24401 })
  name: string;

  @ApiProperty({ example: CategoryExample })
  @IsNotEmpty()
  insurance_category: ICategoryOfInsurance;

  @ApiProperty({ example: '90823bac-a14a-4abf-b0f4-5b59afc03eb9' })
  @IsOptional()
  @IsUUID(4, { message: LIST_ERROR_CODES.SS24417 })
  @Validate(CompanyExistRule)
  company_id: string;

  @ApiProperty()
  @IsString()
  description_insurance: string;
}
export class AdminVerifyInsuranceDto {
  @ApiProperty({ example: InsuranceStatusesTypes.Approved })
  @IsNotEmpty({ message: LIST_ERROR_CODES.SS24437 })
  @IsEnum(InsuranceStatusesTypes, { message: LIST_ERROR_CODES.SS24438 })
  status: IInsuranceStatuses;

  @ApiProperty({ example: 'reason' })
  @IsOptional()
  @IsString()
  reason: string;
}
export class UpdateRatingInsuranceDto {
  rating_scores: number;
  total_rating: number;
}

/** Bảo hiểm nhân thọ */
export class CreateLifeInsuranceDto extends CreateBasicInsuranceDto {
  @ApiProperty()
  @IsString()
  key_benefits: string;

  @ApiProperty({ example: PdfExample })
  @IsOptional()
  documentation_url: string;

  @ApiProperty({ example: PdfExample })
  @IsOptional()
  benefits_illustration_table: string;

  @ApiProperty({})
  @IsArray({ message: LIST_ERROR_CODES.SS24436 })
  objective_of_insurance: IInsuranceObjective[];

  @ApiProperty({ type: CreateLifeAdditionalBenefitsDto })
  @ValidateNested({ each: true })
  @Type(() => CreateLifeAdditionalBenefitsDto)
  additional_benefits: ICreateLifeAdditionalBenefit;

  @ApiProperty({ type: CreateCustomerOrientationByLifeInsuranceDto })
  @ValidateNested({ each: true })
  @Type(() => CreateCustomerOrientationByLifeInsuranceDto)
  customer_orientation: ICreateCustomerOrientationByLifeInsurance;

  @ApiProperty({ type: CreateTermsByLifeInsuranceDto })
  @ValidateNested({ each: true })
  @Type(() => CreateTermsByLifeInsuranceDto)
  terms: ICreateTermsByLifeInsurance;
}
export class CreateHealthInsuranceDto extends CreateBasicInsuranceDto {
  @ApiProperty({ type: CreateCustomerOrientationByHealthInsuranceDto })
  @ValidateNested({ each: true })
  @Type(() => CreateCustomerOrientationByHealthInsuranceDto)
  customer_orientation: ICreateCustomerOrientationByHealthInsurance;

  @ApiProperty({ type: CreateTermsByHealthInsuranceDto })
  @ValidateNested({ each: true })
  @Type(() => CreateTermsByHealthInsuranceDto)
  terms: ICreateTermsByHealthInsurance;

  @ApiProperty({ type: CreateInpatientDto })
  @ValidateNested({ each: true })
  @Type(() => CreateInpatientDto)
  inpatient: IInpatient;

  @ApiProperty({ type: CreateObstetricDto })
  @ValidateNested()
  @ValidateIf((_, value) => value !== null)
  @Type(() => CreateObstetricDto)
  obstetric: IObstetric;

  @ApiProperty({ type: CreateDentalDto })
  @ValidateIf((_, value) => value !== null)
  @ValidateNested({ each: true })
  @Type(() => CreateDentalDto)
  dental: IDental;

  @ApiProperty({ type: CreateOutpatientDto })
  @ValidateNested({ each: true })
  @Type(() => CreateOutpatientDto)
  outpatient: IOutpatient;

  @ApiProperty({
    example: [
      {
        id: 'a0eafd3c-ef98-4abb-95c6-eb2b8dbdde9b',
        created_at: '2023-06-27T04:26:24.297Z',
        updated_at: '2023-06-27T04:26:24.297Z',
        deleted_at: null,
        name: 'Bệnh viện Phú Xuyên',
        address: 'Phú Xuyên Hà Nội',
        province: 'Phú Xuyên',
        district: 'Tp Hà Nội',
      },
      {
        id: '6ffeffd5-ce58-4640-a45f-4dcbfbd813be',
        created_at: '2023-06-27T04:28:15.349Z',
        updated_at: '2023-06-27T04:28:15.349Z',
        deleted_at: null,
        name: 'Bệnh viện Phú Xuyên 1',
        address: 'Phú Xuyên Hà Nội',
        province: 'Phú Xuyên',
        district: 'Tp Hà Nội',
      },
    ],
  })
  @IsOptional()
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => InsuranceHospitalDto)
  hospitals: IHospitalBody[];

  @ApiProperty({
    name: 'additional_benefit',
    isArray: true,
    enum: InsuranceHealthAdditionalBenefit,
    example: [InsuranceHealthAdditionalBenefit.DENTAL],
  })
  @IsArray()
  @IsEnum(InsuranceHealthAdditionalBenefit, { each: true })
  additional_benefit: IInsuranceHealthAdditionalBenefit[];
}
export class UpdateLifeInsuranceDto extends OmitType(CreateLifeInsuranceDto, [
  'company_id',
  'insurance_category',
]) {
  @ApiProperty({ example: '90823bac-a14a-4abf-b0f4-5b59afc03eb9' })
  @IsOptional()
  @IsUUID(4, { message: LIST_ERROR_CODES.SS24417 })
  @Validate(CompanyExistRule)
  @IsOptional()
  company_id: string;
}
export class UpdateHealthInsuranceDto extends OmitType(
  CreateHealthInsuranceDto,
  ['company_id', 'insurance_category'],
) {
  @ApiProperty({ example: '90823bac-a14a-4abf-b0f4-5b59afc03eb9' })
  @IsOptional()
  @IsUUID(4, { message: LIST_ERROR_CODES.SS24417 })
  @Validate(CompanyExistRule)
  @IsOptional()
  company_id: string;
}

export class InsuranceInterestFilterByOperatorDto {
  @IsOptional()
  @IsString()
  insurance_name: string;

  @IsOptional()
  @IsEnum(InsuranceCategoryNames)
  insurance_category_label: IInsuranceCategoriesTypes;

  @IsOptional()
  @IsEnum(InsuranceSort)
  total_interest: `${InsuranceSort}`;

  @IsNumberString()
  page: number;

  @IsNumberString()
  limit: number;
}
