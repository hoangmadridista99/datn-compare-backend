import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { ValidateNested } from 'class-validator';
import { EXAMPLE_HEALTH_INSURANCE_DTO } from '../../constants/insurances.constant';
import { HealthInsuranceDefaultType } from '../../validation/type-insurance.validation';
import { IObstetricType } from '../../interfaces/obstetric.interface';

export class CreateObstetricDto {
  @ApiProperty(EXAMPLE_HEALTH_INSURANCE_DTO)
  @ValidateNested()
  @Type(() => HealthInsuranceDefaultType)
  give_birth_normally: IObstetricType;

  @ApiProperty(EXAMPLE_HEALTH_INSURANCE_DTO)
  @ValidateNested()
  @Type(() => HealthInsuranceDefaultType)
  caesarean_section: IObstetricType;

  @ApiProperty(EXAMPLE_HEALTH_INSURANCE_DTO)
  @ValidateNested()
  @Type(() => HealthInsuranceDefaultType)
  obstetric_complication: IObstetricType;

  @ApiProperty(EXAMPLE_HEALTH_INSURANCE_DTO)
  @ValidateNested()
  @Type(() => HealthInsuranceDefaultType)
  give_birth_abnormality: IObstetricType;

  @ApiProperty(EXAMPLE_HEALTH_INSURANCE_DTO)
  @ValidateNested()
  @Type(() => HealthInsuranceDefaultType)
  after_give_birth_fee: IObstetricType;

  @ApiProperty(EXAMPLE_HEALTH_INSURANCE_DTO)
  @ValidateNested()
  @Type(() => HealthInsuranceDefaultType)
  before_discharged: IObstetricType;

  @ApiProperty(EXAMPLE_HEALTH_INSURANCE_DTO)
  @ValidateNested()
  @Type(() => HealthInsuranceDefaultType)
  postpartum_childcare_cost: IObstetricType;
}
