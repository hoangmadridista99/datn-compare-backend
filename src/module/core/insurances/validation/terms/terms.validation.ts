import { PickType, OmitType } from '@nestjs/swagger';
import { IsArray, IsEnum, IsNotEmpty, IsOptional } from 'class-validator';
import {
  InsuredPersonTypes,
  ProfessionTypes,
  PaymentPolicyTypes,
  CustomerSegmentTypes,
} from '../../enums/insurances.enum';
import {
  IInsuredPersonValue,
  IPaymentPolicyValue,
} from '../../interfaces/terms.interface';
import {
  TermInsuranceTypeDefault,
  InsuranceDetailsTypeDto,
} from '../type-insurance.validation';
import {
  ITextCustomerSegmentType,
  ITextOfProfessionType,
} from '../../interfaces/insurance.interface';

export class InsuranceMinimumFeeType extends OmitType(
  TermInsuranceTypeDefault,
  ['from', 'to', 'type'],
) {}

export class InsuredPersonTypeDto extends PickType(TermInsuranceTypeDefault, [
  'level',
  'description',
]) {
  @IsOptional()
  @IsArray()
  @IsEnum(InsuredPersonTypes, { each: true })
  @IsNotEmpty()
  value: IInsuredPersonValue[];
}

export class DeadlineForDealTypeDto extends TermInsuranceTypeDefault {}

export class ProfessionTypeDto extends OmitType(InsuranceDetailsTypeDto, [
  'text',
]) {
  @IsOptional()
  @IsArray()
  @IsEnum(ProfessionTypes, { each: true })
  text: ITextOfProfessionType[];
}

export class CustomerSegmentTypeDto extends OmitType(InsuranceDetailsTypeDto, [
  'text',
]) {
  @IsOptional()
  @IsArray()
  @IsEnum(CustomerSegmentTypes, { each: true })
  text: ITextCustomerSegmentType;
}

export class DeadlineForPaymentTypeDto extends PickType(
  TermInsuranceTypeDefault,
  ['level', 'description'],
) {
  @IsOptional()
  @IsArray()
  @IsEnum(PaymentPolicyTypes, { each: true })
  value: IPaymentPolicyValue[];
}

export class AgeOfContractTerminationTypeDto extends OmitType(
  TermInsuranceTypeDefault,
  ['value'],
) {}

export class TotalSumInsuredTypeDto extends OmitType(TermInsuranceTypeDefault, [
  'value',
  'type',
]) {}

export class MonthlyFeeTypeDto extends OmitType(TermInsuranceTypeDefault, [
  'value',
  'type',
]) {}

export class AgeEligibilityTypeDto extends MonthlyFeeTypeDto {}
