import {
  IsArray,
  IsEnum,
  IsNotEmpty,
  IsNumber,
  IsString,
  ValidateIf,
} from 'class-validator';
import { IInsuranceDetailsTypeLevel } from '../interfaces/insurance.interface';
import {
  INSURANCE_SCOPE_LIST,
  PriorityLevelTypes,
  ValueType,
} from '../enums/insurances.enum';
import { PickType } from '@nestjs/swagger';

export class InsuranceDetailsTypeDto {
  @IsString()
  @IsEnum(PriorityLevelTypes)
  level: IInsuranceDetailsTypeLevel;

  @IsString()
  @ValidateIf((_object, value) => value !== null)
  text: string | null;
}

export class TermInsuranceTypeDefault {
  @IsNumber()
  @IsNotEmpty()
  @ValidateIf((_object, value) => value !== null)
  value: number;

  @ValidateIf((_object, value) => value !== null)
  @IsNotEmpty()
  @IsEnum(PriorityLevelTypes)
  level: IInsuranceDetailsTypeLevel;

  @IsString()
  @IsNotEmpty()
  @ValidateIf((_object, value) => value !== null)
  description: string;

  @IsNumber()
  @IsNotEmpty()
  @ValidateIf((_object, value) => value !== null)
  from: number;

  @IsNumber()
  @IsNotEmpty()
  @ValidateIf((_object, value) => value !== null)
  to: number;

  @IsEnum(ValueType)
  @IsNotEmpty()
  @ValidateIf((_object, value) => value !== null)
  type: string;
}

export class HealthInsuranceDefaultType extends PickType(
  TermInsuranceTypeDefault,
  ['level', 'value', 'description'],
) {}

export class InsuranceScopeTypeDto extends PickType(TermInsuranceTypeDefault, [
  'level',
]) {
  @IsArray()
  @IsEnum(INSURANCE_SCOPE_LIST, { each: true })
  @ValidateIf((_object, value) => value !== null)
  values: INSURANCE_SCOPE_LIST[];
}

export class WaitingPeriodTypeDto extends PickType(TermInsuranceTypeDefault, [
  'level',
]) {
  @ValidateIf((_, value) => value !== null)
  @IsNumber()
  from: number;

  @ValidateIf((_, value) => value !== null)
  @IsNumber()
  to: number;

  @ValidateIf((_, value) => value !== null)
  @IsString()
  description: string;
}
export class CompensationProcessDto extends PickType(WaitingPeriodTypeDto, [
  'description',
  'level',
]) {}
