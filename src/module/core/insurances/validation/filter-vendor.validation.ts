import {
  IsDateString,
  IsEnum,
  IsNumberString,
  IsOptional,
  IsString,
} from 'class-validator';
import { InsuranceStatusesTypes } from '../enums/insurances.enum';
import { IInsuranceStatuses } from '../interfaces/insurance.interface';
import { InsuranceCategoryNames } from '../../insurance-categories/enums/insurance-category.enum';
import { IInsuranceCategoriesTypes } from '../../insurance-categories/interfaces/insurance-categories.interface';

export class InsuranceFilterByVendorDto {
  @IsNumberString()
  page: number;

  @IsNumberString()
  limit: number;

  @IsOptional()
  @IsString()
  insurance_name?: string;

  @IsOptional()
  @IsEnum(InsuranceStatusesTypes)
  status?: IInsuranceStatuses;

  @IsOptional()
  @IsDateString()
  created_at?: Date;

  @IsOptional()
  @IsEnum(InsuranceCategoryNames)
  insurance_category_label?: IInsuranceCategoriesTypes;

  @IsOptional()
  @IsNumberString()
  monthly_fee?: number;
}
