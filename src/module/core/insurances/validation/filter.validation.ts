import {
  IsArray,
  IsBooleanString,
  IsEnum,
  IsNotEmpty,
  IsNumberString,
  IsOptional,
} from 'class-validator';
import { IInsuredPersonType } from '../interfaces/terms.interface';
import {
  InsuranceSortByClient,
  InsuredPersonTypes,
  ProfessionTypes,
} from '../enums/insurances.enum';
import { InsuranceObjectiveTypes } from '../../insurance-objectives/enums/insurance-objective.enum';
import { IInsuranceObjectiveType } from '../../insurance-objectives/interfaces/insurance-objectives.interface';
enum SubFilterValue {
  true = 'true',
}
export class InsuranceFilterDto {
  @IsNotEmpty()
  @IsNumberString()
  page: number;

  @IsNotEmpty()
  @IsNumberString()
  limit: number;

  @IsEnum(InsuranceSortByClient)
  @IsOptional()
  order_by: `${InsuranceSortByClient}`;

  @IsNotEmpty()
  @IsNumberString()
  deadline_for_deal: number;

  @IsOptional()
  @IsNotEmpty()
  @IsBooleanString()
  is_suggestion: boolean;

  @IsNotEmpty()
  @IsNumberString()
  total_sum_insured: number;

  @IsNotEmpty()
  @IsArray()
  @IsEnum(InsuranceObjectiveTypes, { each: true })
  objective: IInsuranceObjectiveType[];

  @IsNotEmpty()
  @IsArray()
  @IsEnum(InsuredPersonTypes, { each: true })
  insured_person: IInsuredPersonType[];

  @IsNotEmpty()
  @IsNumberString()
  year_of_birth: string;

  @IsNotEmpty()
  @IsEnum(ProfessionTypes)
  profession: string;

  @IsOptional()
  @IsEnum(SubFilterValue)
  serious_illnesses: boolean;

  @IsOptional()
  @IsEnum(SubFilterValue)
  death_or_disability: boolean;

  @IsOptional()
  @IsEnum(SubFilterValue)
  health_care: boolean;

  @IsOptional()
  @IsEnum(SubFilterValue)
  investment_benefit: boolean;

  @IsOptional()
  @IsEnum(SubFilterValue)
  increasing_value_bonus: boolean;

  @IsOptional()
  @IsEnum(SubFilterValue)
  for_child: boolean;

  @IsOptional()
  @IsEnum(SubFilterValue)
  flexible_and_diverse: boolean;

  @IsOptional()
  @IsEnum(SubFilterValue)
  termination_benefits: boolean;

  @IsOptional()
  @IsEnum(SubFilterValue)
  expiration_benefits: boolean;

  @IsOptional()
  @IsEnum(SubFilterValue)
  fee_exemption: boolean;
}
