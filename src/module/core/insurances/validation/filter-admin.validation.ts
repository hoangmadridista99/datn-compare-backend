import {
  IsDateString,
  IsEnum,
  IsNumberString,
  IsOptional,
  IsString,
} from 'class-validator';
import { UserRolesQuery } from '../../users/entities/user.entity';
import { IUserRole } from '../../users/interfaces/users.interface';
import { InsuranceStatusesTypes } from '../enums/insurances.enum';
import { IInsuranceStatuses } from '../interfaces/insurance.interface';
import { InsuranceCategoryNames } from '../../insurance-categories/enums/insurance-category.enum';
import { IInsuranceCategoriesTypes } from '../../insurance-categories/interfaces/insurance-categories.interface';

export class InsurancesFilterAdminDto {
  @IsOptional()
  @IsString()
  insurance_name: string;

  @IsOptional()
  @IsDateString()
  created_at: string;

  @IsOptional()
  @IsEnum(InsuranceCategoryNames)
  insurance_type: IInsuranceCategoriesTypes;

  @IsOptional()
  @IsNumberString()
  total_sum_insured: number;

  @IsString()
  @IsEnum(UserRolesQuery)
  role: Exclude<IUserRole, 'user' | 'super-admin'>;

  @IsOptional()
  @IsEnum(InsuranceStatusesTypes)
  status: IInsuranceStatuses;

  @IsNumberString()
  page: number;

  @IsNumberString()
  limit: string;
}
