import { PickType } from '@nestjs/swagger';
import { IsArray, IsBooleanString, IsEnum, IsNotEmpty } from 'class-validator';
import { InsuranceFilterDto } from './filter.validation';
import { RoomTypes } from '../enums/insurances.enum';
import { IRoomType } from '../interfaces/Inpatient.interface';

export class FilterHealthInsuranceDto extends PickType(InsuranceFilterDto, [
  'insured_person',
  'profession',
  'page',
  'limit',
  'order_by',
  'year_of_birth',
]) {
  @IsArray()
  @IsEnum(RoomTypes, { each: true })
  room_type: IRoomType;

  @IsNotEmpty()
  @IsBooleanString()
  is_dental: boolean;

  @IsNotEmpty()
  @IsBooleanString()
  is_obstetric: boolean;
}
