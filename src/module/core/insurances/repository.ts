import { InjectRepository } from '@nestjs/typeorm';
import { Insurance } from './entities/insurance.entity';
import { Repository, SelectQueryBuilder } from 'typeorm';
import {
  ICreateHealthInsurance,
  ICreateLifeInsurance,
  IUpdateHealthInsurance,
  IUpdateLifeInsurance,
} from './interfaces/insurance.interface';
import { User } from '../users/entities/user.entity';
import {
  LIST_ERROR_CODES,
  getError,
} from 'src/shared/constants/error/base.error';
import { OperationSign } from 'src/shared/enums/operation.enum';
import { UpdateRatingInsuranceDto } from './dto/insurance.dto';

export class InsuranceRepositoryService {
  constructor(
    @InjectRepository(Insurance)
    private readonly insurancesRepository: Repository<Insurance>,
  ) {}

  private getQueryBuilderDetail(query: SelectQueryBuilder<Insurance>) {
    return query
      .withDeleted()
      .leftJoinAndSelect(
        'insurances.additional_benefits',
        'additional_benefits',
      )
      .leftJoinAndSelect('insurances.insurance_category', 'insurance_category')
      .leftJoinAndSelect('insurances.terms', 'terms')
      .leftJoinAndSelect('insurances.objective_of_insurance', 'objective')
      .leftJoinAndSelect(
        'insurances.customer_orientation',
        'customer_orientation',
      )
      .leftJoinAndSelect('insurances.company', 'company')
      .leftJoinAndSelect(
        'insurances.ratings',
        'ratings',
        'ratings.is_verified_by_admin = true AND ratings.is_hide = false',
      )
      .leftJoinAndSelect('insurances.dental', 'dental')
      .leftJoinAndSelect('insurances.obstetric', 'obstetric')
      .leftJoinAndSelect('insurances.outpatient', 'outpatient')
      .leftJoinAndSelect('insurances.inpatient', 'inpatient')
      .leftJoinAndSelect('insurances.hospitals', 'hospitals');
  }

  private getQueryBuilder() {
    try {
      return this.insurancesRepository.createQueryBuilder('insurances');
    } catch (error) {
      throw error;
    }
  }

  getSelectQueryBuilder(): SelectQueryBuilder<Insurance>;
  getSelectQueryBuilder(
    tables: string[],
    columns: string[],
  ): SelectQueryBuilder<Insurance>;
  getSelectQueryBuilder(
    tables?: string[],
    columns?: string[],
  ): SelectQueryBuilder<Insurance> {
    try {
      const queryBuilder =
        this.insurancesRepository.createQueryBuilder('insurances');
      if (!tables || !columns) return this.getQueryBuilderDetail(queryBuilder);
      queryBuilder.select(['insurances.id', ...columns]);
      tables.forEach((table) => {
        queryBuilder.leftJoin(`insurances.${table}`, table);
      });
      return queryBuilder;
    } catch (error) {
      console.log(
        '🚀  file: repository.ts:80  InsuranceRepositoryService ~ error:',
        error,
      );
      throw error;
    }
  }

  async create(
    body: ICreateHealthInsurance | ICreateLifeInsurance,
    user: User,
  ): Promise<Insurance> {
    try {
      const newInsurance = this.insurancesRepository.create({ ...body, user });
      return await this.insurancesRepository.save(newInsurance);
    } catch (error) {
      console.log(
        '🚀  file: insurances.service.ts:41  InsurancesService  create  error:',
        error,
      );
      throw getError(LIST_ERROR_CODES.SS24001);
    }
  }

  async updateTotalInterest(id: string, operationSign: `${OperationSign}`) {
    try {
      const query = this.getQueryBuilder()
        .update(Insurance)
        .set({ total_interest: () => `total_interest ${operationSign} 1` })
        .where({ id });
      if (operationSign === OperationSign.SUBTRACT) {
        query.andWhere('total_interest >= 1');
      }
      await query.execute();
    } catch (error) {
      console.log(
        '🚀  file: insurances.service.ts:163  InsurancesService  updateTotalInterest  error:',
        error,
      );
      throw error;
    }
  }

  async updateRatingInsuranceById(id: string, body: UpdateRatingInsuranceDto) {
    try {
      return await this.insurancesRepository.update(id, body);
    } catch (error) {
      console.log(
        '🚀  file: insurances.service.ts:57  InsurancesService  update  error:',
        error,
      );

      throw getError(LIST_ERROR_CODES.SS24001);
    }
  }

  async updateInsuranceStatusForAdmin(body: Insurance) {
    try {
      const result = await this.insurancesRepository.save(body);
      return !!result;
    } catch (error) {
      console.log(
        '🚀  file: insurances.service.ts:344  InsurancesService  updateInsuranceStatus  error:',
        error,
      );
      return false;
    }
  }

  async updateInsurance(
    insurance: Insurance,
    body: IUpdateHealthInsurance | IUpdateLifeInsurance,
  ) {
    try {
      delete insurance.company;
      delete insurance.insurance_category;
      await this.insurancesRepository.save({ ...insurance, ...body });
      return true;
    } catch (error) {
      console.log(
        '🚀  file: insurances.service.ts:256  InsurancesService  testUpdate  error:',
        error,
      );
      return false;
    }
  }

  async deleteById(id: string) {
    try {
      return await this.insurancesRepository.delete(id);
    } catch (error) {
      console.log(
        '🚀  file: insurances.service.ts:49  InsurancesService  deleteById  error:',
        error,
      );
      throw getError(LIST_ERROR_CODES.SS24001);
    }
  }
}
