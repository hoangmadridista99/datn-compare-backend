import { PriorityLevelTypes } from '../enums/insurances.enum';

export const EXAMPLE_API_PROPERTY = {
  example: {
    level: PriorityLevelTypes.HIGH,
    text: 'string',
  },
};

export const EXAMPLE_TERM_DETAIL = {
  example: {
    level: PriorityLevelTypes.MEDIUM,
    value: 10,
    description: 'description',
  },
};

export const EXAMPLE_HEALTH_INSURANCE_DTO = {
  example: {
    level: PriorityLevelTypes.HIGH,
    value: 10,
    description: 'description',
  },
};
