/** Client Filter */
export const MONTHLY_FEE_QUERY = `(terms.monthly_fee ->> 'from')::numeric <= :query`;
export const TOTAL_SUM_INSURED_QUERY = `(terms.total_sum_insured ->> 'from')::numeric <= :total AND (terms.total_sum_insured ->> 'to')::numeric >=:total`;
export const OBJECTIVE_QUERY =
  'objective_of_insurance.objective_type = ANY(:objective)';
export const YEAR_OF_BIRTH_QUERY = `(terms.age_eligibility ->> 'from')::numeric <= :age AND (terms.age_eligibility ->> 'to')::numeric >= :age`;
export const PROFESSION_QUERY = `terms.profession ->>\'text\' LIKE :profession`;
export const DEADLINE_FOR_DEAL_QUERY = {
  where: `(terms.deadline_for_deal ->> 'value')::numeric = :deadline`,
  orWhere: `(terms.deadline_for_deal ->> 'from')::numeric <= :deadline AND (terms.deadline_for_deal ->> 'to')::numeric >= :deadline`,
};
export const INSURED_PERSON_QUERY = (key: string) =>
  `terms.insured_person ->>\'value\' LIKE :${key}`;

/** Admin filter */
export const NAME_QUERY = `(insurances.name ilike :name)`;
export const ADMIN_MONTHLY_FEE_QUERY = `(terms.monthly_fee ->> 'from')::numeric <= :query AND (terms.monthly_fee ->> 'to')::numeric >= :query`;

export const ROOM_TYPE_QUERY = (index: string) =>
  `inpatient.room_type ->> \'values\' LIKE :${index}`;

export const GET_BEST_TOTAL_SUM_INSURED_SUGGESTION_QUERY =
  "(terms.total_sum_insured ->> 'to')::numeric <= :max AND (terms.total_sum_insured ->> 'to')::numeric > :min";

export function ADDITIONAL_BENEFIT_HIGH_QUERY(value: string) {
  return `additional_benefits.${value} ->> 'level' = 'high'`;
}
export function ADDITIONAL_BENEFIT_MEDIUM_QUERY(value: string) {
  return `additional_benefits.${value} ->> 'level' = 'medium'`;
}

export const TABLES_NEED_TO_JOIN_OF_CLIENT = [
  'terms',
  'additional_benefits',
  'customer_orientation',
  'company',
  'objective_of_insurance',
];

export const COLUMNS_NEED_TO_SELECT_OF_CLIENT = [
  'insurances.id',
  'insurances.name',
  'insurances.created_at',
  'insurances.rating_scores',
  'insurances.total_rating',
  'company.long_name',
  'company.logo',
  'terms.monthly_fee',
  'terms.deadline_for_deal',
  'terms.deadline_for_payment',
  'customer_orientation.acceptance_rate',
  'customer_orientation.reception_and_processing_time',
];

export const TABLES_FIND_ALL_HEALTH_BY_FILTER = [
  'company',
  'terms',
  'customer_orientation',
  'inpatient',
  'outpatient',
  'insurance_category',
  'dental',
  'obstetric',
];
export const COLUMNS_FIND_ALL_HEALTH_BY_FILTER = [
  'insurances.id',
  'insurances.name',
  'insurances.created_at',
  'insurances.description_insurance',
  'insurances.rating_scores',
  'insurances.total_rating',
  'company.long_name',
  'company.logo',
  'inpatient.room_type',
  'inpatient.for_hospitalization',
  'inpatient.for_cancer',
  'terms.monthly_fee',
  'dental.id',
  'obstetric.id',
  'terms.insured_person',
];

export const TABLE_GET_INTEREST_INSURANCES = [
  'company',
  'user_insurances',
  'insurance_category',
];
export const COLUMNS_GET_INTEREST_INSURANCES = [
  'company.logo',
  'insurances.name',
  'insurances.total_interest',
  'user_insurances.id',
  'user_insurances.created_at',
  'user.first_name',
  'user.last_name',
  'insurance_category.label',
];

export const TABLE_GET_FILTER_INSURANCES_BY_OPERATOR = [
  'company',
  'terms',
  'user',
  'insurance_category',
];
export const COLUMN_GET_FILTER_INSURANCE_BY_OPERATOR = [
  'company.logo',
  'company.long_name',
  'insurances.status',
  'insurances.total_rating',
  'insurances.rating_scores',
  'insurances.created_at',
  'user.first_name',
  'user.last_name',
  'user.role',
  'insurances.reason',
  'insurance_category.label',
  'terms.monthly_fee',
  'insurances.name',
];

export const TABLE_GET_FILTER_INSURANCE_BY_VENDOR = [
  'company',
  'insurance_category',
  'terms',
];

export const COLUMN_GET_FILTER_INSURANCE_BY_VENDOR = [
  'insurances.created_at',
  'insurances.name',
  'insurances.status',
  'insurances.total_rating',
  'insurances.rating_scores',
  'company.long_name',
  'company.short_name',
  'company.logo',
  'terms.monthly_fee',
  'insurances.reason',
  'insurance_category.label',
];
