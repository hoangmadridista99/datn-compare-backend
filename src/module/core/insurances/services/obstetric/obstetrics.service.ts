import { InjectRepository } from '@nestjs/typeorm';
import { Obstetric } from '../../entities/obstetric/obstetric.entity';
import { Repository } from 'typeorm';
import { ICreateObstetricDto } from '../../interfaces/obstetric.interface';

export class ObstetricsService {
  constructor(
    @InjectRepository(Obstetric)
    private readonly obstetricsRepository: Repository<Obstetric>,
  ) {}

  async create(body: ICreateObstetricDto) {
    try {
      if (!body) return null;
      const result = this.obstetricsRepository.create(body);
      return await this.obstetricsRepository.save(result);
    } catch (error) {
      throw error;
    }
  }
}
