import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { AdditionalBenefits } from '../../entities/additional-benefits/additional-benefits.entity';
import type { ICreateAdditionalBenefit } from '../../interfaces/additional-benefits.interface';

export class AdditionalBenefitsService {
  constructor(
    @InjectRepository(AdditionalBenefits)
    private readonly additionalBenefitsRepository: Repository<AdditionalBenefits>,
  ) {}

  async create(body: ICreateAdditionalBenefit) {
    try {
      const result = this.additionalBenefitsRepository.create(body);
      return await this.additionalBenefitsRepository.save(result);
    } catch (error) {
      console.log(
        '🚀 ~ file: additional-benefits.service.ts:17 ~ AdditionalBenefitsService ~ create:',
        error,
      );
      throw error;
    }
  }
}
