import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Inpatient } from '../../entities/inpatient/inpatient.entity';
import { ICreateInpatientDto } from '../../interfaces/Inpatient.interface';

export class InpatientService {
  constructor(
    @InjectRepository(Inpatient)
    private readonly inpatientRepository: Repository<Inpatient>,
  ) {}

  async createInpatientService(body: ICreateInpatientDto) {
    try {
      const result = this.inpatientRepository.create(body);
      return await this.inpatientRepository.save(result);
    } catch (error) {
      throw error;
    }
  }
}
