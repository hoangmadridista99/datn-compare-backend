import { InjectRepository } from '@nestjs/typeorm';
import { Term } from '../../entities/terms/term.entity';
import { Repository } from 'typeorm';
import {
  ICreateTermsByHealthInsurance,
  ICreateTermsByLifeInsurance,
} from '../../interfaces/terms.interface';

export class TermsService {
  constructor(
    @InjectRepository(Term)
    private readonly termsRepository: Repository<Term>,
  ) {}

  async create(
    body: ICreateTermsByLifeInsurance | ICreateTermsByHealthInsurance,
  ) {
    try {
      const result = this.termsRepository.create(body);
      return await this.termsRepository.save(result);
    } catch (error) {
      console.log(
        '🚀 ~ file: terms.service.ts:15 ~ TermsService ~ create~ error:',
        error,
      );
      throw error;
    }
  }
}
