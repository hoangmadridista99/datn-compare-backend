import { InjectRepository } from '@nestjs/typeorm';
import { Outpatient } from '../../entities/outpatient/outpatient.entity';
import { Repository } from 'typeorm';
import { ICreateOutpatientDto } from '../../interfaces/outpatient.interface';

export class OutpatientService {
  constructor(
    @InjectRepository(Outpatient)
    private readonly outpatientRepository: Repository<Outpatient>,
  ) {}

  async create(body: ICreateOutpatientDto) {
    try {
      const result = this.outpatientRepository.create(body);
      return await this.outpatientRepository.save(result);
    } catch (error) {
      throw error;
    }
  }
}
