import { InjectRepository } from '@nestjs/typeorm';
import { CustomerOrientation } from '../../entities/customer-orientation/customer-orientation.entity';
import { Repository } from 'typeorm';
import type { ICreateCustomerOrientationByLifeInsurance } from '../../interfaces/customer-orientation.interface';

export class CustomerOrientationsService {
  constructor(
    @InjectRepository(CustomerOrientation)
    private readonly customerOrientationRepository: Repository<CustomerOrientation>,
  ) {}

  async create(body: ICreateCustomerOrientationByLifeInsurance) {
    try {
      const result = this.customerOrientationRepository.create(body);
      return await this.customerOrientationRepository.save(result);
    } catch (error) {
      console.log(
        '🚀 ~ file: customer-orientation.service.ts:16 ~ CustomerOrientationsService ~ create ~ error:',
        error,
      );
      throw error;
    }
  }
}
