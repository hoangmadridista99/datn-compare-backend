import { InjectRepository } from '@nestjs/typeorm';
import { Dental } from '../../entities/dental/dental.entity';
import { Repository } from 'typeorm';
import { ICreateDentalDto } from '../../interfaces/dental.interface';

export class DentalsService {
  constructor(
    @InjectRepository(Dental)
    private readonly dentalsRepository: Repository<Dental>,
  ) {}

  async create(body: ICreateDentalDto) {
    try {
      if (!body) return null;
      const result = this.dentalsRepository.create(body);
      return await this.dentalsRepository.save(result);
    } catch (error) {
      throw error;
    }
  }
}
