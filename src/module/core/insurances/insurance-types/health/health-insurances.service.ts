import { Injectable } from '@nestjs/common';
import { InsuranceRepositoryService } from '../../repository';
import { Brackets, WhereExpressionBuilder } from 'typeorm';
import { InsuranceCategoryNames } from '../../../insurance-categories/enums/insurance-category.enum';
import {
  TABLES_FIND_ALL_HEALTH_BY_FILTER,
  COLUMNS_FIND_ALL_HEALTH_BY_FILTER,
  INSURED_PERSON_QUERY,
  YEAR_OF_BIRTH_QUERY,
  PROFESSION_QUERY,
  ROOM_TYPE_QUERY,
} from '../../constants/query.constant';
import { InsuranceStatusesTypes } from '../../enums/insurances.enum';
import { IHealthInsuranceFilterClient } from '../../interfaces/insurance.interface';
import { InsurancesService } from '../../insurances.service';

@Injectable()
export class HealthInsurancesService {
  constructor(
    private readonly insuranceRepositoryService: InsuranceRepositoryService,
    private readonly insurancesService: InsurancesService,
  ) {}

  private findAllHealthInsurancesByFilter(
    filter: IHealthInsuranceFilterClient,
  ) {
    try {
      const queryBuilder = this.insuranceRepositoryService
        .getSelectQueryBuilder(
          TABLES_FIND_ALL_HEALTH_BY_FILTER,
          COLUMNS_FIND_ALL_HEALTH_BY_FILTER,
        )
        .where('insurance_category.label = :categoryName', {
          categoryName: InsuranceCategoryNames.Health,
        })
        .andWhere(
          new Brackets((qb) => {
            filter.room_type.forEach((value: any) => {
              const key = value.replace('-', '_');
              const parameter = { [key]: `%\"${value}\"%` };
              qb.orWhere(ROOM_TYPE_QUERY(key), parameter);
            });
          }),
        )
        .andWhere({ status: InsuranceStatusesTypes.Approved })
        .andWhere(
          new Brackets((qb: WhereExpressionBuilder) => {
            filter.insured_person.forEach((value: any) => {
              const key = value.replace('-', '_');
              const parameter = { [key]: `%\"${value}\"%` };
              qb.orWhere(INSURED_PERSON_QUERY(key), parameter);
            });
          }),
        )
        .andWhere(YEAR_OF_BIRTH_QUERY, { age: filter.year_of_birth });
      if (filter.is_dental || filter.is_obstetric) {
        queryBuilder.andWhere(
          new Brackets((qb: WhereExpressionBuilder) => {
            if (filter.is_dental) {
              qb.orWhere('dental.id IS NOT NULL');
            }
            if (filter.is_obstetric) {
              qb.orWhere('obstetric.id IS NOT NULL');
            }
          }),
        );
      } else {
        queryBuilder.andWhere('dental IS NULL AND obstetric IS NULL');
      }

      if (!!filter.profession) {
        queryBuilder.andWhere(PROFESSION_QUERY, {
          profession: `%\"${filter.profession}\"%`,
        });
      }
      this.insurancesService.getQueryBuilderBySortFilterClient(
        queryBuilder,
        filter.order_by,
      );
      return queryBuilder;
    } catch (error) {
      throw error;
    }
  }

  async getListHealthInsurancesByFilter(filter: IHealthInsuranceFilterClient) {
    try {
      const queryBuilder = this.findAllHealthInsurancesByFilter(filter);
      const skip = (filter.page - 1) * filter.limit;
      return queryBuilder.limit(filter.limit).offset(skip).getManyAndCount();
    } catch (error) {
      throw error;
    }
  }
}
