import { Injectable } from '@nestjs/common';
import { Brackets, SelectQueryBuilder, WhereExpressionBuilder } from 'typeorm';
import {
  TABLES_NEED_TO_JOIN_OF_CLIENT,
  COLUMNS_NEED_TO_SELECT_OF_CLIENT,
  TOTAL_SUM_INSURED_QUERY,
  OBJECTIVE_QUERY,
  INSURED_PERSON_QUERY,
  YEAR_OF_BIRTH_QUERY,
  DEADLINE_FOR_DEAL_QUERY,
  PROFESSION_QUERY,
  ADDITIONAL_BENEFIT_HIGH_QUERY,
  ADDITIONAL_BENEFIT_MEDIUM_QUERY,
  GET_BEST_TOTAL_SUM_INSURED_SUGGESTION_QUERY,
} from '../../constants/query.constant';
import {
  IInsuranceFilterClient,
  ISubInsuranceFilterClient,
} from '../../interfaces/insurance.interface';
import { InsuranceRepositoryService } from '../../repository';
import { Insurance } from '../../entities/insurance.entity';
import { InsurancesService } from '../../insurances.service';

@Injectable()
export class LifeInsurancesService {
  constructor(
    private readonly insuranceRepositoryService: InsuranceRepositoryService,
    private readonly insurancesService: InsurancesService,
  ) {}
  private createQueryBuilderByFilterClient(filter: IInsuranceFilterClient) {
    try {
      const queryBuilder = this.insuranceRepositoryService
        .getSelectQueryBuilder(
          TABLES_NEED_TO_JOIN_OF_CLIENT,
          COLUMNS_NEED_TO_SELECT_OF_CLIENT,
        )
        .andWhere(TOTAL_SUM_INSURED_QUERY, {
          total: filter.total_sum_insured,
        })
        .andWhere(OBJECTIVE_QUERY, {
          objective: filter.objective,
        })
        .andWhere(
          new Brackets((qb) => {
            filter.insured_person.forEach((value: any) => {
              const key = value.replace('-', '_');
              const parameters = { [key]: `%\"${value}\"%` };
              qb.orWhere(INSURED_PERSON_QUERY(key), parameters);
            });
          }),
        )
        .andWhere(YEAR_OF_BIRTH_QUERY, {
          age: filter.year_of_birth,
        })
        .andWhere(
          new Brackets((queryBuilder: WhereExpressionBuilder) =>
            queryBuilder
              .where(DEADLINE_FOR_DEAL_QUERY.where, {
                deadline: filter.deadline_for_deal,
              })
              .orWhere(DEADLINE_FOR_DEAL_QUERY.orWhere, {
                deadline: filter.deadline_for_deal,
              }),
          ),
        );
      if (!!filter.profession) {
        queryBuilder.andWhere(PROFESSION_QUERY, {
          profession: `%\"${filter.profession}\"%`,
        });
      }
      this.getQueryBuilderWhenHaveSubFilter(queryBuilder, filter.sub);
      return queryBuilder;
    } catch (error) {
      console.log(
        '🚀 ~ file: insurances.service.ts:250 ~ InsurancesService ~ createQueryBuilderByFilterClient ~ error:',
        error,
      );
      throw error;
    }
  }

  private getQueryBuilderWhenHaveSubFilter(
    queryBuilder: SelectQueryBuilder<Insurance>,
    filter: ISubInsuranceFilterClient,
  ) {
    try {
      queryBuilder.andWhere(
        new Brackets((queryBuilder: WhereExpressionBuilder) => {
          Object.keys(filter).forEach((column: string) => {
            if (filter[column]) {
              queryBuilder
                .orWhere(ADDITIONAL_BENEFIT_HIGH_QUERY(column))
                .orWhere(ADDITIONAL_BENEFIT_MEDIUM_QUERY(column));
            }
          });
        }),
      );
    } catch (error) {
      throw error;
    }
  }

  async getListLifeInsurancesByFilter(filter: IInsuranceFilterClient) {
    try {
      const queryBuilder = this.createQueryBuilderByFilterClient(filter);
      const skip = (filter.page - 1) * filter.limit;
      queryBuilder
        .addSelect('COUNT(objective_of_insurance.id) as count')
        .groupBy('insurances.id, terms.id, customer_orientation.id, company.id')
        .offset(skip)
        .limit(filter.limit);
      this.insurancesService.getQueryBuilderBySortFilterClient(
        queryBuilder,
        filter.order_by,
      );
      return await queryBuilder.getManyAndCount();
    } catch (error) {
      throw error;
    }
  }

  async getLifeInsuredSuggestionFilterByClient(filter: IInsuranceFilterClient) {
    try {
      const RATE_MAX_MONEY = 1.3;
      const RATE_MIN_MONEY = 0.7;
      return this.createQueryBuilderByFilterClient(filter)
        .andWhere(GET_BEST_TOTAL_SUM_INSURED_SUGGESTION_QUERY, {
          max: filter.total_sum_insured * RATE_MAX_MONEY,
          min: filter.total_sum_insured * RATE_MIN_MONEY,
        })
        .orderBy("terms.total_sum_insured->>'to'", 'DESC')
        .getOne();
    } catch (error) {
      console.log(
        '🚀 ~ file: insurances.service.ts:218 ~ InsurancesService ~ getInsuredSuggestionFilterByClient ~ error:',
        error,
      );
      return null;
    }
  }
}
