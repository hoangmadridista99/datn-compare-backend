import {
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
} from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ClientInsurancesService } from 'src/module/client/insurances/insurances.service';
import { OperatorInsurancesService } from 'src/module/operator/insurances/insurances.service';
import { VendorInsurancesService } from '../../vendor/insurances/insurances.service';
import { Insurance } from './entities/insurance.entity';
import { InsurancesService } from './insurances.service';
import { AdditionalBenefits } from './entities/additional-benefits/additional-benefits.entity';
import { CustomerOrientation } from './entities/customer-orientation/customer-orientation.entity';
import { CustomerOrientationsService } from './services/customer-orientation/customer-orientation.service';
import { AdditionalBenefitsService } from './services/additional-benefits/additional-benefits.service';
import { TermsService } from './services/terms/terms.service';
import { Term } from './entities/terms/term.entity';
import { UserInsurancesService } from '../user-insurances/user-insurances.service';
import { UserInsurances } from '../user-insurances/entities/user-insurance.entity';
import { User } from '../users/entities/user.entity';
import { getBodyRouteOfMiddleWare } from '../../../shared/helper/system.helper';
import { API_PATH } from '../../../shared/constants/api-path.constant';
import { InsurancesMiddleware } from '../../../shared/middlewares/insurances.middleware';
import { InsuranceRepositoryService } from './repository';
import { OutpatientService } from './services/outpatient-services/outpatient.service';
import { DentalsService } from './services/dentals/dentals.service';
import { InpatientService } from './services/inpatient-services/inpatient.service';
import { ObstetricsService } from './services/obstetric/obstetrics.service';
import { Inpatient } from './entities/inpatient/inpatient.entity';
import { Obstetric } from './entities/obstetric/obstetric.entity';
import { Outpatient } from './entities/outpatient/outpatient.entity';
import { Dental } from './entities/dental/dental.entity';
import { Hospital } from '../hospitals/entities/hospital.entity';
import { HospitalsService } from '../hospitals/hospitals.service';
import { ClientAuthService } from '../../client/auth/auth.service';
import { UsersService } from '../users/users.service';
import { AuthService } from '../auth/auth.service';
import { OtpsService } from '../otps/otps.service';
import { MinioService } from '../minio/minio.service';
import { JwtService } from '@nestjs/jwt';
import { Otp } from '../otps/entities/otp.entity';
import { EmailService } from '../email/email.service';
import { TwilioService } from '../twilio/twilio.service';
import { HttpModule } from '@nestjs/axios';
import { InsuranceCategoriesService } from '../insurance-categories/insurance-categories.service';
import { InsuranceCategory } from '../insurance-categories/entities/insurance-category.entity';
import { HealthInsurancesService } from './insurance-types/health/health-insurances.service';
import { LifeInsurancesService } from './insurance-types/life/life-insurances.service';
import { InsuranceExistRule } from '../../../shared/decorators/insurance-id-validation.decorator';
const createInsuranceAdminRoute = getBodyRouteOfMiddleWare(
  API_PATH.CREATE_INSURANCE_ADMIN,
  RequestMethod.POST,
);
const createInsuranceVendorRoute = getBodyRouteOfMiddleWare(
  API_PATH.CREATE_INSURANCE_VENDOR,
  RequestMethod.POST,
);
const updateInsuranceAdminRoute = getBodyRouteOfMiddleWare(
  API_PATH.UPDATE_INSURANCE_ADMIN,
  RequestMethod.PATCH,
);
const updateInsuranceVendorRoute = getBodyRouteOfMiddleWare(
  API_PATH.UPDATE_INSURANCE_VENDOR,
  RequestMethod.PATCH,
);

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Insurance,
      AdditionalBenefits,
      CustomerOrientation,
      Term,
      UserInsurances,
      User,
      Inpatient,
      Outpatient,
      Obstetric,
      Dental,
      Hospital,
      /** */
      Otp,
      InsuranceCategory,
    ]),
    HttpModule,
  ],
  providers: [
    InsurancesService,
    ClientInsurancesService,
    OperatorInsurancesService,
    VendorInsurancesService,
    AdditionalBenefitsService,
    CustomerOrientationsService,
    TermsService,
    UserInsurancesService,
    InpatientService,
    OutpatientService,
    DentalsService,
    ObstetricsService,
    InsuranceRepositoryService,
    HospitalsService,
    /**  */
    ClientAuthService,
    UsersService,
    AuthService,
    OtpsService,
    MinioService,
    JwtService,
    EmailService,
    TwilioService,
    InsuranceCategoriesService,
    HealthInsurancesService,
    LifeInsurancesService,
    InsuranceExistRule,
  ],
  exports: [
    InsurancesService,
    ClientInsurancesService,
    OperatorInsurancesService,
    VendorInsurancesService,
    AdditionalBenefitsService,
    CustomerOrientationsService,
    TermsService,
  ],
})
export class InsurancesModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(InsurancesMiddleware)
      .forRoutes(
        createInsuranceAdminRoute,
        createInsuranceVendorRoute,
        updateInsuranceAdminRoute,
        updateInsuranceVendorRoute,
      );
  }
}
