import { ApiProperty, OmitType, PartialType, PickType } from '@nestjs/swagger';
import {
  IsEmail,
  IsEnum,
  IsISO8601,
  IsNotEmpty,
  IsOptional,
  IsPhoneNumber,
  IsString,
  IsUUID,
} from 'class-validator';
import { LIST_ERROR_CODES } from '../../../../shared/constants/error/base.error';
import { RegisterUserDto } from '../../auth/dto/auth.dto';
import { IGender, UserRole } from '../entities/user.entity';
import { GenderTypes, VendorStatusTypes } from '../enums/users.enum';

export class UpdateUserDto extends PartialType(
  OmitType(RegisterUserDto, [
    'destination_type',
    'otp_code',
    'password',
  ] as const),
) {
  avatar_profile_url?: string;
}

export class BodyUpdateUserDto {
  @ApiProperty({
    type: 'string',
    format: 'binary',
    required: false,
  })
  @IsOptional()
  file: any;

  @ApiProperty({
    description: 'Email',
    example: 'example@just.engineer.com',
    required: false,
  })
  @IsOptional()
  @IsEmail()
  email: string;

  @ApiProperty({
    description: 'Phone number',
    example: '0912123342',
    required: false,
  })
  @IsOptional()
  @IsString({ message: LIST_ERROR_CODES.SS24311 })
  @IsPhoneNumber('VN', { message: LIST_ERROR_CODES.SS24310 })
  phone: string;

  @ApiProperty({
    description: 'First Name',
    example: 'Loc',
    required: false,
  })
  @IsOptional()
  @IsString({ message: LIST_ERROR_CODES.SS24304 })
  first_name: string;

  @ApiProperty({
    description: 'Last Name',
    example: 'Nguyen',
    required: false,
  })
  @IsOptional()
  @IsString({ message: LIST_ERROR_CODES.SS24306 })
  last_name: string;

  @ApiProperty({
    description: 'Date Of Birth',
    example: '1996-02-26',
    required: false,
  })
  @IsOptional()
  @IsISO8601()
  date_of_birth: string;

  @ApiProperty({
    description: 'Gender',
    example: GenderTypes.male,
    required: false,
  })
  @IsEnum(GenderTypes)
  @IsOptional()
  gender: IGender;

  @ApiProperty({
    description: 'Address Of User',
    example: 'Thanh pho Bac Ninh',
    required: false,
  })
  @IsString({ message: LIST_ERROR_CODES.SS24313 })
  @IsOptional()
  address: string;
}

export class UpdateUserByVendorDto extends OmitType(UpdateUserDto, [
  'email',
  'phone',
]) {}

export class UpdateOperatorProfileDto extends PartialType(
  PickType(UpdateUserDto, ['first_name', 'last_name']),
) {
  @ApiProperty({
    type: 'string',
    format: 'binary',
    required: false,
  })
  @IsOptional()
  file: any;
}

export class UpdateUserByAdminDto extends PartialType(RegisterUserDto) {
  @ApiProperty()
  @IsEnum(UserRole, { message: LIST_ERROR_CODES.SS24316 })
  @IsOptional()
  role: string;

  @ApiProperty()
  @IsEnum(VendorStatusTypes, { message: LIST_ERROR_CODES.SS24209 })
  @IsOptional()
  vendor_status: string;

  @ApiProperty()
  @IsString()
  @IsOptional()
  reason: string;

  @ApiProperty()
  @IsOptional()
  @IsUUID(undefined, { message: LIST_ERROR_CODES.SS24704 })
  company_id: string;
}

export class BodyUpdateVendorDto extends PartialType(UpdateUserByVendorDto) {
  @ApiProperty({
    type: 'string',
    format: 'binary',
    required: false,
  })
  @IsOptional()
  file: any;
}

export class ChangePasswordClientDto {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  current_password: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  new_password: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  confirmation_password: string;
}

export class ChangePasswordVendorDto extends ChangePasswordClientDto {}
export class ChangePasswordAdminDto extends ChangePasswordClientDto {}
