import { ApiProperty, OmitType, PartialType } from '@nestjs/swagger';
import { RegisterUserDto } from '../../auth/dto/auth.dto';
import { IsBoolean, IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { LIST_ERROR_CODES } from '../../../../shared/constants/error/base.error';

export class CreateUserDto extends OmitType(RegisterUserDto, [
  'otp_code',
  'destination_type',
]) {}

export class CreateAdminDto {
  @ApiProperty({ example: 'superadmin' })
  @IsNotEmpty({ message: LIST_ERROR_CODES.SS24314 })
  @IsString({ message: LIST_ERROR_CODES.SS24315 })
  account: string;

  @ApiProperty({
    description: 'First Name',
    example: 'Loc',
  })
  @IsOptional()
  @IsString({ message: LIST_ERROR_CODES.SS24304 })
  @IsNotEmpty({ message: LIST_ERROR_CODES.SS24303 })
  first_name: string;

  @ApiProperty({
    description: 'Last Name',
    example: 'Nguyen',
  })
  @IsOptional()
  @IsString({ message: LIST_ERROR_CODES.SS24306 })
  @IsNotEmpty({ message: LIST_ERROR_CODES.SS24305 })
  last_name: string;

  @ApiProperty({
    description: 'Password',
    example: '12121212',
  })
  @IsNotEmpty({ message: LIST_ERROR_CODES.SS24307 })
  @IsString({ message: LIST_ERROR_CODES.SS24308 })
  password: string;
}

export class UpdateAdminDto extends PartialType(CreateAdminDto) {
  @ApiProperty({
    example: true,
  })
  @IsOptional()
  @IsBoolean()
  is_blocked: boolean;
}
