import { IBaseEntity } from 'src/shared/interfaces/base-entity.interface';
import { UserRole } from '../entities/user.entity';

export type IUserRole = `${UserRole}`;

export interface IUser extends IBaseEntity {
  password?: string;
  first_name: string;
  last_name: string;
  role: string;
  phone: string;
  date_of_birth: string;
  email: string | null;
  address: string;
  company_name: string;
  account: string;
  vendor_status: string;
  avatar_profile_url?: string;
  reason: string | null;
}

export interface IUpdatePasswordDto {
  current_password: string;
  new_password: string;
}

export interface ICreateAdminDto {
  account: string;
  password: string;
  first_name?: string;
  last_name?: string;
}

export type IUpdateAdminDto = Partial<ICreateAdminDto>;
export type IUpdateOperatorProfile = Partial<
  Pick<IUser, 'password' | 'first_name' | 'last_name'>
>;

export interface IFilterAdmin {
  is_blocked?: boolean;
  account?: string;
}

export type IUpdateUser = Partial<Omit<IUser, 'destination_type' | 'otp_code'>>;
