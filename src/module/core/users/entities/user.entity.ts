import { Column, Entity, JoinColumn, ManyToOne, OneToMany } from 'typeorm';
import { Exclude } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';
import { BaseEntity } from 'src/shared/entities/base.entity';
import { Rating } from '../../ratings/entities/rating.entity';
import { Company } from '../../companies/entities/company.entity';
import { GenderTypes } from '../enums/users.enum';
import { UserInsurances } from '../../user-insurances/entities/user-insurance.entity';
import { Hospital } from '../../hospitals/entities/hospital.entity';

export enum UserRole {
  USER = 'user',
  ADMIN = 'admin',
  VENDOR = 'vendor',
  SUPER_ADMIN = 'super-admin',
}

export enum UserRolesQuery {
  ADMIN = 'admin',
  VENDOR = 'vendor',
}

export type IGender = `${GenderTypes}`;

@Entity('users')
export class User extends BaseEntity {
  @Column()
  @Exclude()
  password: string;

  @ApiProperty({
    example: 'Hung',
  })
  @Column()
  first_name: string;

  @Column()
  last_name: string;

  @ApiProperty({
    example: 'user',
  })
  @Column({ type: 'enum', enum: UserRole, default: UserRole.USER })
  role: string;

  @Column({ nullable: true })
  phone: string;

  @Column({ nullable: true })
  date_of_birth: string;

  @Column({ nullable: true })
  email: string | null;

  @Column({ nullable: true, default: null })
  reason: string | null;

  @Column({ nullable: true })
  address: string;

  @Column({ nullable: true, default: null })
  company_name: string;

  @Column({ default: null })
  account: string;

  @Column({ nullable: true, default: null })
  vendor_status: string;

  @Column({ nullable: true })
  gender: IGender;

  @OneToMany(() => Rating, (rating) => rating.user)
  ratings: Rating[];

  @Column({ nullable: true, default: null })
  company_other: string;

  @Column({ nullable: true })
  company_id: string;

  @Column({ default: false })
  is_blocked: boolean;

  @Column({ nullable: true })
  avatar_profile_url: string;

  @ManyToOne(() => Company, (company) => company.id, {
    onDelete: 'SET NULL',
  })
  @JoinColumn({ name: 'company_id', referencedColumnName: 'id' })
  company: Company;

  @OneToMany(() => UserInsurances, (userInsurances) => userInsurances.user)
  user_insurances: UserInsurances[];

  @OneToMany(() => Hospital, (hospital) => hospital.user)
  hospitals: Hospital[];
}
