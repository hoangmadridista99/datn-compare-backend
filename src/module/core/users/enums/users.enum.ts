export enum VendorStatusTypes {
  PENDING = 'pending',
  REJECTED = 'rejected',
  ACCEPTED = 'accepted',
}

export enum GenderTypes {
  male = 'male',
  female = 'female',
}
