import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { OperatorUsersService } from 'src/module/operator/users/users.service';
import { ClientUsersService } from 'src/module/client/users/user.service';
import { User } from './entities/user.entity';
import { UsersService } from './users.service';
import { VendorUsersService } from '../../vendor/users/users.service';
import { MinioService } from '../minio/minio.service';
import { HttpModule } from '@nestjs/axios';
import { SuperAdminUsersService } from '../../super-admin/users/users.service';
import { AuthService } from '../auth/auth.service';
import { JwtService } from '@nestjs/jwt';
import { UserInsurances } from '../user-insurances/entities/user-insurance.entity';

@Module({
  imports: [HttpModule, TypeOrmModule.forFeature([User, UserInsurances])],
  providers: [
    UsersService,
    OperatorUsersService,
    ClientUsersService,
    VendorUsersService,
    MinioService,
    SuperAdminUsersService,
    AuthService,
    JwtService,
  ],
  exports: [
    UsersService,
    OperatorUsersService,
    ClientUsersService,
    VendorUsersService,
    SuperAdminUsersService,
  ],
})
export class UsersModule {}
