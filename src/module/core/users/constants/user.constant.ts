export const USER_CONSTANT = {
  paginate: {
    alias: 'users',
    column: {
      first_name: 'first_name',
      last_name: 'last_name',
      created_at: 'created_at',
      role: 'role',
      phone: 'phone',
      email: 'email',
    },
  },
};
