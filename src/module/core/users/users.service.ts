import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindOptionsWhere, Repository } from 'typeorm';
import { RegisterVendorDto } from '../auth/dto/auth.dto';
import { CreateUserDto } from './dto/create.dto';
import { User, UserRole } from './entities/user.entity';
import { LIST_ERROR_CODES } from '../../../shared/constants/error/base.error';
import { VendorStatusTypes } from './enums/users.enum';
import { MinioService } from '../minio/minio.service';
import {
  getFileName,
  getFileKey,
  getFileUrl,
} from '../../../shared/helper/system.helper';
import { MinioTypes } from '../minio/enums/minio.enum';
import {
  ICreateAdminDto,
  IFilterAdmin,
  IUpdateUser,
} from './interfaces/users.interface';
import { UserFilter } from './models/user.model';

type UsersQueryFindOneOrMany =
  | FindOptionsWhere<User>[]
  | FindOptionsWhere<User>;

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
    private readonly minioService: MinioService,
  ) {}

  async uploadAvatarProfile(file: Express.Multer.File) {
    if (!file) return null;
    const fileName = getFileName(file.originalname);
    const imageKey = getFileKey(MinioTypes.AVATAR_PROFILE, fileName);
    const isUploadFile = await this.minioService.uploadFile(imageKey, file);
    return !isUploadFile
      ? undefined
      : getFileUrl(MinioTypes.AVATAR_PROFILE, fileName);
  }

  async findVendors(filter: UserFilter) {
    try {
      const queryBuilder = this.userRepository
        .createQueryBuilder('users')
        .leftJoinAndSelect('users.company', 'company')
        .where({
          role: UserRole.VENDOR,
        });

      if (filter?.company_name) {
        queryBuilder.andWhere('company.long_name ilike :name', {
          name: `%${filter.company_name}%`,
        });
      }
      if (filter?.vendor_status) {
        queryBuilder.andWhere({
          vendor_status: filter.vendor_status,
        });
      }

      return queryBuilder;
    } catch (error) {
      console.log(
        '🚀 ~ file: users.service.ts:28 ~ UsersService ~ findAllVendors:',
        error,
      );
      throw error;
    }
  }

  // async findAllVendorsByVendorStatus(status: string) {
  //   try {
  //     const queryBuilder = await this.findAllVendors();
  //     return queryBuilder.andWhere({
  //       vendor_status: status,
  //     });
  //   } catch (error) {
  //     console.log(
  //       '🚀 ~ file: users.service.ts:43 ~ UsersService ~ findAllVendorsByVendorStatus:',
  //       error,
  //     );
  //     throw error;
  //   }
  // }

  async createAdmin(body: ICreateAdminDto) {
    try {
      const role = UserRole.ADMIN;
      const createAdmin = this.userRepository.create({
        ...body,
        role,
      });
      await this.userRepository.save(createAdmin);
      return true;
    } catch (error) {
      console.log(
        '🚀 ~ file: users.service.ts:79 ~ UsersService ~ createAdmin ~ error:',
        error,
      );
      return false;
    }
  }

  async findAllAdminBySuperAdmin(filter: IFilterAdmin) {
    try {
      const queryBuilder = this.userRepository
        .createQueryBuilder('users')
        .where({ role: UserRole.ADMIN });
      if (typeof filter?.is_blocked === 'boolean') {
        queryBuilder.andWhere({ is_blocked: filter.is_blocked });
      }
      if (filter?.account) {
        queryBuilder.andWhere('users.account ilike :account', {
          account: `%${filter?.account}%`,
        });
      }
      return queryBuilder;
    } catch (error) {
      console.log(
        '🚀 ~ file: users.service.ts:91 ~ UsersService ~ findAllAdminBySuperAdmin ~ error:',
        error,
      );
      throw error;
    }
  }

  async createVendor(
    body: RegisterVendorDto,
    avatar_profile_url: string | null,
  ) {
    try {
      const createBody = this.userRepository.create({
        ...body,
        role: UserRole.VENDOR,
        vendor_status: VendorStatusTypes.PENDING,
        avatar_profile_url,
      });
      return await this.userRepository.save(createBody);
    } catch (error) {
      console.log(
        '🚀 ~ file: users.service.ts:59 ~ UsersService ~ createVendor ~ error:',
        error,
      );
      throw error;
    }
  }

  async createUser(body: CreateUserDto, avatar_profile_url: string | null) {
    try {
      const create = this.userRepository.create({
        ...body,
        avatar_profile_url,
      });
      return await this.userRepository.save(create);
    } catch (error) {
      console.log(
        '🚀 ~ file: users.service.ts:73 ~ UsersService ~ createUser:',
        error,
      );
      throw error;
    }
  }

  async validateEmailAndPhoneInBody(email: string, phone: string) {
    const user = await this.findOneByUsersQuery([{ phone }, { email }]);
    if (user?.email === email) return LIST_ERROR_CODES.SS24204;
    if (user?.phone === phone) return LIST_ERROR_CODES.SS24205;
    return true;
  }

  async checkEmailExistWhenUpdate(email: string) {
    try {
      const result = await this.findOneByUsersQuery({ email });

      return result?.id ? false : true;
    } catch (error) {
      console.log(
        '🚀 ~ file: users.service.ts:120 ~ UsersService ~ checkEmailExistWhenUpdate ~ error:',
        error,
      );
      throw error;
    }
  }

  async checkPhoneExistWhenUpdate(phone: string) {
    try {
      const result = await this.findOneByUsersQuery({ phone });
      return result?.id ? false : true;
    } catch (error) {
      console.log(
        '🚀 ~ file: users.service.ts:134 ~ UsersService ~ checkPhoneExistWhenUpdate ~ error:',
        error,
      );

      throw error;
    }
  }

  async findOneByUsersQuery(
    query: UsersQueryFindOneOrMany,
  ): Promise<User | null> {
    try {
      return await this.userRepository.findOne({
        where: query,
        relations: ['company'],
      });
    } catch (error) {
      console.log('🚀 ~ file: users.service.ts:94 ~ UsersService:', error);
      throw error;
    }
  }

  async updateUserById(id: string, body: IUpdateUser) {
    try {
      return await this.userRepository.update(id, body);
    } catch (error) {
      console.log(
        '🚀 ~ file: users.service.ts:103 ~ UsersService ~ updateUserById:',
        error,
      );
      throw error;
    }
  }

  async deleteById(id: string) {
    try {
      await this.userRepository.delete(id);
      return true;
    } catch (error) {
      console.log(
        '🚀 ~ file: users.service.ts:203 ~ UsersService ~ error:',
        error,
      );
      return false;
    }
  }
}
