import { ApiProperty, PartialType } from '@nestjs/swagger';
import {
  IsString,
  IsNotEmpty,
  IsUUID,
  IsOptional,
  IsEnum,
} from 'class-validator';
import { LIST_ERROR_CODES } from '../../../../shared/constants/error/base.error';
import { BlogStatusesTypes } from '../enums/blogs.enum';
import { IBlogStatus } from '../interfaces/blogs.interface';

export class CreateBlogDto {
  @ApiProperty({
    description: 'Tiêu đề',
    example: 'Bảo hiểm đã giúp tôi như thế nào?',
  })
  @IsString({ message: LIST_ERROR_CODES.SS24801 })
  @IsNotEmpty({ message: LIST_ERROR_CODES.SS24802 })
  title: string;

  @ApiProperty({
    description: 'Mô tả',
    example: 'Mô tả',
  })
  @IsString({ message: LIST_ERROR_CODES.SS24803 })
  @IsNotEmpty({ message: LIST_ERROR_CODES.SS24804 })
  description: string;

  @ApiProperty({
    description: 'Nội dung',
    example: 'Nội dung',
  })
  @IsString({ message: LIST_ERROR_CODES.SS24805 })
  @IsNotEmpty({ message: LIST_ERROR_CODES.SS24806 })
  content: string;

  @ApiProperty({
    description: 'Bài viết về',
    example: 'Bài viết về',
  })
  @IsUUID(undefined, { message: LIST_ERROR_CODES.SS24807 })
  @IsNotEmpty({ message: LIST_ERROR_CODES.SS24808 })
  blog_category_id: string;

  @ApiProperty({
    description: 'Ảnh banner bài viết',
    example:
      'http://localhost:3333/v1/api/client/file?key=blog_banner/1683638202.801_Screenshot_20221211_093231.png',
  })
  // @IsUrl(undefined, { message: LIST_ERROR_CODES.SS24319 })
  @IsNotEmpty({ message: LIST_ERROR_CODES.SS24320 })
  banner_image_url: string;
}

export class UpdateBlogDto extends PartialType(CreateBlogDto) {}

export class AdminUpdateBlogDto extends UpdateBlogDto {}

export class AdminVerifyBlogDto {
  @ApiProperty({
    description: 'Lý do',
    example: 'Câu từ không hợp lệ',
  })
  @IsOptional()
  @IsString({ message: LIST_ERROR_CODES.SS24810 })
  reason: string;

  @ApiProperty({
    description: 'Trạng thái',
    example: BlogStatusesTypes.Approved,
  })
  @IsEnum(BlogStatusesTypes, { message: LIST_ERROR_CODES.SS24811 })
  status: IBlogStatus;
}

export class VendorUpdateBlogDto extends UpdateBlogDto {}

export class UploadBannerDto {
  @ApiProperty({ type: 'string', format: 'binary', required: true })
  file: any;
}

export class BodyCreateBlog extends CreateBlogDto {
  @ApiProperty({ type: 'string', format: 'binary', required: true })
  file: any;
}
