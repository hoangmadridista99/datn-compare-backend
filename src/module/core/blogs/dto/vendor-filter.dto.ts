import {
  IsDateString,
  IsEnum,
  IsNumberString,
  IsOptional,
  IsString,
  IsUUID,
} from 'class-validator';
import { BlogStatusesTypes } from '../enums/blogs.enum';
import { IBlogStatus } from '../interfaces/blogs.interface';

export class VendorBlogsFilterDto {
  @IsOptional()
  @IsString()
  title: string;

  @IsOptional()
  @IsEnum(BlogStatusesTypes)
  status: IBlogStatus;

  @IsOptional()
  @IsUUID()
  blog_category_id: string;

  @IsDateString()
  @IsOptional()
  created_at: string;

  @IsNumberString()
  page: number;

  @IsNumberString()
  limit: number;
}
