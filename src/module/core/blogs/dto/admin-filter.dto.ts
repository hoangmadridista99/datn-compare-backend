import {
  IsOptional,
  IsString,
  IsEnum,
  IsUUID,
  IsDateString,
  IsNumberString,
} from 'class-validator';
import { BlogStatusesTypes } from '../enums/blogs.enum';
import { IBlogStatus } from '../interfaces/blogs.interface';
import { UserRole } from '../../users/entities/user.entity';
import { IUserRole } from '../../users/interfaces/users.interface';

export class AdminBlogsFilterDto {
  @IsOptional()
  @IsString()
  title: string;

  @IsOptional()
  @IsEnum(BlogStatusesTypes)
  status: IBlogStatus;

  @IsOptional()
  @IsUUID()
  blog_category_id: string;

  @IsOptional()
  @IsDateString()
  created_at: Date;

  @IsOptional()
  @IsEnum(UserRole)
  role: IUserRole;

  @IsNumberString()
  page: number;

  @IsNumberString()
  limit: number;
}
