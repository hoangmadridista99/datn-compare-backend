import { IBaseEntity } from '../../../../shared/interfaces/base-entity.interface';
import { UserRole } from '../../users/entities/user.entity';
import { IUserRole } from '../../users/interfaces/users.interface';
import { BlogStatusesTypes } from '../enums/blogs.enum';

export interface IBlog extends IBaseEntity {
  title: string;
  description: string;
  banner_image_url: string;
  content: string;
  blog_category_id: string;
  quantity_of_blogs: number;
  status: IBlogStatus;
  reason: string;
}
export type ICreateBlogDto = Omit<
  IBlog,
  'quantity_of_blogs' | 'reason' | keyof IBaseEntity
>;
export type ICommonUpdateBlogDto = Partial<
  Omit<IBlog, 'quantity_of_blogs' | keyof IBaseEntity>
>;

export type IVendorUpdateBlogDto = Omit<
  ICommonUpdateBlogDto,
  'status' | 'reason'
>;
export type IAdminUpdateBlogDto = Omit<
  ICommonUpdateBlogDto,
  'status' | 'reason'
>;
export type IAdminVerifyBlogDto = Pick<IBlog, 'status' | 'reason'>;

export type IBlogStatus = `${BlogStatusesTypes}`;

export interface IVendorBlogsFilter {
  title?: string;
  status?: IBlogStatus;
  created_at?: Date;
  blog_category_id?: string;
}

export interface IAdminBlogsFilter extends IVendorBlogsFilter {
  created_at?: Date;
  blog_category_id?: string;
  role?: IUserRole;
  super_admin_role?: super_admin_role;
}

type super_admin_role = `${UserRole.SUPER_ADMIN}`;

export type ICoreBlogsFilter = IVendorBlogsFilter & IAdminBlogsFilter;
