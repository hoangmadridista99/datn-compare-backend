import { InjectRepository } from '@nestjs/typeorm';
import { Blog } from './entities/blog.entity';
import { Between, ILike, Repository } from 'typeorm';
import {
  IAdminBlogsFilter,
  ICoreBlogsFilter,
  ICreateBlogDto,
  ICommonUpdateBlogDto,
  IVendorBlogsFilter,
} from './interfaces/blogs.interface';
import { MinioService } from '../minio/minio.service';
import {
  getFileName,
  getFileKey,
  getFileUrl,
  setStartAndEndOfDateByTime,
} from '../../../shared/helper/system.helper';
import { MinioTypes } from '../minio/enums/minio.enum';
import { PaginateQuery } from 'nestjs-paginate';
import { getPaginate } from '../../../shared/lib/paginate/paginate.lib';
import { BlogCategoriesService } from '../blog-categories/blog-categories.service';
import { BlogStatusesTypes } from './enums/blogs.enum';
import { UserRole } from '../users/entities/user.entity';

export class BlogsService {
  constructor(
    @InjectRepository(Blog)
    private readonly blogsRepository: Repository<Blog>,
    private readonly minioService: MinioService,
    private readonly blogCategoriesService: BlogCategoriesService,
  ) {}

  private async createQueryBuilder() {
    return this.blogsRepository
      .createQueryBuilder('blogs')
      .leftJoinAndSelect('blogs.user', 'user')
      .leftJoinAndSelect('blogs.blog_category', 'category')
      .select([
        'blogs',
        'user.id',
        'user.avatar_profile_url',
        'user.first_name',
        'user.last_name',
        'user.role',
        'category.vn_label',
        'category.en_label',
        'category.quantity_of_blogs',
      ]);
  }

  async uploadBannerImage(file: Express.Multer.File) {
    const fileName = getFileName(file.originalname);
    const imageKey = getFileKey(MinioTypes.BLOG_BANNER, fileName);
    const isUploadFile = await this.minioService.uploadFile(imageKey, file);
    return !isUploadFile
      ? undefined
      : getFileUrl(MinioTypes.BLOG_BANNER, fileName);
  }

  async create(body: ICreateBlogDto, user_id: string) {
    try {
      const result = this.blogsRepository.create({
        ...body,
        user_id,
        blog_category_id: body.blog_category_id,
      });
      await this.blogsRepository.save(result);
      if (body.status === BlogStatusesTypes.Approved)
        await this.blogCategoriesService.updateQuantityOfCategoryAfterNewBlogIsCreated(
          body.blog_category_id,
        );
      return true;
    } catch (error) {
      console.log(
        '🚀 ~ file: blogs.service.ts:20 ~ BlogsService ~ create ~ error:',
        error,
      );
      return false;
    }
  }

  async calculateQuantityOfBlogAfterVerified(
    blogCategoryId: string,
    quantity: number,
  ) {
    await this.blogCategoriesService.updateQuantityAfterVendorBlogIsVerified(
      blogCategoryId,
      quantity,
    );
  }

  async findAllBlogs(query: PaginateQuery, blog_category_id: string | null) {
    try {
      const queryBuilder = await this.createQueryBuilder();
      queryBuilder.where({
        status: BlogStatusesTypes.Approved,
      });
      if (!!blog_category_id) queryBuilder.andWhere({ blog_category_id });

      return await getPaginate(query, queryBuilder);
    } catch (error) {
      console.log(
        '🚀 ~ file: blogs.service.ts:59 ~ BlogsService ~ findAllBlogs ~ error:',
        error,
      );
      throw error;
    }
  }

  async filterBlogsForAdmin(query: PaginateQuery, filter: IAdminBlogsFilter) {
    return await this.filterBlog(query, filter);
  }

  async filterBlog(
    query: PaginateQuery,
    filter: ICoreBlogsFilter,
    userId?: string,
  ) {
    try {
      const queryBuilder = await this.createQueryBuilder();
      if (!!filter.blog_category_id) {
        queryBuilder.andWhere({ blog_category_id: filter.blog_category_id });
      }
      if (!!filter.title) {
        queryBuilder.andWhere({
          title: ILike(`%${filter.title}%`),
        });
      }
      if (!!filter.created_at) {
        const { startOfDay, endOfDay } = setStartAndEndOfDateByTime(
          filter.created_at,
        );
        queryBuilder.andWhere({
          created_at: Between(startOfDay, endOfDay),
        });
      }
      if (!!filter.status) {
        queryBuilder.andWhere({ status: filter.status });
      }

      if (filter?.role === UserRole.VENDOR) {
        queryBuilder.andWhere({ user: { role: filter.role } });
      }

      if (!!filter.super_admin_role) {
        queryBuilder.andWhere([
          { user: { role: UserRole.SUPER_ADMIN } },
          { user: { role: UserRole.ADMIN } },
        ]);
      }

      if (!!userId) {
        queryBuilder.andWhere({ user: { id: userId } });
      }
      return await getPaginate(query, queryBuilder);
    } catch (error) {
      console.log(
        '🚀 ~ file: blogs.service.ts:162 ~ BlogsService ~ error:',
        error,
      );
      throw error;
    }
  }

  async filterBlogsForVendor(
    query: PaginateQuery,
    userId: string,
    filter: IVendorBlogsFilter,
  ) {
    return await this.filterBlog(query, filter, userId);
  }

  async findOneById(id: string) {
    try {
      const queryBuilder = await this.createQueryBuilder();
      return queryBuilder.where({ id }).getOne();
    } catch (error) {
      console.log(
        '🚀 ~ file: blogs.service.ts:32 ~ BlogsService ~ findOneById ~ error:',
        error,
      );
      throw error;
    }
  }

  async update(id: string, body: ICommonUpdateBlogDto) {
    try {
      return await this.blogsRepository.update(id, body);
    } catch (error) {
      console.log(
        '🚀 ~ file: blogs.service.ts:32 ~ BlogsService ~ update ~ error:',
        error,
      );
      throw error;
    }
  }

  async deleteBlogForAdmin(id: string) {
    try {
      const blogDetail = await this.findOneById(id);
      if (!blogDetail) return false;
      await this.deleteBlog(id);
      const currentQuantity = blogDetail.blog_category.quantity_of_blogs;
      const result = currentQuantity - 1;
      await this.blogCategoriesService.updateCategoryById(
        blogDetail.blog_category_id,
        {
          quantity_of_blogs: result,
        },
      );
      return true;
    } catch (error) {
      console.log(
        '🚀 ~ file: blogs.service.ts:44 ~ BlogsService ~ delete ~ error:',
        error,
      );
      return false;
    }
  }

  async deleteBlog(id: string) {
    try {
      await this.blogsRepository.delete(id);
      return true;
    } catch (error) {
      console.log(
        '🚀 ~ file: blogs.service.ts:202 ~ BlogsService ~ deleteBlog ~ error:',
        error,
      );
      return false;
    }
  }
}
