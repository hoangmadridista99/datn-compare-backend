import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Blog } from './entities/blog.entity';
import { BlogsService } from './blogs.service';
import { MinioService } from '../minio/minio.service';
import { HttpModule } from '@nestjs/axios';
import { OperatorBlogsService } from '../../operator/blogs/blogs.service';
import { ClientBlogsService } from '../../client/blogs/blogs.service';
import { BlogCategoriesService } from '../blog-categories/blog-categories.service';
import { BlogCategory } from '../blog-categories/entities/blog-category.entity';
import { VendorBlogsService } from '../../vendor/blogs/blogs.service';

@Module({
  imports: [TypeOrmModule.forFeature([Blog, BlogCategory]), HttpModule],
  providers: [
    BlogsService,
    OperatorBlogsService,
    ClientBlogsService,
    MinioService,
    ClientBlogsService,
    BlogCategoriesService,
    VendorBlogsService,
  ],
  exports: [
    BlogsService,
    OperatorBlogsService,
    ClientBlogsService,
    ClientBlogsService,
    VendorBlogsService,
  ],
})
export class BlogsModule {}
