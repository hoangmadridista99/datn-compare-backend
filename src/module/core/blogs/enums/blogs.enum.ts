export enum BlogStatusesTypes {
  Pending = 'pending',
  Rejected = 'rejected',
  Approved = 'approved',
}
