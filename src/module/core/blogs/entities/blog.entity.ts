import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';
import { BaseEntity } from '../../../../shared/entities/base.entity';
import { User } from '../../users/entities/user.entity';
import { BlogCategory } from '../../blog-categories/entities/blog-category.entity';
import { BlogStatusesTypes } from '../enums/blogs.enum';
import { IBlogStatus } from '../interfaces/blogs.interface';

@Entity('blogs')
export class Blog extends BaseEntity {
  @Column()
  title: string;

  @Column()
  description: string;

  @Column()
  banner_image_url: string;

  @Column()
  content: string;

  @Column({ nullable: true, default: null })
  reason: string;

  @Column()
  user_id: string;

  @Column({ nullable: true })
  blog_category_id: string;

  // @OneToMany(() => BlogComment, (blogComment) => blogComment.blog, {
  //   cascade: true,
  // })
  // comments: BlogComment[];

  @Column({ default: BlogStatusesTypes.Pending })
  status: IBlogStatus;

  @JoinColumn({ name: 'blog_category_id', referencedColumnName: 'id' })
  @ManyToOne(() => BlogCategory, (blog_category) => blog_category.id, {
    onDelete: 'CASCADE',
  })
  blog_category: BlogCategory;

  @JoinColumn({ name: 'user_id', referencedColumnName: 'id' })
  @ManyToOne(() => User, (user) => user.id)
  user: User;
}
