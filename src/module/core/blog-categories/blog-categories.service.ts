import { InjectRepository } from '@nestjs/typeorm';
import { BlogCategory } from './entities/blog-category.entity';
import { Repository } from 'typeorm';
import {
  ICreateBlogCategory,
  IUpdateBlogCategory,
} from './interfaces/blog-categories.interface';
import { PaginateQuery } from 'nestjs-paginate';
import { getPaginate } from 'src/shared/lib/paginate/paginate.lib';

export class BlogCategoriesService {
  constructor(
    @InjectRepository(BlogCategory)
    private readonly blogCategoriesRepository: Repository<BlogCategory>,
  ) {}

  async createBlogCategory(body: ICreateBlogCategory) {
    try {
      const result = this.blogCategoriesRepository.create(body);
      await this.blogCategoriesRepository.save(result);
      return true;
    } catch (error) {
      console.log(
        '🚀 ~ file: blog-categories.service.ts:17 ~ BlogCategoriesService ~ createBlogCategory ~ error:',
        error,
      );
      return false;
    }
  }

  async updateQuantityOfCategoryAfterNewBlogIsCreated(id: string) {
    try {
      return await this.blogCategoriesRepository
        .createQueryBuilder('blog-categories')
        .update(BlogCategory)
        .where({ id })
        .set({ quantity_of_blogs: () => `quantity_of_blogs + 1` })
        .execute();
    } catch (error) {
      console.log(
        '🚀 ~ file: blog-categories.service.ts:33 ~ BlogCategoriesService ~ updateQuantityOfBlogsAfterCreate ~ error:',
        error,
      );
      throw error;
    }
  }

  async updateQuantityAfterVendorBlogIsVerified(id: string, quantity: number) {
    try {
      return await this.blogCategoriesRepository.update(id, {
        quantity_of_blogs: quantity,
      });
    } catch (error) {
      console.log(
        '🚀 ~ file: blog-categories.service.ts:52 ~ BlogCategoriesService ~ updateQuantityAfterVerifyVendorBlog ~ error:',
        error,
      );
      throw error;
    }
  }

  async getOneById(id: string) {
    try {
      return await this.blogCategoriesRepository.findOne({ where: { id } });
    } catch (error) {
      console.log(
        '🚀 ~ file: blog-categories.service.ts:29 ~ BlogCategoriesService ~ getOneById ~ error:',
        error,
      );
      return null;
    }
  }

  async getAllCategories(isAdmin: boolean) {
    try {
      const conditional = isAdmin
        ? undefined
        : {
            where: { is_hide: false },
            select: {
              id: true,
              en_label: true,
              vn_label: true,
              quantity_of_blogs: true,
            },
          };
      return await this.blogCategoriesRepository.find(conditional);
    } catch (error) {
      console.log(
        '🚀 ~ file: blog-categories.service.ts:41 ~ BlogCategoriesService ~ getAllCategories ~ error:',
        error,
      );
      return [];
    }
  }

  async getAllCategoryByOperator(query: PaginateQuery) {
    try {
      const queryBuilder = this.blogCategoriesRepository
        .createQueryBuilder('blog-categories')
        .select([
          'blog-categories.id',
          'blog-categories.en_label',
          'blog-categories.vn_label',
          'blog-categories.quantity_of_blogs',
          'blog-categories.is_hide',
        ]);
      return await getPaginate(query, queryBuilder);
    } catch (error) {
      throw error;
    }
  }

  async updateCategoryById(
    id: string,
    body: IUpdateBlogCategory,
  ): Promise<boolean> {
    try {
      const result = await this.blogCategoriesRepository.update(id, body);
      return !!result.affected;
    } catch (error) {
      console.log(
        '🚀 ~ file: blog-categories.service.ts:53 ~ BlogCategoriesService ~ updateCategory ~ error:',
        error,
      );
      return false;
    }
  }

  async deleteCategoryById(id: string): Promise<boolean> {
    try {
      const result = await this.blogCategoriesRepository.delete(id);
      return !!result.affected;
    } catch (error) {
      console.log(
        '🚀 ~ file: blog-categories.service.ts:69 ~ BlogCategoriesService ~ deleteCategoryById ~ error:',
        error,
      );
      return false;
    }
  }
}
