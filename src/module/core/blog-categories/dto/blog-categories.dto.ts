import { ApiProperty, PartialType } from '@nestjs/swagger';
import { IsBoolean, IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { LIST_ERROR_CODES } from '../../../../shared/constants/error/base.error';

export class CreateBlogCategoryDto {
  @ApiProperty({
    example: '',
  })
  @IsNotEmpty({ message: LIST_ERROR_CODES.SS24907 })
  @IsString({ message: LIST_ERROR_CODES.SS24906 })
  vn_label: string;

  @ApiProperty({
    example: '',
  })
  @IsNotEmpty({ message: LIST_ERROR_CODES.SS24908 })
  @IsString({ message: LIST_ERROR_CODES.SS24909 })
  en_label: string;
}

export class UpdateBlogCategoryDto extends PartialType(CreateBlogCategoryDto) {
  @ApiProperty({ example: 'false' })
  @IsOptional()
  @IsBoolean({ message: LIST_ERROR_CODES.SS24910 })
  is_hide: boolean;
}
