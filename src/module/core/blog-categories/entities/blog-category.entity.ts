import { Column, Entity, OneToMany } from 'typeorm';
import { BaseEntity } from '../../../../shared/entities/base.entity';
import { Blog } from '../../blogs/entities/blog.entity';

@Entity('blog-categories')
export class BlogCategory extends BaseEntity {
  @Column()
  vn_label: string;

  @Column()
  en_label: string;

  @Column({ default: false })
  is_hide: boolean;

  @Column({ default: 0 })
  quantity_of_blogs: number;

  @OneToMany(() => Blog, (blog) => blog.blog_category, { cascade: true })
  blogs: Blog[];
}
