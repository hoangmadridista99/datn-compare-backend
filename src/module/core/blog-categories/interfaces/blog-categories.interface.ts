import { IBaseEntity } from '../../../../shared/interfaces/base-entity.interface';

export interface IBlogCategories extends IBaseEntity {
  vn_label: string;
  en_label: string;
}

export type ICreateBlogCategory = Omit<IBlogCategories, keyof IBaseEntity>;
export interface IUpdateBlogCategory extends Partial<ICreateBlogCategory> {
  is_hide?: boolean;
  quantity_of_blogs?: number;
}
