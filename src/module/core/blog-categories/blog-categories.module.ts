import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BlogCategory } from './entities/blog-category.entity';
import { BlogCategoriesService } from './blog-categories.service';
import { ClientBlogCategoriesService } from '../../client/blog-categories/blog-categories.service';
import { OperatorBlogCategoriesService } from '../../operator/blog-categories/blog-categories.service';

@Module({
  imports: [TypeOrmModule.forFeature([BlogCategory])],
  providers: [
    BlogCategoriesService,
    ClientBlogCategoriesService,
    OperatorBlogCategoriesService,
  ],
  exports: [
    BlogCategoriesService,
    ClientBlogCategoriesService,
    OperatorBlogCategoriesService,
  ],
})
export class BlogCategoriesModule {}
