import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import { MinioService } from './minio.service';
import { BUFFER_ENCODING, RESPONSE_TYPE } from './constants/minio.constant';

@Module({
  imports: [
    HttpModule.register({
      responseType: RESPONSE_TYPE,
      responseEncoding: BUFFER_ENCODING,
    }),
  ],
  providers: [MinioService],
  exports: [MinioService],
})
export class MinioModule {}
