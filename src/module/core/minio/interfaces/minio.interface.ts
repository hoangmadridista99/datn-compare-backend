import internal from 'stream';

export interface IMinioResultDownloadFile {
  size: number;
  steam: internal.Readable;
}
