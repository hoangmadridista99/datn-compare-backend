export enum MinioTypes {
  COMPANY_LOGO = 'company_logo',
  INSURANCE_PDF = 'pdf',
  AVATAR_PROFILE = 'avatar_profile',
  BLOG_BANNER = 'blog_banner',
  APPRAISAL_FORM = 'appraisal_form',
}
