import * as Minio from 'minio';
import { getConfig } from '../../../shared/lib/config/config.lib';
import { HttpService } from '@nestjs/axios';
import { lastValueFrom } from 'rxjs';
import { Injectable } from '@nestjs/common';
import type { IMinioResultDownloadFile } from './interfaces/minio.interface';

@Injectable()
export class MinioService {
  private expiry: number;
  private bucketName: string;
  private minioClient: Minio.Client;

  constructor(private readonly httpService: HttpService) {
    this.minioClient = new Minio.Client({
      endPoint: getConfig('minio.end_point'),
      port: parseInt(getConfig('minio.port')) || 9000,
      useSSL: `${getConfig('minio.use_SSL')}` === 'true',
      accessKey: getConfig('minio.access_key'),
      secretKey: getConfig('minio.secret_key'),
    });
    this.bucketName = getConfig('minio.bucket_name');
    this.expiry = 24 * 60 * 7;
  }

  async uploadFile(key: string, file: Express.Multer.File): Promise<boolean> {
    try {
      if (!key || !file) return false;
      await this.minioClient.putObject(
        this.bucketName,
        key,
        file.buffer,
        file.size,
      );
      return true;
    } catch (error) {
      console.log(
        '🚀 ~ file: minio.service.ts:26 ~ MinioService ~ uploadFile:',
        error.message,
      );

      return false;
    }
  }

  async deleteFile(key: string) {
    try {
      if (!key) return;
      await this.minioClient.removeObject(this.bucketName, key);
    } catch (error) {
      console.log(
        '🚀 ~ file: minio.service.ts:54 ~ MinioService ~ deleteFile ~ error:',
        error.message,
      );
    }
  }

  async getFile(key: string): Promise<Buffer | null> {
    try {
      if (!key) return null;
      const file = await this.minioClient.presignedGetObject(
        this.bucketName,
        key,
        this.expiry,
      );
      const response = this.httpService.get<Buffer>(file);
      const result = await lastValueFrom(response);
      return Buffer.from(result.data);
    } catch (error) {
      console.log(
        '🚀 ~ file: minio.service.ts:68 ~ MinioService ~ getFile ~ error:',
        error.message,
      );
    }
  }

  async downloadFile(key: string): Promise<IMinioResultDownloadFile | null> {
    try {
      if (!key) return null;
      const objectStat = await this.minioClient.statObject(
        this.bucketName,
        key,
      );
      const steam = await this.minioClient.getObject(this.bucketName, key);
      return { size: objectStat.size, steam };
    } catch (error) {
      console.log(
        '🚀 ~ file: minio.service.ts:70 ~ MinioService ~ downloadFile:',
        error.message,
      );

      return null;
    }
  }
}
