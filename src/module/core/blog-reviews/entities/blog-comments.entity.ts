import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';
import { BaseEntity } from '../../../../shared/entities/base.entity';
import { User } from '../../users/entities/user.entity';
import { Blog } from '../../blogs/entities/blog.entity';

@Entity('blog-comments')
export class BlogComment extends BaseEntity {
  @Column()
  comment: string;

  @Column()
  user_id: string;

  @Column()
  blog_id: string;

  @Column({ default: false })
  is_hide: boolean;

  @JoinColumn({ name: 'user_id', referencedColumnName: 'id' })
  @ManyToOne(() => User, (user) => user.id)
  user: User;

  @JoinColumn({ name: 'blog_id', referencedColumnName: 'id' })
  @ManyToOne(() => Blog, (blog) => blog.id, { onDelete: 'CASCADE' })
  blog: Blog;
}
