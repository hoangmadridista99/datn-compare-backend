import { IBaseEntity } from '../../../../shared/interfaces/base-entity.interface';
import { IBlog } from '../../blogs/interfaces/blogs.interface';
import { IUser } from '../../users/interfaces/users.interface';

export interface IBlogComment extends IBaseEntity {
  comment: string;
  user_id: string;
  blog_id: string;
  is_hide: boolean;
  user: IUser;
  blog: IBlog;
}

export type ICreateBlogComment = Pick<IBlogComment, 'comment' | 'blog_id'>;
export type IClientUpdateBlogComment = Pick<IBlogComment, 'is_hide'>;

export interface IFilterBlogComment {
  user_id?: string;
  blog_id?: string;
}
