import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsString, IsUUID } from 'class-validator';
import { LIST_ERROR_CODES } from '../../../../shared/constants/error/base.error';

export class CreateBlogCommentDto {
  @ApiProperty({ example: 'Rat Tot!' })
  @IsString({ message: LIST_ERROR_CODES.SS24901 })
  comment: string;

  @ApiProperty({ example: '' })
  @IsUUID(undefined, { message: LIST_ERROR_CODES.SS24902 })
  blog_id: string;
}

export class UpdateBlogCommentDto {
  @IsBoolean({ message: LIST_ERROR_CODES.SS24903 })
  is_hide: boolean;
}
