import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import {
  IClientUpdateBlogComment,
  ICreateBlogComment,
  IFilterBlogComment,
} from './interfaces/blog-comment.interface';
import { PaginateQuery } from 'nestjs-paginate';
import { getPaginate } from '../../../shared/lib/paginate/paginate.lib';
import { BlogComment } from './entities/blog-comments.entity';

export class BlogCommentsService {
  constructor(
    @InjectRepository(BlogComment)
    private readonly blogCommentsRepository: Repository<BlogComment>,
  ) {}

  private async createQueryBuilder() {
    try {
      return this.blogCommentsRepository
        .createQueryBuilder('blog-comments')
        .leftJoinAndSelect('blog-comments.user', 'user')
        .leftJoinAndSelect('blog-comments.blog', 'blog')
        .select([
          'blog-comments',
          'user.id',
          'user.first_name',
          'user.last_name',
          'user.avatar_profile_url',
          'blog.id',
          'blog.banner_image_url',
          'blog.title',
          'blog.description',
        ]);
    } catch (error) {
      console.log(
        '🚀 ~ file: blog-reviews.service.ts:16 ~ BlogReviewsService ~ createQueryBuilder ~ error:',
        error,
      );
      throw error;
    }
  }

  async createComment(body: ICreateBlogComment, userId: string) {
    try {
      const result = this.blogCommentsRepository.create({
        ...body,
        user_id: userId,
      });
      return await this.blogCommentsRepository.save(result);
    } catch (error) {
      console.log(
        '🚀 ~ file: blog-reviews.service.ts:16 ~ BlogReviewsService ~ createReview ~ error:',
        error,
      );
      return false;
    }
  }

  async getOneById(id: string) {
    try {
      const queryBuilder = await this.createQueryBuilder();
      return queryBuilder.where({ id }).getOne();
    } catch (error) {
      console.log(
        '🚀 ~ file: blog-reviews.service.ts:41 ~ BlogReviewsService ~ getOneById ~ error:',
        error,
      );
      throw error;
    }
  }

  async getAllCommentsByBlogId(id: string, query: PaginateQuery) {
    const queryBuilder = await this.createQueryBuilder();
    queryBuilder.where({ blog: { id } });
    return await getPaginate(query, queryBuilder);
  }

  async getListCommentsByBlogId(
    query: PaginateQuery,
    filter: IFilterBlogComment,
    isAdmin: boolean,
  ) {
    try {
      const queryBuilder = await this.createQueryBuilder();
      if (!isAdmin) queryBuilder.where({ is_hide: false });
      if (filter?.blog_id) {
        queryBuilder.andWhere('blog.id = :blogId', { blogId: filter.blog_id });
      }
      if (filter?.user_id) {
        queryBuilder.andWhere('user.id = :userId', {
          userId: filter.user_id,
        });
      }
      return await getPaginate(query, queryBuilder);
    } catch (error) {
      console.log(
        '🚀 ~ file: blog-reviews.service.ts:65 ~ BlogReviewsService ~ getListReviewsByBlogId ~ error:',
        error,
      );
      throw error;
    }
  }

  async updateComment(id: string, body: IClientUpdateBlogComment) {
    try {
      return await this.blogCommentsRepository.update(id, body);
    } catch (error) {
      console.log(
        '🚀 ~ file: blog-reviews.service.ts:92 ~ BlogReviewsService ~ updateReview ~ error:',
        error,
      );
      throw error;
    }
  }

  async deleteComment(id: string) {
    try {
      return await this.blogCommentsRepository.delete(id);
    } catch (error) {
      console.log(
        '🚀 ~ file: blog-reviews.service.ts:104 ~ BlogReviewsService ~ deleteReview ~ error:',
        error,
      );
      throw error;
    }
  }
}
