import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BlogComment } from './entities/blog-comments.entity';
import { BlogCommentsService } from './blog-comments.service';
import { ClientBlogCommentsService } from '../../client/blog-comments/blog-comments.service';
import { OperatorBlogCommentsService } from '../../operator/blog-comments/blog-comments.service';
import { VendorBlogCommentsService } from '../../vendor/blog-comments/blog-comments.service';

@Module({
  imports: [TypeOrmModule.forFeature([BlogComment])],
  providers: [
    BlogCommentsService,
    ClientBlogCommentsService,
    OperatorBlogCommentsService,
    VendorBlogCommentsService,
  ],
  exports: [
    BlogCommentsService,
    ClientBlogCommentsService,
    OperatorBlogCommentsService,
    VendorBlogCommentsService,
  ],
})
export class BlogCommentsModule {}
