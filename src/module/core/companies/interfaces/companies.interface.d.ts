import type { IBaseEntity } from 'src/shared/interfaces/base-entity.interface';

export type ICreateCompany = Pick<
  ICompany,
  'homepage' | 'logo' | 'long_name' | 'short_name'
>;
export type IUpdateCompany = Partial<ICreateCompany>;

export interface ICompany extends IBaseEntity {
  logo: string;
  long_name: string;
  short_name: string;
  homepage: string;
}
