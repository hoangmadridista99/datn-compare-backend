import { PartialType } from '@nestjs/mapped-types';
import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsString, IsUrl } from 'class-validator';
import { LIST_ERROR_CODES } from 'src/shared/constants/error/base.error';

export class CreateCompanyDto {
  @ApiProperty({
    description: 'Long name company',
    example: 'Công ty TNHH Manulife',
  })
  @IsString({ message: LIST_ERROR_CODES.SS24702 })
  long_name: string;

  @ApiProperty({
    description: 'Short name company',
    example: 'Manulife',
  })
  @IsString({ message: LIST_ERROR_CODES.SS24702 })
  short_name: string;

  @ApiProperty({
    description: 'Link homepage',
    example: 'https://www.manulife.com.vn/',
  })
  @IsUrl(undefined, { message: LIST_ERROR_CODES.SS24701 })
  homepage: string;
}
export class CreateCompanyHaveFileDto extends CreateCompanyDto {
  @ApiProperty({
    type: 'string',
    format: 'binary',
  })
  file: any;
}
export class UpdateCompanyDto extends PartialType(CreateCompanyDto) {}
export class UpdateCompanyHaveFileDto extends UpdateCompanyDto {
  @ApiProperty({
    type: 'string',
    format: 'binary',
    required: false,
  })
  @IsOptional()
  file: any;
}
