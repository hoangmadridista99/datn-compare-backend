import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, Repository, SelectQueryBuilder } from 'typeorm';
import { Company } from './entities/company.entity';
import {
  ICreateCompany,
  IUpdateCompany,
} from './interfaces/companies.interface';

export class CompaniesService {
  constructor(
    @InjectRepository(Company)
    private readonly repositoryCompanies: Repository<Company>,
  ) {}

  queryBuilder(): SelectQueryBuilder<Company> | null {
    try {
      return this.repositoryCompanies.createQueryBuilder('companies');
    } catch (error) {
      console.log(
        '🚀 ~ file: companies.service.ts:19 ~ CompaniesService ~ queryBuilder:',
        error,
      );

      return null;
    }
  }

  async findAllByQuery(
    query?: FindManyOptions<Company>,
  ): Promise<Company[] | null> {
    try {
      return await this.repositoryCompanies.find(query);
    } catch (error) {
      console.log(
        '🚀 ~ file: companies.service.ts:19 ~ CompaniesService ~ findAll:',
        error,
      );

      return null;
    }
  }

  async findById(id: string): Promise<Company | null> {
    try {
      return await this.repositoryCompanies.findOneBy({ id });
    } catch (error) {
      console.log(
        '🚀 ~ file: companies.service.ts:47 ~ CompaniesService ~ findById:',
        error,
      );

      return null;
    }
  }

  async createCompany(data: ICreateCompany): Promise<Company | null> {
    try {
      const company = this.repositoryCompanies.create(data);
      return await this.repositoryCompanies.save(company);
    } catch (error) {
      console.log(
        '🚀 ~ file: companies.service.ts:16 ~ CompaniesService ~ createCompany:',
        error,
      );

      return null;
    }
  }

  async updateCompany(id: string, data: IUpdateCompany): Promise<boolean> {
    try {
      const result = await this.repositoryCompanies.update(id, data);
      return !!result.affected;
    } catch (error) {
      console.log(
        '🚀 ~ file: companies.service.ts:31 ~ CompaniesService ~ updateCompany:',
        error,
      );

      return false;
    }
  }

  async removeCompany(id: string): Promise<boolean> {
    try {
      const result = await this.repositoryCompanies.delete(id);
      return !!result.affected;
    } catch (error) {
      console.log(
        '🚀 ~ file: companies.service.ts:48 ~ CompaniesService ~ removeCompany:',
        error,
      );

      return false;
    }
  }
}
