import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Company } from './entities/company.entity';
import { CompaniesService } from './companies.service';
import { OperatorCompaniesService } from 'src/module/operator/companies/companies.service';
import { ClientCompaniesService } from 'src/module/client/companies/companies.service';
import { MinioModule } from '../minio/minio.module';
import { CompanyExistRule } from '../../../shared/decorators/company-id-validation.decorator';

@Module({
  imports: [TypeOrmModule.forFeature([Company]), MinioModule],
  providers: [
    CompaniesService,
    OperatorCompaniesService,
    ClientCompaniesService,
    CompanyExistRule,
  ],
  exports: [CompaniesService, OperatorCompaniesService, ClientCompaniesService],
})
export class CompaniesModule {}
