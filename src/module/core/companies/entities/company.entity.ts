import { Column, Entity, OneToMany } from 'typeorm';
import { BaseEntity } from 'src/shared/entities/base.entity';
import { Insurance } from '../../insurances/entities/insurance.entity';
export enum CompanyTypes {
  Default = 'default',
  Vehicle = 'physical',
  Compulsory = 'compulsory ',
}
type ICompanyTypes = CompanyTypes;
@Entity('companies')
export class Company extends BaseEntity {
  @Column()
  logo: string;

  @Column({ default: CompanyTypes.Default })
  type: ICompanyTypes;

  @Column()
  long_name: string;

  @Column()
  short_name: string;

  @Column()
  homepage: string;

  @OneToMany(() => Insurance, (insurance) => insurance.company)
  insurance: Insurance;
}
