import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppraisalForm } from './entities/appraisal-form.entity';
import { AppraisalFormsService } from './appraisal-forms.service';
import { ClientAppraisalFormsService } from '../../client/appraisal-forms/appraisal-forms.service';
import { MinioService } from '../minio/minio.service';
import { HttpModule } from '@nestjs/axios';
import { OperatorAppraisalFormsService } from '../../operator/appraisal-forms/appraisal-forms.service';
import { CarInsurancesService } from '../car-insurances/car-insurances.service';
import { CarInsurance } from '../car-insurances/entities/car-insurance.entity';
import { OperatorCompaniesService } from '../../operator/companies/companies.service';
import { CompaniesService } from '../companies/companies.service';
import { Company } from '../companies/entities/company.entity';
import { AppraisalsService } from '../appraisals/appraisals.service';
import { Appraisal } from '../appraisals/entities/appraisal.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([AppraisalForm, CarInsurance, Company, Appraisal]),
    HttpModule,
  ],
  providers: [
    AppraisalFormsService,
    ClientAppraisalFormsService,
    MinioService,
    OperatorAppraisalFormsService,
    CarInsurancesService,
    OperatorCompaniesService,
    CompaniesService,
    AppraisalsService,
  ],
  exports: [
    AppraisalFormsService,
    ClientAppraisalFormsService,
    OperatorAppraisalFormsService,
  ],
})
export class AppraisalFormsModule {}
