export enum AppraisalFormTypes {
  Unexpired = 'unexpired',
  HasExpired = 'has-expired',
  NewCar = 'new-car',
}

export enum InvoiceIssuanceTypes {
  Business = 'business',
  Individual = 'individual',
}

export enum AppraisalFormStatuses {
  Pending = 'pending',
  Approved = 'approved',
  Rejected = 'rejected',
  Paid = 'paid',
  Null = 'null',
}

export enum PurposeOfUseTypes {
  FamilyCar = 'family-car',
  ContractedService = 'contracted-service',
  TechnologyBusiness = 'technology-business',
}
