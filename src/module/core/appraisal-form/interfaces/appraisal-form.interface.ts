import { IBaseEntity } from '../../../../shared/interfaces/base-entity.interface';
import {
  AppraisalFormStatuses,
  AppraisalFormTypes,
  InvoiceIssuanceTypes,
} from '../enum/appraisal-form.enum';

/** Entity */
export interface IAppraisalForm extends IBaseEntity {
  /** Mã số */
  form_code?: string;
  /** Trạng thái */
  form_status?: IAppraisalFormStatus;

  /** Biển số xe */
  car_license_plate: string;

  /** Số chỗ ngồi */
  seating_capacity: number;

  /** Nhãn hiệu */
  car_brand: string;

  /** Dòng xe */
  car_model: string;

  /** Phiên bản */
  car_version: string;

  /** Năm sản xuất */
  year_of_production: string;

  /** Chọn loại hình bảo hiểm hiện có */
  form_type: IAppraisalFormType;

  /** Giá trị xe */
  car_cost?: number | null;

  /** Khấu trừ 1.000.000 vnđ */
  is_deduct_one_million_vnd: boolean;

  /** Tổn thất toàn bộ xe */
  is_complete_vehicle_damage: boolean;

  /** Tổn thất bộ phận xe */
  is_component_vehicle_damage: boolean;

  /** Mất cắp toàn bộ xe */
  is_total_vehicle_theft: boolean;

  /** Miễn phí cứu hộ */
  free_roadside_assistance: boolean;

  /** Lựa chọn cơ sở chính hãng */
  is_choosing_service_center: boolean;

  /** Mất cắp bộ phận */
  is_component_vehicle_theft: boolean;

  /** Không tính khấu hao phụ tùng, vật tư thay mới */
  is_no_depreciation_cost: boolean;

  /** Thiệt hại động cơ do ảnh hưởng của nước */
  is_water_damage: boolean;

  /** Bảo hiểm tai nạn người ngồi trên xe: 100.000.000 vnđ/ chỗ */
  is_insured_for_each_person: boolean;

  /** Ngày hết hạn GCN đang có */
  certificate_expiration_date: string;

  /** Tải lên hình ảnh GCN */
  certificate_image_url?: string | null;

  /** Tải lên hình ảnh xe của bạn  */
  vehicle_condition_image_urls?: Array<string> | [];

  /** Thời hạn bảo hiểm */
  insurance_period_from: Date;
  insurance_period_to: Date;

  /** Chủ hợp đồng */
  policyholder_full_name: string;
  policyholder_phone_number: string;
  policyholder_address: string;
  policyholder_email: string;

  /** Thông tin ngân hàng */
  bank_name: string | null;
  bank_branch: string | null;
  branch_address: string | null;

  /** Thông tin nhân viên phụ trách */
  staff_full_name: string | null;
  staff_phone_number: string | null;
  staff_other_requirements: string | null;

  /** Xuất hóa đơn */
  invoice_type: `${InvoiceIssuanceTypes}`;
  invoice_name: string | null;
  invoice_tax_id: string | null;
  invoice_address: string | null;
  invoice_email: string | null;
}

/** DTO */
export type ICreateAppraisalFromDto = Omit<IAppraisalForm, keyof IBaseEntity>;
export interface IUpdateAppraisalFormDto
  extends Partial<ICreateAppraisalFromDto> {
  update_image_urls?: string[];
}
export interface IUpdateAppraisalFormDtoPipe
  extends Omit<IUpdateAppraisalFormDto, 'update_image_urls'> {
  update_image_urls: string;
}
export interface IVerifyAppraisalFormDto
  extends Pick<IUpdateAppraisalFormDto, 'form_status'> {
  reason: string;
}

/** Filter */
export interface IAppraisalFormsFilter {
  page: number;
  limit: number;
  form_status?: IAppraisalFormStatus;
  form_code?: string;
}

export type IAppraisalFormType = `${AppraisalFormTypes}`;
export type IAppraisalFormStatus = `${AppraisalFormStatuses}`;
