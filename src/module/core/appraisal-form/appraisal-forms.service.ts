import { InjectRepository } from '@nestjs/typeorm';
import { AppraisalForm } from './entities/appraisal-form.entity';
import { FindOptionsWhere, ILike, Repository } from 'typeorm';
import {
  IAppraisalFormsFilter,
  ICreateAppraisalFromDto,
  IUpdateAppraisalFormDto,
} from './interfaces/appraisal-form.interface';
import {
  generateRandomString,
  getFileKey,
  getFileName,
  getFileUrl,
} from '../../../shared/helper/system.helper';
import { MinioService } from '../minio/minio.service';
import { MinioTypes } from '../minio/enums/minio.enum';
import { AppraisalFormStatuses } from './enum/appraisal-form.enum';
import { getPaginate } from '../../../shared/lib/paginate/paginate.lib';
import { PaginateQuery } from 'nestjs-paginate';
type IQueryType =
  | FindOptionsWhere<AppraisalForm>
  | Array<FindOptionsWhere<AppraisalForm>>;

export class AppraisalFormsService {
  constructor(
    @InjectRepository(AppraisalForm)
    private readonly appraisalFormsRepository: Repository<AppraisalForm>,
    private readonly minioService: MinioService,
  ) {}

  private getQueryBuilder() {
    try {
      return this.appraisalFormsRepository.createQueryBuilder(
        'appraisal-forms',
      );
    } catch (error) {
      throw error;
    }
  }

  async generateRandomFormCode(length: number) {
    const code = generateRandomString(length);
    const checkExistCode = await this.findOneFormByQuery({ form_code: code });
    if (!!checkExistCode) return this.generateRandomFormCode(8);
    return code;
  }

  async createAppraisalForm(userId: string, body: ICreateAppraisalFromDto) {
    try {
      const formCode = await this.generateRandomFormCode(8);
      const result = this.appraisalFormsRepository.create({
        ...body,
        user_id: userId,
        form_status: AppraisalFormStatuses.Pending,
        form_code: formCode,
      });
      return await this.appraisalFormsRepository.save(result);
    } catch (error) {
      throw error;
    }
  }

  async findOneFormByQuery(query: IQueryType): Promise<AppraisalForm | null> {
    try {
      const queryBuilder = this.getQueryBuilder()
        .leftJoinAndSelect('appraisal-forms.appraisal', 'appraisal_data')
        .where(query);
      return await queryBuilder.getOne();
    } catch (error) {
      throw error;
    }
  }

  async saveAppraisalForm(body: AppraisalForm) {
    try {
      return await this.appraisalFormsRepository.save(body);
    } catch (error) {
      throw error;
    }
  }

  async getFormsListForAdmin(
    query: PaginateQuery,
    filter: IAppraisalFormsFilter,
  ) {
    try {
      const queryBuilder = this.getQueryBuilder()
        .leftJoin('appraisal-forms.user', 'user')
        .select([
          'appraisal-forms.id',
          'appraisal-forms.car_cost',
          'appraisal-forms.form_code',
          'appraisal-forms.form_status',
          'appraisal-forms.created_at',
          'appraisal-forms.reason',
          'user.first_name',
          'user.last_name',
        ]);
      if (
        filter.form_status &&
        filter.form_status !== AppraisalFormStatuses.Null
      ) {
        queryBuilder.where({ form_status: filter.form_status });
      }
      if (filter.form_code) {
        queryBuilder.andWhere({ form_code: ILike(`%${filter.form_code}%`) });
      }

      return await getPaginate(query, queryBuilder);
    } catch (error) {
      throw error;
    }
  }

  async getListFormsByUserId(userId: string, query: PaginateQuery) {
    try {
      const queryBuilder = this.getQueryBuilder().where({ user_id: userId });
      return await getPaginate(query, queryBuilder);
    } catch (error) {
      throw error;
    }
  }

  async updateAppraisalFormById(id: string, body: IUpdateAppraisalFormDto) {
    try {
      const result = await this.appraisalFormsRepository.update(id, body);
      return !!result.affected;
    } catch (error) {
      throw error;
    }
  }

  async uploadAppraisalFormImages(files: Array<Express.Multer.File>) {
    if (!files.length) return null;
    const result = files.map(async (file) => {
      const fileName = getFileName(file.originalname);
      const imageKey = getFileKey(MinioTypes.APPRAISAL_FORM, fileName);
      const uploadImage = await this.minioService.uploadFile(imageKey, file);
      return !uploadImage
        ? null
        : getFileUrl(MinioTypes.APPRAISAL_FORM, fileName) ?? null;
    });
    return Promise.all(result);
  }

  async deleteAppraisalFormById(id: string) {
    try {
      await this.appraisalFormsRepository.delete(id);
    } catch (error) {
      throw error;
    }
  }
}
