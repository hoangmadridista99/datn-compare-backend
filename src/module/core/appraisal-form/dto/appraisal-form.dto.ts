import { ApiProperty, PartialType } from '@nestjs/swagger';
import {
  IsArray,
  IsBooleanString,
  IsDateString,
  IsEnum,
  IsNotEmpty,
  IsNumberString,
  IsOptional,
  IsPhoneNumber,
  IsString,
  ValidateIf,
} from 'class-validator';
import {
  IAppraisalFormStatus,
  IAppraisalFormType,
} from '../interfaces/appraisal-form.interface';
import {
  AppraisalFormStatuses,
  AppraisalFormTypes,
  InvoiceIssuanceTypes,
  PurposeOfUseTypes,
} from '../enum/appraisal-form.enum';
import { LIST_ERROR_CODES } from '../../../../shared/constants/error/base.error';

export class VerifyAppraisalFormDto {
  @ApiProperty({ example: AppraisalFormStatuses.Approved })
  @IsEnum(AppraisalFormStatuses)
  form_status: IAppraisalFormStatus;

  @ApiProperty({ example: 'reason' })
  @IsString()
  reason: string;
}
export class AppraisalFormDto {
  @ApiProperty({ example: 'string' })
  @IsString()
  @IsNotEmpty()
  car_license_plate: string;

  @ApiProperty({ example: '2023-12-30' })
  @ValidateIf((_, value) => !!value)
  @IsDateString()
  @IsNotEmpty()
  @IsOptional()
  certificate_expiration_date: string;

  @ApiProperty({ example: 5 })
  @IsNumberString()
  @IsNotEmpty()
  seating_capacity: number;

  @ApiProperty({ example: 'string' })
  @IsString()
  @IsNotEmpty()
  car_brand: string;

  @ApiProperty({ example: 'string' })
  @IsString()
  @IsNotEmpty()
  car_model: string;

  @ApiProperty({ example: 'string' })
  @IsString()
  @IsNotEmpty()
  car_version: string;

  @ApiProperty({ example: 'string' })
  @IsString()
  @IsNotEmpty()
  year_of_production: string;

  @ApiProperty({ example: AppraisalFormTypes.Unexpired })
  @IsEnum(AppraisalFormTypes)
  @IsNotEmpty()
  form_type: IAppraisalFormType;

  @ApiProperty({ example: true })
  @IsBooleanString()
  @IsNotEmpty()
  is_transportation_business: boolean;

  @ApiProperty({ example: PurposeOfUseTypes.ContractedService })
  @ValidateIf((_, value) => value !== null)
  @IsEnum(PurposeOfUseTypes)
  purpose: `${PurposeOfUseTypes}`;

  @ApiProperty({ example: true })
  @IsBooleanString()
  @IsNotEmpty()
  is_deduct_one_million_vnd: boolean;

  @ApiProperty({ example: true })
  @IsBooleanString()
  @IsNotEmpty()
  is_complete_vehicle_damage: boolean;

  @ApiProperty({ example: true })
  @IsBooleanString()
  @IsNotEmpty()
  is_component_vehicle_damage: boolean;

  @ApiProperty({ example: true })
  @IsBooleanString()
  @IsNotEmpty()
  is_total_vehicle_theft: boolean;

  @ApiProperty({ example: true })
  @IsBooleanString()
  @IsNotEmpty()
  free_roadside_assistance: boolean;

  @ApiProperty({ example: true })
  @IsBooleanString()
  @IsNotEmpty()
  is_choosing_service_center: boolean;

  @ApiProperty({ example: true })
  @IsBooleanString()
  @IsNotEmpty()
  is_component_vehicle_theft: boolean;

  @ApiProperty({ example: true })
  @IsBooleanString()
  @IsNotEmpty()
  is_no_depreciation_cost: boolean;

  @ApiProperty({ example: true })
  @IsBooleanString()
  @IsNotEmpty()
  is_water_damage: boolean;

  @ApiProperty({ example: true })
  @IsBooleanString()
  @IsNotEmpty()
  is_insured_for_each_person: boolean;

  @ApiProperty({ example: '2022-12-30' })
  @IsDateString()
  @IsNotEmpty()
  insurance_period_from: Date;

  @ApiProperty({ example: '2023-12-30' })
  @IsDateString()
  @IsNotEmpty()
  insurance_period_to: Date;

  @ApiProperty({ example: 'string' })
  @IsString()
  @IsNotEmpty()
  policyholder_full_name: string;

  @ApiProperty({ example: 'string' })
  @IsString()
  @IsNotEmpty()
  policyholder_address: string;

  @ApiProperty({ example: '0909090909' })
  @IsString()
  @IsNotEmpty()
  @IsPhoneNumber('VN', { message: LIST_ERROR_CODES.SS24310 })
  policyholder_phone_number: string;

  @ApiProperty({ example: 'string' })
  @IsString()
  @IsNotEmpty()
  policyholder_email: string;

  @ApiProperty({ example: 'string' })
  @ValidateIf((_, value) => !!value)
  @IsOptional()
  @IsString()
  @IsNotEmpty()
  bank_name: string;

  @ApiProperty({ example: 'string' })
  @ValidateIf((_, value) => !!value)
  @IsOptional()
  @IsString()
  @IsNotEmpty()
  bank_branch: string;

  @ApiProperty({ example: 'string' })
  @ValidateIf((_, value) => !!value)
  @IsOptional()
  @IsString()
  @IsNotEmpty()
  branch_address: string;

  @ApiProperty({ example: 'string' })
  @ValidateIf((_, value) => !!value)
  @IsOptional()
  @IsString()
  @IsNotEmpty()
  staff_full_name: string;

  @ApiProperty({ example: '0909090909' })
  @ValidateIf((_, value) => !!value)
  @IsOptional()
  @IsString()
  @IsNotEmpty()
  staff_phone_number: string;

  @ApiProperty({ example: 'string' })
  @ValidateIf((_, value) => !!value)
  @IsOptional()
  @IsString()
  @IsNotEmpty()
  staff_other_requirements: string;

  @ApiProperty({ example: InvoiceIssuanceTypes.Business })
  @ValidateIf((_, value) => !!value)
  @IsOptional()
  @IsEnum(InvoiceIssuanceTypes)
  @IsNotEmpty()
  invoice_type: `${InvoiceIssuanceTypes}`;

  @ApiProperty({ example: 'string' })
  @ValidateIf((_, value) => !!value)
  @IsOptional()
  @IsString()
  @IsNotEmpty()
  invoice_name: string;

  @ApiProperty({ example: 'string' })
  @ValidateIf((_, value) => !!value)
  @IsOptional()
  @IsString()
  @IsNotEmpty()
  invoice_tax_id: string;

  @ApiProperty({ example: 'string' })
  @ValidateIf((_, value) => !!value)
  @IsOptional()
  @IsString()
  @IsNotEmpty()
  invoice_email: string;

  @ApiProperty({ example: 'string' })
  @ValidateIf((_, value) => !!value)
  @IsOptional()
  @IsString()
  @IsNotEmpty()
  invoice_address: string;
}

export class UpdateAppraisalFormDto extends PartialType(AppraisalFormDto) {
  @ApiProperty({
    type: 'array',
    items: {
      type: 'string',
    },
    required: false,
  })
  @IsArray()
  @IsString({ each: true })
  @IsOptional()
  @ValidateIf((_, value) => !!value)
  update_image_urls?: string[];
}

export class CreateAppraisalFormDto extends AppraisalFormDto {
  @ApiProperty({
    type: 'array',
    items: {
      type: 'string',
      format: 'binary',
    },
    required: false,
  })
  @IsOptional()
  files: any;
}
export class UpdateAppraisalApiBody extends UpdateAppraisalFormDto {
  @ApiProperty({
    type: 'array',
    items: {
      type: 'string',
      format: 'binary',
    },
    required: false,
  })
  @IsOptional()
  files: any;
}
