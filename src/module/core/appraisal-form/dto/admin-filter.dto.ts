import { IsEnum, IsNumberString, IsOptional, IsString } from 'class-validator';
import { AppraisalFormStatuses } from '../enum/appraisal-form.enum';
import { IAppraisalFormStatus } from '../interfaces/appraisal-form.interface';

export class AdminFilterAppraisalFormsListDto {
  @IsNumberString()
  page: number;

  @IsNumberString()
  limit: number;

  @IsOptional()
  @IsEnum(AppraisalFormStatuses)
  form_status: IAppraisalFormStatus;

  @IsOptional()
  @IsString()
  form_code: string;
}
