import { Column, Entity, JoinColumn, ManyToOne, OneToOne } from 'typeorm';
import { BaseEntity } from '../../../../shared/entities/base.entity';
import {
  IAppraisalFormStatus,
  IAppraisalFormType,
} from '../interfaces/appraisal-form.interface';
import {
  AppraisalFormStatuses,
  InvoiceIssuanceTypes,
  PurposeOfUseTypes,
} from '../enum/appraisal-form.enum';
import { User } from '../../users/entities/user.entity';
import { Appraisal } from '../../appraisals/entities/appraisal.entity';

@Entity('appraisal-forms')
export class AppraisalForm extends BaseEntity {
  /** Mã số */
  @Column({ nullable: true })
  form_code: string;

  /** Lý do */
  @Column({ nullable: true, default: null })
  reason: string;

  /** Trạng thái */
  @Column({ type: 'enum', enum: AppraisalFormStatuses })
  form_status: IAppraisalFormStatus;

  /** Biển số xe */
  @Column()
  car_license_plate: string;

  /** Số chỗ ngồi */
  @Column()
  seating_capacity: number;

  /** Nhãn hiệu */
  @Column()
  car_brand: string;

  /** Dòng xe */
  @Column()
  car_model: string;

  /** Phiên bản */
  @Column()
  car_version: string;

  /** Năm sản xuất */
  @Column()
  year_of_production: string;

  /** Chọn loại hình bảo hiểm hiện có */
  @Column()
  form_type: IAppraisalFormType;

  @Column()
  is_transportation_business: boolean;

  @Column({ type: 'enum', enum: PurposeOfUseTypes })
  purpose: `${PurposeOfUseTypes}`;

  /** Giá trị xe */
  @Column({ nullable: true, type: 'bigint' })
  car_cost: number | null;

  /** Khấu trừ 1.000.000 vnđ */
  @Column()
  is_deduct_one_million_vnd: boolean;

  /** Tổn thất toàn bộ xe */
  @Column()
  is_complete_vehicle_damage: boolean;

  /** Tổn thất bộ phận xe */
  @Column()
  is_component_vehicle_damage: boolean;

  /** Mất cắp toàn bộ xe */
  @Column()
  is_total_vehicle_theft: boolean;

  /** Miễn phí cứu hộ */
  @Column()
  free_roadside_assistance: boolean;

  /** Lựa chọn cơ sở chính hãng */
  @Column()
  is_choosing_service_center: boolean;

  /** Mất cắp bộ phận */
  @Column()
  is_component_vehicle_theft: boolean;

  /** Không tính khấu hao phụ tùng, vật tư thay mới */
  @Column()
  is_no_depreciation_cost: boolean;

  /** Thiệt hại động cơ do ảnh hưởng của nước */
  @Column()
  is_water_damage: boolean;

  /** Bảo hiểm tai nnaj người ngồi trên xe: 100.000.000 vnđ/ chỗ */
  @Column()
  is_insured_for_each_person: boolean;

  /** Ngày hết hạn GCN đang có */
  @Column({ nullable: true, default: null })
  certificate_expiration_date: string;

  /** Tải lên hình ảnh GCN */
  @Column({ nullable: true, default: null })
  certificate_image_url: string;

  /** Tải lên hình ảnh xe của bạn  */
  @Column({ type: 'jsonb', nullable: true, default: null })
  vehicle_condition_image_urls: Array<string>;

  /** Thời hạn bảo hiểm (1 năm) */
  @Column()
  insurance_period_from: Date;

  @Column()
  insurance_period_to: Date;

  /** Chủ hợp đông */
  @Column()
  policyholder_full_name: string;

  @Column()
  policyholder_phone_number: string;

  @Column()
  policyholder_address: string;

  @Column()
  policyholder_email: string;

  /** Tên ngân hàng */
  @Column({ nullable: true, default: null })
  bank_name: string;

  /** Chi nhánh ngân hàng */
  @Column({ nullable: true, default: null })
  bank_branch: string;

  /** Địa chỉ chi nhánh */
  @Column({ nullable: true, default: null })
  branch_address: string;

  /** Thông tin nhân viên phụ trách */
  /** Tên nhân viên */
  @Column({ nullable: true, default: null })
  staff_full_name: string;

  /** Số điện thoại */
  @Column({ nullable: true, default: null })
  staff_phone_number: string;

  /** Yêu cầu khác */
  @Column({ nullable: true, default: null })
  staff_other_requirements: string;

  /** Kiểu (doanh nghiệp / cá nhân) */
  @Column({
    nullable: true,
    default: null,
  })
  invoice_type: `${InvoiceIssuanceTypes}`;

  /** Tên */
  @Column({ nullable: true, default: null })
  invoice_name: string;

  /** Mã số thuế */
  @Column({ nullable: true, default: null })
  invoice_tax_id: string;

  /** Địa chỉ */
  @Column({ nullable: true, default: null })
  invoice_address: string;

  /** Email */
  @Column({ nullable: true, default: null })
  invoice_email: string;

  @Column()
  user_id: string;

  @JoinColumn({ name: 'user_id', referencedColumnName: 'id' })
  @ManyToOne(() => User, (user) => user.id)
  user: User;

  @JoinColumn()
  @OneToOne(() => Appraisal)
  appraisal: Appraisal;
}
