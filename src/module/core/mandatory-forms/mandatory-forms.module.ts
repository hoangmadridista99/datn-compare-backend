import { TypeOrmModule } from '@nestjs/typeorm';
import { MandatoryForm } from './entities/mandatory-form.entity';
import { Module } from '@nestjs/common';
import { ClientMandatoryFormsService } from '../../client/mandatory-forms/mandatory-forms.service';
import { MandatoryFormsService } from './mandatory-forms.service';

@Module({
  imports: [TypeOrmModule.forFeature([MandatoryForm])],
  providers: [MandatoryFormsService, ClientMandatoryFormsService],
  exports: [MandatoryFormsService, ClientMandatoryFormsService],
})
export class MandatoryFormsModule {}
