import {
  IsBoolean,
  IsDateString,
  IsEnum,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsPhoneNumber,
  IsString,
  IsUUID,
  ValidateIf,
} from 'class-validator';
import {
  IMandatoryCarType,
  MandatoryCarTypes,
} from '../interfaces/mandatory-form.interface';
import { LIST_ERROR_CODES } from '../../../../shared/constants/error/base.error';
import { ApiProperty } from '@nestjs/swagger';

export class CreateMandatoryFormDto {
  @ApiProperty({ example: true })
  @IsBoolean()
  @IsNotEmpty()
  is_transportation_business: boolean;

  @ApiProperty({ example: MandatoryCarTypes.PassengerCargoCar })
  @IsEnum(MandatoryCarTypes)
  @IsNotEmpty()
  car_type: IMandatoryCarType;

  @ApiProperty({ example: 7 })
  @IsNumber()
  @IsNotEmpty()
  car_seats: number;

  @ApiProperty({ example: 'string' })
  @IsString()
  @IsNotEmpty()
  registration_holder_name: string;

  @ApiProperty({ example: 'string' })
  @IsString()
  @IsNotEmpty()
  registration_address: string;

  @ApiProperty({ example: 'string' })
  @IsString()
  @IsNotEmpty()
  registration_brand: string;

  @ApiProperty({ example: 'string' })
  @IsString()
  @IsNotEmpty()
  @IsOptional()
  @ValidateIf((_, value) => !!value)
  registration_car_license_plate: string;

  @ApiProperty({ example: 'string' })
  @IsString()
  @IsNotEmpty()
  @IsOptional()
  @ValidateIf((_, value) => !!value)
  registration_chassis_number: string;

  @ApiProperty({ example: 'string' })
  @IsString()
  @IsNotEmpty()
  @IsOptional()
  @ValidateIf((_, value) => !!value)
  registration_engine_number: string;

  @ApiProperty({ example: 'string' })
  @IsString()
  @IsNotEmpty()
  certificate_holder_name: string;

  @ApiProperty({ example: '0912123342' })
  @IsString()
  @IsNotEmpty()
  @IsPhoneNumber('VN', { message: LIST_ERROR_CODES.SS24310 })
  certificate_holder_phone: string;

  @ApiProperty({ example: 'string' })
  @IsString()
  @IsNotEmpty()
  certificate_holder_identification_card: string;

  @ApiProperty({ example: 'string' })
  @IsString()
  @IsNotEmpty()
  certificate_holder_email: string;

  @ApiProperty({ example: 'string' })
  @IsString()
  @IsNotEmpty()
  certificate_holder_address: string;

  @ApiProperty({ example: 'string' })
  @IsUUID()
  @IsNotEmpty()
  car_insurance_id: string;

  @ApiProperty({ example: '2023-01-30' })
  @IsDateString()
  @IsNotEmpty()
  expiration_from: Date;

  @ApiProperty({ example: '2024-01-30' })
  @IsDateString()
  @IsNotEmpty()
  expiration_to: Date;
}
