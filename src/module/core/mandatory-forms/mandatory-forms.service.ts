import { InjectRepository } from '@nestjs/typeorm';
import { MandatoryForm } from './entities/mandatory-form.entity';
import { Repository } from 'typeorm';
import { User } from '../users/entities/user.entity';
import {
  ICreateMandatoryFormDto,
  MandatoryCarPaymentStatusTypes,
} from './interfaces/mandatory-form.interface';
import { PaginateQuery } from 'nestjs-paginate';
import { getPaginate } from '../../../shared/lib/paginate/paginate.lib';

export class MandatoryFormsService {
  constructor(
    @InjectRepository(MandatoryForm)
    private readonly mandatoryFormsRepository: Repository<MandatoryForm>,
  ) {}

  async createNewMandatoryForm(user: User, body: ICreateMandatoryFormDto) {
    try {
      const result = this.mandatoryFormsRepository.create({
        ...body,
        user,
        form_payment_status: MandatoryCarPaymentStatusTypes.InProgress,
      });
      return await this.mandatoryFormsRepository.save(result);
    } catch (error) {
      throw error;
    }
  }

  async getMandatoryFormDetails(id: string) {
    try {
      return await this.mandatoryFormsRepository.find({
        where: { id },
        relations: ['car_insurance', 'user'],
      });
    } catch (error) {
      throw error;
    }
  }

  async getMandatoryFormsList(
    query: PaginateQuery,
    userId: string,
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    _filter: any,
  ) {
    try {
      const queryBuilder = this.mandatoryFormsRepository
        .createQueryBuilder('mandatory-forms')
        .leftJoinAndSelect('mandatory-forms.car_insurance', 'car_insurance')
        .leftJoinAndSelect('mandatory-forms.user', 'user')
        .where({ user: { id: userId } });
      return getPaginate(query, queryBuilder);
    } catch (error) {
      throw error;
    }
  }
}
