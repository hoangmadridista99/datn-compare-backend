import { IBaseEntity } from '../../../../shared/interfaces/base-entity.interface';
import { CarInsurance } from '../../car-insurances/entities/car-insurance.entity';
import { User } from '../../users/entities/user.entity';

export enum MandatoryCarTypes {
  LessThanSix = 'less-than-six',
  SixToEleven = 'six-to-eleven',
  PassengerCargoCar = 'passenger-cargo-car',
  SixSeats = '6',
  SevenSeats = '7',
  EightSeats = '8',
  NineSeats = '9',
  TenSeats = '10',
  ElevenSeats = '11',
}
export enum MandatoryCarPaymentStatusTypes {
  InProgress = 'in-progress',
  Failed = 'failed',
  Success = 'success',
}

export type IMandatoryFormPaymentStatus = `${MandatoryCarPaymentStatusTypes}`;
export type IMandatoryCarType = `${MandatoryCarTypes}`;

export interface IMandatoryForm extends IBaseEntity {
  is_transportation_business: boolean;
  car_type: IMandatoryCarType;
  car_seats: number;
  registration_holder_name: string;
  registration_address: string;
  registration_brand: string;
  registration_car_license_plate: string | null;
  registration_chassis_number: string | null;
  registration_engine_number: string | null;
  certificate_holder_name: string;
  certificate_holder_phone: string;
  certificate_holder_identification_card: string;
  certificate_holder_email: string;
  certificate_holder_address: string;
  car_insurance_id: string;
  expiration_from: Date;
  expiration_to: Date;
  car_insurance: CarInsurance;
  user: User;
}

export type ICreateMandatoryFormDto = Omit<
  IMandatoryForm,
  keyof IBaseEntity | 'car_insurance' | 'user'
>;
