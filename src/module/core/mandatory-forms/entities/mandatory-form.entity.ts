import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';
import { BaseEntity } from '../../../../shared/entities/base.entity';
import {
  IMandatoryCarType,
  IMandatoryFormPaymentStatus,
  MandatoryCarPaymentStatusTypes,
  MandatoryCarTypes,
} from '../interfaces/mandatory-form.interface';
import { CarInsurance } from '../../car-insurances/entities/car-insurance.entity';
import { User } from '../../users/entities/user.entity';

@Entity('mandatory-forms')
export class MandatoryForm extends BaseEntity {
  @Column()
  is_transportation_business: boolean;

  @Column({ type: 'enum', enum: MandatoryCarTypes })
  car_type: IMandatoryCarType;

  @Column({ type: 'enum', enum: MandatoryCarPaymentStatusTypes })
  form_payment_status: IMandatoryFormPaymentStatus;

  @Column()
  car_seats: number;

  @Column()
  registration_holder_name: string;

  @Column()
  registration_address: string;

  @Column()
  registration_brand: string;

  @Column({ nullable: true })
  registration_car_license_plate: string;

  @Column({ nullable: true })
  registration_chassis_number: string;

  @Column({ nullable: true })
  registration_engine_number: string;

  @Column()
  certificate_holder_name: string;

  @Column()
  certificate_holder_phone: string;

  @Column()
  certificate_holder_identification_card: string;

  @Column()
  certificate_holder_email: string;

  @Column()
  certificate_holder_address: string;

  @Column()
  car_insurance_id: string;

  @Column({ type: 'timestamptz' })
  expiration_from: Date;

  @Column({ type: 'timestamptz' })
  expiration_to: Date;

  @ManyToOne(() => CarInsurance, (carInsurance) => carInsurance.id)
  @JoinColumn({ name: 'car_insurance_id', referencedColumnName: 'id' })
  car_insurance: CarInsurance;

  @ManyToOne(() => User, (user) => user.id)
  user: User;
}
