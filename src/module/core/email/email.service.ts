import * as nodemailer from 'nodemailer';
import type { SentMessageInfo } from 'nodemailer/lib/smtp-transport';
import { ENV_CONFIG } from 'src/shared/constants/env.constant';
import { Options } from 'nodemailer/lib/mailer';

export class EmailService {
  protected transport: nodemailer.Transporter<SentMessageInfo>;

  constructor() {
    this.transport = nodemailer.createTransport({
      service: 'gmail',
      auth: {
        ...ENV_CONFIG.gmail,
      },
    });
  }

  async sendEmail(destination: string, otp: string) {
    try {
      // TODO: Change Template In HTML
      const options: Options = {
        to: destination,
        subject: 'Sosanh24',
        html: `<p>Your OTP: <b>${otp}</b></p>`,
      };

      const result = await this.transport.sendMail(options);

      return result.accepted.length !== 0;
    } catch (error) {
      console.log(
        '🚀 ~ file: email.service.ts:26 ~ EmailService ~ sendEmail ~ error:',
        error,
      );

      return false;
    }
  }
}
