import { Column, Entity } from 'typeorm';
import { BaseEntity } from '../../../../shared/entities/base.entity';
import { AppraisalStatuses } from '../enum/appraisal.enum';

@Entity('appraisals')
export class Appraisal extends BaseEntity {
  @Column({ type: 'bigint' })
  car_cost: number;

  @Column({
    type: 'enum',
    enum: AppraisalStatuses,
    default: AppraisalStatuses.Created,
  })
  status: `${AppraisalStatuses}`;

  @Column({
    type: 'jsonb',
    nullable: true,
    transformer: {
      to(value: string) {
        try {
          return JSON.stringify(value);
        } catch (error) {
          return null;
        }
      },
      from(value: string) {
        try {
          return JSON.parse(value);
        } catch (error) {
          return null;
        }
      },
    },
  })
  appraisal_data: IAppraisalData | any;
}

export interface IAppraisalData {
  physical_setting_id: string;
  cost_of_purchasing: number;
  choosing_service_center: number;
  component_vehicle_theft: number;
  no_depreciation_cost: number;
  water_damage: number;
  insured_for_each_person: bigint;
  insurance: {
    id: string;
    name: string;
    company: {
      logo: string;
    };
  };
  maximum_price: number;
}
