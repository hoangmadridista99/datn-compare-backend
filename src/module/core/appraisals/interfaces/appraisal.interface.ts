import { IBaseEntity } from '../../../../shared/interfaces/base-entity.interface';
import { IAppraisalForm } from '../../appraisal-form/interfaces/appraisal-form.interface';
import { IInsurance } from '../../insurances/interfaces/insurance.interface';

export interface IAppraisal extends IBaseEntity {
  appraisal: IAppraisalForm;
  car_cost: number;
  insurance: IInsurance;
  appraisal_form_id: string;
}

export type ICreateAppraisalDto = Omit<
  IAppraisal,
  keyof IBaseEntity | 'appraisal' | 'insurance' | 'appraisal_form_id'
>;

export interface IApproveFormDto {
  appraisal_form_id: string;
  physical_settings_ids: Array<string>;
}
