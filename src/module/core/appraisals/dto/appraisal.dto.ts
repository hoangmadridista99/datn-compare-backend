import { ApiProperty } from '@nestjs/swagger';
import { IsArray, IsNotEmpty, IsNumber, IsUUID } from 'class-validator';

export class CreateAppraisalDto {
  @ApiProperty({ example: 1000 })
  @IsNumber()
  @IsNotEmpty()
  car_cost: number;
}

export class UpdateAppraisalDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsUUID()
  appraisal_form_id: string;

  @ApiProperty()
  @IsArray()
  @IsUUID(undefined, { each: true })
  physical_settings_ids: Array<string>;
}
