import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Appraisal } from './entities/appraisal.entity';
import { AppraisalsService } from './appraisals.service';
import { OperatorAppraisalsService } from '../../operator/appraisals/appraisals.service';
import { AppraisalFormsService } from '../appraisal-form/appraisal-forms.service';
import { AppraisalForm } from '../appraisal-form/entities/appraisal-form.entity';
import { MinioService } from '../minio/minio.service';
import { HttpModule } from '@nestjs/axios';
import { CarInsurancesService } from '../car-insurances/car-insurances.service';
import { CarInsurance } from '../car-insurances/entities/car-insurance.entity';
import { OperatorCompaniesService } from '../../operator/companies/companies.service';
import { CompaniesService } from '../companies/companies.service';
import { Company } from '../companies/entities/company.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Appraisal, AppraisalForm, CarInsurance, Company]),
    HttpModule,
  ],
  providers: [
    AppraisalsService,
    OperatorAppraisalsService,
    AppraisalFormsService,
    MinioService,
    CarInsurancesService,
    OperatorCompaniesService,
    CompaniesService,
  ],
  exports: [AppraisalsService, OperatorAppraisalsService],
})
export class AppraisalsModule {}
