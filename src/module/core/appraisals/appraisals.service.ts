import { InjectRepository } from '@nestjs/typeorm';
import { Appraisal, IAppraisalData } from './entities/appraisal.entity';
import { Repository } from 'typeorm';
import {
  IApproveFormDto,
  ICreateAppraisalDto,
} from './interfaces/appraisal.interface';
import { getPaginate } from '../../../shared/lib/paginate/paginate.lib';
import { PaginateQuery } from 'nestjs-paginate';
import { AppraisalFormsService } from '../appraisal-form/appraisal-forms.service';
import { AppraisalFormStatuses } from '../appraisal-form/enum/appraisal-form.enum';
import {
  LIST_ERROR_CODES,
  getError,
} from '../../../shared/constants/error/base.error';
import { HttpStatus } from '@nestjs/common';
import {
  IAppraisalSetting,
  SettingTypes,
} from '../car-insurances/interfaces/car-insurance.interface';
import { CarInsurancesService } from '../car-insurances/car-insurances.service';
import { CarInsurance } from '../car-insurances/entities/car-insurance.entity';

export class AppraisalsService {
  constructor(
    @InjectRepository(Appraisal)
    private readonly appraisalsRepository: Repository<Appraisal>,
    private readonly appraisalFormsService: AppraisalFormsService,
    private readonly carInsurancesService: CarInsurancesService,
  ) {}

  private calculateAddonValueByCarCost(
    carCost: number,
    data: IAppraisalSetting,
  ) {
    if (!data?.type) return;
    if (data.type === SettingTypes.Cost) return data.value;
    return Number(((carCost / 100) * data.value).toFixed());
  }

  private getAppraisalData(data: CarInsurance[], carCost: number) {
    return data.map(({ id, ...setting }) => {
      let maximumPrice = 0;
      const physical_setting_id = id;
      Object.keys(setting).forEach((key) => {
        if (typeof setting[key] === 'object') {
          setting[key] = this.calculateAddonValueByCarCost(
            carCost,
            setting[key],
          );
          maximumPrice += setting[key];
        }
      });
      return {
        physical_setting_id,
        ...setting,
        maximum_price: maximumPrice,
      };
    });
  }

  async handleAppraisal(body: ICreateAppraisalDto) {
    try {
      const carCost = body.car_cost;
      const settingsList =
        await this.carInsurancesService.getCarInsuranceSettings();
      const calculationData = this.getAppraisalData(settingsList, carCost);
      return await this.appraisalsRepository.save({
        ...body,
        appraisal_data: calculationData,
      });
    } catch (error) {
      throw error;
    }
  }

  async saveAppraisal(body: Appraisal) {
    try {
      return await this.appraisalsRepository.save(body);
    } catch (error) {
      throw error;
    }
  }

  async getOneByQuery(query: any) {
    try {
      return await this.appraisalsRepository.findOneBy(query);
    } catch (error) {
      throw error;
    }
  }

  async getAppraisalsList(query: PaginateQuery) {
    try {
      const queryBuilder =
        this.appraisalsRepository.createQueryBuilder('appraisals');
      return await getPaginate(query, queryBuilder);
    } catch (error) {
      throw error;
    }
  }

  async updateAppraisalDataById(id: string, body: IApproveFormDto) {
    try {
      const appraisal = await this.appraisalsRepository.findOneBy({
        id,
      });
      if (!appraisal)
        return getError(LIST_ERROR_CODES.SS24101, HttpStatus.NOT_FOUND);
      const appraisalForm = await this.appraisalFormsService.findOneFormByQuery(
        { id: body.appraisal_form_id },
      );
      if (!appraisalForm)
        return getError(LIST_ERROR_CODES.SS241001, HttpStatus.BAD_REQUEST);
      const newAppraisalData = appraisal.appraisal_data.filter(
        (item: IAppraisalData) =>
          body.physical_settings_ids.includes(item.physical_setting_id),
      );
      appraisal.appraisal_data = newAppraisalData;
      appraisal.car_cost = Number(appraisal.car_cost);
      const updatedAppraisal = await this.appraisalsRepository.save(appraisal);

      return await this.appraisalFormsService.saveAppraisalForm({
        ...appraisalForm,
        car_cost: updatedAppraisal.car_cost,
        form_status: AppraisalFormStatuses.Approved,
        appraisal: updatedAppraisal,
      });
    } catch (error) {
      throw error;
    }
  }
}
