import * as twilio from 'twilio';
import type { Twilio } from 'twilio';
import { ENV_CONFIG } from 'src/shared/constants/env.constant';
import type { MessageListInstanceCreateOptions } from 'twilio/lib/rest/api/v2010/account/message';

export class TwilioService {
  private client: Twilio;

  constructor() {
    this.client = twilio(
      ENV_CONFIG.twilio.accountSID,
      ENV_CONFIG.twilio.authToken,
      {
        region: ENV_CONFIG.twilio.options.region,
      },
    );
  }

  private convertPhoneNumber(phone: string) {
    return `+84${phone.slice(1)}`;
  }

  private makeOtpMessageContent(otp: string) {
    return `Your OTP: ${otp}`;
  }

  async sendSms(phoneNumber: string, otp: string): Promise<boolean> {
    try {
      const convertPhoneNumber = this.convertPhoneNumber(phoneNumber);

      const data: MessageListInstanceCreateOptions = {
        from: ENV_CONFIG.twilio.phoneNumber,
        to: convertPhoneNumber,
        body: this.makeOtpMessageContent(otp),
      };

      await this.client.messages.create(data);
      return true;
    } catch (error) {
      console.log(
        '🚀 ~ file: twilio.service.ts:32 ~ TwilioService ~ sendSms ~ error:',
        error,
      );

      return false;
    }
  }
}
