import {
  IsString,
  IsNotEmpty,
  IsUUID,
  IsOptional,
  IsDateString,
} from 'class-validator';
import { ApiProperty, PartialType } from '@nestjs/swagger';

export class CreateHospitalDto {
  @ApiProperty({ example: 'Bệnh viện 110' })
  @IsString()
  @IsNotEmpty()
  name: string;

  @ApiProperty({ example: 'Thị Cầu, Bắc Ninh' })
  @IsString()
  @IsNotEmpty()
  address: string;

  @ApiProperty({ example: 'Tp.Bắc Ninh' })
  @IsString()
  @IsNotEmpty()
  province: string;

  @ApiProperty({ example: 'Bắc Ninh' })
  @IsString()
  @IsNotEmpty()
  district: string;
}
export class UpdateHospitalDto extends PartialType(CreateHospitalDto) {}

export class InsuranceHospitalDto extends CreateHospitalDto {
  @IsOptional()
  @IsUUID()
  id: string;

  @IsOptional()
  @IsDateString()
  created_at: string;

  @IsOptional()
  @IsDateString()
  updated_at: string;

  @IsOptional()
  @IsDateString()
  deleted_at: string;
}
