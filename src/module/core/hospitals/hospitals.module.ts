import { Module } from '@nestjs/common';
import { HospitalsService } from './hospitals.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Hospital } from './entities/hospital.entity';
import { VendorHospitalsService } from 'src/module/vendor/hospitals/hospitals.service';
import { OperatorHospitalsService } from 'src/module/operator/hospitals/hospitals.service';

@Module({
  imports: [TypeOrmModule.forFeature([Hospital])],
  providers: [
    HospitalsService,
    OperatorHospitalsService,
    VendorHospitalsService,
  ],
  exports: [HospitalsService, OperatorHospitalsService, VendorHospitalsService],
})
export class HospitalsModule {}
