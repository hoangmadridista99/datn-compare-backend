import { Column, Entity, ManyToMany, ManyToOne } from 'typeorm';
import { BaseEntity } from 'src/shared/entities/base.entity';
import { Insurance } from '../../insurances/entities/insurance.entity';
import { User } from '../../users/entities/user.entity';

@Entity('hospitals')
export class Hospital extends BaseEntity {
  @Column()
  name: string;

  @Column()
  address: string;

  @Column()
  province: string;

  @Column()
  district: string;

  @ManyToMany(() => Insurance, (insurance) => insurance.hospitals)
  insurances: Insurance[];

  @ManyToOne(() => User, (user) => user.hospitals)
  user: User;
}
