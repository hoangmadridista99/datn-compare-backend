import { IBaseEntity } from 'src/shared/interfaces/base-entity.interface';

export type ICreateHospital = Omit<IHospital, keyof IBaseEntity>;
export type IUpdateHospital = Partial<ICreateHospital>;

export interface IHospital extends IBaseEntity {
  name: string;
  address: string;
  province: string;
  district: string;
}
export interface IHospitalsPending extends ICreateHospital {
  id: string;
}
export type IHospitalBody = Partial<IBaseEntity> & ICreateHospital;
