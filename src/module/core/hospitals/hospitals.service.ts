import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Hospital } from './entities/hospital.entity';
import type {
  ICreateHospital,
  IHospital,
  IUpdateHospital,
} from './interfaces/hospital.interface';
import { User } from '../users/entities/user.entity';

export class HospitalsService {
  constructor(
    @InjectRepository(Hospital)
    private readonly hospitalRepository: Repository<Hospital>,
  ) {}

  async saveHospitals(data: Hospital[]) {
    try {
      return await this.hospitalRepository.save(data);
    } catch (error) {
      console.log(
        '🚀 ~ file: hospitals.service.ts:25 ~ HospitalsService ~ saveHospitals ~ error:',
        error,
      );
      throw error;
    }
  }

  async isHospitalCreatedByUser(
    userId: string,
    hospitalId: string,
  ): Promise<boolean> {
    try {
      const hospital = await this.hospitalRepository.findOneBy({
        id: hospitalId,
        user: { id: userId },
      });
      return !!hospital;
    } catch (error) {
      console.log(
        '🚀 ~ file: hospitals.service.ts:21 ~ HospitalsService ~ isHospitalCreatedByUser ~ error:',
        error,
      );
      return false;
    }
  }

  async findAll(name?: string): Promise<IHospital[]> {
    try {
      const queryBuilder =
        this.hospitalRepository.createQueryBuilder('hospitals');
      if (name) {
        queryBuilder.where('name ilike :name', { name: `%${name}%` });
      }
      return await queryBuilder.getMany();
    } catch (error) {
      throw error;
    }
  }

  async createHospital(
    user: User,
    body: ICreateHospital,
  ): Promise<IHospital | null> {
    try {
      const hospital = this.hospitalRepository.create({ ...body, user });
      const result = await this.hospitalRepository.save(hospital);
      return {
        id: result.id,
        created_at: result.created_at,
        updated_at: result.updated_at,
        deleted_at: result.deleted_at,
        ...body,
      };
    } catch (error) {
      return null;
    }
  }

  async updateHospital(id: string, body: IUpdateHospital): Promise<boolean> {
    try {
      const result = await this.hospitalRepository.update(id, body);
      return !!result.affected;
    } catch (error) {
      return false;
    }
  }

  async removeHospital(id: string): Promise<boolean> {
    try {
      const result = await this.hospitalRepository.softDelete(id);
      return !!result.affected;
    } catch (error) {
      return false;
    }
  }
}
