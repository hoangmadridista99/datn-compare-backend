import {
  IsEmail,
  IsEnum,
  IsOptional,
  IsPhoneNumber,
  IsString,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { OtpTypes, DestinationTypes } from '../enums/otp.enum';
import type {
  IOtpDestinationType,
  IOtpType,
} from '../interfaces/otp.interface';
import { LIST_ERROR_CODES } from '../../../../shared/constants/error/base.error';

export class SendOtpDto {
  @ApiProperty()
  @IsEnum(DestinationTypes, { message: LIST_ERROR_CODES.SS24503 })
  destination_type: IOtpDestinationType;

  @ApiProperty()
  @IsEnum(OtpTypes, { message: LIST_ERROR_CODES.SS24504 })
  type: IOtpType;

  @ApiProperty({
    description: 'Email',
    example: 'example@just.engineer.com',
  })
  @IsEmail()
  email: string;

  @ApiProperty({
    description: 'Phone number',
    example: '0912123342',
  })
  @IsString({ message: LIST_ERROR_CODES.SS24311 })
  @IsPhoneNumber('VN', { message: LIST_ERROR_CODES.SS24310 })
  phone: string;
}

export class SendOtpLoginForVendorDto {
  @ApiProperty()
  @IsEnum(DestinationTypes, { message: LIST_ERROR_CODES.SS24503 })
  destination_type: IOtpDestinationType;

  @ApiProperty({
    description: 'Email',
    example: 'example@just.engineer.com',
  })
  @IsOptional()
  @IsEmail()
  email: string;

  @ApiProperty({
    description: 'Phone number',
    example: '0912123342',
  })
  @IsOptional()
  @IsString({ message: LIST_ERROR_CODES.SS24311 })
  @IsPhoneNumber('VN', { message: LIST_ERROR_CODES.SS24310 })
  phone: string;
}
