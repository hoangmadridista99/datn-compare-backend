import { MoreThan, Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Otp } from './entities/otp.entity';
import type {
  IOtpType,
  IOtpCreate,
  IOtpDestinationType,
} from './interfaces/otp.interface';
import { RegisterUserDto, RegisterVendorDto } from '../auth/dto/auth.dto';
import { DestinationTypes, OtpStatusTypes, OtpTypes } from './enums/otp.enum';
import {
  getRandomStringNumberByLength,
  setStartAndEndOfDateByTime,
} from '../../../shared/helper/system.helper';
import { EmailService } from '../email/email.service';
import { TwilioService } from '../twilio/twilio.service';

export class OtpsService {
  private THREE_MINUTES_IN_MILLISECONDS = 3 * 60 * 1000;
  private ONE_HOUR_IN_MILLISECONDS = 1000 * 60 * 60;
  private OTP_LENGTH = 6;

  constructor(
    @InjectRepository(Otp) private readonly otpRepository: Repository<Otp>,
    private readonly emailService: EmailService,
    private readonly twilioService: TwilioService,
  ) {}

  private queryDefault(
    destination: string,
    destination_type: IOtpDestinationType,
    otp_type: IOtpType,
  ) {
    try {
      return this.otpRepository
        .createQueryBuilder('otp')
        .where({
          destination,
          destination_type,
          otp_type,
        })
        .orderBy('otp.created_at', 'DESC');
    } catch (error) {
      console.log(
        '🚀 ~ file: otps.service.ts:44 ~ OtpsService ~ error:',
        error,
      );
      throw error;
    }
  }

  private async queryInDayByCurrentTime(
    type: IOtpType,
    destination_type: IOtpDestinationType,
    destination: string,
  ) {
    const now = new Date();
    const { startOfDay, endOfDay } = setStartAndEndOfDateByTime(now);
    return this.queryDefault(destination, destination_type, type).andWhere(
      'otp.created_at BETWEEN :startOfDay AND :endOfDay',
      {
        startOfDay,
        endOfDay,
      },
    );
  }

  async findManyInDayByCurrentTime(
    type: IOtpType,
    destination_type: IOtpDestinationType,
    destination: string,
  ) {
    try {
      const queryBuilder = await this.queryInDayByCurrentTime(
        type,
        destination_type,
        destination,
      );
      return queryBuilder.getMany();
    } catch (error) {
      console.log(
        '🚀 ~ file: otps.service.ts:84 ~ OtpsService ~ findManyInDayByCurrentTime:',
        error,
      );
    }
  }

  async createOtp(
    code: string,
    destination: string,
    type: IOtpType,
    destination_type: IOtpDestinationType,
    numberOfSendingOtp: number,
  ) {
    try {
      const expired_at = new Date(
        Date.now() + this.THREE_MINUTES_IN_MILLISECONDS,
      );
      const dataOtp: IOtpCreate = {
        code,
        destination,
        expired_at,
        otp_type: type,
        destination_type: destination_type,
        block_expired_at: null,
      };

      const isBlockSending = numberOfSendingOtp >= 2;
      if (isBlockSending) {
        const block_expired_at = new Date(
          Date.now() + this.ONE_HOUR_IN_MILLISECONDS,
        );
        dataOtp.block_expired_at = block_expired_at;
      }

      await this.createNewRecord(dataOtp);
    } catch (error) {
      console.log(
        '🚀 ~ file: otps.service.ts:12 ~ OtpsService ~ createOtp:',
        error,
      );
      throw error;
    }
  }

  private async createNewRecord(body: IOtpCreate) {
    try {
      const record = this.otpRepository.create(body);
      await this.otpRepository.save(record);
    } catch (error) {
      console.log(
        '🚀 ~ file: otps.service.ts:119 ~ OtpsService ~ createNewRecord ~ error:',
        error,
      );
      throw error;
    }
  }

  async sendOtp(destination: string, destination_type: string) {
    const code = getRandomStringNumberByLength(this.OTP_LENGTH);

    let isSent = false;
    if (destination_type === DestinationTypes.EMAIL) {
      isSent = await this.emailService.sendEmail(destination, code);
    } else {
      isSent = await this.twilioService.sendSms(destination, code);
    }
    return !isSent ? null : code;
  }

  async updateSuccessOtp(id: string) {
    try {
      await this.otpRepository.update(id, {
        otp_status: OtpStatusTypes.VERIFIED,
      });
    } catch (error) {
      console.log(
        '🚀 ~ file: otps.service.ts:142 ~ OtpsService ~ updateSuccessOtp ~ error:',
        error,
      );
    }
  }

  async checkOtp(
    destination: string,
    type: IOtpType,
    destination_type: IOtpDestinationType,
    code: string,
  ): Promise<boolean> {
    try {
      const otp = await this.queryDefault(destination, destination_type, type)
        .andWhere({
          expired_at: MoreThan(new Date()),
          otp_status: OtpStatusTypes.PENDING,
        })
        .getOne();
      if (!otp || otp.code !== code) return false;
      await this.updateSuccessOtp(otp.id);
      return true;
    } catch (error) {
      console.log(
        '🚀 ~ file: otps.service.ts:65 ~ OtpsService ~ checkOtp:',
        error,
      );
      throw error;
    }
  }

  async checkOtpBeforeLogin(
    otpCode: string,
    destinationType: IOtpDestinationType,
    destination: string,
  ) {
    const otpType = OtpTypes.LOGIN;
    const isValidOtp = await this.checkOtp(
      destination,
      otpType,
      destinationType,
      otpCode,
    );
    return isValidOtp;
  }

  async checkOtpBeforeRegister(body: RegisterUserDto | RegisterVendorDto) {
    const { otp_code, destination_type, email, phone } = body;
    const destination =
      destination_type === DestinationTypes.EMAIL ? email : phone;

    const isOtp = await this.checkOtp(
      destination,
      OtpTypes.REGISTER,
      destination_type,
      otp_code,
    );

    return isOtp;
  }
}
