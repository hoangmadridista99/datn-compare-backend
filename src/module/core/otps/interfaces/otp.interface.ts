import type { IBaseEntity } from '../../../../shared/interfaces/base-entity.interface';

// Export Types
export type IOtpType = 'login' | 'register';
export type IOtpDestinationType = 'phone' | 'email';
export type IOtpCreate = Pick<
  IOtp,
  | 'code'
  | 'otp_type'
  | 'expired_at'
  | 'destination'
  | 'destination_type'
  | 'block_expired_at'
>;

// Export Interfaces
export interface IOtp extends IBaseEntity {
  code: string;
  otp_type: IOtpType;
  destination_type: IOtpDestinationType;
  destination: string;
  expired_at: Date;
  block_expired_at: Date | null;
}

export interface IOtpDTO {
  destination_type: IOtpDestinationType;
  type: IOtpType;
  email: string;
  phone: string;
  numberOfSendingOtp: number;
  destination: string;
}

export type IOtpLoginDto = Omit<IOtpDTO, 'type' | 'phone' | 'email'>;
