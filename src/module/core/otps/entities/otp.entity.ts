import { Entity, Column } from 'typeorm';
import { BaseEntity } from 'src/shared/entities/base.entity';
import { OtpTypes, DestinationTypes, OtpStatusTypes } from '../enums/otp.enum';
import type {
  IOtpType,
  IOtpDestinationType,
} from '../interfaces/otp.interface';

@Entity()
export class Otp extends BaseEntity {
  @Column()
  destination: string;

  @Column({ type: 'enum', enum: OtpTypes })
  otp_type: IOtpType;

  @Column({ type: 'enum', enum: DestinationTypes })
  destination_type: IOtpDestinationType;

  @Column()
  code: string;

  @Column({ type: 'timestamptz' })
  expired_at: Date;

  @Column({ default: OtpStatusTypes.PENDING })
  otp_status: string;

  @Column({ type: 'timestamptz', nullable: true })
  block_expired_at: Date;
}
