export enum OtpTypes {
  LOGIN = 'login',
  REGISTER = 'register',
}
export enum DestinationTypes {
  PHONE = 'phone',
  EMAIL = 'email',
}

export enum OtpStatusTypes {
  VERIFIED = 'verified',
  PENDING = 'pending',
  FAILED = 'failed',
}
