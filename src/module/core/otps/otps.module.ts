import {
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
} from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ClientOtpsService } from '../../client/otps/otps.service';
import { EmailService } from '../email/email.service';
import { User } from '../users/entities/user.entity';
import { UsersService } from '../users/users.service';
import { Otp } from './entities/otp.entity';
import { OtpsService } from './otps.service';
import { VendorOtpsService } from '../../vendor/otps/otps.service';
import { TwilioService } from '../twilio/twilio.service';
import { SendOtpMiddleware } from '../../../shared/middlewares/otps.middleware';
import { getBodyRouteOfMiddleWare } from '../../../shared/helper/system.helper';
import { API_PATH } from '../../../shared/constants/api-path.constant';
import { MinioService } from '../minio/minio.service';
import { HttpModule } from '@nestjs/axios';
const sendOtpClientRoute = getBodyRouteOfMiddleWare(
  API_PATH.SEND_OTP_CLIENT,
  RequestMethod.POST,
);
const sendOtpRegisterVendorRoute = getBodyRouteOfMiddleWare(
  API_PATH.SEND_OTP_REGISTER_VENDOR,
  RequestMethod.POST,
);
const sendOtpLoginVendorRoute = getBodyRouteOfMiddleWare(
  API_PATH.SEND_OTP_LOGIN_VENDOR,
  RequestMethod.POST,
);
@Module({
  imports: [TypeOrmModule.forFeature([Otp, User]), HttpModule],
  providers: [
    OtpsService,
    ClientOtpsService,
    VendorOtpsService,
    UsersService,
    EmailService,
    TwilioService,
    MinioService,
  ],
  exports: [OtpsService, ClientOtpsService, VendorOtpsService],
})
export class OtpsModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(SendOtpMiddleware)
      .forRoutes(
        sendOtpClientRoute,
        sendOtpRegisterVendorRoute,
        sendOtpLoginVendorRoute,
      );
  }
}
