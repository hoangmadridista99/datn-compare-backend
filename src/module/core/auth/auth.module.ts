import { Module } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ClientAuthService } from '../../client/auth/auth.service';
import { OperatorAuthService } from '../../operator/auth/auth.service';
import { VendorAuthService } from '../../vendor/auth/auth.service';
import { Otp } from '../otps/entities/otp.entity';
import { OtpsService } from '../otps/otps.service';
import { User } from '../users/entities/user.entity';
import { UsersService } from '../users/users.service';
import { AuthService } from './auth.service';
import { AdminStrategy } from './strategies/admin/admin.strategy';
import { JwtUserStrategy } from './strategies/jwt.strategy';
import { LocalUserStrategy } from './strategies/local/local.strategy';
import { VendorStrategy } from './strategies/vendor/vendor.strategy';
import { EmailService } from '../email/email.service';
import { TwilioService } from '../twilio/twilio.service';
import { CompaniesService } from '../companies/companies.service';
import { Company } from '../companies/entities/company.entity';
import { MinioService } from '../minio/minio.service';
import { HttpModule } from '@nestjs/axios';

@Module({
  imports: [
    PassportModule,
    HttpModule,
    TypeOrmModule.forFeature([User, Otp, Company]),
  ],
  providers: [
    AuthService,
    ClientAuthService,
    JwtService,
    LocalUserStrategy,
    JwtUserStrategy,
    VendorAuthService,
    VendorStrategy,
    AdminStrategy,
    OperatorAuthService,
    OtpsService,
    UsersService,
    EmailService,
    TwilioService,
    CompaniesService,
    MinioService,
  ],
  exports: [
    AuthService,
    ClientAuthService,
    JwtService,
    LocalUserStrategy,
    JwtUserStrategy,
    VendorAuthService,
    VendorStrategy,
    AdminStrategy,
    OperatorAuthService,
    CompaniesService,
  ],
})
export class AuthModule {}
