import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import { ENV_CONFIG } from 'src/shared/constants/env.constant';
import {
  getError,
  LIST_ERROR_CODES,
} from '../../../shared/constants/error/base.error';
import { User } from '../users/entities/user.entity';
import { UsersService } from '../users/users.service';
import { RegisterUserDto, RegisterVendorDto } from './dto/auth.dto';
import { Payload, ResponseAuthUser } from './models/auth.model';

@Injectable()
export class AuthService {
  constructor(
    private readonly jwt: JwtService,
    private readonly usersService: UsersService,
  ) {}

  async getResponseLogin(user: User) {
    const payload: Payload = {
      id: user.id,
      sub: user.id,
    };
    const accessToken = await this.generateJwtToken(payload);
    return { ...user, accessToken };
  }

  async validateUser(destination: string, password: string) {
    const user = await this.usersService.findOneByUsersQuery([
      { phone: destination },
      { email: destination },
    ]);
    if (!user) return LIST_ERROR_CODES.SS24208;

    const passwordValid = await this.checkPassword(password, user.password);

    if (!passwordValid) return LIST_ERROR_CODES.SS24203;
    return user;
  }

  async validateAdmin(account: string, password: string) {
    const admin = await this.usersService.findOneByUsersQuery({ account });
    if (!admin) return LIST_ERROR_CODES.SS24210;
    const passwordValid = await this.checkPassword(password, admin.password);
    if (!passwordValid) return LIST_ERROR_CODES.SS24203;
    return admin;
  }

  async checkPassword(input: string, password: string) {
    try {
      return await bcrypt.compare(input, password);
    } catch (error) {
      console.log(
        '🚀 ~ file: auth.service.ts:15 ~ AuthService ~ checkPassword ~ error:',
        error,
      );
      throw getError(LIST_ERROR_CODES.SS24001);
    }
  }

  async validateToken(token: string) {
    try {
      return await this.jwt.verify(token, { secret: ENV_CONFIG.jwt.secret });
    } catch (error) {
      console.log(
        '🚀 ~ file: auth.service.ts:67 ~ AuthService ~ validateToken ~ error:',
        error,
      );
    }
  }

  async handleRegister(
    body: RegisterVendorDto | RegisterUserDto,
    file: Express.Multer.File | null,
    isVendor: boolean,
  ) {
    const hashPassword = await this.hashPassword(body.password);
    body.password = hashPassword;
    const avatar_profile_url = await this.usersService.uploadAvatarProfile(
      file,
    );
    const user = isVendor
      ? await this.usersService.createVendor(
          body as RegisterVendorDto,
          avatar_profile_url,
        )
      : await this.usersService.createUser(body, avatar_profile_url);

    const payload: Payload = {
      id: user.id,
      sub: user.id,
    };
    const accessToken = await this.generateJwtToken(payload);
    const result: ResponseAuthUser = { ...user, accessToken };
    return result;
  }

  async hashPassword(password: string) {
    try {
      const salt = await bcrypt.genSalt(10);
      return await bcrypt.hash(password, salt);
    } catch (error) {
      console.log(
        '🚀 ~ file: auth.service.ts:88 ~ AuthService ~ hashPassword ~ error:',
        error,
      );
      throw getError(LIST_ERROR_CODES.SS24001);
    }
  }

  async generateJwtToken(payload: Payload) {
    try {
      return this.jwt.sign(payload, { secret: ENV_CONFIG.jwt.secret });
    } catch (error) {
      console.log(
        '🚀 ~ file: auth.service.ts:100 ~ AuthService ~ generateJwtToken ~ error:',
        error,
      );
    }
  }

  async checkBodyRegister(email: string, phone: string, account?: string) {
    const findConditionDefault = [{ email }, { phone }];
    const findCondition = !account
      ? findConditionDefault
      : [...findConditionDefault, { account }];
    const user = await this.usersService.findOneByUsersQuery(findCondition);

    if (account && user?.account === account) return LIST_ERROR_CODES.SS24206;
    if (user?.email === email) return LIST_ERROR_CODES.SS24204;
    if (user?.phone === phone) return LIST_ERROR_CODES.SS24205;

    return true;
  }
}
