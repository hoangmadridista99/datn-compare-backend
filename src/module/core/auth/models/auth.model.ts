import { ApiProperty } from '@nestjs/swagger';
import { User } from '../../users/entities/user.entity';

export enum AuthUserStrategy {
  JWT = 'jwt',
  LOCAL = 'local',
  VENDOR = 'vendor',
  ADMIN = 'admin',
  SUPER_ADMIN = 'super-admin',
}

export class ResponseAuthUser extends User {
  @ApiProperty({
    example:
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImFmNDc5OGQ4LTI3NDMtNGY1MS04ZTgzLWU3NTc2M2Y2OWJjYyIsInN1YiI6ImFmNDc5OGQ4LTI3NDMtNGY1MS04ZTgzLWU3NTc2M2Y2OWJjYyIsImlhdCI6MTY2MzU1NDUyOH0.OGnKg4mwQgRynPTmZ3hNUOEdObNFWeEqJcqqSAjWvZk',
  })
  accessToken: string;
}

export class Payload {
  id: string;
  sub: string;
}
