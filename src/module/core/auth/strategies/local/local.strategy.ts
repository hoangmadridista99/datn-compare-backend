import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-local';
import { AuthUserStrategy, ResponseAuthUser } from '../../models/auth.model';
import { BasicResponse } from '../../../../../shared/basic.response';
import { ClientAuthService } from '../../../../client/auth/auth.service';

@Injectable()
export class LocalUserStrategy extends PassportStrategy(
  Strategy,
  AuthUserStrategy.LOCAL,
) {
  constructor(private readonly authService: ClientAuthService) {
    super({
      usernameField: 'destination',
    });
  }

  async validate(
    destination: string,
    password: string,
  ): Promise<BasicResponse | ResponseAuthUser> {
    return await this.authService.validateBasic(destination, password);
  }
}
