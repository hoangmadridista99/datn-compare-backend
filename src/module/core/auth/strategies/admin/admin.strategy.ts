import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-local';
import { AuthUserStrategy, ResponseAuthUser } from '../../models/auth.model';
import { BasicResponse } from '../../../../../shared/basic.response';
import { OperatorAuthService } from '../../../../operator/auth/auth.service';

@Injectable()
export class AdminStrategy extends PassportStrategy(
  Strategy,
  AuthUserStrategy.ADMIN,
) {
  constructor(private readonly authService: OperatorAuthService) {
    super({
      usernameField: 'account',
    });
  }

  async validate(
    account: string,
    password: string,
  ): Promise<BasicResponse | ResponseAuthUser> {
    return await this.authService.validateAdmin(account, password);
  }
}
