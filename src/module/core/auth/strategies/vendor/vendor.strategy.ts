import { HttpStatus, Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-local';
import { AuthUserStrategy, ResponseAuthUser } from '../../models/auth.model';
import { BasicResponse } from '../../../../../shared/basic.response';
import { VendorAuthService } from '../../../../vendor/auth/auth.service';
import { Request } from 'express';
import {
  LIST_ERROR_CODES,
  getError,
} from '../../../../../shared/constants/error/base.error';
import { plainToClass } from 'class-transformer';
import { VendorValidationLoginDto } from '../../dto/vendor-login-validate.dto';
import { validate } from 'class-validator';
import { getValidateOption } from '../../../../../shared/helper/system.helper';

@Injectable()
export class VendorStrategy extends PassportStrategy(
  Strategy,
  AuthUserStrategy.VENDOR,
) {
  constructor(private readonly authService: VendorAuthService) {
    super({
      usernameField: 'destination',
      passReqToCallback: true,
    });
  }

  async validate(req: Request): Promise<BasicResponse | ResponseAuthUser> {
    const { body } = req;
    const dto = plainToClass(VendorValidationLoginDto, body);
    const errors = await validate(dto, getValidateOption(true));

    if (!!errors.length)
      return getError(LIST_ERROR_CODES.SS24108, HttpStatus.BAD_REQUEST);
    return await this.authService.validateLoginVendor(
      body.destination,
      body.password,
      body.otp_code,
      body.destination_type,
    );
  }
}
