// import { Injectable } from '@nestjs/common';
// import { PassportStrategy } from '@nestjs/passport';
// import { Strategy } from 'passport-local';
// import { AuthUserStrategy, ResponseAuthUser } from '../../models/auth.model';
// import { BasicResponse } from '../../../../../shared/basic.response';
// import { SuperAdminAuthService } from '../../../../super-admin/auth/auth.service';

// @Injectable()
// export class SuperAdminStrategy extends PassportStrategy(
//   Strategy,
//   AuthUserStrategy.SUPER_ADMIN,
// ) {
//   constructor(private readonly authService: SuperAdminAuthService) {
//     super({
//       usernameField: 'account',
//     });
//   }

//   async validate(
//     account: string,
//     password: string,
//   ): Promise<BasicResponse | ResponseAuthUser> {
//     return await this.authService.validateSuperAdmin(account, password);
//   }
// }
