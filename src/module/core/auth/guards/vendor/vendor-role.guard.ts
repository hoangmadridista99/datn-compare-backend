import {
  Injectable,
  CanActivate,
  ExecutionContext,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { UserRole } from 'src/module/core/users/entities/user.entity';
import { VendorStatusTypes } from 'src/module/core/users/enums/users.enum';
import {
  LIST_ERROR_CODES,
  getError,
} from 'src/shared/constants/error/base.error';
import { RolesKey } from 'src/shared/decorators/role.decorator';

@Injectable()
export class VendorRolesGuard implements CanActivate {
  constructor(private reflector: Reflector) {}

  canActivate(context: ExecutionContext): boolean {
    const requiredRoles = this.reflector.getAllAndOverride<UserRole[]>(
      RolesKey.VENDOR,
      [context.getHandler(), context.getClass()],
    );
    if (!requiredRoles) {
      return true;
    }
    const { user } = context.switchToHttp().getRequest();
    if (user?.vendor_status !== VendorStatusTypes.ACCEPTED)
      throw new HttpException(
        getError(LIST_ERROR_CODES.SS24003),
        HttpStatus.FORBIDDEN,
      );
    return requiredRoles.some((role) => user.role?.includes(role));
  }
}
