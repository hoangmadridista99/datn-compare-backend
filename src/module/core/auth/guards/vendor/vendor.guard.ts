import { Injectable } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AuthUserStrategy } from '../../models/auth.model';

@Injectable()
export class VendorAuthGuard extends AuthGuard(AuthUserStrategy.VENDOR) {}
