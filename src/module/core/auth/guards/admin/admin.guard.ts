import { Injectable } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AuthUserStrategy } from '../../models/auth.model';

@Injectable()
export class AdminAuthGuard extends AuthGuard(AuthUserStrategy.ADMIN) {}
