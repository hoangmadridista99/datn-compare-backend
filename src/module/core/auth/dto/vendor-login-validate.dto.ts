import { IsEnum, IsString } from 'class-validator';
import { DestinationTypes } from '../../otps/enums/otp.enum';
import { IOtpDestinationType } from '../../otps/interfaces/otp.interface';

export class VendorValidationLoginDto {
  @IsString()
  destination: string;

  @IsString()
  password: string;

  @IsString()
  otp_code: string;

  @IsEnum(DestinationTypes)
  destination_type: IOtpDestinationType;
}
