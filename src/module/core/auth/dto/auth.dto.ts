import { ApiProperty } from '@nestjs/swagger';
import {
  IsEmail,
  IsEnum,
  IsISO8601,
  IsNotEmpty,
  IsOptional,
  IsPhoneNumber,
  IsString,
  IsUUID,
} from 'class-validator';
import { LIST_ERROR_CODES } from '../../../../shared/constants/error/base.error';
import { DestinationTypes } from '../../otps/enums/otp.enum';
import type { IOtpDestinationType } from '../../otps/interfaces/otp.interface';
import { GenderTypes } from '../../users/enums/users.enum';
import { IGender } from '../../users/entities/user.entity';
import { IsDestination } from '../../../../shared/decorators/is-destination.decorator';

/** LOGIN DTO */
export class LoginUserDto {
  @ApiProperty({
    description: 'Email or Phone',
    example: 'locboybn26@gmail.com',
  })
  @IsDestination()
  destination: string;

  @ApiProperty({
    description: 'Password',
    example: '12121212',
  })
  @IsNotEmpty({ message: LIST_ERROR_CODES.SS24307 })
  @IsString({ message: LIST_ERROR_CODES.SS24308 })
  password: string;
}

export class LoginVendorDto extends LoginUserDto {
  @ApiProperty({
    description: 'Destination Type',
    example: 'phone',
  })
  @IsEnum(DestinationTypes, { message: LIST_ERROR_CODES.SS24503 })
  destination_type: IOtpDestinationType;

  @ApiProperty({
    description: 'OTP Code',
    example: '123456',
  })
  @IsString({ message: LIST_ERROR_CODES.SS24506 })
  otp_code: string;
}

export class LoginAdminDto {
  @ApiProperty({
    description: 'Account',
    example: 'admin',
  })
  @IsNotEmpty({ message: LIST_ERROR_CODES.SS24314 })
  @IsString({ message: LIST_ERROR_CODES.SS24315 })
  account: string;

  @ApiProperty({
    description: 'Password',
    example: '12121212',
  })
  @IsNotEmpty({ message: LIST_ERROR_CODES.SS24307 })
  @IsString({ message: LIST_ERROR_CODES.SS24308 })
  password: string;
}

/** REGISTER DTO */
export class RegisterUserDto {
  @ApiProperty({
    description: 'Email',
    example: 'example@just.engineer.com',
  })
  @IsOptional()
  @IsEmail()
  email: string;

  @ApiProperty({
    description: 'Phone number',
    example: '0912123342',
  })
  @IsOptional()
  @IsString({ message: LIST_ERROR_CODES.SS24311 })
  @IsPhoneNumber('VN', { message: LIST_ERROR_CODES.SS24310 })
  phone: string;

  @ApiProperty({
    description: 'OTP Code',
    example: '123456',
  })
  @IsString({ message: LIST_ERROR_CODES.SS24506 })
  otp_code: string;

  @ApiProperty({
    description: 'Destination Type',
    example: 'phone',
  })
  @IsEnum(DestinationTypes, { message: LIST_ERROR_CODES.SS24503 })
  destination_type: IOtpDestinationType;

  @ApiProperty({
    description: 'First Name',
    example: 'Loc',
  })
  @IsString({ message: LIST_ERROR_CODES.SS24304 })
  @IsNotEmpty({ message: LIST_ERROR_CODES.SS24303 })
  first_name: string;

  @ApiProperty({
    description: 'Last Name',
    example: 'Nguyen',
  })
  @IsString({ message: LIST_ERROR_CODES.SS24306 })
  @IsNotEmpty({ message: LIST_ERROR_CODES.SS24305 })
  last_name: string;

  @ApiProperty({
    description: 'Password',
    example: '12121212',
  })
  @IsNotEmpty({ message: LIST_ERROR_CODES.SS24307 })
  @IsString({ message: LIST_ERROR_CODES.SS24308 })
  password: string;

  @ApiProperty({
    description: 'Date Of Birth',
    example: '1996-02-26',
  })
  @IsISO8601()
  date_of_birth: string;

  @ApiProperty({ description: 'Gender', example: GenderTypes.male })
  @IsEnum(GenderTypes)
  @IsNotEmpty()
  gender: IGender;

  @ApiProperty({
    description: 'Address Of User',
    example: 'Thanh pho Bac Ninh',
  })
  @IsString({ message: LIST_ERROR_CODES.SS24313 })
  @IsOptional()
  address: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  confirmation_password: string;
}

export class CreateRegisterUserDto extends RegisterUserDto {
  @ApiProperty({
    type: 'string',
    format: 'binary',
    required: true,
  })
  @IsOptional()
  file: any;
}

export class RegisterManagerDto extends RegisterUserDto {
  @IsNotEmpty({ message: LIST_ERROR_CODES.SS24314 })
  @IsString({ message: LIST_ERROR_CODES.SS24315 })
  account: string;
}

export class RegisterVendorDto extends RegisterUserDto {
  @ApiProperty({
    description: 'Company ID',
    example: 'MANULIFE',
  })
  @IsOptional()
  @IsUUID(undefined, { message: LIST_ERROR_CODES.SS24317 })
  @IsNotEmpty({ message: LIST_ERROR_CODES.SS24318 })
  company_id: string;

  @ApiProperty({
    description: 'Company other',
  })
  @IsOptional()
  @IsString({ message: LIST_ERROR_CODES.SS24317 })
  @IsNotEmpty({ message: LIST_ERROR_CODES.SS24318 })
  company_other: string;
}

export class CreateRegisterVendorDto extends RegisterVendorDto {
  @ApiProperty({
    type: 'string',
    format: 'binary',
    required: true,
  })
  @IsOptional()
  file: any;
}
