export enum InsuranceCategoryNames {
  Health = 'health',
  Life = 'life',
  Car = 'car',
}
