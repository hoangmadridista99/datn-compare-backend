import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { InsuranceCategory } from './entities/insurance-category.entity';
import { InsuranceCategoriesService } from './insurance-categories.service';
import { ClientInsuranceCategoriesService } from '../../client/insurance-categories/insurance-categories.service';
import { OperatorInsuranceCategoriesService } from '../../operator/insurance-categories/insurance-categories.service';

@Module({
  imports: [TypeOrmModule.forFeature([InsuranceCategory])],
  providers: [
    InsuranceCategoriesService,
    ClientInsuranceCategoriesService,
    OperatorInsuranceCategoriesService,
  ],
  exports: [
    InsuranceCategoriesService,
    ClientInsuranceCategoriesService,
    OperatorInsuranceCategoriesService,
  ],
})
export class InsuranceCategoriesModule {}
