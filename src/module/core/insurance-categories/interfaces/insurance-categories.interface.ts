import { IBaseEntity } from '../../../../shared/interfaces/base-entity.interface';
import { IInsurance } from '../../insurances/interfaces/insurance.interface';
import { InsuranceCategoryNames } from '../enums/insurance-category.enum';

export type IInsuranceCategoriesTypes = `${InsuranceCategoryNames}`;

export interface IInsuranceCategory extends IBaseEntity {
  label: IInsuranceCategoriesTypes;
  is_hide: boolean;
  quantity_of_insurances: number;
  insurances: IInsurance[] | [];
}

export type ICreateInsuranceCategory = Omit<
  IInsuranceCategory,
  keyof IBaseEntity | 'quantity_of_insurances' | 'insurances' | 'is_hide'
>;

export type IUpdateInsuranceCategory = Partial<
  Omit<IInsuranceCategory, keyof IBaseEntity | 'insurances'>
>;
export type ICategoryOfInsurance = Omit<
  IInsuranceCategory,
  | 'updated_at'
  | 'deleted_at'
  | 'is_hide'
  | 'quantity_of_insurances'
  | 'insurances'
>;
