import { ApiProperty, PartialType } from '@nestjs/swagger';
import {
  IsBoolean,
  IsDefined,
  IsEnum,
  IsNotEmpty,
  IsOptional,
} from 'class-validator';
import { InsuranceCategoryNames } from '../enums/insurance-category.enum';
import { IInsuranceCategoriesTypes } from '../interfaces/insurance-categories.interface';

export class CreateInsuranceCategoryDto {
  @ApiProperty({
    example: InsuranceCategoryNames.Life,
  })
  @IsEnum(InsuranceCategoryNames)
  @IsNotEmpty()
  @IsDefined()
  label: IInsuranceCategoriesTypes;
}

export class UpdateInsuranceCategoryDto extends PartialType(
  CreateInsuranceCategoryDto,
) {
  @ApiProperty({
    example: true,
  })
  @IsOptional()
  @IsBoolean()
  is_hide: boolean;
}
