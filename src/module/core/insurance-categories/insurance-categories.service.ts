import { InjectRepository } from '@nestjs/typeorm';
import { InsuranceCategory } from './entities/insurance-category.entity';
import { Repository } from 'typeorm';
import {
  ICreateInsuranceCategory,
  IUpdateInsuranceCategory,
} from './interfaces/insurance-categories.interface';
import { NO_CONTENT_SUCCESS_RESPONSE } from '../../../shared/lib/responses/response';
import { OperationSign } from '../../../shared/enums/operation.enum';

export class InsuranceCategoriesService {
  constructor(
    @InjectRepository(InsuranceCategory)
    private readonly insuranceCategoriesRepository: Repository<InsuranceCategory>,
  ) {}

  async createInsuranceCategory(body: ICreateInsuranceCategory) {
    try {
      const result = this.insuranceCategoriesRepository.create(body);
      await this.insuranceCategoriesRepository.save(result);
      return NO_CONTENT_SUCCESS_RESPONSE;
    } catch (error) {
      throw error;
    }
  }

  async updateQuantityOfCategoryByInsurance(
    id: string,
    operationTypes: OperationSign,
  ) {
    try {
      return await this.insuranceCategoriesRepository
        .createQueryBuilder('insurance-categories')
        .update(InsuranceCategory)
        .where({ id })
        .set({
          quantity_of_insurances: () =>
            `quantity_of_insurances ${operationTypes} 1`,
        })
        .execute();
    } catch (error) {
      console.log(
        '🚀 ~ file: blog-categories.service.ts:33 ~ BlogCategoriesService ~ updateQuantityOfBlogsAfterCreate ~ error:',
        error,
      );
      throw error;
    }
  }

  async getListCategories(isAdmin: boolean) {
    try {
      const findManyOptions = !isAdmin
        ? {
            where: { is_hide: false },
            select: {
              id: true,
              label: true,
              quantity_of_insurances: true,
            },
          }
        : undefined;
      return this.insuranceCategoriesRepository.find(findManyOptions);
    } catch (error) {
      throw error;
    }
  }

  async updateInsuranceCategory(
    id: string,
    body: IUpdateInsuranceCategory,
  ): Promise<boolean> {
    try {
      const result = await this.insuranceCategoriesRepository.update(id, body);
      return !!result.affected;
    } catch (error) {
      throw error;
    }
  }

  async deleteInsuranceCategory(id: string) {
    try {
      const result = await this.insuranceCategoriesRepository.delete(id);
      return !!result.affected;
    } catch (error) {
      throw error;
    }
  }

  async getOneCategoryById(id: string) {
    try {
      return await this.insuranceCategoriesRepository.findOne({
        where: { id },
      });
    } catch (error) {
      throw error;
    }
  }
}
