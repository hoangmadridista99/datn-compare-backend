import { Column, Entity, OneToMany } from 'typeorm';
import { BaseEntity } from '../../../../shared/entities/base.entity';
import { Insurance } from '../../insurances/entities/insurance.entity';
import { IInsuranceCategoriesTypes } from '../interfaces/insurance-categories.interface';

@Entity('insurance-categories')
export class InsuranceCategory extends BaseEntity {
  @Column({ default: false })
  is_hide: boolean;

  @Column()
  label: IInsuranceCategoriesTypes;

  @Column({ default: 0 })
  quantity_of_insurances: number;

  @OneToMany(() => Insurance, (insurance) => insurance.insurance_category, {
    cascade: true,
  })
  insurances: Insurance[];
}
