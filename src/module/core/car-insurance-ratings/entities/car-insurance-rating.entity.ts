import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';
import { BaseEntity } from '../../../../shared/entities/base.entity';
import { CarInsurance } from '../../car-insurances/entities/car-insurance.entity';
import { User } from '../../users/entities/user.entity';

@Entity('car-insurance-ratings')
export class CarInsuranceRating extends BaseEntity {
  @Column()
  score: number;

  @Column()
  comment: string;

  @ManyToOne(() => CarInsurance, (carInsurance) => carInsurance.id, {
    onDelete: 'CASCADE',
    eager: true,
  })
  @JoinColumn({ name: 'car_insurance_id', referencedColumnName: 'id' })
  car_insurance: CarInsurance;

  @Column()
  car_insurance_id: string;

  @ManyToOne(() => User, (user) => user.ratings)
  user: User;

  @Column({ default: false })
  is_verified_by_admin: boolean;

  @Column({ default: false })
  is_hide: boolean;
}
