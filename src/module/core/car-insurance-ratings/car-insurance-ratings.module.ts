import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CarInsuranceRating } from './entities/car-insurance-rating.entity';
import { CarInsuranceRatingsService } from './car-insurance-ratings.service';
import { ClientCarInsuranceRatingsService } from '../../client/car-insurance-ratings/car-insurance-ratings.service';
import { OperatorCarInsuranceRatingsService } from '../../operator/car-insurance-ratings/car-insurance-ratings.service';
import { CarInsurancesService } from '../car-insurances/car-insurances.service';
import { CarInsurance } from '../car-insurances/entities/car-insurance.entity';
import { OperatorCompaniesService } from '../../operator/companies/companies.service';
import { CompaniesService } from '../companies/companies.service';
import { MinioService } from '../minio/minio.service';
import { HttpModule } from '@nestjs/axios';
import { Company } from '../companies/entities/company.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([CarInsuranceRating, CarInsurance, Company]),
    HttpModule,
  ],
  providers: [
    CarInsuranceRatingsService,
    ClientCarInsuranceRatingsService,
    OperatorCarInsuranceRatingsService,
    CarInsurancesService,
    OperatorCompaniesService,
    CompaniesService,
    MinioService,
  ],
  exports: [
    CarInsuranceRatingsService,
    ClientCarInsuranceRatingsService,
    OperatorCarInsuranceRatingsService,
  ],
})
export class CarInsuranceRatingsModule {}
