import { InjectRepository } from '@nestjs/typeorm';
import { CarInsuranceRating } from './entities/car-insurance-rating.entity';
import { Repository } from 'typeorm';
import { User } from '../users/entities/user.entity';
import {
  ICreateCarInsuranceRatingDto,
  ISortByType,
} from '../ratings/interfaces/ratings.interface';
import { PaginateQuery } from 'nestjs-paginate';
import { getPaginate } from '../../../shared/lib/paginate/paginate.lib';
import {
  IFilterCarInsuranceRating,
  IVerifyCarInsuranceRatingDto,
} from './interfaces/car-insurance-rating.interface';
import { CarInsurancesService } from '../car-insurances/car-insurances.service';

export class CarInsuranceRatingsService {
  constructor(
    @InjectRepository(CarInsuranceRating)
    private readonly carInsuranceRatingsRepository: Repository<CarInsuranceRating>,
    private readonly carInsurancesService: CarInsurancesService,
  ) {}
  private getQueryBuilder() {
    try {
      return this.carInsuranceRatingsRepository
        .createQueryBuilder('car-insurance-ratings')
        .leftJoin('car-insurance-ratings.user', 'user')
        .leftJoin('car-insurance-ratings.car_insurance', 'car_insurance');
    } catch (error) {
      throw error;
    }
  }
  private async calculateAvgRatings(carInsuranceId: string): Promise<number> {
    try {
      const result = await this.carInsuranceRatingsRepository.average('score', {
        car_insurance: { id: carInsuranceId },
        is_verified_by_admin: true,
        is_hide: false,
      });
      return Math.round(result * 10) / 10;
    } catch (error) {
      throw error;
    }
  }

  private async calculateTotalRatings(carInsuranceId: string): Promise<number> {
    try {
      return await this.carInsuranceRatingsRepository.count({
        where: {
          car_insurance: { id: carInsuranceId },
          is_verified_by_admin: true,
          is_hide: false,
        },
      });
    } catch (error) {
      throw error;
    }
  }

  async createNewRating(user: User, body: ICreateCarInsuranceRatingDto) {
    try {
      const result = this.carInsuranceRatingsRepository.create({
        ...body,
        user,
      });
      return await this.carInsuranceRatingsRepository.save(result);
    } catch (error) {
      throw error;
    }
  }

  async findAllByCarInsuranceForClient(
    query: PaginateQuery,
    carInsuranceId: string,
    sortBy?: ISortByType,
  ) {
    try {
      const queryBuilder = this.getQueryBuilder()
        .select([
          'car-insurance-ratings',
          'user.first_name',
          'user.last_name',
          'user.avatar_profile_url',
        ])
        .where({
          car_insurance: { id: carInsuranceId },
          is_verified_by_admin: true,
          is_hide: false,
        });
      if (!!sortBy) {
        queryBuilder.orderBy('car-insurance-ratings.score', sortBy);
      }
      return getPaginate(query, queryBuilder);
    } catch (error) {
      throw error;
    }
  }

  async verifyRatingForAdmin(id: string, body: IVerifyCarInsuranceRatingDto) {
    try {
      const result = await this.carInsuranceRatingsRepository.update(id, body);
      return !!result.affected;
    } catch (error) {
      throw error;
    }
  }

  async findRatingById(id: string): Promise<CarInsuranceRating | null> {
    try {
      return await this.carInsuranceRatingsRepository.findOneBy({ id });
    } catch (error) {
      console.log(
        '🚀 ~ file: car-insurance-ratings.service.ts:85 ~ CarInsuranceRatingsService ~ findRatingById ~ error:',
        error,
      );
      return null;
    }
  }

  async findAllByOperator(
    query: PaginateQuery,
    filter: IFilterCarInsuranceRating,
  ) {
    try {
      const queryBuilder = this.getQueryBuilder()
        .select(['car-insurance-ratings', 'user', 'car_insurance'])
        .where({ is_verified_by_admin: false });

      if (filter.insurance_name) {
        queryBuilder.andWhere('car_insurance.insurance_name ilike :name', {
          name: `%${filter.insurance_name}%`,
        });
      }
      if (filter.rating_score) {
        queryBuilder.andWhere({
          score: filter.rating_score,
        });
      }
      return getPaginate(query, queryBuilder);
    } catch (error) {
      console.log(
        '🚀 ~ file: ratings.service.ts:86 ~ RatingsService ~ findAllByOperator:',
        error,
      );
      return null;
    }
  }

  async findAllRatingsByCarInsurance(
    query: PaginateQuery,
    carInsuranceId: string,
    sortBy?: ISortByType,
  ) {
    try {
      const queryBuilder = this.getQueryBuilder()
        .select(['car-insurance-ratings', 'user'])
        .where({
          car_insurance: { id: carInsuranceId },
          is_verified_by_admin: true,
          is_hide: false,
        });
      if (!!sortBy) {
        queryBuilder.orderBy('car-insurance-ratings.score', sortBy);
      }
      return getPaginate(query, queryBuilder);
    } catch (error) {
      throw error;
    }
  }

  async handleCalculateRatingAfterVerify(insuranceId: string) {
    const [rating_scores, total_rating] = await Promise.all([
      this.calculateAvgRatings(insuranceId),
      this.calculateTotalRatings(insuranceId),
    ]);
    return await this.carInsurancesService.updateRatingsOfCarInsuranceById(
      insuranceId,
      {
        rating_scores,
        total_rating,
      },
    );
  }

  async deleteRatingById(id: string) {
    try {
      const result = await this.carInsuranceRatingsRepository.delete(id);
      return !!result.affected;
    } catch (error) {
      throw error;
    }
  }
}
