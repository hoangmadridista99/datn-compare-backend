import { ApiProperty } from '@nestjs/swagger';
import {
  IsNumber,
  Min,
  Max,
  IsNotEmpty,
  IsString,
  IsUUID,
  IsBoolean,
  IsOptional,
  IsEnum,
  ValidateIf,
} from 'class-validator';
import { LIST_ERROR_CODES } from '../../../../shared/constants/error/base.error';
import { CarInsuranceRatingTypes } from '../interfaces/car-insurance-rating.interface';

export class CreateCarInsuranceRatingDto {
  @ApiProperty({ example: 5 })
  @IsNumber()
  @Min(1)
  @Max(5)
  @IsNotEmpty()
  score: number;

  @ApiProperty({ example: 'comment' })
  @IsString()
  @IsNotEmpty()
  @ValidateIf((_, value) => !!value)
  comment: string;

  @ApiProperty({})
  @IsUUID(undefined, { message: LIST_ERROR_CODES.SS24317 })
  @IsNotEmpty()
  car_insurance_id: string;
}

export class UpdateCarInsuranceRatingDto {
  @ApiProperty({ example: true })
  @IsBoolean()
  @IsNotEmpty()
  @IsOptional()
  is_verified_by_admin: boolean;

  @ApiProperty({ example: true })
  @IsBoolean()
  @IsNotEmpty()
  @IsOptional()
  is_hide: boolean;
}

export class FilterCarInsuranceRatingDto {
  @IsString()
  @IsOptional()
  insurance_name: string;

  @IsOptional()
  @IsEnum(CarInsuranceRatingTypes)
  rating_score: `${CarInsuranceRatingTypes}`;
}
