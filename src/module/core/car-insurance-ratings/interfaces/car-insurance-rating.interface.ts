import { IBaseEntity } from '../../../../shared/interfaces/base-entity.interface';
import { ICarInsurance } from '../../car-insurances/interfaces/car-insurance.interface';
import { IUser } from '../../users/interfaces/users.interface';

export interface ICarInsuranceRating extends IBaseEntity {
  score: number;
  comment: string;
  car_insurance: ICarInsurance;
  user: IUser;
  is_verified_by_admin: boolean;
  car_insurance_id: string;
  is_hide: boolean;
}

export type ICreateCarInsuranceRatingDto = Omit<
  ICarInsuranceRating,
  | keyof IBaseEntity
  | 'user'
  | 'car_insurance'
  | 'is_verified_by_admin'
  | 'is_hide'
>;
export type IVerifyCarInsuranceRatingDto = Partial<
  Pick<ICarInsuranceRating, 'is_verified_by_admin' | 'is_hide'>
>;

export enum CarInsuranceRatingTypes {
  One = '1',
  Two = '2',
  Three = '3',
  For = '4',
  Five = '5',
}

export interface IFilterCarInsuranceRating {
  insurance_name?: string;
  rating_score?: `${CarInsuranceRatingTypes}`;
}
