import { ApiProperty, PartialType } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber } from 'class-validator';

export class CreateMandatorySettingDto {
  @ApiProperty({ example: 100000 })
  @IsNumber()
  @IsNotEmpty()
  non_business_less_than: number;

  @ApiProperty({ example: 100000 })
  @IsNumber()
  @IsNotEmpty()
  non_business_more_than_or_equal: number;

  @ApiProperty({ example: 100000 })
  @IsNumber()
  @IsNotEmpty()
  non_business_passenger_cargo_car: number;

  @ApiProperty({ example: 100000 })
  @IsNumber()
  @IsNotEmpty()
  business_less_than: number;

  @ApiProperty({ example: 100000 })
  @IsNumber()
  @IsNotEmpty()
  business_six_seats: number;

  @ApiProperty({ example: 100000 })
  @IsNumber()
  @IsNotEmpty()
  business_seven_seats: number;

  @ApiProperty({ example: 100000 })
  @IsNumber()
  @IsNotEmpty()
  business_eight_seats: number;

  @ApiProperty({ example: 100000 })
  @IsNumber()
  @IsNotEmpty()
  business_nine_seats: number;

  @ApiProperty({ example: 100000 })
  @IsNumber()
  @IsNotEmpty()
  business_ten_seats: number;

  @ApiProperty({ example: 100000 })
  @IsNumber()
  @IsNotEmpty()
  business_eleven_seats: number;

  @ApiProperty({ example: 100000 })
  @IsNumber()
  @IsNotEmpty()
  business_passenger_cargo_car: number;
}

export class UpdateMandatorySettingDto extends PartialType(
  CreateMandatorySettingDto,
) {}
