import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CarInsuranceSettingsService } from './car-insurance-settings.service';
import { OperatorCarInsuranceSettingsService } from '../../operator/car-insurance-settings/car-insurance-settings.service';
import { CarInsurancesService } from '../car-insurances/car-insurances.service';
import { CarInsurance } from '../car-insurances/entities/car-insurance.entity';
import { OperatorCompaniesService } from '../../operator/companies/companies.service';
import { CompaniesService } from '../companies/companies.service';
import { MinioService } from '../minio/minio.service';
import { HttpModule } from '@nestjs/axios';
import { Company } from '../companies/entities/company.entity';
import { MandatorySetting } from './entities/mandatory-setting.entity';
import { ClientCarInsuranceSettingsService } from '../../client/car-insurance-settings/car-insurance-settings.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([MandatorySetting, CarInsurance, Company]),
    HttpModule,
  ],
  providers: [
    CarInsuranceSettingsService,
    OperatorCarInsuranceSettingsService,
    CarInsurancesService,
    OperatorCompaniesService,
    CompaniesService,
    MinioService,
    ClientCarInsuranceSettingsService,
  ],
  exports: [
    CarInsuranceSettingsService,
    OperatorCarInsuranceSettingsService,
    ClientCarInsuranceSettingsService,
  ],
})
export class CarInsuranceSettingsModule {}
