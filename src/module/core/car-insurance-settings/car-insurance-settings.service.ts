import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { MandatorySetting } from './entities/mandatory-setting.entity';
import { ICreateMandatorySettingDto } from './interfaces/mandatory-setting.interface';
import { User } from '../users/entities/user.entity';

export class CarInsuranceSettingsService {
  constructor(
    @InjectRepository(MandatorySetting)
    private readonly mandatorySettingsRepository: Repository<MandatorySetting>,
  ) {}

  /** Mandatory */
  async createNewMandatorySetting(
    body: ICreateMandatorySettingDto,
    user: User,
  ) {
    try {
      const currentMandatorySetting = await this.getMandatorySettingDetails();
      let result = body;
      if (!currentMandatorySetting[0]) {
        result = this.mandatorySettingsRepository.create({
          ...body,
          user,
        });
      } else {
        result = {
          ...currentMandatorySetting[0],
          ...body,
        };
      }
      return await this.mandatorySettingsRepository.save(result);
    } catch (error) {
      throw error;
    }
  }

  async getMandatorySettingDetails() {
    try {
      return await this.mandatorySettingsRepository.find();
    } catch (error) {
      throw error;
    }
  }
}
