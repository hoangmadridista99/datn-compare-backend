import { Column, Entity, ManyToOne } from 'typeorm';
import { BaseEntity } from '../../../../shared/entities/base.entity';
import { User } from '../../users/entities/user.entity';

@Entity('mandatory-settings')
export class MandatorySetting extends BaseEntity {
  @Column()
  non_business_less_than: number;

  @Column()
  non_business_more_than_or_equal: number;

  @Column()
  non_business_passenger_cargo_car: number;

  @Column()
  business_less_than: number;

  @Column()
  business_six_seats: number;

  @Column()
  business_seven_seats: number;

  @Column()
  business_eight_seats: number;

  @Column()
  business_nine_seats: number;

  @Column()
  business_ten_seats: number;

  @Column()
  business_eleven_seats: number;

  @Column()
  business_passenger_cargo_car: number;

  @ManyToOne(() => User, (user) => user.id)
  user: User;
}
