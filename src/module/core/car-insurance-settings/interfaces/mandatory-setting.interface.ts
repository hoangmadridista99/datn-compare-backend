import { IBaseEntity } from '../../../../shared/interfaces/base-entity.interface';

export interface IMandatorySetting extends IBaseEntity {
  non_business_less_than: number;
  non_business_more_than_or_equal: number;
  non_business_passenger_cargo_car: number;
  business_less_than: number;
  business_six_seats: number;
  business_seven_seats: number;
  business_eight_seats: number;
  business_nine_seats: number;
  business_ten_seats: number;
  business_eleven_seats: number;
  business_passenger_cargo_car: number;
}

export type ICreateMandatorySettingDto = Omit<
  IMandatorySetting,
  keyof IBaseEntity
>;
export type IUpdateMandatorySettingDto = Partial<ICreateMandatorySettingDto>;
