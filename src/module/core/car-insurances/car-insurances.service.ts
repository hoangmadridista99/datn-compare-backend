import { InjectRepository } from '@nestjs/typeorm';
import { CarInsurance } from './entities/car-insurance.entity';
import { FindOptionsWhere, Not, Repository } from 'typeorm';
import { User } from '../users/entities/user.entity';
import {
  ICarInsurancesFilter,
  ICreateCarInsuranceDto,
  IUpdateCarInsuranceDto,
  IUpdateRatingForCarInsuranceDto,
} from './interfaces/car-insurance.interface';
import { NO_CONTENT_SUCCESS_RESPONSE } from '../../../shared/lib/responses/response';
import { OperatorCompaniesService } from '../../operator/companies/companies.service';
import { PaginateQuery } from 'nestjs-paginate';
import { getPaginate } from '../../../shared/lib/paginate/paginate.lib';
import { CarInsuranceTypes } from './enums/car-insurance.enum';
type ICarInsuranceQuery =
  | FindOptionsWhere<CarInsurance>
  | FindOptionsWhere<CarInsurance>[];

export class CarInsurancesService {
  constructor(
    @InjectRepository(CarInsurance)
    private readonly carInsurancesRepository: Repository<CarInsurance>,
    private readonly operatorCompaniesService: OperatorCompaniesService,
  ) {}

  async createCarInsurance(
    user: User,
    body: ICreateCarInsuranceDto,
    file: Express.Multer.File,
  ) {
    try {
      const logo = await this.operatorCompaniesService.uploadLogo(file);
      if (typeof logo !== 'string') return logo;

      const result = this.carInsurancesRepository.create({
        ...body,
        user,
        company_logo_url: logo,
      });
      await this.carInsurancesRepository.save(result);

      return NO_CONTENT_SUCCESS_RESPONSE;
    } catch (error) {
      throw error;
    }
  }

  async getCarInsurancesList(
    query: PaginateQuery,
    filter: ICarInsurancesFilter,
  ) {
    try {
      const queryBuilder = this.carInsurancesRepository
        .createQueryBuilder('car-insurances')
        .leftJoin('car-insurances.user', 'user')
        .select(['car-insurances', 'user.first_name', 'user.last_name']);
      if (filter.insurance_type) {
        queryBuilder
          .where({
            insurance_type: filter.insurance_type,
          })
          .orWhere({
            insurance_type: CarInsuranceTypes.Both,
          });
      }
      return await getPaginate(query, queryBuilder);
    } catch (error) {
      throw error;
    }
  }

  async getCarInsuranceSettings() {
    try {
      return await this.carInsurancesRepository
        .createQueryBuilder('car-insurances')
        .select([
          'car-insurances.id',
          'car-insurances.company_logo_url',
          'car-insurances.company_name',
          'car-insurances.cost_of_purchasing',
          'car-insurances.choosing_service_center',
          'car-insurances.component_vehicle_theft',
          'car-insurances.no_depreciation_cost',
          'car-insurances.water_damage',
          'car-insurances.insured_for_each_person',
        ])
        .where({ insurance_type: Not(CarInsuranceTypes.Mandatory) })
        .getMany();
    } catch (error) {
      throw error;
    }
  }

  async findOneByQuery(query: ICarInsuranceQuery) {
    try {
      return await this.carInsurancesRepository.findOne({
        where: query,
      });
    } catch (error) {
      throw error;
    }
  }

  async saveCarInsurance(body: CarInsurance) {
    try {
      return await this.carInsurancesRepository.save(body);
    } catch (error) {
      throw error;
    }
  }

  async deleteCarInsuranceById(id: string) {
    try {
      const result = await this.carInsurancesRepository.delete(id);
      return !!result.affected;
    } catch (error) {
      throw error;
    }
  }

  async updateCarInsuranceById(
    id: string,
    body: IUpdateCarInsuranceDto,
    file: Express.Multer.File,
  ) {
    try {
      if (!!file) {
        const logo = await this.operatorCompaniesService.uploadLogo(file);
        if (typeof logo !== 'string') return logo;
        body.company_logo_url = logo;
      }
      await this.carInsurancesRepository.update(id, body);
    } catch (error) {
      throw error;
    }
  }

  async updateRatingsOfCarInsuranceById(
    id: string,
    body: IUpdateRatingForCarInsuranceDto,
  ) {
    try {
      const result = await this.carInsurancesRepository.update(id, body);
      return !!result.affected;
    } catch (error) {
      console.log(
        '🚀 ~ file: car-insurances.service.ts:143 ~ CarInsurancesService ~ error:',
        error,
      );
      return false;
    }
  }
}
