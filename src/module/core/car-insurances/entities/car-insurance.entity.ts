import { Column, Entity, ManyToOne } from 'typeorm';
import { BaseEntity } from '../../../../shared/entities/base.entity';
import {
  IAppraisalSetting,
  ICarInsuranceType,
} from '../interfaces/car-insurance.interface';
import { User } from '../../users/entities/user.entity';

@Entity('car-insurances')
export class CarInsurance extends BaseEntity {
  @Column()
  company_logo_url: string;

  @Column()
  company_name: string;

  @Column()
  insurance_name: string;

  @Column()
  insurance_description: string;

  @Column()
  insurance_type: ICarInsuranceType;

  @Column({ type: 'float', default: 0 })
  rating_scores: number;

  @Column({ type: 'int', default: 0 })
  total_rating: number;

  /** Chi phí mua bảo hiểm */
  @Column({ type: 'jsonb', nullable: true })
  cost_of_purchasing: IAppraisalSetting;

  /** Lựa chọn cơ sở chính hãng */
  @Column({ type: 'jsonb', nullable: true })
  choosing_service_center: IAppraisalSetting;

  /** Mất cắp bộ phận */
  @Column({ type: 'jsonb', nullable: true })
  component_vehicle_theft: IAppraisalSetting;

  /** Không tính khấu hao phụ tùng, vật tư thay mới */
  @Column({ type: 'jsonb', nullable: true })
  no_depreciation_cost: IAppraisalSetting;

  /** Thiệt hại động cơ do ảnh hưởng của nước */
  @Column({ type: 'jsonb', nullable: true })
  water_damage: IAppraisalSetting;

  /** Bảo hiểm tai nạn người ngồi trên xe */
  @Column({ type: 'jsonb', nullable: true })
  insured_for_each_person: IAppraisalSetting;

  @ManyToOne(() => User, (user) => user.id)
  user: User;
}
