import { ApiProperty, PartialType } from '@nestjs/swagger';
import {
  IsEnum,
  IsNotEmpty,
  IsNumber,
  IsNumberString,
  IsOptional,
  IsString,
  ValidateNested,
} from 'class-validator';
import {
  API_PROPERTY_EXAMPLE,
  IAppraisalSetting,
  ICarInsuranceType,
  SettingTypes,
} from '../interfaces/car-insurance.interface';
import { CarInsuranceTypes } from '../enums/car-insurance.enum';
import { Type } from 'class-transformer';

class AppraisalSetting {
  @IsEnum(SettingTypes)
  @IsNotEmpty()
  @IsString()
  type: `${SettingTypes}`;

  @IsNumber()
  @IsNotEmpty()
  value: number;
}

export class CreateCarInsuranceDto {
  @ApiProperty({ example: 'string' })
  @IsString()
  @IsNotEmpty()
  company_name: string;

  @ApiProperty({ example: 'string' })
  @IsString()
  @IsNotEmpty()
  insurance_name: string;

  @ApiProperty({ example: 'string' })
  @IsString()
  @IsNotEmpty()
  insurance_description: string;

  @ApiProperty({ example: CarInsuranceTypes.Both })
  @IsEnum(CarInsuranceTypes)
  @IsNotEmpty()
  insurance_type: ICarInsuranceType;

  @ApiProperty({ example: API_PROPERTY_EXAMPLE })
  @IsOptional()
  @ValidateNested()
  @Type(() => AppraisalSetting)
  cost_of_purchasing: IAppraisalSetting;

  @ApiProperty({ example: API_PROPERTY_EXAMPLE })
  @IsOptional()
  @ValidateNested()
  @Type(() => AppraisalSetting)
  choosing_service_center: IAppraisalSetting;

  @ApiProperty({ example: API_PROPERTY_EXAMPLE })
  @IsOptional()
  @ValidateNested()
  @Type(() => AppraisalSetting)
  component_vehicle_theft: IAppraisalSetting;

  @ApiProperty({ example: API_PROPERTY_EXAMPLE })
  @IsOptional()
  @ValidateNested()
  @Type(() => AppraisalSetting)
  no_depreciation_cost: IAppraisalSetting;

  @ApiProperty({ example: API_PROPERTY_EXAMPLE })
  @IsOptional()
  @ValidateNested()
  @Type(() => AppraisalSetting)
  water_damage: IAppraisalSetting;

  @ApiProperty({ example: API_PROPERTY_EXAMPLE })
  @IsOptional()
  @ValidateNested()
  @Type(() => AppraisalSetting)
  insured_for_each_person: IAppraisalSetting;
}

export class UpdateCarInsuranceDto extends PartialType(CreateCarInsuranceDto) {}

export class ApiBodyCreateCarInsurance extends CreateCarInsuranceDto {
  @ApiProperty({
    type: 'string',
    format: 'binary',
    required: false,
  })
  @IsOptional()
  file: any;
}

export class ApiBodyUpdateCarInsurance extends UpdateCarInsuranceDto {
  @ApiProperty({
    type: 'string',
    format: 'binary',
    required: false,
  })
  @IsOptional()
  file: any;
}

export class CarInsuranceFilterDto {
  @IsEnum(CarInsuranceTypes)
  @IsNotEmpty()
  @IsOptional()
  insurance_type: ICarInsuranceType;

  @IsNumberString()
  page: number;

  @IsNumberString()
  limit: number;
}
