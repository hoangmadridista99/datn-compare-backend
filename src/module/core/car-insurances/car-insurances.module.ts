import { TypeOrmModule } from '@nestjs/typeorm';
import { CarInsurance } from './entities/car-insurance.entity';
import { Module } from '@nestjs/common';
import { CarInsurancesService } from './car-insurances.service';
import { OperatorCarInsurancesService } from '../../operator/car-insurances/car-insurances.service';
import { OperatorCompaniesService } from '../../operator/companies/companies.service';
import { CompaniesService } from '../companies/companies.service';
import { Company } from '../companies/entities/company.entity';
import { MinioService } from '../minio/minio.service';
import { HttpModule } from '@nestjs/axios';
import { ClientCarInsurancesService } from '../../client/car-insurances/car-insurances.service';
import { CarInsuranceSettingsService } from '../car-insurance-settings/car-insurance-settings.service';
import { MandatorySetting } from '../car-insurance-settings/entities/mandatory-setting.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([CarInsurance, Company, MandatorySetting]),
    HttpModule,
  ],
  providers: [
    CarInsurancesService,
    OperatorCarInsurancesService,
    CompaniesService,
    OperatorCompaniesService,
    MinioService,
    ClientCarInsurancesService,
    CarInsuranceSettingsService,
  ],
  exports: [
    CarInsurancesService,
    OperatorCarInsurancesService,
    ClientCarInsurancesService,
  ],
})
export class CarInsurancesModule {}
