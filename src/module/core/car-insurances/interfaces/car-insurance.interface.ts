import { IBaseEntity } from '../../../../shared/interfaces/base-entity.interface';
import { CarInsuranceTypes } from '../enums/car-insurance.enum';

export type ICarInsuranceType = `${CarInsuranceTypes}`;

export interface ICarInsurance extends IBaseEntity {
  company_logo_url: string;
  company_name: string;
  insurance_name: string;
  insurance_description: string;
  insurance_type: ICarInsuranceType;
  cost_of_purchasing: IAppraisalSetting;
  choosing_service_center: IAppraisalSetting;
  component_vehicle_theft: IAppraisalSetting;
  no_depreciation_cost: IAppraisalSetting;
  water_damage: IAppraisalSetting;
  rating_scores: number;
  total_rating: number;
  insured_for_each_person: IAppraisalSetting;
}

export type ICreateCarInsuranceDto = Omit<
  ICarInsurance,
  | keyof IBaseEntity
  | 'physical_setting'
  | 'company_logo_url'
  | 'total_rating'
  | 'rating_scores'
>;
export interface IUpdateCarInsuranceDto extends ICreateCarInsuranceDto {
  company_logo_url?: string;
}
export type IUpdateRatingForCarInsuranceDto = Pick<
  ICarInsurance,
  'total_rating' | 'rating_scores'
>;

export type ICarInsurancesFilter = Pick<ICarInsurance, 'insurance_type'>;

export enum SettingTypes {
  Percentage = 'percentage',
  Cost = 'cost',
}

export interface IAppraisalSetting {
  type: `${SettingTypes}`;
  value: number;
}

export const API_PROPERTY_EXAMPLE = { type: SettingTypes.Cost, value: 100 };
