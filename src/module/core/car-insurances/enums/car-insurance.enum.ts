export enum CarInsuranceTypes {
  Physical = 'physical',
  Mandatory = 'mandatory',
  Both = 'both',
}
