import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Rating } from './entities/rating.entity';
import { RatingsService } from './ratings.service';

import { ClientRatingsService } from 'src/module/client/ratings/ratings.service';

import { OperatorRatingsService } from 'src/module/operator/ratings/ratings.service';

import { InsurancesService } from '../insurances/insurances.service';
import { Insurance } from '../insurances/entities/insurance.entity';
import { VendorRatingsService } from '../../vendor/ratings/ratings.service';
import { UserInsurances } from '../user-insurances/entities/user-insurance.entity';
import { UserInsurancesService } from '../user-insurances/user-insurances.service';
import { InsuranceRepositoryService } from '../insurances/repository';
import { CarInsurance } from '../car-insurances/entities/car-insurance.entity';
import { OperatorCompaniesService } from '../../operator/companies/companies.service';
import { CompaniesService } from '../companies/companies.service';
import { MinioService } from '../minio/minio.service';
import { Company } from '../companies/entities/company.entity';
import { HttpModule } from '@nestjs/axios';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Rating,
      Insurance,
      UserInsurances,
      CarInsurance,
      Company,
    ]),
    HttpModule,
  ],
  providers: [
    RatingsService,
    ClientRatingsService,
    InsurancesService,
    OperatorRatingsService,
    VendorRatingsService,
    UserInsurancesService,
    InsuranceRepositoryService,
    OperatorCompaniesService,
    CompaniesService,
    MinioService,
  ],
  exports: [
    RatingsService,
    ClientRatingsService,
    OperatorRatingsService,
    VendorRatingsService,
  ],
})
export class RatingsModule {}
