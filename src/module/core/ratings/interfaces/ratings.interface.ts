import type { IInsurance } from '../../insurances/interfaces/insurance.interface';
import type { IUser } from '../../users/interfaces/users.interface';
import { IBaseEntity } from 'src/shared/interfaces/base-entity.interface';
import { SortRatingTypes } from '../enums/ratings.enum';
import { ICarInsurance } from '../../car-insurances/interfaces/car-insurance.interface';

export type IRatingCreate = Pick<
  IRating,
  'score' | 'comment' | 'is_verified_by_admin'
>;
export interface ICreateCarInsuranceRatingDto
  extends Pick<IRating, 'score' | 'comment'> {
  car_insurance_id: string;
}
export type IRatingUpdate = Partial<IRatingCreate>;
export interface IVerifyRatingByAdmin {
  is_verified_by_admin: boolean;
  is_hide: boolean;
}

export interface IRating extends IBaseEntity {
  score: number;
  comment: string;
  insurance: IInsurance | null;
  user: IUser;
  is_verified_by_admin?: boolean;
  car_insurance: ICarInsurance | null;
}

export type ISortByType = `${SortRatingTypes}`;
export type IQuery = {
  limit: number;
  page: number;
  sort_by: ISortByType;
};
