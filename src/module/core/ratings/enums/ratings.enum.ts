export enum RatingScore {
  MIN = 1,
  MAX = 5,
}

export enum SortRatingTypes {
  Desc = 'DESC',
  Asc = 'ASC',
}

export enum InsuranceRatingTypes {
  default = 'default',
  car = 'car',
}
