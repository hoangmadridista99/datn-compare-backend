import { IsBoolean, IsOptional, IsString, IsUUID } from 'class-validator';
import { ApiProperty, PickType } from '@nestjs/swagger';
import { LIST_ERROR_CODES } from 'src/shared/constants/error/base.error';
import { IsRatingScore } from '../../../../shared/decorators/is-rating-score';

export class CreateRatingDto {
  @ApiProperty({ description: 'Insurance ID' })
  @IsUUID(4, { message: LIST_ERROR_CODES.SS24606 })
  insurance_id: string;

  @ApiProperty({ description: 'Rating Score', example: 4 })
  @IsRatingScore()
  score: number;

  @ApiProperty({ description: 'Comment', example: 'Good' })
  @IsString({ message: LIST_ERROR_CODES.SS24602 })
  comment: string;
}

export class UpdateCommentRatingDto extends PickType(CreateRatingDto, [
  'comment',
]) {}

export class VerifyRatingForAdminDto {
  @ApiProperty({ description: 'Verify rating for admin', example: true })
  @IsOptional()
  @IsBoolean()
  is_verified_by_admin: boolean;

  @ApiProperty({ description: 'Hide the comment', example: true })
  @IsOptional()
  @IsBoolean()
  is_hide: boolean;
}
