import { Column, Entity, ManyToOne } from 'typeorm';
import { BaseEntity } from 'src/shared/entities/base.entity';
import { Insurance } from '../../insurances/entities/insurance.entity';
import { User } from '../../users/entities/user.entity';

@Entity('ratings')
export class Rating extends BaseEntity {
  @Column()
  score: number;

  @Column()
  comment: string;

  @ManyToOne(() => Insurance, (insurance) => insurance.id, {
    onDelete: 'CASCADE',
    eager: true,
  })
  insurance: Insurance;

  @ManyToOne(() => User, (user) => user.ratings)
  user: User;

  @Column({ default: false })
  is_verified_by_admin: boolean;

  @Column({ default: false })
  is_hide: boolean;
}
