import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Rating } from './entities/rating.entity';
import { Insurance } from '../insurances/entities/insurance.entity';
import { User } from '../users/entities/user.entity';
import type {
  IRatingCreate,
  ISortByType,
  IVerifyRatingByAdmin,
} from './interfaces/ratings.interface';
import { getPaginate } from 'src/shared/lib/paginate/paginate.lib';
import { PaginateQuery, Paginated } from 'nestjs-paginate';
import { InsurancesService } from '../insurances/insurances.service';
import { InsuranceRepositoryService } from '../insurances/repository';

export class RatingsService {
  constructor(
    @InjectRepository(Rating)
    private readonly ratingsRepository: Repository<Rating>,
    private readonly insurancesService: InsurancesService,
    private readonly insuranceRepositoryService: InsuranceRepositoryService,
  ) {}

  private queryBuilder() {
    try {
      return this.ratingsRepository
        .createQueryBuilder('ratings')
        .leftJoinAndSelect('ratings.insurance', 'insurance')
        .leftJoinAndSelect('insurance.company', 'company')
        .leftJoinAndSelect('ratings.user', 'user')
        .leftJoin('insurance.insurance_category', 'insurance_category')
        .select([
          'ratings',
          'user.first_name',
          'user.last_name',
          'user.id',
          'user.avatar_profile_url',
          'company.logo',
          'insurance_category.label',
        ]);
    } catch (error) {
      console.log(
        '🚀 ~ file: ratings.service.ts:23 ~ RatingsService ~ queryBuilder:',
        error,
      );
      throw error;
    }
  }

  async calculateAvgRatings(insuranceId: string): Promise<number> {
    try {
      const result = await this.ratingsRepository.average('score', {
        insurance: { id: insuranceId },
        is_verified_by_admin: true,
        is_hide: false,
      });
      return Math.round(result * 10) / 10;
    } catch (error) {
      console.log(
        '🚀 ~ file: ratings.service.ts:35 ~ RatingsService ~ calculateAvgRatings:',
        error,
      );
      throw error;
    }
  }

  async calculateTotalRatings(insuranceId: string): Promise<number> {
    try {
      return await this.ratingsRepository.count({
        where: {
          insurance: { id: insuranceId },
          is_verified_by_admin: true,
          is_hide: false,
        },
      });
    } catch (error) {
      console.log(
        '🚀 ~ file: ratings.service.ts:50 ~ RatingsService ~ calculateTotalRatings:',
        error,
      );

      throw error;
    }
  }

  async createRating(
    user: User,
    insurance: Insurance,
    body: IRatingCreate,
  ): Promise<boolean> {
    try {
      const rating = this.ratingsRepository.create({
        ...body,
        user,
        insurance,
      });
      await this.ratingsRepository.save(rating);
      return true;
    } catch (error) {
      console.log(
        '🚀 ~ file: ratings.service.ts:18 ~ RatingsService ~ createRating:',
        error,
      );

      return false;
    }
  }

  async handleCalculateAfterVerify(insuranceId: string): Promise<boolean> {
    const [rating_scores, total_rating] = await Promise.all([
      this.calculateAvgRatings(insuranceId),
      this.calculateTotalRatings(insuranceId),
    ]);
    const result =
      await this.insuranceRepositoryService.updateRatingInsuranceById(
        insuranceId,
        {
          rating_scores,
          total_rating,
        },
      );
    return !!result.affected;
  }

  async findAllByInsurance(
    query: PaginateQuery,
    id: string,
    sortBy?: ISortByType,
  ): Promise<Paginated<Rating> | null> {
    try {
      const queryBuilder = this.queryBuilder().where({
        insurance: {
          id,
        },
        is_verified_by_admin: true,
        is_hide: false,
      });
      if (!!sortBy) {
        queryBuilder.orderBy('ratings.score', sortBy);
      }
      return getPaginate(query, queryBuilder);
    } catch (error) {
      console.log(
        '🚀 ~ file: ratings.service.ts:37 ~ RatingsService ~ findAllByInsurance:',
        error,
      );
      return null;
    }
  }

  async findAllByOperator(
    query: PaginateQuery,
    insurance_category?: string,
    insurance_name?: string,
  ): Promise<Paginated<Rating> | null> {
    try {
      const queryBuilder = this.queryBuilder()
        .addSelect('insurance')
        .where({ is_verified_by_admin: false });
      if (insurance_category) {
        queryBuilder.andWhere('insurance_category.label = :label', {
          label: insurance_category,
        });
      }

      if (insurance_name) {
        queryBuilder.andWhere('insurance.name ilike :name', {
          name: `%${insurance_name}%`,
        });
      }

      return getPaginate(query, queryBuilder);
    } catch (error) {
      console.log(
        '🚀 ~ file: ratings.service.ts:86 ~ RatingsService ~ findAllByOperator:',
        error,
      );
      return null;
    }
  }

  async findById(id: string): Promise<Rating | null> {
    try {
      return await this.ratingsRepository.findOneBy({ id });
    } catch (error) {
      console.log(
        '🚀 ~ file: ratings.service.ts:63 ~ RatingsService ~ findById:',
        error,
      );
      return null;
    }
  }

  async deleteRating(id: string): Promise<boolean> {
    try {
      const result = await this.ratingsRepository.delete(id);
      return !!result.affected;
    } catch (error) {
      console.log(
        '🚀 ~ file: ratings.service.ts:91 ~ RatingsService ~ deleteRating:',
        error,
      );
      return false;
    }
  }

  async updateComment(id: string, comment: string): Promise<boolean> {
    try {
      const result = await this.ratingsRepository.update(id, { comment });
      return !!result.affected;
    } catch (error) {
      console.log(
        '🚀 ~ file: ratings.service.ts:125 ~ RatingsService ~ updateComment:',
        error,
      );
      return false;
    }
  }

  async updateVerifiedByAdmin(id: string, data: IVerifyRatingByAdmin) {
    try {
      return await this.ratingsRepository.update(id, data);
    } catch (error) {
      console.log(
        '🚀 ~ file: ratings.service.ts:106 ~ RatingsService ~ updateComment:',
        error,
      );
      throw error;
    }
  }
}
