import { HttpStatus, Injectable } from '@nestjs/common';
import { AppraisalFormsService } from '../../core/appraisal-form/appraisal-forms.service';
import {
  IAppraisalForm,
  IAppraisalFormType,
  ICreateAppraisalFromDto,
  IUpdateAppraisalFormDto,
} from '../../core/appraisal-form/interfaces/appraisal-form.interface';
import {
  AppraisalFormStatuses,
  AppraisalFormTypes,
} from '../../core/appraisal-form/enum/appraisal-form.enum';
import { NO_CONTENT_SUCCESS_RESPONSE } from '../../../shared/lib/responses/response';
import { PaginateQuery } from 'nestjs-paginate';
import {
  LIST_ERROR_CODES,
  getError,
} from '../../../shared/constants/error/base.error';
import { MinioService } from '../../core/minio/minio.service';
import { CarInsurancesService } from '../../core/car-insurances/car-insurances.service';
import { IAppraisalData } from '../../core/appraisals/entities/appraisal.entity';
import { AppraisalForm } from '../../core/appraisal-form/entities/appraisal-form.entity';

interface IAppraisalFormDetailsError {
  statusCode: number;
  error_code: string;
}

@Injectable()
export class ClientAppraisalFormsService {
  constructor(
    private readonly appraisalFormsService: AppraisalFormsService,
    private readonly minioService: MinioService,
    private readonly carInsurance: CarInsurancesService,
  ) {}

  private async handleAppraisalData(appraisalForm: AppraisalForm) {
    const result = appraisalForm;
    const appraisalData = result.appraisal.appraisal_data.map(
      async (data: IAppraisalData) => {
        const carInsurance = await this.carInsurance.findOneByQuery({
          id: data.physical_setting_id,
        });
        if (!carInsurance) return;
        return {
          ...data,
          company_name: carInsurance.company_name,
          company_logo_url: carInsurance.company_logo_url,
          total_rating: carInsurance.total_rating,
          rating_scores: carInsurance.rating_scores,
          insurance_description: carInsurance.insurance_description,
        };
      },
    );

    result.appraisal.appraisal_data = (await Promise.all(appraisalData)).filter(
      (e: IAppraisalData) => !!e,
    );
    return result;
  }

  private async handleUpdateAppraisalFormBody(
    body: IUpdateAppraisalFormDto,
    {
      certificate_image_url,
      vehicle_condition_image_urls,
      form_type,
    }: IAppraisalForm,
    files: Array<Express.Multer.File>,
    update_image_urls?: string[],
  ) {
    body.certificate_image_url = null;
    body.vehicle_condition_image_urls = null;
    /** Remove images in cloud */
    if (form_type !== body.form_type) {
      this.removeAppraisalFormImageFiles(
        !vehicle_condition_image_urls
          ? certificate_image_url
          : vehicle_condition_image_urls,
      );
    } else {
      this.removeAppraisalFormImageFiles(
        !update_image_urls ? certificate_image_url : update_image_urls,
      );
    }

    const imageUrls =
      await this.appraisalFormsService.uploadAppraisalFormImages(files);
    switch (body.form_type) {
      case AppraisalFormTypes.Unexpired:
        body.certificate_image_url = imageUrls[0];
        break;
      case AppraisalFormTypes.HasExpired:
      case AppraisalFormTypes.NewCar:
        if (form_type === AppraisalFormTypes.Unexpired) {
          body.vehicle_condition_image_urls = imageUrls;
        } else {
          const result = vehicle_condition_image_urls.map((url: string) =>
            update_image_urls.includes(url)
              ? imageUrls[update_image_urls.indexOf(url)]
              : url,
          );
          body.vehicle_condition_image_urls = result;
        }
        break;
      default:
        break;
    }
    body.form_status = AppraisalFormStatuses.Pending;
    return body;
  }

  private removeAppraisalFormImageFiles(urls: string | Array<string>) {
    if (!urls) return;
    const result = typeof urls === 'string' ? [urls] : urls;
    result.forEach(async (url) => {
      const key = url.split('=')[1];
      return await this.minioService.deleteFile(key);
    });
  }

  private async validateAppraisalFormBodyBeforeUpdate(
    id: string,
    userId: string,
  ) {
    const appraisalFormDetails =
      await this.appraisalFormsService.findOneFormByQuery({ id });

    if (!appraisalFormDetails)
      return getError(LIST_ERROR_CODES.SS24101, HttpStatus.NOT_FOUND);

    if (
      appraisalFormDetails.user_id !== userId ||
      appraisalFormDetails.form_status === AppraisalFormStatuses.Paid
    )
      return getError(LIST_ERROR_CODES.SS24103, HttpStatus.BAD_REQUEST);
    return appraisalFormDetails;
  }

  private validateImageFiles(
    filesLength: number,
    bodyFormType: IAppraisalFormType,
    vehicle_condition_image_urls: string[],
    appraisalFormType: IAppraisalFormType,
    certificate_image_url: string,
    update_image_urls: string[],
  ) {
    if (
      (filesLength !== 1 && bodyFormType === AppraisalFormTypes.Unexpired) ||
      (filesLength === 1 &&
        !update_image_urls &&
        bodyFormType !== AppraisalFormTypes.Unexpired) ||
      (appraisalFormType !== AppraisalFormTypes.Unexpired &&
        bodyFormType !== AppraisalFormTypes.Unexpired &&
        !update_image_urls)
    )
      return getError(LIST_ERROR_CODES.SS24108, HttpStatus.BAD_REQUEST);

    let isUpdateImageUrl = true;
    update_image_urls.forEach((url) => {
      switch (appraisalFormType) {
        case AppraisalFormTypes.Unexpired:
          isUpdateImageUrl = certificate_image_url.includes(url);
          break;
        case AppraisalFormTypes.NewCar:
        case AppraisalFormTypes.HasExpired:
          isUpdateImageUrl = vehicle_condition_image_urls?.includes(url);
          break;
        default:
          isUpdateImageUrl = false;
          break;
      }
    });
    if (!isUpdateImageUrl)
      return getError(LIST_ERROR_CODES.SS241002, HttpStatus.BAD_REQUEST);
  }

  private convertBooleanStringToBooleanInBody(
    body: ICreateAppraisalFromDto | IUpdateAppraisalFormDto,
  ) {
    Object.keys(body).forEach((key) => {
      if (body[key] === 'true' || body[key] === 'false')
        body[key] = JSON.parse(body[key]);
    });
    return body;
  }

  async createNewAppraisalForm(
    userId: string,
    body: ICreateAppraisalFromDto,
    files: Array<Express.Multer.File>,
  ) {
    const imagesUrl =
      await this.appraisalFormsService.uploadAppraisalFormImages(files);
    if (body.form_type === AppraisalFormTypes.Unexpired) {
      body.certificate_image_url = imagesUrl[0];
    } else {
      body.vehicle_condition_image_urls = imagesUrl;
    }
    const payload = this.convertBooleanStringToBooleanInBody(
      body,
    ) as ICreateAppraisalFromDto;
    await this.appraisalFormsService.createAppraisalForm(userId, payload);
    return NO_CONTENT_SUCCESS_RESPONSE;
  }

  async getListFormsByUserId(userId: string, query: PaginateQuery) {
    return await this.appraisalFormsService.getListFormsByUserId(userId, query);
  }

  async getFormDetailsById(id: string, userId: string) {
    const result = await this.appraisalFormsService.findOneFormByQuery({
      id,
      user_id: userId,
    });
    if (!result)
      return getError(LIST_ERROR_CODES.SS24101, HttpStatus.NOT_FOUND);
    if (
      result.form_status === AppraisalFormStatuses.Approved &&
      result.appraisal &&
      result.appraisal.appraisal_data.length
    )
      return await this.handleAppraisalData(result);
    return result;
  }

  async updateFormById(
    id: string,
    { update_image_urls, ...body }: IUpdateAppraisalFormDto,
    userId: string,
    files?: Array<Express.Multer.File>,
  ) {
    const checkAppraisalFormDetails:
      | IAppraisalForm
      | IAppraisalFormDetailsError =
      await this.validateAppraisalFormBodyBeforeUpdate(id, userId);

    if ((checkAppraisalFormDetails as IAppraisalFormDetailsError).statusCode)
      return checkAppraisalFormDetails;

    const appraisalFormDetails = checkAppraisalFormDetails as IAppraisalForm;

    if (!!files) {
      if (
        body.form_type !== AppraisalFormTypes.Unexpired &&
        appraisalFormDetails.form_type === AppraisalFormTypes.Unexpired
      ) {
        if (
          files.length !== 5 ||
          update_image_urls.length !== 1 ||
          !appraisalFormDetails.certificate_image_url.includes(
            update_image_urls[0],
          )
        )
          return getError(LIST_ERROR_CODES.SS24108, HttpStatus.BAD_REQUEST);
      } else {
        const validateFiles = this.validateImageFiles(
          files.length,
          body.form_type,
          appraisalFormDetails.vehicle_condition_image_urls,
          appraisalFormDetails.form_type,
          appraisalFormDetails.certificate_image_url,
          update_image_urls,
        );
        if (!!validateFiles) return validateFiles;
      }
      body = await this.handleUpdateAppraisalFormBody(
        body,
        appraisalFormDetails,
        files,
        update_image_urls,
      );
    }
    const result = this.convertBooleanStringToBooleanInBody(
      body,
    ) as IUpdateAppraisalFormDto;
    await this.appraisalFormsService.updateAppraisalFormById(id, result);
    return NO_CONTENT_SUCCESS_RESPONSE;
  }

  async deleteFormById(id: string, userId: string) {
    const appraisalFormDetails =
      await this.appraisalFormsService.findOneFormByQuery({ id });

    if (!appraisalFormDetails)
      return getError(LIST_ERROR_CODES.SS24101, HttpStatus.NOT_FOUND);
    if (appraisalFormDetails.user_id !== userId)
      return getError(LIST_ERROR_CODES.SS24102, HttpStatus.BAD_REQUEST);

    await this.appraisalFormsService.deleteAppraisalFormById(id);
    return NO_CONTENT_SUCCESS_RESPONSE;
  }
}
