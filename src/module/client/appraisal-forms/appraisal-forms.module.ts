import { Module } from '@nestjs/common';
import { AppraisalFormsModule } from '../../core/appraisal-form/appraisal-forms.module';

@Module({
  imports: [AppraisalFormsModule],
  providers: [AppraisalFormsModule],
  exports: [AppraisalFormsModule],
})
export class ClientAppraisalFormsModule {}
