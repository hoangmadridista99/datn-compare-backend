import {
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  Param,
  Patch,
  Post,
  UploadedFiles,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiTags,
  ApiBearerAuth,
  ApiOperation,
  ApiBody,
  ApiConsumes,
  ApiQuery,
} from '@nestjs/swagger';
import { JwtUserAuthGuard } from '../../core/auth/guards/jwt-auth.guard';
import { ClientAppraisalFormsService } from './appraisal-forms.service';
import { AuthResponse } from '../../../shared/decorators/response-auth/auth.decorator';
import { User } from '../../core/users/entities/user.entity';
import {
  AppraisalFormDto,
  CreateAppraisalFormDto,
  UpdateAppraisalApiBody,
} from '../../core/appraisal-form/dto/appraisal-form.dto';
import {
  LIST_ERROR_CODES,
  getError,
} from '../../../shared/constants/error/base.error';
import { FilesInterceptor } from '@nestjs/platform-express';
import {
  ImagesValidationPipe,
  UpdateImagesValidationPipe,
} from '../../../shared/pipes/file-image.pipe';
import { AppraisalFormTypes } from '../../core/appraisal-form/enum/appraisal-form.enum';
import { Paginate, PaginateQuery } from 'nestjs-paginate';
import {
  LIMIT_QUERY,
  PAGE_QUERY,
} from '../../../shared/constants/swagger/constant';
import { UUIDPipe } from '../../../shared/pipes/uuid.pipe';
import {
  AppraisalFormDtoPipe,
  UpdateAppraisalFormDtoPipe,
} from '../../../shared/pipes/appraisal-form.pipe';
import { IUpdateAppraisalFormDto } from '../../core/appraisal-form/interfaces/appraisal-form.interface';

@ApiTags('Appraisal-Forms')
@Controller('appraisal-forms')
@ApiBearerAuth()
@UseGuards(JwtUserAuthGuard)
export class ClientAppraisalFormsController {
  constructor(
    private readonly clientAppraisalFormsService: ClientAppraisalFormsService,
  ) {}

  @Post()
  @ApiConsumes('multipart/form-data')
  @UseInterceptors(FilesInterceptor('files'))
  @ApiOperation({ summary: 'Create new appraisal form' })
  @ApiBody({ type: CreateAppraisalFormDto })
  async createNewAppraisalForm(
    @AuthResponse() { id }: User,
    @Body(new AppraisalFormDtoPipe()) body: AppraisalFormDto,
    @UploadedFiles(new ImagesValidationPipe())
    files: Array<Express.Multer.File>,
  ) {
    try {
      if (
        (files.length === 1 &&
          body.form_type !== AppraisalFormTypes.Unexpired) ||
        (files.length !== 1 && body.form_type === AppraisalFormTypes.Unexpired)
      )
        return getError(LIST_ERROR_CODES.SS24109, HttpStatus.BAD_REQUEST);
      return await this.clientAppraisalFormsService.createNewAppraisalForm(
        id,
        body,
        files,
      );
    } catch (error) {
      console.log(
        '🚀 ~ file: appraisal-forms.controller.ts:29 ~ ClientAppraisalFormsController ~ error:',
        error,
      );
      return getError(
        LIST_ERROR_CODES.SS24001,
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  @Get('users')
  @ApiQuery(PAGE_QUERY)
  @ApiQuery(LIMIT_QUERY)
  @ApiOperation({ summary: 'Get list forms for each user' })
  async getListFormsByUserId(
    @AuthResponse() { id }: User,
    @Paginate() query: PaginateQuery,
  ) {
    try {
      return await this.clientAppraisalFormsService.getListFormsByUserId(
        id,
        query,
      );
    } catch (error) {
      console.log(
        '🚀 ~ file: appraisal-forms.controller.ts:79 ~ ClientAppraisalFormsController ~ getListFormsByUserId ~ error:',
        error,
      );
      return getError(
        LIST_ERROR_CODES.SS24001,
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  @Get(':id')
  @ApiOperation({ summary: 'Get appraisal form details' })
  async getDetails(
    @Param('id', new UUIDPipe()) id: string,
    @AuthResponse() { id: userId }: User,
  ) {
    try {
      return this.clientAppraisalFormsService.getFormDetailsById(id, userId);
    } catch (error) {
      console.log(
        '🚀 ~ file: appraisal-forms.controller.ts:112 ~ ClientAppraisalFormsController ~ getDetails ~ error:',
        error,
      );
      return getError(
        LIST_ERROR_CODES.SS24001,
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  @Patch(':id')
  @ApiBody({ type: UpdateAppraisalApiBody })
  @ApiConsumes('multipart/form-data')
  @UseInterceptors(FilesInterceptor('files'))
  @ApiOperation({ summary: 'Update appraisal form for user' })
  async updateAppraisalForm(
    @Param('id', new UUIDPipe()) id: string,
    @Body(new UpdateAppraisalFormDtoPipe()) body: IUpdateAppraisalFormDto,
    @AuthResponse() user: User,
    @UploadedFiles(new UpdateImagesValidationPipe())
    files?: Array<Express.Multer.File>,
  ) {
    try {
      if (!!body.update_image_urls && !files)
        return getError(LIST_ERROR_CODES.SS24109, HttpStatus.BAD_REQUEST);
      return await this.clientAppraisalFormsService.updateFormById(
        id,
        body,
        user.id,
        files,
      );
    } catch (error) {
      console.log(
        '🚀 ~ file: appraisal-forms.controller.ts:143 ~ ClientAppraisalFormsController ~ error:',
        error,
      );
      return getError(LIST_ERROR_CODES.SS24001);
    }
  }

  @Delete(':id')
  @ApiOperation({ summary: 'Delete appraisal form for client ' })
  async deleteAppraisalForm(
    @Param('id', new UUIDPipe()) id: string,
    @AuthResponse() user: User,
  ) {
    try {
      return await this.clientAppraisalFormsService.deleteFormById(id, user.id);
    } catch (error) {
      console.log(
        '🚀 ~ file: appraisal-forms.controller.ts:172 ~ ClientAppraisalFormsController ~ error:',
        error,
      );
      return getError(LIST_ERROR_CODES.SS24001);
    }
  }
}
