import { Controller, Get } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { ClientBlogCategoriesService } from './blog-categories.service';
import { Public } from '../../../shared/decorators/public.decorator';
import {
  LIST_ERROR_CODES,
  getError,
} from '../../../shared/constants/error/base.error';

@ApiTags('Blog-Categories')
@Controller('blog-categories')
export class ClientBlogCategoriesController {
  constructor(
    private readonly clientBlogCategoriesService: ClientBlogCategoriesService,
  ) {}

  @Get()
  @Public()
  @ApiOperation({ summary: 'Get list categories of blog' })
  async getListCategories() {
    try {
      return await this.clientBlogCategoriesService.getListCategories();
    } catch (error) {
      console.log(
        '🚀 ~ file: blog-categories.controller.ts:20 ~ ClientBlogCategoriesController ~ getListCategories ~ error:',
        error,
      );
      return getError(LIST_ERROR_CODES.SS24001);
    }
  }
}
