import { Injectable } from '@nestjs/common';
import { BlogCategoriesService } from '../../core/blog-categories/blog-categories.service';

@Injectable()
export class ClientBlogCategoriesService {
  constructor(private readonly blogCategoriesService: BlogCategoriesService) {}

  async getListCategories() {
    return await this.blogCategoriesService.getAllCategories(false);
  }
}
