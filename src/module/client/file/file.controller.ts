import { Controller, Get, Query, Res } from '@nestjs/common';
import { ApiTags, ApiQuery } from '@nestjs/swagger';
import { ClientFileService } from './file.service';
import type { Response } from 'express';

@Controller('file')
@ApiTags('File')
export class ClientFileController {
  constructor(private readonly clientFileService: ClientFileService) {}

  @Get()
  @ApiQuery({
    required: true,
    name: 'key',
    explode: true,
  })
  async getFile(@Res() res: Response, @Query('key') key?: string) {
    try {
      return await this.clientFileService.getFile(res, key);
    } catch (error) {
      console.log(
        '🚀 ~ file: file.controller.ts:21 ~ ClientFileController ~ getFile:',
        error,
      );
      throw error;
    }
  }

  @Get('/download')
  @ApiQuery({
    required: true,
    name: 'key',
    explode: true,
  })
  async downloadFile(@Res() res: Response, @Query('key') key?: string) {
    try {
      return this.clientFileService.downloadFile(res, key);
    } catch (error) {
      console.log(
        '🚀 ~ file: file.controller.ts:44 ~ ClientFileController ~ downloadFile:',
        error,
      );
      throw error;
    }
  }
}
