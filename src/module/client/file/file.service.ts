import { HttpStatus, Injectable } from '@nestjs/common';
import { MinioService } from 'src/module/core/minio/minio.service';
import type { Response } from 'express';
import {
  LIST_ERROR_CODES,
  getError,
} from 'src/shared/constants/error/base.error';
import { MinioTypes } from 'src/module/core/minio/enums/minio.enum';

@Injectable()
export class ClientFileService {
  constructor(private readonly minioService: MinioService) {}

  private responseNotFound(res: Response) {
    res.status(HttpStatus.NOT_FOUND).send('Not found!');

    return;
  }

  private sendImageFile(res: Response, data: Buffer) {
    res.setHeader('Content-Type', 'image/png');
    res.send(data);
  }

  private sendFileInsurancePdf(res: Response, data: Buffer, fileName: string) {
    res.setHeader('Content-Type', 'application/pdf');
    res.setHeader('Content-Disposition', `attachment; filename=${fileName}`);
    res.send(data);
  }

  async getFile(res: Response, key?: string) {
    try {
      if (!key) return this.responseNotFound(res);
      const file = await this.minioService.getFile(key);
      if (!file) return this.responseNotFound(res);
      const [type, filename] = key.split('/');

      switch (type) {
        case MinioTypes.COMPANY_LOGO:
        case MinioTypes.AVATAR_PROFILE:
        case MinioTypes.BLOG_BANNER:
        case MinioTypes.APPRAISAL_FORM:
          return this.sendImageFile(res, file);
        case MinioTypes.INSURANCE_PDF:
          return this.sendFileInsurancePdf(res, file, filename);
        default:
          return this.responseNotFound(res);
      }
    } catch (error) {
      console.log('🚀 ~ file: file.service.ts:14 ~ ClientFileService:', error);

      return getError(LIST_ERROR_CODES.SS24001, 500);
    }
  }

  async downloadFile(res: Response, key?: string) {
    try {
      if (!key) return this.responseNotFound(res);
      const result = await this.minioService.downloadFile(key);
      if (!result) return this.responseNotFound(res);
      const [, fileName] = key.split('/');
      res.setHeader('Content-Length', result.size);
      res.setHeader('Content-Type', 'application/octet-stream');
      res.setHeader('Content-Disposition', `attachment; filename=${fileName}`);
      result.steam.pipe(res);
    } catch (error) {
      console.log(
        '🚀 ~ file: file.service.ts:36 ~ ClientFileService ~ downloadFile:',
        error,
      );

      return getError(LIST_ERROR_CODES.SS24001, 500);
    }
  }
}
