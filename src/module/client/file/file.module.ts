import { Module } from '@nestjs/common';
import { MinioModule } from 'src/module/core/minio/minio.module';
import { ClientFileService } from './file.service';

@Module({
  imports: [MinioModule],
  providers: [ClientFileService],
  exports: [ClientFileService],
})
export class ClientFileModule {}
