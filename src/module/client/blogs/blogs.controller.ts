import { Controller, Get, Param, Query } from '@nestjs/common';
import { ApiOperation, ApiQuery, ApiTags } from '@nestjs/swagger';
import { ClientBlogsService } from './blogs.service';
import { Paginate, PaginateQuery } from 'nestjs-paginate';
import {
  LIST_ERROR_CODES,
  getError,
} from '../../../shared/constants/error/base.error';
import { Public } from '../../../shared/decorators/public.decorator';
import { UUIDPipe } from '../../../shared/pipes/uuid.pipe';
import {
  PAGE_QUERY,
  LIMIT_QUERY,
} from '../../../shared/constants/swagger/constant';

@ApiTags('Blogs')
@Controller('blogs')
export class ClientBlogsController {
  constructor(private readonly clientBlogsService: ClientBlogsService) {}

  @Get()
  @Public()
  @ApiQuery(PAGE_QUERY)
  @ApiQuery(LIMIT_QUERY)
  @ApiOperation({ summary: 'Get list all blogs' })
  async getListBlogs(
    @Paginate() query: PaginateQuery,
    @Query('blog_category_id', new UUIDPipe()) blog_category_id?: string,
  ) {
    try {
      return await this.clientBlogsService.getListBlogsForClient(
        query,
        blog_category_id,
      );
    } catch (error) {
      console.log(
        '🚀 ~ file: blogs.controller.ts:23 ~ ClientBlogsController ~ getListBlogs ~ error:',
        error,
      );
      return getError(LIST_ERROR_CODES.SS24001);
    }
  }

  @Public()
  @Get(':id')
  @ApiOperation({ summary: 'Get detail blog' })
  async getDetailBlog(@Param('id', new UUIDPipe()) id: string) {
    try {
      return await this.clientBlogsService.getOneById(id);
    } catch (error) {
      console.log(
        '🚀 ~ file: blogs.controller.ts:48 ~ ClientBlogsController ~ getDetailBlog ~ error:',
        error,
      );
      return getError(LIST_ERROR_CODES.SS24001);
    }
  }
}
