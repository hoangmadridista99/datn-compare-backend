import { HttpStatus, Injectable } from '@nestjs/common';
import { BlogsService } from '../../core/blogs/blogs.service';
import { PaginateQuery } from 'nestjs-paginate';
import {
  LIST_ERROR_CODES,
  getError,
} from '../../../shared/constants/error/base.error';
import { BlogStatusesTypes } from '../../core/blogs/enums/blogs.enum';

@Injectable()
export class ClientBlogsService {
  constructor(private readonly blogsService: BlogsService) {}

  async getListBlogsForClient(
    query: PaginateQuery,
    blog_category_id: string | null,
  ) {
    return await this.blogsService.findAllBlogs(query, blog_category_id);
  }

  async getOneById(id: string) {
    const result = await this.blogsService.findOneById(id);
    return !result
      ? getError(LIST_ERROR_CODES.SS24101, HttpStatus.NOT_FOUND)
      : result.status !== BlogStatusesTypes.Approved
      ? getError(LIST_ERROR_CODES.SS24101, HttpStatus.NOT_FOUND)
      : result;
  }
}
