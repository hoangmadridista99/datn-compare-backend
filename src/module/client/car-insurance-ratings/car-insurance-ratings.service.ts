import { HttpStatus, Injectable } from '@nestjs/common';
import { CarInsuranceRatingsService } from '../../core/car-insurance-ratings/car-insurance-ratings.service';
import { User } from '../../core/users/entities/user.entity';
import { ICreateCarInsuranceRatingDto } from '../../core/car-insurance-ratings/interfaces/car-insurance-rating.interface';
import {
  LIST_ERROR_CODES,
  getError,
} from '../../../shared/constants/error/base.error';
import { ISortByType } from '../../core/ratings/interfaces/ratings.interface';
import { PaginateQuery } from 'nestjs-paginate';
import { NO_CONTENT_SUCCESS_RESPONSE } from '../../../shared/lib/responses/response';

@Injectable()
export class ClientCarInsuranceRatingsService {
  constructor(
    private readonly carInsuranceRatingsService: CarInsuranceRatingsService,
  ) {}

  async createNewRatings(user: User, body: ICreateCarInsuranceRatingDto) {
    await this.carInsuranceRatingsService.createNewRating(user, body);
    return NO_CONTENT_SUCCESS_RESPONSE;
  }

  async findAllByInsurance(
    query: PaginateQuery,
    id: string,
    sortBy: ISortByType,
  ) {
    return (
      (await this.carInsuranceRatingsService.findAllByCarInsuranceForClient(
        query,
        id,
        sortBy,
      )) ?? getError(LIST_ERROR_CODES.SS24101, HttpStatus.NOT_FOUND)
    );
  }
}
