import {
  Body,
  Controller,
  Get,
  HttpStatus,
  Param,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import {
  ApiTags,
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiQuery,
} from '@nestjs/swagger';
import { JwtUserAuthGuard } from '../../core/auth/guards/jwt-auth.guard';
import { ClientCarInsuranceRatingsService } from './car-insurance-ratings.service';
import { AuthResponse } from '../../../shared/decorators/response-auth/auth.decorator';
import { User } from '../../core/users/entities/user.entity';
import { CreateCarInsuranceRatingDto } from '../../core/car-insurance-ratings/dto/car-insurance-rating.dto';
import {
  LIST_ERROR_CODES,
  getError,
} from '../../../shared/constants/error/base.error';
import { Paginate, PaginateQuery } from 'nestjs-paginate';
import { Public } from '../../../shared/decorators/public.decorator';
import { UUIDPipe } from '../../../shared/pipes/uuid.pipe';
import { SortRatingTypes } from '../../core/ratings/enums/ratings.enum';
import { IQuery } from '../../core/ratings/interfaces/ratings.interface';
import {
  LIMIT_QUERY,
  PAGE_QUERY,
} from '../../../shared/constants/swagger/constant';

@ApiTags('Car-Insurance-Ratings')
@Controller('car-insurance-ratings')
@ApiBearerAuth()
@UseGuards(JwtUserAuthGuard)
export class ClientCarInsuranceRatingsController {
  constructor(
    private readonly clientCarInsuranceRatingsService: ClientCarInsuranceRatingsService,
  ) {}

  @Post()
  @ApiOperation({ summary: 'Create Rating for insurance' })
  @ApiResponse({ status: 204, description: 'Create Rating Success' })
  async createRating(
    @AuthResponse() user: User,
    @Body() data: CreateCarInsuranceRatingDto,
  ) {
    try {
      return await this.clientCarInsuranceRatingsService.createNewRatings(
        user,
        data,
      );
    } catch (error) {
      console.log(
        '🚀 ~ file: ratings.controller.ts:58 ~ ClientRatingsController ~ createRating:',
        error,
      );

      return getError(LIST_ERROR_CODES.SS24001);
    }
  }

  @Get('car-insurances/:id')
  @Public()
  @ApiQuery(PAGE_QUERY)
  @ApiQuery(LIMIT_QUERY)
  @ApiQuery({
    example: SortRatingTypes.Desc,
    enum: SortRatingTypes,
    name: 'sort_by',
    required: false,
  })
  @ApiOperation({ summary: 'Get rating by insurance' })
  async findAllByInsurance(
    @Paginate() query: PaginateQuery,
    @Param('id', new UUIDPipe()) id: string,
    @Query() { sort_by }: IQuery,
  ) {
    try {
      if (
        !!sort_by &&
        sort_by !== SortRatingTypes.Asc &&
        sort_by !== SortRatingTypes.Desc
      )
        return getError(LIST_ERROR_CODES.SS24429, HttpStatus.BAD_REQUEST);
      return await this.clientCarInsuranceRatingsService.findAllByInsurance(
        query,
        id,
        sort_by,
      );
    } catch (error) {
      console.log(
        '🚀 ~ file: ratings.controller.ts:14 ~ ClientRatingsController ~ findAllByInsurance:',
        error,
      );

      return getError(LIST_ERROR_CODES.SS24001);
    }
  }
}
