import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Get,
  Patch,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiBody,
  ApiConsumes,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { User } from '../../core/users/entities/user.entity';
import { AuthResponse } from '../../../shared/decorators/response-auth/auth.decorator';
import { JwtUserAuthGuard } from '../../core/auth/guards/jwt-auth.guard';
import {
  BodyUpdateUserDto,
  UpdateUserDto,
} from '../../core/users/dto/update-user.dto';
import { ClientUsersService } from './user.service';
import {
  getError,
  LIST_ERROR_CODES,
} from '../../../shared/constants/error/base.error';
import { FileImageValidationPipe } from '../../../shared/pipes/file-image.pipe';
import { FileInterceptor } from '@nestjs/platform-express';

@ApiTags('Users')
@Controller('users')
@ApiBearerAuth()
@UseGuards(JwtUserAuthGuard)
@UseInterceptors(ClassSerializerInterceptor)
export class ClientUserController {
  constructor(private readonly clientUsersService: ClientUsersService) {}

  @Get('/profile')
  @ApiOperation({ summary: 'Get user profile' })
  @ApiResponse({ status: 401, description: 'Unauthorized.' })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  getProfile(@AuthResponse() user: User) {
    return user;
  }

  @Patch('/profile')
  @ApiConsumes('multipart/form-data')
  @UseInterceptors(FileInterceptor('file'))
  @ApiBody({ type: BodyUpdateUserDto })
  @ApiOperation({ summary: 'Update user profile' })
  @ApiResponse({ status: 401, description: 'Unauthorized.' })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  async updateProfile(
    @AuthResponse() user: User,
    @Body() body: UpdateUserDto,
    @UploadedFile(new FileImageValidationPipe()) file: Express.Multer.File,
  ) {
    try {
      return await this.clientUsersService.updateProfile(user, body, file);
    } catch (error) {
      console.log(
        '🚀 ~ file: user.controller.ts:44 ~ ClientUserController ~ updateProfile ~ error:',
        error,
      );
      throw getError(LIST_ERROR_CODES.SS24001);
    }
  }
}
