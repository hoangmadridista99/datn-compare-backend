import { HttpStatus, Injectable } from '@nestjs/common';
import {
  getError,
  LIST_ERROR_CODES,
} from '../../../shared/constants/error/base.error';
import { UpdateUserDto } from '../../core/users/dto/update-user.dto';
import { UsersService } from '../../core/users/users.service';
import { User } from '../../core/users/entities/user.entity';

@Injectable()
export class ClientUsersService {
  constructor(private readonly usersService: UsersService) {}

  async updateProfile(
    user: User,
    body: UpdateUserDto,
    file: Express.Multer.File,
  ) {
    const { email, phone } = body;
    let validateEmail = true;
    if (email !== user.email) {
      validateEmail = await this.usersService.checkEmailExistWhenUpdate(
        body.email,
      );
    }
    if (!validateEmail)
      return getError(
        LIST_ERROR_CODES.SS24204,
        HttpStatus.UNPROCESSABLE_ENTITY,
      );
    let validatePhone = true;
    if (phone !== user.phone) {
      validatePhone = await this.usersService.checkPhoneExistWhenUpdate(
        body.phone,
      );
    }
    if (!validatePhone)
      return getError(
        LIST_ERROR_CODES.SS24205,
        HttpStatus.UNPROCESSABLE_ENTITY,
      );
    const avatarUrl = !file
      ? undefined
      : await this.usersService.uploadAvatarProfile(file);
    body.avatar_profile_url = avatarUrl;
    const result = await this.usersService.updateUserById(user.id, body);
    return !result.affected
      ? getError(LIST_ERROR_CODES.SS24103, HttpStatus.NOT_FOUND)
      : { avatar_profile_url: avatarUrl };
  }
}
