import {
  Body,
  Controller,
  Get,
  HttpStatus,
  Param,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import { ClientRatingsService } from './ratings.service';
import { UUIDPipe } from 'src/shared/pipes/uuid.pipe';
import {
  ApiTags,
  ApiOperation,
  ApiResponse,
  ApiBearerAuth,
  ApiQuery,
} from '@nestjs/swagger';
import {
  LIST_ERROR_CODES,
  getError,
} from 'src/shared/constants/error/base.error';
import { CreateRatingDto } from 'src/module/core/ratings/dto/ratings.dto';
import { AuthResponse } from 'src/shared/decorators/response-auth/auth.decorator';
import { User } from 'src/module/core/users/entities/user.entity';
import { JwtUserAuthGuard } from 'src/module/core/auth/guards/jwt-auth.guard';
import { Paginate, PaginateQuery } from 'nestjs-paginate';
import { Public } from 'src/shared/decorators/public.decorator';
import { SortRatingTypes } from '../../core/ratings/enums/ratings.enum';
import { IQuery } from '../../core/ratings/interfaces/ratings.interface';

@ApiTags('Ratings')
@Controller('ratings')
@ApiBearerAuth()
@UseGuards(JwtUserAuthGuard)
export class ClientRatingsController {
  constructor(private readonly clientRatingsService: ClientRatingsService) {}

  @Get('/insurances/:id')
  @Public()
  @ApiQuery({
    example: 1,
    name: 'page',
    required: false,
  })
  @ApiQuery({
    example: 10,
    name: 'limit',
    required: false,
  })
  @ApiQuery({
    example: SortRatingTypes.Desc,
    enum: SortRatingTypes,
    name: 'sort_by',
    required: false,
  })
  @ApiOperation({ summary: 'Get rating by insurance' })
  async findAllByInsurance(
    @Paginate() query: PaginateQuery,
    @Param('id', new UUIDPipe()) id: string,
    @Query() { sort_by }: IQuery,
  ) {
    try {
      if (
        !!sort_by &&
        sort_by !== SortRatingTypes.Asc &&
        sort_by !== SortRatingTypes.Desc
      )
        return getError(LIST_ERROR_CODES.SS24429, HttpStatus.BAD_REQUEST);
      return await this.clientRatingsService.findAllByInsurance(
        query,
        id,
        sort_by,
      );
    } catch (error) {
      console.log(
        '🚀 ~ file: ratings.controller.ts:14 ~ ClientRatingsController ~ findAllByInsurance:',
        error,
      );

      return getError(LIST_ERROR_CODES.SS24001);
    }
  }

  @Post()
  @ApiOperation({ summary: 'Create Rating for insurance' })
  @ApiResponse({ status: 204, description: 'Create Rating Success' })
  async createRating(
    @AuthResponse() user: User,
    @Body() data: CreateRatingDto,
  ) {
    try {
      return await this.clientRatingsService.createRating(user, data);
    } catch (error) {
      console.log(
        '🚀 ~ file: ratings.controller.ts:58 ~ ClientRatingsController ~ createRating:',
        error,
      );

      return getError(LIST_ERROR_CODES.SS24001);
    }
  }

  // @Patch(':id')
  // @ApiOperation({ summary: 'Update Comment Rating' })
  // @ApiResponse({ status: 204, description: 'Update Rating Success' })
  // async updateComment(
  //   @Param('id', new UUIDPipe()) id: string,
  //   @Body() { comment }: UpdateCommentRatingDto,
  // ) {
  //   try {
  //     return await this.clientRatingsService.updateComment(id, comment);
  //   } catch (error) {
  //     console.log(
  //       '🚀 ~ file: ratings.controller.ts:95 ~ ClientRatingsController ~ updateComment:',
  //       error,
  //     );
  //     return getError(LIST_ERROR_CODES.SS24001);
  //   }
  // }

  // @Delete(':id')
  // @ApiOperation({ summary: 'Delete Rating' })
  // @ApiResponse({ status: 204, description: 'Delete Rating Success' })
  // async deleteRating(@Param('id', new UUIDPipe()) id: string) {
  //   try {
  //     return await this.clientRatingsService.deleteRating(id);
  //   } catch (error) {
  //     console.log(
  //       '🚀 ~ file: ratings.controller.ts:81 ~ ClientRatingsController ~ deleteRating:',
  //       error,
  //     );
  //     return getError(LIST_ERROR_CODES.SS24001);
  //   }
  // }
}
