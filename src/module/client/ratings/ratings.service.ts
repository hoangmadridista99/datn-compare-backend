import { HttpStatus, Injectable } from '@nestjs/common';
import { PaginateQuery } from 'nestjs-paginate';
import { InsurancesService } from 'src/module/core/insurances/insurances.service';
import { RatingsService } from 'src/module/core/ratings/ratings.service';
import { User } from 'src/module/core/users/entities/user.entity';
import {
  LIST_ERROR_CODES,
  getError,
} from 'src/shared/constants/error/base.error';
import { NO_CONTENT_SUCCESS_RESPONSE } from 'src/shared/lib/responses/response';
import { CreateRatingDto } from '../../core/ratings/dto/ratings.dto';
import { ISortByType } from '../../core/ratings/interfaces/ratings.interface';

@Injectable()
export class ClientRatingsService {
  constructor(
    private readonly ratingsService: RatingsService,
    private readonly insurancesService: InsurancesService,
  ) {}

  async findAllByInsurance(
    query: PaginateQuery,
    id: string,
    sortBy: ISortByType,
  ) {
    return (
      (await this.ratingsService.findAllByInsurance(query, id, sortBy)) ??
      getError(LIST_ERROR_CODES.SS24101, HttpStatus.NOT_FOUND)
    );
  }

  async createRating(user: User, data: CreateRatingDto) {
    const insurance = await this.insurancesService.findOneByQuery({
      id: data.insurance_id,
    });
    if (!insurance)
      return getError(LIST_ERROR_CODES.SS24604, HttpStatus.NOT_FOUND);

    const rating = await this.ratingsService.createRating(
      user,
      insurance,
      data,
    );
    return !rating
      ? getError(LIST_ERROR_CODES.SS24104, HttpStatus.INTERNAL_SERVER_ERROR)
      : NO_CONTENT_SUCCESS_RESPONSE;
  }
}
