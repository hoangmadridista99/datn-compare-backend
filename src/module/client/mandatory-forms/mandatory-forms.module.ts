import { Module } from '@nestjs/common';
import { MandatoryFormsModule } from '../../core/mandatory-forms/mandatory-forms.module';

@Module({
  imports: [MandatoryFormsModule],
  providers: [MandatoryFormsModule],
  exports: [MandatoryFormsModule],
})
export class ClientMandatoryFormsModule {}
