import { HttpStatus, Injectable } from '@nestjs/common';
import { MandatoryFormsService } from '../../core/mandatory-forms/mandatory-forms.service';
import { User } from '../../core/users/entities/user.entity';
import { ICreateMandatoryFormDto } from '../../core/mandatory-forms/interfaces/mandatory-form.interface';
import { NO_CONTENT_SUCCESS_RESPONSE } from '../../../shared/lib/responses/response';
import { PaginateQuery } from 'nestjs-paginate';
import {
  LIST_ERROR_CODES,
  getError,
} from '../../../shared/constants/error/base.error';

@Injectable()
export class ClientMandatoryFormsService {
  constructor(private readonly mandatoryFormsService: MandatoryFormsService) {}

  async createNewMandatoryForm(user: User, body: ICreateMandatoryFormDto) {
    await this.mandatoryFormsService.createNewMandatoryForm(user, body);
    return NO_CONTENT_SUCCESS_RESPONSE;
  }

  async getListMandatoryForms(
    query: PaginateQuery,
    userId: string,
    filter: any,
  ) {
    return await this.mandatoryFormsService.getMandatoryFormsList(
      query,
      userId,
      filter,
    );
  }

  async getMandatoryFormDetails(id: string) {
    return (
      (await this.mandatoryFormsService.getMandatoryFormDetails(id)) ??
      getError(LIST_ERROR_CODES.SS24101, HttpStatus.NOT_FOUND)
    );
  }
}
