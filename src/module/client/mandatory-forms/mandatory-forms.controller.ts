import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import {
  ApiTags,
  ApiBearerAuth,
  ApiOperation,
  ApiQuery,
} from '@nestjs/swagger';
import { JwtUserAuthGuard } from '../../core/auth/guards/jwt-auth.guard';
import { ClientMandatoryFormsService } from './mandatory-forms.service';
import { AuthResponse } from '../../../shared/decorators/response-auth/auth.decorator';
import { User } from '../../core/users/entities/user.entity';
import { CreateMandatoryFormDto } from '../../core/mandatory-forms/dto/mandatory-form.dto';
import {
  LIST_ERROR_CODES,
  getError,
} from '../../../shared/constants/error/base.error';
import { UUIDPipe } from '../../../shared/pipes/uuid.pipe';
import { Paginate, PaginateQuery } from 'nestjs-paginate';
import {
  LIMIT_QUERY,
  PAGE_QUERY,
} from '../../../shared/constants/swagger/constant';

@ApiTags('Mandatory-Forms')
@Controller('mandatory-forms')
@ApiBearerAuth()
@UseGuards(JwtUserAuthGuard)
export class ClientMandatoryFormsController {
  constructor(
    private readonly clientMandatoryFormsService: ClientMandatoryFormsService,
  ) {}

  @Post()
  @ApiOperation({ summary: 'Create new mandatory form for user' })
  async createNewMandatoryForm(
    @AuthResponse() user: User,
    @Body() body: CreateMandatoryFormDto,
  ) {
    try {
      return await this.clientMandatoryFormsService.createNewMandatoryForm(
        user,
        body,
      );
    } catch (error) {
      console.log(
        '🚀 ~ file: mandatory-forms.controller.ts:27 ~ ClientMandatoryFormsController ~ error:',
        error,
      );
      return getError(LIST_ERROR_CODES.SS24001);
    }
  }

  @Get(':id')
  @ApiOperation({ summary: 'Get mandatory form details ' })
  async getMandatoryFormDetails(@Param('id', new UUIDPipe()) id: string) {
    try {
      return await this.clientMandatoryFormsService.getMandatoryFormDetails(id);
    } catch (error) {
      console.log(
        '🚀 ~ file: mandatory-forms.controller.ts:49 ~ ClientMandatoryFormsController ~ getMandatoryFormDetails ~ error:',
        error,
      );
      return getError(LIST_ERROR_CODES.SS24001);
    }
  }

  @Get()
  @ApiQuery(PAGE_QUERY)
  @ApiQuery(LIMIT_QUERY)
  @ApiOperation({ summary: 'Get mandatory forms list for each user' })
  async getMandatoryFormsList(
    @AuthResponse() user: User,
    @Paginate() query: PaginateQuery,
    @Query() filter: any,
  ) {
    try {
      return await this.clientMandatoryFormsService.getListMandatoryForms(
        query,
        user.id,
        filter,
      );
    } catch (error) {
      console.log(
        '🚀 ~ file: mandatory-forms.controller.ts:67 ~ ClientMandatoryFormsController ~ error:',
        error,
      );
      return getError(LIST_ERROR_CODES.SS24001);
    }
  }
}
