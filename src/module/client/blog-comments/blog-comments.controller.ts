import {
  Body,
  Controller,
  Get,
  HttpStatus,
  Param,
  Post,
  UseGuards,
} from '@nestjs/common';
import {
  ApiTags,
  ApiBearerAuth,
  ApiOperation,
  ApiQuery,
} from '@nestjs/swagger';
import { JwtUserAuthGuard } from '../../core/auth/guards/jwt-auth.guard';
import { ClientBlogCommentsService } from './blog-comments.service';
import { CreateBlogCommentDto } from '../../core/blog-reviews/dto/blog-comment.dto';
import { AuthResponse } from '../../../shared/decorators/response-auth/auth.decorator';
import { User } from '../../core/users/entities/user.entity';
import {
  LIST_ERROR_CODES,
  getError,
} from '../../../shared/constants/error/base.error';
import { UUIDPipe } from '../../../shared/pipes/uuid.pipe';
import { BlogCommentsFilter } from '../../../shared/decorators/blog-comments/filter-blog-comments.decorator';
import { IFilterBlogComment } from '../../core/blog-reviews/interfaces/blog-comment.interface';
import { Paginate, PaginateQuery } from 'nestjs-paginate';
import { Public } from '../../../shared/decorators/public.decorator';
import {
  PAGE_QUERY,
  LIMIT_QUERY,
} from '../../../shared/constants/swagger/constant';
export const USER_ID_QUERY = {
  name: 'user_id',
  type: 'string',
  example: '',
  require: false,
};
export const BLOG_ID_QUERY = {
  name: 'blog_id',
  type: 'string',
  example: '',
  require: false,
};

@ApiTags('Blog-Comments')
@Controller('blog-comments')
@ApiBearerAuth()
@UseGuards(JwtUserAuthGuard)
export class ClientBlogCommentsController {
  constructor(
    private readonly clientBlogCommentsService: ClientBlogCommentsService,
  ) {}

  @Post()
  @ApiOperation({ summary: 'Create new comments' })
  async createNewComment(
    @Body() body: CreateBlogCommentDto,
    @AuthResponse() user: User,
  ) {
    try {
      return await this.clientBlogCommentsService.createNewComment(
        body,
        user.id,
      );
    } catch (error) {
      console.log(
        '🚀 ~ file: blog-comments.controller.ts:24 ~ ClientBlogCommentsController ~ createNewComment ~ error:',
        error,
      );
      return getError(LIST_ERROR_CODES.SS24001);
    }
  }

  @Public()
  @Get('list')
  @ApiQuery(PAGE_QUERY)
  @ApiQuery(LIMIT_QUERY)
  @ApiQuery(USER_ID_QUERY)
  @ApiQuery(BLOG_ID_QUERY)
  @ApiOperation({ summary: 'Get list comments' })
  async getListComment(
    @BlogCommentsFilter() filter: IFilterBlogComment,
    @Paginate() query: PaginateQuery,
  ) {
    try {
      return !filter
        ? getError(LIST_ERROR_CODES.SS24108, HttpStatus.BAD_REQUEST)
        : await this.clientBlogCommentsService.getListComments(query, filter);
    } catch (error) {
      console.log(
        '🚀 ~ file: blog-comments.controller.ts:87 ~ ClientBlogCommentsController ~ error:',
        error,
      );
      return getError(LIST_ERROR_CODES.SS24001);
    }
  }

  @Get(':id')
  @Public()
  @ApiOperation({ summary: 'Get detail comments' })
  async getDetailComment(@Param('id', new UUIDPipe()) id: string) {
    try {
      return this.clientBlogCommentsService.getOneById(id);
    } catch (error) {
      console.log(
        '🚀 ~ file: blog-comments.controller.ts:49 ~ ClientBlogCommentsController ~ getDetailComment ~ error:',
        error,
      );
      return getError(LIST_ERROR_CODES.SS24001);
    }
  }
}
