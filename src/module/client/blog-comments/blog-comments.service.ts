import { HttpStatus, Injectable } from '@nestjs/common';
import { BlogCommentsService } from '../../core/blog-reviews/blog-comments.service';
import {
  ICreateBlogComment,
  IFilterBlogComment,
} from '../../core/blog-reviews/interfaces/blog-comment.interface';
import {
  LIST_ERROR_CODES,
  getError,
} from '../../../shared/constants/error/base.error';
import { PaginateQuery } from 'nestjs-paginate';
import { NO_CONTENT_SUCCESS_RESPONSE } from '../../../shared/lib/responses/response';

@Injectable()
export class ClientBlogCommentsService {
  constructor(private readonly blogCommentsService: BlogCommentsService) {}

  async createNewComment(body: ICreateBlogComment, userId: string) {
    const result = this.blogCommentsService.createComment(body, userId);
    return !result
      ? getError(LIST_ERROR_CODES.SS24104, HttpStatus.INTERNAL_SERVER_ERROR)
      : NO_CONTENT_SUCCESS_RESPONSE;
  }

  async getOneById(id: string) {
    return (
      this.blogCommentsService.getOneById(id) ??
      getError(LIST_ERROR_CODES.SS24101, HttpStatus.NOT_FOUND)
    );
  }

  async getListComments(query: PaginateQuery, filter: IFilterBlogComment) {
    return await this.blogCommentsService.getListCommentsByBlogId(
      query,
      filter,
      false,
    );
  }
}
