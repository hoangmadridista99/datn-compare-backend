import { Controller, Get } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { ClientInsuranceObjectivesService } from './insurance-objectives.service';
import { Public } from '../../../shared/decorators/public.decorator';

@ApiTags('Insurance-Objectives')
@Controller('insurance-objectives')
export class ClientInsuranceObjectivesController {
  constructor(
    private readonly clientInsuranceObjectivesService: ClientInsuranceObjectivesService,
  ) {}

  @Get()
  @Public()
  async getAllObjective() {
    try {
      return await this.clientInsuranceObjectivesService.findAllObjective();
    } catch (error) {
      console.log(
        '🚀 ~ file: insurance-objectives.controller.ts:18 ~ ClientInsuranceObjectivesController ~ getAllObjective ~ error:',
        error,
      );
      throw error;
    }
  }
}
