import { Module } from '@nestjs/common';
import { InsuranceObjectivesModule } from '../../core/insurance-objectives/insurance-objectives.module';

@Module({
  imports: [InsuranceObjectivesModule],
  providers: [InsuranceObjectivesModule],
  exports: [InsuranceObjectivesModule],
})
export class ClientInsuranceObjectivesModule {}
