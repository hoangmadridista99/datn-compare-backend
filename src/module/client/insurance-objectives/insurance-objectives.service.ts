import { Injectable } from '@nestjs/common';
import { InsuranceObjectivesService } from '../../core/insurance-objectives/insurance-objectives.service';

@Injectable()
export class ClientInsuranceObjectivesService {
  constructor(
    private readonly insuranceObjectivesService: InsuranceObjectivesService,
  ) {}

  async findAllObjective() {
    return await this.insuranceObjectivesService.findAllInsuranceObjectives();
  }
}
