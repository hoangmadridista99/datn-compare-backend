import { Injectable } from '@nestjs/common';
import { CarInsuranceSettingsService } from '../../core/car-insurance-settings/car-insurance-settings.service';

@Injectable()
export class ClientCarInsuranceSettingsService {
  constructor(
    private readonly carInsuranceSettingsService: CarInsuranceSettingsService,
  ) {}

  async getMandatorySettingDetails() {
    return (
      (
        await this.carInsuranceSettingsService.getMandatorySettingDetails()
      )[0] ?? null
    );
  }
}
