import { Controller, Get } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { ClientCarInsuranceSettingsService } from './car-insurance-settings.service';
import {
  LIST_ERROR_CODES,
  getError,
} from '../../../shared/constants/error/base.error';

@ApiTags('Car-Insurance-Settings')
@Controller('car-insurance-settings')
export class ClientCarInsuranceSettingsController {
  constructor(
    private readonly clientCarInsuranceSettingsService: ClientCarInsuranceSettingsService,
  ) {}

  @Get('mandatory/details')
  @ApiOperation({ summary: 'Get mandatory setting details' })
  async getMandatorySettingDetails() {
    try {
      return await this.clientCarInsuranceSettingsService.getMandatorySettingDetails();
    } catch (error) {
      console.log(
        '🚀 ~ file: car-insurance-settings.controller.ts:18 ~ ClientCarInsuranceSettingsController ~ getMandatorySettingDetails ~ error:',
        error,
      );
      return getError(LIST_ERROR_CODES.SS24001);
    }
  }
}
