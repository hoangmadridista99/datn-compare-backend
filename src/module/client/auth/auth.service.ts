import { HttpStatus, Injectable } from '@nestjs/common';
import { UsersService } from 'src/module/core/users/users.service';
import { AuthService } from 'src/module/core/auth/auth.service';
import {
  Payload,
  ResponseAuthUser,
} from 'src/module/core/auth/models/auth.model';
import { RegisterUserDto } from 'src/module/core/auth/dto/auth.dto';
import { AuthInterface } from 'src/shared/interfaces/auth.interface';
import { BasicResponse } from 'src/shared/basic.response';
import { OtpsService } from 'src/module/core/otps/otps.service';
import {
  LIST_ERROR_CODES,
  getError,
} from 'src/shared/constants/error/base.error';
import { IUpdatePasswordDto } from '../../core/users/interfaces/users.interface';
import { User } from '../../core/users/entities/user.entity';
import { NO_CONTENT_SUCCESS_RESPONSE } from '../../../shared/lib/responses/response';

@Injectable()
export class ClientAuthService implements AuthInterface {
  constructor(
    private readonly usersService: UsersService,
    private readonly authService: AuthService,
    private readonly otpsService: OtpsService,
  ) {}

  async validateBasic(
    destination: string,
    password: string,
  ): Promise<BasicResponse | ResponseAuthUser> {
    try {
      const user = await this.authService.validateUser(destination, password);
      if (typeof user === 'string')
        return getError(user, HttpStatus.UNPROCESSABLE_ENTITY);

      const payload: Payload = {
        id: user.id,
        sub: user.id,
      };
      const accessToken = await this.authService.generateJwtToken(payload);
      const data: ResponseAuthUser = { ...user, accessToken };

      return data;
    } catch (error) {
      console.log(
        '🚀 ~ file: auth.service.ts:43 ~ ClientAuthService ~ validateBasic:',
        error,
      );
      throw error;
    }
  }

  async register(
    body: RegisterUserDto,
    file: Express.Multer.File,
  ): Promise<BasicResponse | ResponseAuthUser> {
    try {
      const checkOtp = await this.otpsService.checkOtpBeforeRegister(body);
      if (!checkOtp)
        return getError(
          LIST_ERROR_CODES.SS24505,
          HttpStatus.UNPROCESSABLE_ENTITY,
        );
      const checkBodyRegister = await this.authService.checkBodyRegister(
        body.email,
        body.phone,
      );
      if (typeof checkBodyRegister === 'string')
        return getError(checkBodyRegister, HttpStatus.UNPROCESSABLE_ENTITY);

      return await this.authService.handleRegister(body, file, false);
    } catch (error) {
      console.log(
        '🚀 ~ file: auth.service.ts:70 ~ ClientAuthService ~ error:',
        error,
      );
      throw error;
    }
  }

  async validateByToken(id: string) {
    try {
      const user = await this.usersService.findOneByUsersQuery({ id });
      if (!user) {
        return null;
      }
      return user;
    } catch (error) {
      console.log(
        '🚀 ~ file: auth.service.ts:86 ~ ClientAuthService ~ validateByToken ~ error:',
        error,
      );
      return null;
    }
  }

  async changePassword(user: User, body: IUpdatePasswordDto) {
    const isValidCurrentPassword = await this.authService.checkPassword(
      body.current_password,
      user.password,
    );
    if (!isValidCurrentPassword)
      return getError(LIST_ERROR_CODES.SS24321, HttpStatus.BAD_REQUEST);

    const newHashPassword = await this.authService.hashPassword(
      body.new_password,
    );
    const result = await this.usersService.updateUserById(user.id, {
      password: newHashPassword,
    });
    return !!result.affected
      ? getError(LIST_ERROR_CODES.SS24103, HttpStatus.BAD_REQUEST)
      : NO_CONTENT_SUCCESS_RESPONSE;
  }
}
