import {
  Body,
  Controller,
  HttpStatus,
  Post,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiBody,
  ApiConsumes,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import {
  CreateRegisterUserDto,
  LoginUserDto,
  RegisterUserDto,
} from 'src/module/core/auth/dto/auth.dto';
import { ResponseAuthUser } from 'src/module/core/auth/models/auth.model';
import { ClientAuthService } from './auth.service';
import { LocalUserAuthGuard } from '../../core/auth/guards/local/local.guard';
import { BasicResponse } from 'src/shared/basic.response';
import { User } from '../../core/users/entities/user.entity';
import { AuthResponse } from '../../../shared/decorators/response-auth/auth.decorator';
import {
  getError,
  LIST_ERROR_CODES,
} from '../../../shared/constants/error/base.error';
import { FileImageValidationPipe } from '../../../shared/pipes/file-image.pipe';
import { FileInterceptor } from '@nestjs/platform-express';
import { ChangePasswordClientDto } from '../../core/users/dto/update-user.dto';
import { JwtUserAuthGuard } from '../../core/auth/guards/jwt-auth.guard';

@ApiTags('Authentication')
@Controller('auth')
export class ClientAuthController {
  constructor(private readonly cliAuthService: ClientAuthService) {}

  @Post('/login')
  @UseGuards(LocalUserAuthGuard)
  @ApiBody({ type: LoginUserDto })
  @ApiOperation({ summary: 'Login for user' })
  async login(@AuthResponse() user: User): Promise<User> {
    return user;
  }

  @Post('/register')
  @ApiConsumes('multipart/form-data')
  @ApiBody({ type: CreateRegisterUserDto })
  @UseInterceptors(FileInterceptor('file'))
  @ApiOperation({ summary: 'Register for user' })
  async register(
    @UploadedFile(new FileImageValidationPipe()) file: Express.Multer.File,
    @Body() body: RegisterUserDto,
  ): Promise<BasicResponse | ResponseAuthUser> {
    try {
      if (body.confirmation_password !== body.password)
        return getError(LIST_ERROR_CODES.SS24108, HttpStatus.BAD_REQUEST);
      delete body.confirmation_password;
      return await this.cliAuthService.register(body, file);
    } catch (error) {
      throw getError(LIST_ERROR_CODES.SS24001);
    }
  }

  @Post('change-password')
  @ApiBearerAuth()
  @UseGuards(JwtUserAuthGuard)
  @ApiOperation({ summary: 'Change password for client' })
  async changePassword(
    @AuthResponse() user: User,
    @Body() { confirmation_password, ...body }: ChangePasswordClientDto,
  ) {
    try {
      if (confirmation_password !== body.new_password)
        return getError(LIST_ERROR_CODES.SS24108, HttpStatus.BAD_REQUEST);
      return await this.cliAuthService.changePassword(user, body);
    } catch (error) {
      console.log(
        '🚀 ~ file: user.controller.ts:86 ~ ClientUserController ~ error:',
        error,
      );
      throw getError(LIST_ERROR_CODES.SS24001);
    }
  }
}
