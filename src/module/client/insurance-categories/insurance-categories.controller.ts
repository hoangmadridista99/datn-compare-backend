import { Controller, Get } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { ClientInsuranceCategoriesService } from './insurance-categories.service';
import { Public } from '../../../shared/decorators/public.decorator';
import {
  LIST_ERROR_CODES,
  getError,
} from '../../../shared/constants/error/base.error';

@ApiTags('Insurance-Categories')
@Controller('insurance-categories')
export class ClientInsuranceCategoriesController {
  constructor(
    private readonly clientInsuranceCategoriesService: ClientInsuranceCategoriesService,
  ) {}

  @Public()
  @Get()
  async getListInsuranceCategories() {
    try {
      return this.clientInsuranceCategoriesService.getListCategories();
    } catch (error) {
      console.log(
        '🚀 ~ file: insurance-categories.controller.ts:19 ~ ClientInsuranceCategoriesController ~ getListInsuranceCategories ~ error:',
        error,
      );
      return getError(LIST_ERROR_CODES.SS24001);
    }
  }
}
