import { Injectable } from '@nestjs/common';
import { InsuranceCategoriesService } from '../../core/insurance-categories/insurance-categories.service';

@Injectable()
export class ClientInsuranceCategoriesService {
  constructor(
    private readonly insuranceCategoriesService: InsuranceCategoriesService,
  ) {}

  async getListCategories() {
    const isAdmin = false;
    return this.insuranceCategoriesService.getListCategories(isAdmin);
  }
}
