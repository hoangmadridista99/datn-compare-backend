import { Controller, Get, HttpStatus, Param } from '@nestjs/common';
import { ApiTags, ApiOperation, ApiQuery } from '@nestjs/swagger';
import { ClientInsurancesService } from './insurances.service';
import { UUIDPipe } from 'src/shared/pipes/uuid.pipe';
import { LifeInsurancesFilterClient } from '../../../shared/decorators/insurances/client/filter-life-insurances.decorator';
import {
  IHealthInsuranceFilterClient,
  IInsuranceFilterClient,
} from '../../core/insurances/interfaces/insurance.interface';
import {
  LIST_ERROR_CODES,
  getError,
} from '../../../shared/constants/error/base.error';

import { GetUserIdByTokenDecorator } from 'src/shared/decorators/get-user-id-by-token.decorator';
import { HealthInsurancesFilterClient } from '../../../shared/decorators/insurances/client/filter-health-insurances.decorator';
import {
  PAGE_QUERY,
  LIMIT_QUERY,
} from '../../../shared/constants/swagger/constant';
import {
  DEATH_OR_DISABILITY_QUERY,
  SERIOUS_ILLNESSES_QUERY,
  HEALTH_CARE_QUERY,
  INVESTMENT_BENEFIT_QUERY,
  INCREASING_VALUE_BONUS_QUERY,
  FOR_CHILD_QUERY,
  FLEXIBLE_AND_DIVERSE_QUERY,
  TERMINATION_BENEFITS_QUERY,
  EXPIRATION_BENEFITS_QUERY,
  FEE_EXEMPTION_QUERY,
  IS_SUGGESTION_QUERY,
  ORDER_BY_QUERY,
  IS_DENTAL_QUERY,
  IS_OBSTETRIC_QUERY,
  OBJECTIVE_QUERY,
  TOTAL_SUM_INSURED_QUERY,
  INSURED_PERSON_QUERY,
  YEAR_OF_BIRTH_QUERY,
  PROFESSION_QUERY,
  DEADLINE_FOR_DEAL_QUERY,
  ROOM_TYPE_QUERY,
} from '../../../shared/constants/swagger/insurances/insurances.constant';

@ApiTags('Insurances')
@Controller('insurances')
export class ClientInsurancesController {
  constructor(private insurancesService: ClientInsurancesService) {}

  @Get('life')
  @ApiQuery(PAGE_QUERY)
  @ApiQuery(LIMIT_QUERY)
  @ApiQuery(OBJECTIVE_QUERY)
  @ApiQuery(TOTAL_SUM_INSURED_QUERY)
  @ApiQuery(INSURED_PERSON_QUERY)
  @ApiQuery(YEAR_OF_BIRTH_QUERY)
  @ApiQuery(PROFESSION_QUERY)
  @ApiQuery(DEADLINE_FOR_DEAL_QUERY)
  @ApiQuery(DEATH_OR_DISABILITY_QUERY)
  @ApiQuery(SERIOUS_ILLNESSES_QUERY)
  @ApiQuery(HEALTH_CARE_QUERY)
  @ApiQuery(INVESTMENT_BENEFIT_QUERY)
  @ApiQuery(INCREASING_VALUE_BONUS_QUERY)
  @ApiQuery(FOR_CHILD_QUERY)
  @ApiQuery(FLEXIBLE_AND_DIVERSE_QUERY)
  @ApiQuery(TERMINATION_BENEFITS_QUERY)
  @ApiQuery(EXPIRATION_BENEFITS_QUERY)
  @ApiQuery(FEE_EXEMPTION_QUERY)
  @ApiQuery(IS_SUGGESTION_QUERY)
  @ApiQuery(ORDER_BY_QUERY)
  @ApiOperation({ summary: 'Filter life insurances' })
  async getLifeInsurancesFilter(
    @LifeInsurancesFilterClient() filter: IInsuranceFilterClient,
    @GetUserIdByTokenDecorator() userId?: string,
  ) {
    try {
      if (!filter)
        return getError(LIST_ERROR_CODES.SS24429, HttpStatus.BAD_REQUEST);
      return await this.insurancesService.findAllLifeInsurances(filter, userId);
    } catch (error) {
      throw error;
    }
  }

  @Get('health')
  @ApiQuery(PAGE_QUERY)
  @ApiQuery(LIMIT_QUERY)
  @ApiQuery(INSURED_PERSON_QUERY)
  @ApiQuery(YEAR_OF_BIRTH_QUERY)
  @ApiQuery(PROFESSION_QUERY)
  @ApiQuery(IS_DENTAL_QUERY)
  @ApiQuery(IS_OBSTETRIC_QUERY)
  @ApiQuery(ROOM_TYPE_QUERY)
  @ApiQuery(ORDER_BY_QUERY)
  async getHealthInsurancesFilter(
    @HealthInsurancesFilterClient() filter: IHealthInsuranceFilterClient,
    @GetUserIdByTokenDecorator() userId?: string,
  ) {
    try {
      if (!filter)
        return getError(LIST_ERROR_CODES.SS24429, HttpStatus.BAD_REQUEST);
      return await this.insurancesService.findAllHealthInsurances(
        filter,
        userId,
      );
    } catch (error) {
      throw error;
    }
  }

  @Get(':id')
  @ApiOperation({ summary: 'Get insurance' })
  async findOne(@Param('id', new UUIDPipe()) id: string) {
    try {
      return await this.insurancesService.findById(id);
    } catch (error) {
      throw error;
    }
  }
}
