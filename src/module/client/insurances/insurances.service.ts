import { HttpStatus, Injectable } from '@nestjs/common';
import {
  getError,
  LIST_ERROR_CODES,
} from '../../../shared/constants/error/base.error';
import { InsurancesService } from '../../core/insurances/insurances.service';
import {
  IHealthInsuranceFilterClient,
  IInsuranceFilterClient,
} from '../../core/insurances/interfaces/insurance.interface';
import { Insurance } from 'src/module/core/insurances/entities/insurance.entity';
import { getPaginationMetaData } from '../../../shared/helper/system.helper';
import { UserInsurancesService } from 'src/module/core/user-insurances/user-insurances.service';
import { HealthInsurancesService } from '../../core/insurances/insurance-types/health/health-insurances.service';
import { LifeInsurancesService } from '../../core/insurances/insurance-types/life/life-insurances.service';

@Injectable()
export class ClientInsurancesService {
  constructor(
    private readonly insurancesService: InsurancesService,
    private readonly userInsurancesService: UserInsurancesService,
    private readonly healthInsurancesService: HealthInsurancesService,
    private readonly lifeInsurancesService: LifeInsurancesService,
  ) {}

  async findAllLifeInsurances(filter: IInsuranceFilterClient, userId?: string) {
    let insuredSuggestion: Insurance | null = null;
    const [data, totalItems] =
      await this.lifeInsurancesService.getListLifeInsurancesByFilter(filter);
    const meta = getPaginationMetaData(totalItems, filter.page, filter.limit);
    const isListInsurancesWithoutSuggestion =
      filter.page !== 1 ||
      filter.is_suggestion ||
      data.length <= 2 ||
      !!filter.order_by;
    if (!isListInsurancesWithoutSuggestion) {
      insuredSuggestion =
        await this.lifeInsurancesService.getLifeInsuredSuggestionFilterByClient(
          filter,
        );
    }
    const listInsurances = [
      ...(insuredSuggestion ? [insuredSuggestion] : []),
      ...data,
    ];
    const listInsurancesHaveFieldIsSave =
      await this.userInsurancesService.addIsSavedFields(listInsurances, userId);
    if (isListInsurancesWithoutSuggestion)
      return {
        meta,
        data: listInsurancesHaveFieldIsSave,
      };
    const [insured_suggestion, ...dataListInsuranceHaveFieldIsSave] =
      listInsurancesHaveFieldIsSave;
    return {
      insured_suggestion,
      objective_suggestion:
        listInsurancesHaveFieldIsSave[
          listInsurancesHaveFieldIsSave[0].id === insured_suggestion.id ? 1 : 0
        ],
      meta,
      data: insuredSuggestion
        ? dataListInsuranceHaveFieldIsSave
        : listInsurancesHaveFieldIsSave,
    };
  }

  async findById(id: string) {
    const result = await this.insurancesService.findOneByQuery({ id });
    return result ?? getError(LIST_ERROR_CODES.SS24101, HttpStatus.NOT_FOUND);
  }

  async findAllHealthInsurances(
    filter: IHealthInsuranceFilterClient,
    userId?: string,
  ) {
    const [data, totalItems] =
      await this.healthInsurancesService.getListHealthInsurancesByFilter(
        filter,
      );
    const listInsurancesHaveFieldIsSave =
      await this.userInsurancesService.addIsSavedFields(data, userId);
    const meta = getPaginationMetaData(totalItems, filter.page, filter.limit);
    return {
      data: listInsurancesHaveFieldIsSave,
      meta,
    };
  }
}
