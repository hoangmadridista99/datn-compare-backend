import { Module } from '@nestjs/common';
import { ClientAuthController } from './auth/auth.controller';
import { ClientAuthModule } from './auth/auth.module';
import { ClientUserController } from './users/user.controller';
import { ClientUserModule } from './users/user.module';
import { ClientInsurancesModule } from './insurances/insurances.module';
import { ClientInsurancesController } from './insurances/insurances.controller';
import { ClientOtpsModule } from './otps/opts.module';
import { ClientOtpsController } from './otps/otps.controller';
import { ClientRatingsModule } from './ratings/ratings.module';
import { ClientRatingsController } from './ratings/ratings.controller';
import { ClientCompaniesModule } from './companies/companies.module';
import { ClientCompaniesController } from './companies/companies.controller';
import { ClientFileModule } from './file/file.module';
import { ClientFileController } from './file/file.controller';
import { ClientInsuranceObjectivesModule } from './insurance-objectives/insurance-objectives.module';
import { ClientInsuranceObjectivesController } from './insurance-objectives/insurance-objectives.controller';
import { ClientBlogCommentsModule } from './blog-comments/blog-comments.module';
import { ClientBlogCommentsController } from './blog-comments/blog-comments.controller';
import { ClientBlogCategoriesController } from './blog-categories/blog-categories.controller';
import { ClientBlogCategoriesModule } from './blog-categories/blog-categories.module';
import { ClientBlogsController } from './blogs/blogs.controller';
import { ClientBlogsModule } from './blogs/blogs.module';
import { ClientUserInsurancesModule } from './user-insurances/user-insurances.module';
import { ClientUserInsurancesController } from './user-insurances/user-insurances.controller';
import { ClientInsuranceCategoriesController } from './insurance-categories/insurance-categories.controller';
import { ClientInsuranceCategoriesModule } from './insurance-categories/insurance-categories.module';
import { ClientAppraisalFormsController } from './appraisal-forms/appraisal-forms.controller';
import { ClientAppraisalFormsModule } from './appraisal-forms/appraisal-forms.module';
import { ClientMandatoryFormsController } from './mandatory-forms/mandatory-forms.controller';
import { ClientMandatoryFormsModule } from './mandatory-forms/mandatory-forms.module';
import { ClientCarInsurancesController } from './car-insurances/car-insurances.controller';
import { ClientCarInsurancesModule } from './car-insurances/car-insurances.module';
import { ClientCarInsuranceSettingsModule } from './car-insurance-settings/car-insurance-settings.module';
import { ClientCarInsuranceSettingsController } from './car-insurance-settings/car-insurance-settings.controller';
import { ClientCarInsuranceRatingsController } from './car-insurance-ratings/car-insurance-ratings.controller';
import { ClientCarInsuranceRatingsModule } from './car-insurance-ratings/car-insurance-ratings.module';

@Module({
  imports: [
    ClientAuthModule,
    ClientUserModule,
    ClientInsurancesModule,
    ClientOtpsModule,
    ClientRatingsModule,
    ClientCompaniesModule,
    ClientFileModule,
    ClientInsuranceObjectivesModule,
    ClientBlogCommentsModule,
    ClientBlogCategoriesModule,
    ClientBlogsModule,
    ClientUserInsurancesModule,
    ClientInsuranceCategoriesModule,
    ClientAppraisalFormsModule,
    ClientMandatoryFormsModule,
    ClientCarInsurancesModule,
    ClientCarInsuranceSettingsModule,
    ClientCarInsuranceRatingsModule,
  ],
  controllers: [
    ClientAuthController,
    ClientUserController,
    ClientInsurancesController,
    ClientOtpsController,
    ClientRatingsController,
    ClientCompaniesController,
    ClientFileController,
    ClientInsuranceObjectivesController,
    ClientBlogCommentsController,
    ClientBlogCategoriesController,
    ClientBlogsController,
    ClientUserInsurancesController,
    ClientInsuranceCategoriesController,
    ClientAppraisalFormsController,
    ClientMandatoryFormsController,
    ClientCarInsurancesController,
    ClientCarInsuranceSettingsController,
    ClientCarInsuranceRatingsController,
  ],
  exports: [],
})
export class ClientModule {}
