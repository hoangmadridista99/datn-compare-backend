import { Module } from '@nestjs/common';
import { UserInsurancesModule } from 'src/module/core/user-insurances/user-insurances.module';

@Module({
  imports: [UserInsurancesModule],
  providers: [UserInsurancesModule],
  exports: [UserInsurancesModule],
})
export class ClientUserInsurancesModule {}
