import { HttpStatus, Injectable } from '@nestjs/common';
import { UserInsurancesService } from 'src/module/core/user-insurances/user-insurances.service';
import { InsurancesService } from 'src/module/core/insurances/insurances.service';
import { User } from 'src/module/core/users/entities/user.entity';
import { NO_CONTENT_SUCCESS_RESPONSE } from 'src/shared/lib/responses/response';
import { PaginateQuery } from 'nestjs-paginate';
import {
  LIST_ERROR_CODES,
  getError,
} from '../../../shared/constants/error/base.error';
import { InsuranceRepositoryService } from 'src/module/core/insurances/repository';
import { IInsuranceCategoriesTypes } from '../../core/insurance-categories/interfaces/insurance-categories.interface';

@Injectable()
export class ClientUserInsurancesService {
  constructor(
    private readonly userInsurancesService: UserInsurancesService,
    private readonly insuranceService: InsurancesService,
    private readonly insuranceRepositoryService: InsuranceRepositoryService,
  ) {}

  async getListByUserId(
    query: PaginateQuery,
    userId: string,
    name?: string,
    categoryType?: IInsuranceCategoriesTypes,
  ) {
    try {
      return await this.userInsurancesService.getListByUserId(
        query,
        userId,
        name,
        categoryType,
      );
    } catch (error) {
      console.log(
        '🚀 ~ file: user-insurances.service.ts:22 ~ ClientUserInsurancesService ~ getListByUserId ~ error:',
        error,
      );
      throw error;
    }
  }

  async actionSaveByInsuranceId(user: User, insuranceId: string) {
    try {
      const insurance = await this.insuranceService.findOneByQuery({
        id: insuranceId,
      });
      if (!insurance)
        return getError(LIST_ERROR_CODES.SS24101, HttpStatus.NOT_FOUND);
      const operation =
        await this.userInsurancesService.actionSaveByInsuranceId(
          user,
          insurance,
        );
      await this.insuranceRepositoryService.updateTotalInterest(
        insuranceId,
        operation,
      );
      return NO_CONTENT_SUCCESS_RESPONSE;
    } catch (error) {
      console.log(
        '🚀 ~ file: user-insurances.service.ts:22 ~ ClientUserInsurancesService ~ actionSaveByInsuranceId ~ error:',
        error,
      );
      throw error;
    }
  }
}
