import { Controller, Post, UseGuards, Body, Query, Get } from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiBody,
  ApiOperation,
  ApiQuery,
  ApiTags,
} from '@nestjs/swagger';
import { CreateUserInsuranceDto } from 'src/module/core/user-insurances/dto/user-insurances.dto';
import { JwtUserAuthGuard } from 'src/module/core/auth/guards/jwt-auth.guard';
import { AuthResponse } from 'src/shared/decorators/response-auth/auth.decorator';
import { ClientUserInsurancesService } from './user-insurances.service';
import { User } from 'src/module/core/users/entities/user.entity';
import {
  LIST_ERROR_CODES,
  getError,
} from 'src/shared/constants/error/base.error';
import { Paginate, PaginateQuery } from 'nestjs-paginate';
import { IInsuranceCategoriesTypes } from '../../core/insurance-categories/interfaces/insurance-categories.interface';
import {
  PAGE_QUERY,
  LIMIT_QUERY,
} from '../../../shared/constants/swagger/constant';

@Controller('user-insurances')
@ApiTags('User-Insurances')
@UseGuards(JwtUserAuthGuard)
@ApiBearerAuth()
export class ClientUserInsurancesController {
  constructor(
    private readonly clientUserInsurancesService: ClientUserInsurancesService,
  ) {}

  @Get()
  @ApiQuery(PAGE_QUERY)
  @ApiQuery(LIMIT_QUERY)
  @ApiQuery({
    required: false,
    name: 'name',
  })
  @ApiQuery({
    required: false,
    name: 'category_type',
  })
  async getListByUserId(
    @AuthResponse() user: User,
    @Paginate() query: PaginateQuery,
    @Query('name') name?: string,
    @Query('category_type') type?: IInsuranceCategoriesTypes,
  ) {
    try {
      return this.clientUserInsurancesService.getListByUserId(
        query,
        user.id,
        name,
        type,
      );
    } catch (error) {
      throw getError(LIST_ERROR_CODES.SS24001);
    }
  }

  @Post()
  @ApiBody({ type: CreateUserInsuranceDto })
  @ApiOperation({
    summary: 'Action save or unsaved insurances by insurance id',
  })
  async actionSaveOrUnsavedByInsuranceId(
    @AuthResponse() user: User,
    @Body() { insuranceId }: CreateUserInsuranceDto,
  ) {
    try {
      return await this.clientUserInsurancesService.actionSaveByInsuranceId(
        user,
        insuranceId,
      );
    } catch (error) {
      throw getError(LIST_ERROR_CODES.SS24001);
    }
  }
}
