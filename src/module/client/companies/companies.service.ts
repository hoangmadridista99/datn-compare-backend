import { Injectable } from '@nestjs/common';
import { CompaniesService } from 'src/module/core/companies/companies.service';
import {
  LIST_ERROR_CODES,
  getError,
} from 'src/shared/constants/error/base.error';

@Injectable()
export class ClientCompaniesService {
  constructor(private readonly companiesService: CompaniesService) {}

  async getListSelect() {
    try {
      return await this.companiesService.findAllByQuery({
        select: ['id', 'long_name', 'short_name'],
      });
    } catch (error) {
      console.log(
        '🚀 ~ file: companies.service.ts:13 ~ ClientCompaniesService ~ getListSelect:',
        error,
      );

      return getError(LIST_ERROR_CODES.SS24001, 500);
    }
  }
}
