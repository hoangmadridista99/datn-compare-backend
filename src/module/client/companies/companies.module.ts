import { Module } from '@nestjs/common';
import { CompaniesModule } from 'src/module/core/companies/companies.module';

@Module({
  imports: [CompaniesModule],
  providers: [CompaniesModule],
  exports: [CompaniesModule],
})
export class ClientCompaniesModule {}
