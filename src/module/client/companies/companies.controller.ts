import { Controller, Get } from '@nestjs/common';
import { ApiTags, ApiOperation } from '@nestjs/swagger';
import { ClientCompaniesService } from './companies.service';

@Controller('companies')
@ApiTags('Companies')
export class ClientCompaniesController {
  constructor(
    private readonly clientCompaniesService: ClientCompaniesService,
  ) {}

  @Get()
  @ApiOperation({ summary: 'Get list select company' })
  async getListSelect() {
    try {
      return await this.clientCompaniesService.getListSelect();
    } catch (error) {
      console.log(
        '🚀 ~ file: companies.controller.ts:18 ~ ClientCompaniesController ~ getListSelect:',
        error,
      );
      throw error;
    }
  }
}
