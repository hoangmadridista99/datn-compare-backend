import { Injectable } from '@nestjs/common/decorators/core/injectable.decorator';
import {
  getError,
  LIST_ERROR_CODES,
} from '../../../shared/constants/error/base.error';
import { NO_CONTENT_SUCCESS_RESPONSE } from '../../../shared/lib/responses/response';
import { OtpsService } from '../../core/otps/otps.service';
import { UsersService } from '../../core/users/users.service';
import { HttpStatus } from '@nestjs/common';
import { IOtpDTO } from '../../core/otps/interfaces/otp.interface';
@Injectable()
export class ClientOtpsService {
  constructor(
    private readonly otpsService: OtpsService,
    private readonly usersService: UsersService,
  ) {}

  async sendOtpForRegister(body: IOtpDTO) {
    try {
      const {
        destination_type,
        type,
        phone,
        email,
        numberOfSendingOtp,
        destination,
      } = body;

      const resultValidateBody =
        await this.usersService.validateEmailAndPhoneInBody(email, phone);
      if (typeof resultValidateBody === 'string')
        return getError(resultValidateBody, HttpStatus.UNPROCESSABLE_ENTITY);

      const code = await this.otpsService.sendOtp(
        destination,
        destination_type,
      );
      if (!code)
        return getError(LIST_ERROR_CODES.SS24501, HttpStatus.BAD_REQUEST);

      await this.otpsService.createOtp(
        code,
        destination,
        type,
        destination_type,
        numberOfSendingOtp,
      );
      return NO_CONTENT_SUCCESS_RESPONSE;
    } catch (error) {
      console.log(
        '🚀 ~ file: otps.service.ts:43 ~ ClientOtpsService ~ sendOtp ~ error:',
        error,
      );
      return getError(LIST_ERROR_CODES.SS24001);
    }
  }
}
