import { Body, Controller, HttpCode, Post } from '@nestjs/common';
import { ApiBody, ApiTags } from '@nestjs/swagger';
import {
  getError,
  LIST_ERROR_CODES,
} from '../../../shared/constants/error/base.error';
import { ClientOtpsService } from './otps.service';
import { IOtpDTO } from '../../core/otps/interfaces/otp.interface';
import { SendOtpDto } from 'src/module/core/otps/dto/otp.dto';

@ApiTags('Otps')
@Controller('otps')
export class ClientOtpsController {
  constructor(private readonly clientOtpsService: ClientOtpsService) {}

  @Post('send-otp')
  @ApiBody({ type: SendOtpDto })
  @HttpCode(204)
  async sendOtp(@Body() body: IOtpDTO) {
    try {
      return await this.clientOtpsService.sendOtpForRegister(body);
    } catch (error) {
      console.log(
        '🚀 ~ file: otps.controller.ts:17 ~ ClientOtpsController ~ sendOtp ~ error:',
        error,
      );
      throw getError(LIST_ERROR_CODES.SS24001);
    }
  }
}
