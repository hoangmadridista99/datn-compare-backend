import { Injectable } from '@nestjs/common';
import { CarInsurancesService } from '../../core/car-insurances/car-insurances.service';
import { PaginateQuery } from 'nestjs-paginate';
import { ICarInsurancesFilter } from '../../core/car-insurances/interfaces/car-insurance.interface';

@Injectable()
export class ClientCarInsurancesService {
  constructor(private readonly carInsurancesService: CarInsurancesService) {}

  async getCarInsurancesList(
    query: PaginateQuery,
    filter: ICarInsurancesFilter,
  ) {
    return await this.carInsurancesService.getCarInsurancesList(query, filter);
  }
}
