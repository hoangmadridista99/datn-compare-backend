import { Controller, Get, Query } from '@nestjs/common';
import { ApiTags, ApiQuery, ApiOperation } from '@nestjs/swagger';
import { Paginate, PaginateQuery } from 'nestjs-paginate';
import {
  getError,
  LIST_ERROR_CODES,
} from '../../../shared/constants/error/base.error';
import { CAR_INSURANCE_TYPE_QUERY } from '../../../shared/constants/swagger/car-insurances/car-insurances.constant';
import {
  PAGE_QUERY,
  LIMIT_QUERY,
} from '../../../shared/constants/swagger/constant';
import { CarInsuranceFilterDto } from '../../core/car-insurances/dto/car-insurance.dto';
import { ClientCarInsurancesService } from './car-insurances.service';

@ApiTags('Car-Insurances')
@Controller('car-insurances')
export class ClientCarInsurancesController {
  constructor(
    private readonly clientCarInsurancesService: ClientCarInsurancesService,
  ) {}

  @Get()
  @ApiQuery(PAGE_QUERY)
  @ApiQuery(LIMIT_QUERY)
  @ApiQuery(CAR_INSURANCE_TYPE_QUERY)
  @ApiOperation({ summary: 'Filter car insurances list' })
  async getCarInsurancesList(
    @Paginate() query: PaginateQuery,
    @Query() filter: CarInsuranceFilterDto,
  ) {
    try {
      return await this.clientCarInsurancesService.getCarInsurancesList(
        query,
        filter,
      );
    } catch (error) {
      console.log(
        '🚀 ~ file: car-insurances.controller.ts:77 ~ OperatorCarInsurancesController ~ getCarInsurancesList ~ error:',
        error,
      );
      return getError(LIST_ERROR_CODES.SS24001);
    }
  }
}
