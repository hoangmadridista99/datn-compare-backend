import { DataSource, DataSourceOptions } from 'typeorm';
import { ENTITIES, ENV_CONFIG } from './shared/constants/env.constant';

export const dataSourceOption: DataSourceOptions = {
  ...ENV_CONFIG.database,
  type: 'postgres',
  entities: [...ENTITIES],
  migrations: ['dist/migrations/*.js', 'src/migration/*.ts'],
  synchronize: true,
};

const dataSource = new DataSource(dataSourceOption);

export default dataSource;
