import { PipeTransform, HttpException, HttpStatus } from '@nestjs/common';
import { LIST_ERROR_CODES, getError } from '../constants/error/base.error';
const SIZE_SUPPORTED = 16 * 1024 * 1000;
const MIME_TYPE_SUPPORTED = 'image';

export class FileImageValidationPipe implements PipeTransform<string, string> {
  transform(value: any) {
    if (!value) return null;
    const isMimeType = (value?.mimetype as string)?.includes(
      MIME_TYPE_SUPPORTED,
    );

    const isSize = (value?.size as number) < SIZE_SUPPORTED;
    if (!isMimeType)
      throw new HttpException(
        getError(LIST_ERROR_CODES.SS24107),
        HttpStatus.UNSUPPORTED_MEDIA_TYPE,
      );
    if (!isSize)
      throw new HttpException(
        getError(LIST_ERROR_CODES.SS24106),
        HttpStatus.BAD_REQUEST,
      );
    return value;
  }
}

export class ImagesValidationPipe
  implements PipeTransform<Array<string>, Array<string> | null>
{
  transform(images: Array<any>) {
    try {
      const minCount = 1;
      const maxCount = 5;
      if (
        !images.length ||
        (images.length !== minCount && images.length !== maxCount)
      )
        throw new HttpException(
          getError(LIST_ERROR_CODES.SS24109),
          HttpStatus.BAD_REQUEST,
        );
      images.forEach((image) => {
        const isMimeType = (image?.mimetype as string)?.includes(
          MIME_TYPE_SUPPORTED,
        );
        if (!isMimeType)
          throw new HttpException(
            getError(LIST_ERROR_CODES.SS24107),
            HttpStatus.UNSUPPORTED_MEDIA_TYPE,
          );
        const isSize = (image?.size as number) < SIZE_SUPPORTED;
        if (!isSize)
          throw new HttpException(
            getError(LIST_ERROR_CODES.SS24106),
            HttpStatus.BAD_REQUEST,
          );
      });
      return images;
    } catch (error) {
      console.log(
        '🚀 ~ file: file-image.pipe.ts:72 ~ transform ~ error:',
        error,
      );
      throw new HttpException(
        getError(LIST_ERROR_CODES.SS24105),
        HttpStatus.BAD_REQUEST,
      );
    }
  }
}

export class UpdateImagesValidationPipe
  implements PipeTransform<Array<string>, Array<string> | null>
{
  transform(images: Array<any>) {
    try {
      if (!images?.length) return;
      const maxCount = 5;
      if (images.length > maxCount)
        throw new HttpException(
          getError(LIST_ERROR_CODES.SS24109),
          HttpStatus.BAD_REQUEST,
        );
      images.forEach((image) => {
        const isMimeType = (image?.mimetype as string)?.includes(
          MIME_TYPE_SUPPORTED,
        );
        if (!isMimeType)
          throw new HttpException(
            getError(LIST_ERROR_CODES.SS24107),
            HttpStatus.UNSUPPORTED_MEDIA_TYPE,
          );
        const isSize = (image?.size as number) < SIZE_SUPPORTED;
        if (!isSize)
          throw new HttpException(
            getError(LIST_ERROR_CODES.SS24106),
            HttpStatus.BAD_REQUEST,
          );
      });
      return images;
    } catch (error) {
      console.log(
        '🚀 ~ file: file-image.pipe.ts:72 ~ transform ~ error:',
        error,
      );
      throw new HttpException(
        getError(LIST_ERROR_CODES.SS24105),
        HttpStatus.BAD_REQUEST,
      );
    }
  }
}
