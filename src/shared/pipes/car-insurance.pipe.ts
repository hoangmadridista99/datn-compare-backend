import { HttpException, HttpStatus, PipeTransform } from '@nestjs/common';
import { plainToClass } from 'class-transformer';
import { validate } from 'class-validator';
import { LIST_ERROR_CODES, getError } from '../constants/error/base.error';
import {
  ICreateCarInsuranceDto,
  IUpdateCarInsuranceDto,
} from '../../module/core/car-insurances/interfaces/car-insurance.interface';
import {
  CreateCarInsuranceDto,
  UpdateCarInsuranceDto,
} from '../../module/core/car-insurances/dto/car-insurance.dto';
import { getValidateOption } from '../helper/system.helper';

export class CreateCarInsuranceDtoPipe
  implements
    PipeTransform<ICreateCarInsuranceDto, Promise<ICreateCarInsuranceDto>>
{
  async transform(body: ICreateCarInsuranceDto) {
    Object.keys(body).forEach((key) => {
      if (typeof body[key] === 'string') {
        try {
          body[key] = JSON.parse(body[key]);
        } catch (error) {}
      }
    });
    const classObject = plainToClass(CreateCarInsuranceDto, body);
    const errors = await validate(classObject, getValidateOption(true));
    if (!!errors.length)
      throw new HttpException(
        getError(LIST_ERROR_CODES.SS24108),
        HttpStatus.BAD_REQUEST,
      );
    return body;
  }
}

export class UpdateCarInsuranceDtoPipe
  implements
    PipeTransform<IUpdateCarInsuranceDto, Promise<IUpdateCarInsuranceDto>>
{
  async transform(body: IUpdateCarInsuranceDto) {
    Object.keys(body).forEach((key) => {
      if (typeof body[key] === 'string') {
        try {
          body[key] = JSON.parse(body[key]);
        } catch (error) {}
      }
    });
    const classObject = plainToClass(UpdateCarInsuranceDto, body);
    const errors = await validate(classObject, getValidateOption(true));

    if (!!errors.length)
      throw new HttpException(
        getError(LIST_ERROR_CODES.SS24108),
        HttpStatus.BAD_REQUEST,
      );

    return body;
  }
}
