import {
  HttpException,
  HttpStatus,
  Injectable,
  PipeTransform,
} from '@nestjs/common';
import { getError, LIST_ERROR_CODES } from '../constants/error/base.error';

@Injectable()
export class RegisterVendorValidateCompanyPipe
  implements PipeTransform<string, string>
{
  transform(value: any): string {
    if (!value?.company_id && !value?.company_other) {
      throw new HttpException(
        getError(LIST_ERROR_CODES.SS24318),
        HttpStatus.BAD_REQUEST,
      );
    }
    return value;
  }
}
