import {
  Injectable,
  PipeTransform,
  HttpException,
  HttpStatus,
  ArgumentMetadata,
} from '@nestjs/common';
import { isUUID } from 'class-validator';
import { getError, LIST_ERROR_CODES } from '../constants/error/base.error';

@Injectable()
export class UUIDPipe implements PipeTransform<string, string> {
  transform(value: string, metadata: ArgumentMetadata): string {
    if (metadata.data === 'blog_category_id') {
      if (!!value && !isUUID(value)) {
        throw new HttpException(
          getError(LIST_ERROR_CODES.SS24207),
          HttpStatus.BAD_REQUEST,
        );
      }
      return value;
    }
    if (!isUUID(value)) {
      throw new HttpException(
        getError(LIST_ERROR_CODES.SS24207),
        HttpStatus.BAD_REQUEST,
      );
    }
    return value;
  }
}
