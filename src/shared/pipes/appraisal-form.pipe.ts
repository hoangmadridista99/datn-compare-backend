import { HttpException, HttpStatus, PipeTransform } from '@nestjs/common';
import { plainToClass } from 'class-transformer';
import {
  ICreateAppraisalFromDto,
  IUpdateAppraisalFormDtoPipe,
} from '../../module/core/appraisal-form/interfaces/appraisal-form.interface';
import {
  AppraisalFormDto,
  UpdateAppraisalFormDto,
} from '../../module/core/appraisal-form/dto/appraisal-form.dto';
import { validate } from 'class-validator';
import { LIST_ERROR_CODES, getError } from '../constants/error/base.error';

export class AppraisalFormDtoPipe
  implements
    PipeTransform<ICreateAppraisalFromDto, Promise<ICreateAppraisalFromDto>>
{
  async transform(body: ICreateAppraisalFromDto) {
    const classObject = plainToClass(AppraisalFormDto, body);
    const errors = await validate(classObject);
    if (!!errors.length)
      throw new HttpException(
        getError(LIST_ERROR_CODES.SS24108),
        HttpStatus.BAD_REQUEST,
      );
    return body;
  }
}

export class UpdateAppraisalFormDtoPipe
  implements
    PipeTransform<
      IUpdateAppraisalFormDtoPipe,
      Promise<IUpdateAppraisalFormDtoPipe>
    >
{
  async transform(body: IUpdateAppraisalFormDtoPipe) {
    if (body.update_image_urls) {
      try {
        body.update_image_urls = JSON.parse(body.update_image_urls);
      } catch (error) {}
    }
    const classObject = plainToClass(UpdateAppraisalFormDto, body);
    const errors = await validate(classObject);
    if (!!errors.length)
      throw new HttpException(
        getError(LIST_ERROR_CODES.SS24108),
        HttpStatus.BAD_REQUEST,
      );
    return body;
  }
}
