import {
  PipeTransform,
  Injectable,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { LIST_ERROR_CODES, getError } from '../constants/error/base.error';

@Injectable()
export class FilePdfValidationPipe implements PipeTransform<string, string> {
  transform(value: any) {
    const SIZE_SUPPORTED = 10 * 1024 * 1000;
    const MIME_TYPE_SUPPORTED = 'application/pdf';
    const isMimeType = value?.mimetype === MIME_TYPE_SUPPORTED;
    const isSize = (value?.size as number) < SIZE_SUPPORTED;

    if (!isMimeType)
      throw new HttpException(
        getError(LIST_ERROR_CODES.SS24107),
        HttpStatus.UNSUPPORTED_MEDIA_TYPE,
      );
    if (!isSize)
      throw new HttpException(
        getError(LIST_ERROR_CODES.SS24106),
        HttpStatus.BAD_REQUEST,
      );

    return value;
  }
}
