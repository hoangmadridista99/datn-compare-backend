export interface IGetError {
  error_code: string;
  status?: number;
}
