import {
  RegisterManagerDto,
  RegisterUserDto,
} from 'src/module/core/auth/dto/auth.dto';
import { ResponseAuthUser } from 'src/module/core/auth/models/auth.model';
import { BasicResponse } from 'src/shared/basic.response';

import { User } from '../../module/core/users/entities/user.entity';

export interface AuthInterface {
  validateBasic(
    phone: string,
    password: string,
  ): Promise<ResponseAuthUser | BasicResponse>;

  register(
    body: RegisterManagerDto | RegisterUserDto,
    file: Express.Multer.File,
  ): Promise<ResponseAuthUser | BasicResponse>;

  validateByToken(id: string): Promise<User>;
}
