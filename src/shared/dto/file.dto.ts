import { ApiProperty } from '@nestjs/swagger';

export class BaseUploadFileDto {
  @ApiProperty({ type: 'string', format: 'binary', required: true })
  file: any;
}
