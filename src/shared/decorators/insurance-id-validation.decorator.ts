import { Injectable } from '@nestjs/common';
import {
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';
import { LIST_ERROR_CODES } from '../constants/error/base.error';
import { InsurancesService } from '../../module/core/insurances/insurances.service';

@ValidatorConstraint({ name: 'CompanyExist', async: true })
@Injectable()
export class InsuranceExistRule implements ValidatorConstraintInterface {
  constructor(private insurancesService: InsurancesService) {}

  async validate(value: string) {
    try {
      const isExist =
        (await this.insurancesService.findOneByQuery({ id: value })) ?? false;
      return !!isExist;
    } catch (error) {
      false;
    }
  }

  defaultMessage() {
    return LIST_ERROR_CODES.SS24439;
  }
}
