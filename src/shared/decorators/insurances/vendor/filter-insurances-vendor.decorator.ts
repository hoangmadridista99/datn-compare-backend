import { ExecutionContext, createParamDecorator } from '@nestjs/common';
import { plainToClass } from 'class-transformer';
import { validate } from 'class-validator';
import { InsuranceFilterByVendorDto } from '../../../../module/core/insurances/validation/filter-vendor.validation';
import { getValidateOption } from '../../../helper/system.helper';

export const VendorInsurancesFilter = createParamDecorator(
  async (data: unknown, ctx: ExecutionContext) => {
    try {
      const request = ctx.switchToHttp().getRequest();
      const { query } = request;
      const validateQuery = plainToClass(InsuranceFilterByVendorDto, query);
      const errors = await validate(validateQuery, getValidateOption(true));
      if (!!errors.length) return false;
      const { created_at, monthly_fee, ...result } = query;
      return {
        ...result,
        created_at: created_at ? new Date(created_at) : undefined,
        monthly_fee: monthly_fee ? parseInt(monthly_fee) : undefined,
      };
    } catch (error) {
      console.log(
        '🚀 ~ file: filter-insurances-vendor.decorator.ts:21 ~ error:',
        error,
      );
      return false;
    }
  },
);
