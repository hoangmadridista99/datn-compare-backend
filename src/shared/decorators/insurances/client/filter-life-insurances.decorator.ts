import { ExecutionContext, createParamDecorator } from '@nestjs/common';
import { IInsuranceFilterClient } from '../../../../module/core/insurances/interfaces/insurance.interface';
import { plainToClass } from 'class-transformer';
import { InsuranceFilterDto } from '../../../../module/core/insurances/validation/filter.validation';
import { validate } from 'class-validator';
import { ProfessionTypes } from '../../../../module/core/insurances/enums/insurances.enum';
import { getValidateOption } from '../../../helper/system.helper';

export const LifeInsurancesFilterClient = createParamDecorator(
  async (_data: unknown, ctx: ExecutionContext) => {
    try {
      const request = ctx.switchToHttp().getRequest();
      const { query } = request;
      const {
        year_of_birth,
        profession,
        is_suggestion,
        order_by,
        objective,
        insured_person,
      } = query;
      const LIMIT_DEFAULT = 5;

      /** Nếu là nghề nghiệp khác thì sẽ không filter theo profession */
      const professionFilter =
        profession === ProfessionTypes.OTHER_PROFESSION
          ? undefined
          : profession;

      query.objective = typeof objective === 'string' ? [objective] : objective;
      query.insured_person =
        typeof insured_person === 'string' ? [insured_person] : insured_person;

      const validateQuery = plainToClass(InsuranceFilterDto, query);
      const errors = await validate(validateQuery, getValidateOption(true));
      if (!!errors.length) return false;

      const insuranceFilter: IInsuranceFilterClient = {
        page: parseInt(query.page),
        limit:
          query.limit < LIMIT_DEFAULT ? LIMIT_DEFAULT : parseInt(query.limit),
        objective: query.objective,
        total_sum_insured: parseInt(query.total_sum_insured),
        insured_person: query.insured_person,
        year_of_birth: new Date().getFullYear() - year_of_birth,
        profession: professionFilter,
        deadline_for_deal: parseInt(query.deadline_for_deal),
        is_admin: false,
        is_suggestion: !!is_suggestion
          ? JSON.parse(query.is_suggestion)
          : undefined,
        order_by,
        sub: {
          serious_illnesses: query.serious_illnesses ? true : false,
          death_or_disability: query.death_or_disability ? true : false,
          health_care: query.health_care ? true : false,
          investment_benefit: query.investment_benefit ? true : false,
          increasing_value_bonus: query.increasing_value_bonus ? true : false,
          for_child: query.for_child ? true : false,
          flexible_and_diverse: query.flexible_and_diverse ? true : false,
          termination_benefits: query.termination_benefits ? true : false,
          expiration_benefits: query.expiration_benefits ? true : false,
          fee_exemption: query.fee_exemption ? true : false,
        },
      };
      return insuranceFilter;
    } catch (error) {
      console.log(
        '🚀 ~ file: filter-insurances.decorator.ts:59 ~ error:',
        error,
      );
      return false;
    }
  },
);
