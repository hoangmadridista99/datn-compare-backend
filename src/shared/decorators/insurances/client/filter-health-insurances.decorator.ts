/* eslint-disable @typescript-eslint/no-unused-vars */
import { ExecutionContext, createParamDecorator } from '@nestjs/common';
import { plainToClass } from 'class-transformer';
import { validate } from 'class-validator';
import { IHealthInsuranceFilterClient } from '../../../../module/core/insurances/interfaces/insurance.interface';
import { FilterHealthInsuranceDto } from '../../../../module/core/insurances/validation/filter-health-insurance';
import { ProfessionTypes } from '../../../../module/core/insurances/enums/insurances.enum';
import { getValidateOption } from '../../../helper/system.helper';
export const HealthInsurancesFilterClient = createParamDecorator(
  async (_data: unknown, ctx: ExecutionContext) => {
    try {
      const request = ctx.switchToHttp().getRequest();
      const { query } = request;
      const {
        is_dental,
        is_obstetric,
        room_type,
        insured_person,
        year_of_birth,
        profession,
        order_by,
      } = query;

      const limitDefault = 5;
      query.room_type = typeof room_type === 'string' ? [room_type] : room_type;
      query.insured_person =
        typeof query.insured_person === 'string'
          ? [insured_person]
          : insured_person;

      const bodyDto = plainToClass(FilterHealthInsuranceDto, query);
      const errors = await validate(bodyDto, getValidateOption(true));
      if (!!errors.length) return false;

      const result: IHealthInsuranceFilterClient = {
        page: parseInt(query.page),
        limit:
          query.limit < limitDefault ? limitDefault : parseInt(query.limit),
        is_dental: JSON.parse(is_dental),
        is_obstetric: JSON.parse(is_obstetric),
        room_type: query.room_type,
        insured_person: query.insured_person,
        year_of_birth: new Date().getFullYear() - year_of_birth,
        profession:
          profession === ProfessionTypes.OTHER_PROFESSION
            ? undefined
            : profession,
        order_by,
      };
      return result;
    } catch (error) {
      console.log(
        '🚀 ~ file: filter-insurances.decorator.ts:59 ~ error:',
        error,
      );
      return false;
    }
  },
);
