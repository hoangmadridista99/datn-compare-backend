import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { InsurancesFilterAdminDto } from '../../../../module/core/insurances/validation/filter-admin.validation';
import { validate } from 'class-validator';
import { plainToClass } from 'class-transformer';
import { getValidateOption } from '../../../helper/system.helper';

export const InsurancesFilterAdmin = createParamDecorator(
  async (_data: unknown, ctx: ExecutionContext) => {
    try {
      const request = ctx.switchToHttp().getRequest();
      const { query } = request;
      const validateQuery = plainToClass(InsurancesFilterAdminDto, query);
      const errors = await validate(validateQuery, getValidateOption(true));
      if (!!errors.length) return false;
      const { created_at, total_sum_insured, ...result } = query;
      return {
        ...result,
        created_at: created_at ? new Date(created_at) : undefined,
        total_sum_insured: total_sum_insured
          ? parseInt(total_sum_insured)
          : undefined,
      };
    } catch (error) {
      console.log(
        '🚀 ~ file: filter-insurances-admin.decorator.ts:8 ~ error:',
        error,
      );
      return false;
    }
  },
);
