/* eslint-disable @typescript-eslint/no-unused-vars */
import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { validate } from 'class-validator';
import { plainToClass } from 'class-transformer';
import { IUserInsurancesQueryFilter } from 'src/module/core/user-insurances/interfaces/user-insurance.interface';
import { InsuranceInterestFilterByOperatorDto } from 'src/module/core/insurances/dto/insurance.dto';
import { getValidateOption } from '../../../helper/system.helper';

export const InsuranceInterestFilterByOperatorDecorator = createParamDecorator(
  async (_data: unknown, ctx: ExecutionContext) => {
    try {
      const request = ctx.switchToHttp().getRequest();
      const { query } = request;
      const validateQuery = plainToClass(
        InsuranceInterestFilterByOperatorDto,
        query,
      );
      const errors = await validate(validateQuery, getValidateOption(true));
      if (!!errors.length) return false;
      const result: IUserInsurancesQueryFilter = {
        insurance_category_label: query.insurance_category_label,
        insurance_name: query.insurance_name,
        total_interest: query.total_interest,
      };
      return result;
    } catch (error) {
      console.log(
        '🚀 ~ file: filter-insurance-interest.decorator.ts:28 ~ error:',
        error,
      );
      return false;
    }
  },
);
