import { ExecutionContext, createParamDecorator } from '@nestjs/common';
import { IFilterAdmin } from '../../../../module/core/users/interfaces/users.interface';
import { plainToClass } from 'class-transformer';
import { FilterAccountsForSuperAdmin } from '../../../../module/super-admin/dto/filter-accounts.dto';
import { validate } from 'class-validator';
import { getValidateOption } from '../../../helper/system.helper';

export const AdminAccountFilter = createParamDecorator(
  async (_data: unknown, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest();
    const { query } = request;
    const dto = plainToClass(FilterAccountsForSuperAdmin, query);
    const errors = await validate(dto, getValidateOption(true));
    if (!!errors.length) return false;
    const filter: IFilterAdmin = {
      is_blocked: JSON.parse(query.is_blocked ?? null),
      account: query.account,
    };
    return filter;
  },
);
