import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { validate } from 'class-validator';
import { plainToClass } from 'class-transformer';
import { getValidateOption } from '../../../helper/system.helper';
import { OperatorFilterVendor } from 'src/module/operator/users/dto/user.filter';

export const FilterVendorByOperatorDecorator = createParamDecorator(
  async (_data: unknown, ctx: ExecutionContext) => {
    try {
      const request = ctx.switchToHttp().getRequest();
      const { query } = request;
      const validateQuery = plainToClass(OperatorFilterVendor, query);
      const errors = await validate(validateQuery, getValidateOption(true));
      if (!!errors.length) return false;
      return query;
    } catch (error) {
      console.log('🚀 ~ file: vendor.decorator.ts:17 ~ error:', error);
      return false;
    }
  },
);
