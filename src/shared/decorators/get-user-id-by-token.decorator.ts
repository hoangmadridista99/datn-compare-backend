import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { getConfig } from '../lib/config/config.lib';

export const GetUserIdByTokenDecorator = createParamDecorator(
  (_data: unknown, ctx: ExecutionContext) => {
    try {
      const request = ctx.switchToHttp().getRequest();
      const bearer = request.headers?.authorization;
      if (bearer) {
        const token = request.headers.authorization.split(' ')[1];
        if (token) {
          const jwtService = new JwtService({
            secret: getConfig('jwt.secret'),
          });
          const decodedToken = jwtService.verify(token);
          if (decodedToken) return decodedToken.id;
        }
      }
    } catch (error) {
      return;
    }
  },
);
