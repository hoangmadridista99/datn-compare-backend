import { SetMetadata } from '@nestjs/common';
import { UserRole } from 'src/module/core/users/entities/user.entity';

export enum RolesKey {
  ROLES = 'admin',
  VENDOR = 'vendor',
  SUPER_ADMIN = 'super-admin',
}
export const AdminRoles = (...role: UserRole[]) =>
  SetMetadata(RolesKey.ROLES, role);

export const SuperAdminRoles = (...role: UserRole[]) =>
  SetMetadata(RolesKey.SUPER_ADMIN, role);

export const VendorRoles = (...role: UserRole[]) =>
  SetMetadata(RolesKey.VENDOR, role);
