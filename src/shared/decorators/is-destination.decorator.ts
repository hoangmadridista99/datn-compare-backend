import {
  registerDecorator,
  ValidationOptions,
  isPhoneNumber,
  isEmail,
} from 'class-validator';
import { LIST_ERROR_CODES } from '../constants/error/base.error';

export function IsDestination(validationOptions?: ValidationOptions) {
  // eslint-disable-next-line @typescript-eslint/ban-types
  return function (object: Object, propertyName: string) {
    registerDecorator({
      name: 'isDestination',
      target: object.constructor,
      propertyName,
      constraints: [],
      options: validationOptions,
      validator: {
        validate(value: any) {
          if (typeof value !== 'string') return false;
          if (!isEmail(value) && !isPhoneNumber(value, 'VN')) return false;

          return true;
        },
        defaultMessage() {
          return LIST_ERROR_CODES.SS24502;
        },
      },
    });
  };
}
