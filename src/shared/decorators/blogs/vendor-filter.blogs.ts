import { ExecutionContext, createParamDecorator } from '@nestjs/common';
import { IVendorBlogsFilter } from '../../../module/core/blogs/interfaces/blogs.interface';
import { plainToClass } from 'class-transformer';
import { VendorBlogsFilterDto } from '../../../module/core/blogs/dto/vendor-filter.dto';
import { validate } from 'class-validator';
import { getValidateOption } from '../../helper/system.helper';

export const VendorFilterBlogs = createParamDecorator(
  async (_data: unknown, ctx: ExecutionContext) => {
    try {
      const request = ctx.switchToHttp().getRequest();
      const { query } = request;
      const { title, status, created_at, blog_category_id } = query;
      const filterDto = plainToClass(VendorBlogsFilterDto, query);
      const errors = await validate(filterDto, getValidateOption(true));
      if (!!errors.length) return false;
      const filter: IVendorBlogsFilter = {
        created_at: !created_at ? undefined : new Date(created_at),
        title,
        status,
        blog_category_id,
      };
      return filter;
    } catch (error) {
      console.log('🚀 ~ file: vendor-filter.blogs.ts:8 ~ error:', error);
      return false;
    }
  },
);
