import { ExecutionContext, createParamDecorator } from '@nestjs/common';
import { IAdminBlogsFilter } from '../../../module/core/blogs/interfaces/blogs.interface';
import { plainToClass } from 'class-transformer';
import { validate } from 'class-validator';
import { AdminBlogsFilterDto } from '../../../module/core/blogs/dto/admin-filter.dto';
import { UserRole } from '../../../module/core/users/entities/user.entity';
import { getValidateOption } from '../../helper/system.helper';

export const AdminFilterBlogs = createParamDecorator(
  async (_data: unknown, ctx: ExecutionContext) => {
    try {
      const request = ctx.switchToHttp().getRequest();
      const { query } = request;
      const { status, created_at, blog_category_id, role } = query;
      const filterDto = plainToClass(AdminBlogsFilterDto, query);
      const errors = await validate(filterDto, getValidateOption(true));
      if (!!errors.length) return false;
      let superAdminRole = undefined;
      if (role === UserRole.ADMIN) {
        superAdminRole = UserRole.SUPER_ADMIN;
      }
      const filter: IAdminBlogsFilter = {
        title: query.title,
        status,
        created_at: !created_at ? undefined : new Date(created_at),
        blog_category_id,
        role,
        super_admin_role: superAdminRole,
      };
      return filter;
    } catch (error) {
      console.log('🚀 ~ file: vendor-filter.blogs.ts:8 ~ error:', error);
      return false;
    }
  },
);
