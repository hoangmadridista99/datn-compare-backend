import { Injectable } from '@nestjs/common';
import {
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';
import { LIST_ERROR_CODES } from '../constants/error/base.error';
import { AppraisalFormsService } from '../../module/core/appraisal-form/appraisal-forms.service';

@ValidatorConstraint({ name: 'CompanyExist', async: true })
@Injectable()
export class AppraisalFormExistRule implements ValidatorConstraintInterface {
  constructor(private appraisalFormsService: AppraisalFormsService) {}

  async validate(value: string) {
    try {
      const isExist =
        (await this.appraisalFormsService.findOneFormByQuery({ id: value })) ??
        false;
      return !!isExist;
    } catch (error) {
      false;
    }
  }

  defaultMessage() {
    return LIST_ERROR_CODES.SS24440;
  }
}
