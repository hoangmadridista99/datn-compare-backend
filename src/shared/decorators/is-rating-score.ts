import {
  registerDecorator,
  ValidationOptions,
  max,
  min,
  isInt,
} from 'class-validator';
import { LIST_ERROR_CODES } from '../constants/error/base.error';
import { RatingScore } from 'src/module/core/ratings/enums/ratings.enum';

export function IsRatingScore(validationOptions?: ValidationOptions) {
  // eslint-disable-next-line @typescript-eslint/ban-types
  return function (object: Object, propertyName: string) {
    registerDecorator({
      name: 'isRatingScore',
      target: object.constructor,
      propertyName,
      constraints: [],
      options: validationOptions,
      validator: {
        validate(value: any) {
          return (
            isInt(value) &&
            min(value, RatingScore.MIN) &&
            max(value, RatingScore.MAX)
          );
        },
        defaultMessage() {
          return LIST_ERROR_CODES.SS24601;
        },
      },
    });
  };
}
