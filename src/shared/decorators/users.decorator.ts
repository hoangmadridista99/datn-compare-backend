import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { UserFilter } from 'src/module/core/users/models/user.model';
import { isVendorStatusTypes } from '../helper/system.helper';

export const UserFilterDecor = createParamDecorator(
  (_data: unknown, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest();
    const { query } = request;
    if (query.vendor_status) !isVendorStatusTypes(query.vendor_status);
    const filter: UserFilter = {
      vendor_status: query.vendor_status,
    };
    return filter;
  },
);
