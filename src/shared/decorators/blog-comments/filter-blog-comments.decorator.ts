import { ExecutionContext, createParamDecorator } from '@nestjs/common';
import { isUUID } from 'class-validator';
import { IFilterBlogComment } from '../../../module/core/blog-reviews/interfaces/blog-comment.interface';

export const BlogCommentsFilter = createParamDecorator(
  async (_data: unknown, ctx: ExecutionContext) => {
    try {
      const request = ctx.switchToHttp().getRequest();
      const { query } = request;
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const { page, limit, user_id, blog_id, ...invalid_data } = query;

      if (Object.values(invalid_data).length > 0) return false;
      if (!!user_id && !isUUID(user_id)) return false;
      if (!!blog_id && !isUUID(blog_id)) return false;

      const filter: IFilterBlogComment = {
        user_id,
        blog_id,
      };
      return filter;
    } catch (error) {
      console.log(
        '🚀 ~ file: filter-blog-comments.decorator.ts:8 ~ error:',
        error,
      );
      return false;
    }
  },
);
