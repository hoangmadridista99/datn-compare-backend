import { Injectable } from '@nestjs/common';
import {
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';
import { CompaniesService } from '../../module/core/companies/companies.service';
import { LIST_ERROR_CODES } from '../constants/error/base.error';

@ValidatorConstraint({ name: 'CompanyExist', async: true })
@Injectable()
export class CompanyExistRule implements ValidatorConstraintInterface {
  constructor(private companiesService: CompaniesService) {}

  async validate(value: string) {
    try {
      const isExist = (await this.companiesService.findById(value)) ?? false;
      return !!isExist;
    } catch (error) {
      false;
    }
  }

  defaultMessage() {
    return LIST_ERROR_CODES.SS24703;
  }
}
