import { paginate, PaginateQuery, Paginated } from 'nestjs-paginate';
import { Repository, SelectQueryBuilder } from 'typeorm';
import { BaseEntity } from 'src/shared/entities/base.entity';
import type { Column, SortBy } from 'nestjs-paginate/lib/helper';

export async function getPaginate<T extends BaseEntity>(
  query: PaginateQuery,
  repository: Repository<T> | SelectQueryBuilder<T>,
): Promise<Paginated<T>> {
  try {
    return await paginate(query, repository, {
      maxLimit: query.limit,
      defaultLimit: query.page,
      sortableColumns: ['created_at'] as Column<T>[],
      defaultSortBy: [['created_at', 'DESC']] as SortBy<T>,
    });
  } catch (error) {
    console.log('🚀 ~ file: paginate.lib.ts:16 ~ error:', error);
    throw error;
  }
}

export interface IMetaPaginated {
  itemsPerPage: number;
  totalItems: number;
  currentPage: number;
  totalPages: number;
  sortBy: SortBy<any>;
}
