import { HttpStatus } from '@nestjs/common';

export const NO_CONTENT_SUCCESS_RESPONSE = {
  statusCode: HttpStatus.NO_CONTENT,
};

export function getInsurancesMiddlewareError(statusCode: HttpStatus) {
  return {
    statusCode,
    errorCode: HttpStatus.UNAUTHORIZED ? 'SS24006' : 'SS24005',
  };
}
