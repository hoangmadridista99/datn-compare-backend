import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
  HttpStatus,
  RequestTimeoutException,
} from '@nestjs/common';
import { Observable, throwError, TimeoutError } from 'rxjs';
import { catchError, map, timeout } from 'rxjs/operators';
import { instanceToPlain } from 'class-transformer';

export interface Response<T> {
  data: T;
}

@Injectable()
export class TransformInterceptor<T>
  implements NestInterceptor<T, Response<T>>
{
  intercept(
    context: ExecutionContext,
    next: CallHandler,
  ): Observable<Response<T>> {
    const request = context.switchToHttp().getRequest();
    const { url, method } = request;
    const now = new Date().toISOString();
    return next.handle().pipe(
      map((data) => this.matching(method, instanceToPlain(data), context)),
      timeout(5000),
      catchError((err) => {
        console.log(`💥ERROR💥 ${method}  ~ ${url}... ${now}`);

        if (err instanceof TimeoutError) {
          return throwError(() => new RequestTimeoutException());
        }

        return throwError(() => err);
      }),
    );
  }

  matching(method: string, data: any, context: ExecutionContext) {
    let status = HttpStatus.OK;

    switch (true) {
      case data?.statusCode && typeof data?.statusCode === 'number':
        status = data?.statusCode;
        break;
      case data?.error_code &&
        typeof data?.error_code === 'string' &&
        data?.error_code === 'SS24001':
        status = HttpStatus.INTERNAL_SERVER_ERROR;
        break;
      case !data?.statusCode && !data?.error_code && method === HttpMethod.POST:
        status = HttpStatus.CREATED;
        break;
    }
    context.switchToHttp().getResponse().status(status);

    if (data?.password) delete data.password;
    if (data?.statusCode) {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const { statusCode, ...result } = data;
      return result;
    }
    return data;
  }
}

export enum HttpMethod {
  GET = 'GET',
  POST = 'POST',
  PATCH = 'PATCH',
}
