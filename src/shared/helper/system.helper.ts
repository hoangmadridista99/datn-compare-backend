import { HttpException, HttpStatus } from '@nestjs/common';
import { VendorStatusTypes } from '../../module/core/users/enums/users.enum';
import { LIST_ERROR_CODES, getError } from '../constants/error/base.error';
import { getConfig } from '../lib/config/config.lib';
import { MinioTypes } from 'src/module/core/minio/enums/minio.enum';

export function setStartAndEndOfDateByTime(date: Date) {
  const startOfDay = new Date(date.setUTCHours(0, 0, 0, 0));
  const endOfDay = new Date(date.setUTCHours(23, 59, 59, 999));
  return { startOfDay, endOfDay };
}

export function isVendorStatusTypes(vendor_status: string) {
  if (
    vendor_status !== VendorStatusTypes.ACCEPTED &&
    vendor_status !== VendorStatusTypes.PENDING &&
    vendor_status !== VendorStatusTypes.REJECTED
  ) {
    throw new HttpException(
      getError(LIST_ERROR_CODES.SS24209),
      HttpStatus.BAD_REQUEST,
    );
  }
  return true;
}

export function generateRandomString(length: number) {
  const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
  let result = '';
  for (let i = 0; i < length; i++) {
    const randomIndex = getRandomNumber(0, characters.length - 1);
    result += characters.charAt(randomIndex);
  }
  return result;
}

export function getRandomNumber(min: number, max: number) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

export function getValidateOption(param: boolean) {
  return {
    whitelist: param,
    forbidNonWhitelisted: param,
  };
}

export function getBodyRouteOfMiddleWare(path: string, method: number) {
  return { path, method };
}

export function getRandomStringNumberByLength(length: number) {
  const min = Math.pow(10, length - 1);
  const max = min * 9;
  const code = Math.round(Math.random() * max) + min;
  return String(code);
}

export function getFileName(originalname: string) {
  return `${new Date().getTime() / 1000}_${originalname}`;
}

export function getFileKey(type: MinioTypes, filename: string) {
  return `${type}/${filename}`;
}

export function getFileUrl(type: MinioTypes, filename: string) {
  return `${getConfig(
    'minio.api_url',
  )}/v1/api/client/file?key=${type}/${filename}`;
}

export function getPaginationMetaData(
  totalItems: number,
  page: number,
  limit: number,
) {
  const totalPages = Math.ceil(totalItems / limit);
  return {
    totalItems,
    totalPages,
    currentPage: page,
  };
}
