export class BasicResponse {
  statusCode?: number | string;
  error?: string | object;
  error_code?: string;
}
