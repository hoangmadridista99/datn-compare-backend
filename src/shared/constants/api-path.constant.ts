export const API_PATH = {
  SEND_OTP_CLIENT: 'v1/api/client/otps/send-otp',
  SEND_OTP_REGISTER_VENDOR: 'v1/api/vendor/otps/register/send-otp',
  SEND_OTP_LOGIN_VENDOR: 'v1/api/vendor/otps/login/send-otp',
  CREATE_INSURANCE_ADMIN: '/v1/api/admin/insurances',
  CREATE_INSURANCE_VENDOR: '/v1/api/vendor/insurances',
  UPDATE_INSURANCE_ADMIN: '/v1/api/admin/insurances/:id',
  UPDATE_INSURANCE_VENDOR: '/v1/api/vendor/insurances/:id',
};
