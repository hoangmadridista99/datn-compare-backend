import { ClientModule } from '../../module/client/client.module';
import { OperatorModule } from '../../module/operator/operator.module';
import { VendorModule } from '../../module/vendor/vendor.module';

export const AllModules = {
  client: ClientModule,
  admin: OperatorModule,
  vendor: VendorModule,
};

const routerModulePaths = ['api/admin', 'api/client', 'api/vendor'];

export const registerRouterModule = () => {
  return routerModulePaths.map((path: string) => {
    const moduleName = path.split('/')[1];
    return { path, module: AllModules[moduleName] };
  });
};
