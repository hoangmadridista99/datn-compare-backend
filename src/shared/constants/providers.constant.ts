import { APP_FILTER, APP_INTERCEPTOR } from '@nestjs/core';
import { TransformInterceptor } from '../interceptors/transform.interceptor';
import {
  BadRequestExceptionFilter,
  ForbiddenExceptionFilter,
  UnauthorizedExceptionFilter,
} from '../filters/filter';

const allUsedClasses = [
  { useClass: TransformInterceptor },
  { useClass: BadRequestExceptionFilter },
  { useClass: ForbiddenExceptionFilter },
  { useClass: UnauthorizedExceptionFilter },
];

export const importAllProviders = () => {
  return allUsedClasses.map(({ useClass }) => {
    const isNotFilterClass = typeof useClass === typeof TransformInterceptor;
    return {
      provide: isNotFilterClass ? APP_INTERCEPTOR : APP_FILTER,
      useClass,
    };
  });
};
