export const LIST_ERROR_CODES = {
  /** AUTHORIZATION error */
  SS24001: 'INTERNAL_SERVER_ERROR',
  SS24002:
    'Vendor account has not been activated! Please wait for Admin confirmation.',
  SS24003: 'Only vendor are allowed to login to this page',
  SS24004: 'Only admin are allowed to login to this page',
  SS24005: 'You do not have permission to access this resource',
  SS24006:
    'Authentication failed or user is not authorized to access this resource',
  SS24007: 'Only admins are allowed to update role!',
  SS24008: 'Your account is blocked!',
  SS24009: 'Only super admin are allowed to update this!',

  /** COMMON error */
  SS24101: 'Resources Has Not Found!',
  SS24102: 'Failed To Delete!',
  SS24103: 'Failed To Update!',
  SS24104: 'Failed To Create!',
  SS24105: 'Failed To Upload',
  SS24106: 'File too large',
  SS24107: 'File format not supported',
  SS24108: 'Invalid request!',
  SS24109: 'Number of photos invalid',

  /** AUTHENTICATION error */
  SS24202: '...',
  SS24203: 'Password incorrect!',
  SS24204: 'Email already existed!',
  SS24205: 'Phone number has already existed!',
  SS24206: 'Account already existed!',
  SS24207: 'Parameter validation failed! Parameter value id must be an UUID',
  SS24208: 'User account not exist!',
  SS24209: 'vendor_status must be a valid enum value!',
  SS24210: 'Admin account not exist!',
  SS24211: 'destination invalid!',
  SS24212: 'Super admin account not exist!',

  /** VALIDATION Register User */
  SS24301: 'email must be an email',
  SS24302: 'email must not be empty!',
  SS24303: 'first_name should not be empty!',
  SS24304: 'first_name must be a string!',
  SS24305: 'last_name should not be empty!',
  SS24306: 'last_name must be a string!',
  SS24307: 'password should not be empty!',
  SS24308: 'password must be a string!',
  SS24309: 'phone should not be empty!',
  SS24310: 'phone must be a valid phone number',
  SS24311: 'phone must be a string!',
  SS24312: 'date_of_birth must be a valid ISO 8601 date string',
  SS24313: 'address must be a string!',
  SS24314: 'account should not be empty!',
  SS24315: 'account must be a string!',
  SS24316: 'role must be a valid enum value',
  SS24317: 'company must be a string!',
  SS24318: 'company should not be empty!',
  SS24319: 'banner_image_url must be a valid url!',
  SS24320: 'banner_image_url should not be empty!',
  SS24321: 'wrong password!',
  SS24322: 'update password failed!',

  /** VALIDATION Insurances */

  // common
  SS24437: 'status should not be empty!',
  SS24438: 'status must be a valid enum!',
  SS24439: 'insurance_id not exist!',
  SS24440: 'appraisal_form_id not exist',
  SS24442: 'appraisal_form_id has existed!',

  // additional_benefits
  SS24401: 'name should not be empty!',
  SS24402: 'name must be a string!',
  SS24403: 'objective_type must be a string!',
  SS24404: 'objective_type has already existed!',
  SS24405: 'additional_benefits.death_or_disability should not be empty!',
  SS24406: 'additional_benefits.serious_illnesses should not be empty!',
  SS24407: 'additional_benefits.health_care should not be empty!',
  SS24408: 'additional_benefits.investment_benefit should not be empty!',
  SS24409: 'additional_benefits.increasing_value_bonus should not be empty!',
  SS24410: 'additional_benefits.for_child should not be empty!',
  SS24411: 'additional_benefits.flexible_and_diverse should not be empty!',
  SS24412: 'additional_benefits.termination_benefits should not be empty!',
  SS24413: 'additional_benefits.expiration_benefits should not be empty!',
  SS24416: 'additional_benefits.fee_exemption should not be empty!',
  SS24436: 'objective_of_insurance invalid!',

  // customer_orientation
  SS24418: 'customer_orientation.acceptance_rate should not be null!',
  SS24419: 'customer_orientation.completion_time_deal should not be null!',
  SS24420: 'customer_orientation.end_of_process should not be null!',
  SS24421: 'customer_orientation.withdrawal_time should not be null!',
  SS24422:
    'customer_orientation.reception_and_processing_time should not be null!',

  // terms
  SS24423: 'terms.age_eligibility should not be empty!',
  SS24424: 'terms.insurance_minimum_fee should not be empty!',
  SS24425: 'terms.insured_person should not be empty!',
  SS24427: 'terms.total_sum_insured should not be empty!',
  SS24428: 'terms.monthly_fee should not be empty!',
  SS24430: 'terms.profession should not be empty!',
  SS24431: 'terms.deadline_for_payment should not be empty!',
  SS24432: 'terms.age_of_contract_termination should not be empty!',
  SS24433: 'terms.termination_conditions should not be empty!',
  SS24434: 'terms.total_sum_insured should not be empty!',
  SS24435: 'terms.monthly_fee should not be empty!',

  SS24414: 'failed to validate body request!',
  SS24415: 'insurance_logo must be a valid url!',
  SS24417: 'company_id must be a UUID!',
  SS24429: 'Parameters of filter invalid!!!',

  /** Car Insurance Settings */
  SS24441: 'Setting by this insurance has existed!',

  /** SEND OTP */
  SS24501: 'Failed to send otp!',
  SS24502: 'The destination must be a phone number or email!',
  SS24503: 'destination_type must be a valid enum value!',
  SS24504: 'type must be a valid enum value!',
  SS24505: 'OTP invalid',
  SS24506: 'otp_code must be a string!',
  SS24507: 'You can only send OTP after 1 hour from the last send!',

  /** Ratings & Insurance */
  SS24601: 'The rating score is only from 0 to 5',
  SS24602: 'Comment must be a string!',
  SS24603: 'Create Rating Failed',
  SS24604: 'insurance has not found!',
  SS24605: 'Update Rating Failed',
  SS24606: 'insurance_id must be a UUID!',
  SS24607: 'car_insurance_id must be a UUID!',
  SS24608: 'car insurance has not found!',

  /** Company */
  SS24701: 'homepage must be a string',
  SS24702: 'company_other must be a string',
  SS24703: 'company not found',
  SS24704: 'company id must be a UUID',

  /** Blog */
  SS24801: 'title must be a string!',
  SS24802: 'title should not be empty!',
  SS24803: 'description must be a string!',
  SS24804: 'description should not be empty!',
  SS24805: 'content must be a string!',
  SS24806: 'content should not be empty!',
  SS24807: 'blog_category_id must be a valid UUID!',
  SS24808: 'blog_category_id should not be empty!',
  SS24809: 'the blog can only be updated when it is in pending state!',
  SS24810: 'reason must be a string!',
  SS24811: 'status must be a valid enum!',

  /** Blog Comments & Blog Categories */
  SS24901: 'comment must be a string!',
  SS24902: 'blog_id must be a UUID!',
  SS24903: 'is_hide must be a boolean!',
  SS24904: 'value must be a string',
  SS24905: 'value should not be empty',
  SS24906: 'vn_label must be a string',
  SS24907: 'vn_label should not be empty',
  SS24908: 'en_label should not be empty',
  SS24909: 'en_label must be a string',
  SS24910: 'is_hide must be a boolean',

  /** Appraisal */
  SS241001: 'appraisal_form has not found!',
  SS241002: 'image url has not found!',
};

export function getErrorCodeByErrorMessage(error: string) {
  const listError = LIST_ERROR_CODES;
  return (
    Object.keys(listError).find(
      (errorCode) => listError[errorCode] === error,
    ) ?? 'SS24108'
  );
}

export function getError(error: string, statusCode?: number) {
  return {
    statusCode: !statusCode ? undefined : statusCode,
    error_code: getErrorCodeByErrorMessage(error),
  };
}
