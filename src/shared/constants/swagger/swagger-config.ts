import { INestApplication } from '@nestjs/common';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AllModules } from '../modules.constant';

const swaggerPath = ['client/docs/api', 'admin/docs/api', 'vendor/docs/api'];

const getTitle = (resourceName: string) => {
  return `Project APIs for ${resourceName}`;
};
const swaggerDescription = 'API documentation for version 1 project';
const swaggerVersion = '1.0';

export const getSwaggerBuilderConfig = async (app: INestApplication) => {
  swaggerPath.forEach((path: string) => {
    const resourceName = path.split('/')[0];
    const Swagger = new DocumentBuilder()
      .setTitle(getTitle(resourceName))
      .setDescription(swaggerDescription)
      .setVersion(swaggerVersion)
      .addBearerAuth()
      .build();
    const Document = SwaggerModule.createDocument(app, Swagger, {
      include: [AllModules[resourceName]],
    });
    SwaggerModule.setup(path, app, Document);
  });
};
