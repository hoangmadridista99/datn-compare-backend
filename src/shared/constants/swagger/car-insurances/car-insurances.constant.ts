import { CarInsuranceTypes } from '../../../../module/core/car-insurances/enums/car-insurance.enum';

export const CAR_INSURANCE_TYPE_QUERY = {
  required: false,
  enum: CarInsuranceTypes,
  name: 'insurance_type',
  type: 'string',
  example: CarInsuranceTypes.Both,
};
