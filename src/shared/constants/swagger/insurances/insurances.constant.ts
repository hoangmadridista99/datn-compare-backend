/** Mục đích tham gia bảo hiểm  */

import { InsuranceCategoryNames } from '../../../../module/core/insurance-categories/enums/insurance-category.enum';
import {
  InsuranceSortByClient,
  InsuranceStatusesTypes,
} from '../../../../module/core/insurances/enums/insurances.enum';

/** Tổng tiền mong muốn */
export const TOTAL_SUM_INSURED_QUERY = {
  required: false,
  name: 'total_sum_insured',
  type: 'number',
  example: 100000000,
};

export const OBJECTIVE_QUERY = {
  required: false,
  name: 'objective',
  type: 'array',
  isArray: true,
};

/** Bạn muốn mua bảo hiểm cho ai */
export const INSURED_PERSON_QUERY = {
  required: false,
  name: 'insured_person',
  type: 'array',
};
/** Năm sinh */
export const YEAR_OF_BIRTH_QUERY = {
  required: false,
  name: 'year_of_birth',
  type: 'string',
  example: '1996',
};
/** Nghề nghiệp */
export const PROFESSION_QUERY = {
  required: false,
  name: 'profession',
  type: 'string',
  example: 'OTHER_PROFESSION',
};
/** Thời hạn bảo hiểm */
export const DEADLINE_FOR_DEAL_QUERY = {
  required: false,
  name: 'deadline_for_deal',
  explode: true,
  type: 'number',
  example: 10,
};

/** Quyền lợi bảo vệ trước tử vong/ thương tật */
export const DEATH_OR_DISABILITY_QUERY = {
  required: false,
  name: 'death_or_disability',
  explode: true,
  type: 'boolean',
  example: true,
};
/** Quyền lợi bảo vệ bệnh ung thư, hiểm nghèo */
export const SERIOUS_ILLNESSES_QUERY = {
  required: false,
  name: 'serious_illnesses',
  explode: true,
  type: 'boolean',
  example: true,
};
/** Quyền lợi chăm sóc y tế */
export const HEALTH_CARE_QUERY = {
  required: false,
  name: 'health_care',
  explode: true,
  type: 'boolean',
  example: true,
};
/** Quyền lợi đầu tư của sản phẩm liên kết chung */
export const INVESTMENT_BENEFIT_QUERY = {
  required: false,
  name: 'investment_benefit',
  explode: true,
  type: 'boolean',
  example: true,
};
/** Quyền lợi thưởng gia tăng giá trị hợp đồng */
export const INCREASING_VALUE_BONUS_QUERY = {
  required: false,
  name: 'increasing_value_bonus',
  explode: true,
  type: 'boolean',
  example: true,
};
/** Quyền lợi cho con */
export const FOR_CHILD_QUERY = {
  required: false,
  name: 'for_child',
  explode: true,
  type: 'boolean',
  example: true,
};
/** Quyền lợi linh hoạt, đa dạng */
export const FLEXIBLE_AND_DIVERSE_QUERY = {
  required: false,
  name: 'flexible_and_diverse',
  explode: true,
  type: 'boolean',
  example: true,
};
/** Quyền lợi kết thúc hợp đồng */
export const TERMINATION_BENEFITS_QUERY = {
  required: false,
  name: 'termination_benefits',
  explode: true,
  type: 'boolean',
  example: true,
};
/** Quyền lợi đáo hạn */
export const EXPIRATION_BENEFITS_QUERY = {
  required: false,
  name: 'expiration_benefits',
  explode: true,
  type: 'boolean',
  example: true,
};
/** Quyền lợi miễn đóng phí */
export const FEE_EXEMPTION_QUERY = {
  required: false,
  name: 'fee_exemption',
  explode: true,
  type: 'boolean',
  example: true,
};
export const IS_SUGGESTION_QUERY = {
  name: 'is_suggestion',
  type: 'boolean',
  required: false,
};
export const ORDER_BY_QUERY = {
  name: 'order_by',
  type: 'enum',
  enum: InsuranceSortByClient,
  required: false,
};
/** Có hỗ trợ nha khoa không */
export const IS_DENTAL_QUERY = {
  name: 'is_dental',
  type: 'string',
  required: false,
  example: 'true',
};

/** Có hỗ trợ thai sản không */
export const IS_OBSTETRIC_QUERY = {
  name: 'is_obstetric',
  type: 'string',
  required: false,
  example: 'true',
};

/** Loại phòng */
export const ROOM_TYPE_QUERY = {
  required: false,
  name: 'room_type',
  type: 'array',
};

export const NAME_QUERY = {
  required: false,
  name: 'insurance_name',
  type: 'string',
  example: 'AIA',
};

export const INSURANCE_TYPE_QUERY = {
  required: false,
  name: 'insurance_type',
  type: 'string',
  example: 'life',
};

export const STATUS_INSURANCES_QUERY = {
  required: false,
  enum: InsuranceStatusesTypes,
  name: 'status',
};
export const INSURANCE_NAME_QUERY = {
  required: false,
  name: 'insurance_name',
};
export const CREATED_AT_QUERY = {
  required: false,
  name: 'created_at',
};
export const INSURANCE_CATEGORY_LABEL_QUERY = {
  required: false,
  name: 'insurance_category_label',
  enum: InsuranceCategoryNames,
};
export const MONTHLY_FEE_QUERY_QUERY = {
  required: false,
  name: 'monthly_fee',
};
