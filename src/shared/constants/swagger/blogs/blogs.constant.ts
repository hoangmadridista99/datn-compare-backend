import { BlogStatusesTypes } from '../../../../module/core/blogs/enums/blogs.enum';

export const BLOG_CATEGORY_ID_QUERY = {
  required: false,
  name: 'blog_category_id',
  type: 'string',
  example: '',
};

export const TITLE_QUERY = {
  required: false,
  name: 'title',
  type: 'string',
  example: 'Good',
};
export const STATUS_QUERY = {
  required: false,
  enum: BlogStatusesTypes,
  name: 'status',
  type: 'string',
  example: BlogStatusesTypes.Pending,
};
