import { UserRolesQuery } from '../../../module/core/users/entities/user.entity';

export const PAGE_QUERY = {
  required: false,
  example: 1,
  name: 'page',
};

export const LIMIT_QUERY = {
  required: false,
  example: 10,
  name: 'limit',
};

export const CREATED_AT_QUERY = {
  required: false,
  name: 'created_at',
  type: 'string',
  example: '2023-04-26',
};

export const ROLE_USER_QUERY = {
  required: true,
  name: 'role',
  enum: UserRolesQuery,
  example: UserRolesQuery.VENDOR,
};
