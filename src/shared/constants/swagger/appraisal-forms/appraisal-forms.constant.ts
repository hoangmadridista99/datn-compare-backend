import { AppraisalFormStatuses } from '../../../../module/core/appraisal-form/enum/appraisal-form.enum';

export const APPRAISAL_FORM_STATUS_QUERY = {
  required: false,
  name: 'form_status',
  type: 'enum',
  enum: AppraisalFormStatuses,
  example: AppraisalFormStatuses.Approved,
};

export const APPRAISAL_FORM_CODE_QUERY = {
  required: false,
  name: 'form_code',
};
