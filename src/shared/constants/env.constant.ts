import { Company } from '../../module/core/companies/entities/company.entity';
import { InsuranceObjective } from '../../module/core/insurance-objectives/entities/insurance-objective.entity';
import { CustomerOrientation } from '../../module/core/insurances/entities/customer-orientation/customer-orientation.entity';
import { Insurance } from '../../module/core/insurances/entities/insurance.entity';
import { AdditionalBenefits } from '../../module/core/insurances/entities/additional-benefits/additional-benefits.entity';
import { Term } from '../../module/core/insurances/entities/terms/term.entity';
import { Otp } from '../../module/core/otps/entities/otp.entity';
import { Rating } from '../../module/core/ratings/entities/rating.entity';
import { User } from '../../module/core/users/entities/user.entity';
import { getConfig } from '../lib/config/config.lib';
import { Blog } from '../../module/core/blogs/entities/blog.entity';
import { BlogComment } from '../../module/core/blog-reviews/entities/blog-comments.entity';
import { BlogCategory } from '../../module/core/blog-categories/entities/blog-category.entity';
import { UserInsurances } from 'src/module/core/user-insurances/entities/user-insurance.entity';
import { InsuranceCategory } from '../../module/core/insurance-categories/entities/insurance-category.entity';
import { Hospital } from 'src/module/core/hospitals/entities/hospital.entity';
import { Inpatient } from '../../module/core/insurances/entities/inpatient/inpatient.entity';
import { Outpatient } from '../../module/core/insurances/entities/outpatient/outpatient.entity';
import { Dental } from '../../module/core/insurances/entities/dental/dental.entity';
import { Obstetric } from '../../module/core/insurances/entities/obstetric/obstetric.entity';
import { AppraisalForm } from '../../module/core/appraisal-form/entities/appraisal-form.entity';
import { Appraisal } from '../../module/core/appraisals/entities/appraisal.entity';
import { CarInsurance } from '../../module/core/car-insurances/entities/car-insurance.entity';
import { MandatorySetting } from '../../module/core/car-insurance-settings/entities/mandatory-setting.entity';
import { MandatoryForm } from '../../module/core/mandatory-forms/entities/mandatory-form.entity';
import { CarInsuranceRating } from '../../module/core/car-insurance-ratings/entities/car-insurance-rating.entity';

export const ENV_CONFIG = {
  database: {
    host: getConfig('database.host'),
    username: getConfig('database.username'),
    password: getConfig('database.password'),
    database: getConfig('database.database'),
  },
  jwt: {
    secret: getConfig('jwt.secret'),
  },
  system: {
    port: getConfig('system.port') || 3000,
    apiVersion: getConfig('system.api_version'),
  },
  gmail: {
    user: getConfig('gmail.user'),
    pass: getConfig('gmail.pass'),
  },
  twilio: {
    accountSID: getConfig('twilio.account_sid'),
    authToken: getConfig('twilio.auth_token'),
    phoneNumber: getConfig('twilio.phone_number'),
    options: {
      region: 'au1',
    },
  },
};

export const ENTITIES = [
  User,
  Insurance,
  Otp,
  Rating,
  InsuranceObjective,
  Company,
  AdditionalBenefits,
  CustomerOrientation,
  Term,
  Blog,
  BlogComment,
  BlogCategory,
  UserInsurances,
  InsuranceCategory,
  Hospital,
  Inpatient,
  Outpatient,
  Dental,
  Obstetric,
  AppraisalForm,
  Appraisal,
  CarInsurance,
  MandatorySetting,
  MandatoryForm,
  CarInsuranceRating,
];
