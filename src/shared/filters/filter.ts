import {
  ExceptionFilter,
  Catch,
  ArgumentsHost,
  BadRequestException,
  ForbiddenException,
  UnauthorizedException,
} from '@nestjs/common';
import { Response } from 'express';
import { getError, LIST_ERROR_CODES } from '../constants/error/base.error';

@Catch(BadRequestException)
export class BadRequestExceptionFilter implements ExceptionFilter {
  catch(exception: BadRequestException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    const status = exception.getStatus();
    const getRes: any = exception.getResponse();
    console.log(
      '🚀 ~ file: filter.ts:19 ~ BadRequestExceptionFilter ~ getRes:',
      getRes,
    );
    const makeErrorResponse = getError(getRes?.message[0]);
    const data = !makeErrorResponse.error_code
      ? getError(LIST_ERROR_CODES.SS24108)
      : makeErrorResponse;
    response.status(status).json(data);
  }
}

@Catch(ForbiddenException)
export class ForbiddenExceptionFilter implements ExceptionFilter {
  catch(exception: ForbiddenException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    const status = exception.getStatus();
    response.status(status).json(getError(LIST_ERROR_CODES.SS24005));
  }
}

@Catch(UnauthorizedException)
export class UnauthorizedExceptionFilter implements ExceptionFilter {
  catch(exception: ForbiddenException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    const status = exception.getStatus();
    response.status(status).json(getError(LIST_ERROR_CODES.SS24006));
  }
}
