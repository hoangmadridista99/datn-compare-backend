import { HttpStatus, Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import { getErrorCodeByErrorMessage } from '../constants/error/base.error';
import { plainToClass } from 'class-transformer';
import { InsuranceCategoryNames } from '../../module/core/insurance-categories/enums/insurance-category.enum';
import {
  CreateLifeInsuranceDto,
  CreateHealthInsuranceDto,
  UpdateLifeInsuranceDto,
  UpdateHealthInsuranceDto,
} from '../../module/core/insurances/dto/insurance.dto';
import { validate } from 'class-validator';
import { AuthService } from '../../module/core/auth/auth.service';
import { UsersService } from '../../module/core/users/users.service';
import { IUser } from '../../module/core/users/interfaces/users.interface';
import { API_PATH } from '../constants/api-path.constant';
import { UserRole } from '../../module/core/users/entities/user.entity';
import { getInsurancesMiddlewareError } from '../lib/responses/response';
import { InsuranceHealthAdditionalBenefit } from 'src/module/core/insurances/enums/insurances.enum';
import { HttpMethod } from '../interceptors/transform.interceptor';
import { getValidateOption } from '../helper/system.helper';

enum MethodTypes {
  Post = 'POST',
  Patch = 'PATCH',
}

declare module 'express' {
  interface Request {
    user: IUser;
  }
}

@Injectable()
export class InsurancesMiddleware implements NestMiddleware {
  constructor(
    private readonly authService: AuthService,
    private readonly usersService: UsersService,
  ) {}
  async use(req: Request, res: Response, next: NextFunction) {
    const { body, method, headers, route } = req;
    /** Authenticate & Authorization JWT token */
    const token = headers?.authorization?.split(' ')[1];
    const user: IUser | any = await this.verifyToken(token, route?.path);
    if (user.statusCode)
      return res.status(user.statusCode).send({ error_code: user.errorCode });

    /** Validate create insurance body request */
    const errors = await this.validateRequestBody(body, method as MethodTypes);
    if (!errors || errors.length) {
      const errorCode =
        Array.isArray(errors) && !errors[0]?.children?.length
          ? getErrorCodeByErrorMessage(Object.values(errors[0]?.constraints)[0])
          : 'SS24108';
      return res.status(HttpStatus.BAD_REQUEST).send({ error_code: errorCode });
    }
    if (body.insurance_category.label === InsuranceCategoryNames.Health) {
      const { additional_benefit, ...result } = body;
      const updateBody = {
        ...result,
        dental: additional_benefit.includes(
          InsuranceHealthAdditionalBenefit.DENTAL,
        )
          ? result.dental
          : null,
        obstetric: additional_benefit.includes(
          InsuranceHealthAdditionalBenefit.OBSTETRIC,
        )
          ? result.obstetric
          : null,
      };
      req.body = updateBody;
    }

    req.user = user as IUser;
    next();
  }

  async validateRequestBody(body: any, method: `${MethodTypes}`) {
    const isWhiteList = method === MethodTypes.Post;
    const validateData = { ...body };
    if (method === HttpMethod.PATCH) {
      delete validateData.insurance_category;
    }
    switch (body?.insurance_category?.label) {
      case InsuranceCategoryNames.Life: {
        const bodyToValidate = plainToClass(
          method === MethodTypes.Patch
            ? UpdateLifeInsuranceDto
            : CreateLifeInsuranceDto,
          validateData,
        );
        return await validate(bodyToValidate, getValidateOption(isWhiteList));
      }
      case InsuranceCategoryNames.Health: {
        const bodyToValidate = plainToClass(
          method === MethodTypes.Patch
            ? UpdateHealthInsuranceDto
            : CreateHealthInsuranceDto,
          validateData,
        );
        return await validate(bodyToValidate, getValidateOption(isWhiteList));
      }

      default:
        return null;
    }
  }

  async verifyToken(token: string, path: string) {
    const validateToken = await this.authService.validateToken(token);
    if (!validateToken)
      return getInsurancesMiddlewareError(HttpStatus.UNAUTHORIZED);

    const user = await this.usersService.findOneByUsersQuery({
      id: validateToken.id,
    });
    if (!user) return getInsurancesMiddlewareError(HttpStatus.UNAUTHORIZED);

    switch (path) {
      case API_PATH.CREATE_INSURANCE_ADMIN:
      case API_PATH.UPDATE_INSURANCE_ADMIN:
        if (user.role !== UserRole.ADMIN && user.role !== UserRole.SUPER_ADMIN)
          return getInsurancesMiddlewareError(HttpStatus.FORBIDDEN);
        break;
      case API_PATH.CREATE_INSURANCE_VENDOR:
      case API_PATH.UPDATE_INSURANCE_VENDOR:
        if (user.role !== UserRole.VENDOR)
          return getInsurancesMiddlewareError(HttpStatus.FORBIDDEN);
        break;
      default:
        return getInsurancesMiddlewareError(HttpStatus.UNAUTHORIZED);
    }
    return user;
  }
}
