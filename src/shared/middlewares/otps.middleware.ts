import { HttpStatus, Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import { OtpsService } from '../../module/core/otps/otps.service';
import { DestinationTypes } from '../../module/core/otps/enums/otp.enum';
import { LIST_ERROR_CODES, getError } from '../constants/error/base.error';
import { plainToClass } from 'class-transformer';
import { validate } from 'class-validator';
import {
  SendOtpDto,
  SendOtpLoginForVendorDto,
} from '../../module/core/otps/dto/otp.dto';
import { getValidateOption } from '../helper/system.helper';

@Injectable()
export class SendOtpMiddleware implements NestMiddleware {
  constructor(private readonly otpsService: OtpsService) {}
  async use(req: Request, res: Response, next: NextFunction) {
    const { destination_type, type, email, phone } = req.body;
    const otpDto = plainToClass(
      !type ? SendOtpLoginForVendorDto : SendOtpDto,
      req.body,
    );
    const errors = await validate(otpDto, getValidateOption(true));
    if (errors.length > 0) {
      return res
        .status(HttpStatus.BAD_REQUEST)
        .json(getError(Object.values(errors[0].constraints)[0]));
    }

    const destination =
      destination_type !== DestinationTypes.PHONE ? email : phone;

    /** Check the number of times an OTP message has been sent */
    const arrayOfOtpsSortedByDESC =
      await this.otpsService.findManyInDayByCurrentTime(
        type,
        destination_type,
        destination,
      );
    const isBlockedSendingOtp =
      !!arrayOfOtpsSortedByDESC[0]?.block_expired_at &&
      new Date(arrayOfOtpsSortedByDESC[0]?.block_expired_at) > new Date();

    if (isBlockedSendingOtp)
      return res
        .status(HttpStatus.TOO_MANY_REQUESTS)
        .json(getError(LIST_ERROR_CODES.SS24507));

    req.body.destination = destination;
    req.body.numberOfSendingOtp = arrayOfOtpsSortedByDESC.length;

    next();
  }
}
