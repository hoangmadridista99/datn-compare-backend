import { MigrationInterface, QueryRunner } from 'typeorm';

export class UpdateMigration1683815745167 implements MigrationInterface {
  name = 'UpdateMigration1683815745167';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "ratings" DROP CONSTRAINT "FK_7c17613c916717c7e99f51108ed"`,
    );
    await queryRunner.query(
      `ALTER TABLE "insurance-objectives" DROP CONSTRAINT "FK_0a08d3edff248918fc65ee86c2d"`,
    );
    await queryRunner.query(
      `CREATE TABLE "blogs" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "deleted_at" TIMESTAMP WITH TIME ZONE, "title" character varying NOT NULL, "description" character varying NOT NULL, "banner_image_url" character varying NOT NULL, "content" character varying NOT NULL, "target_about" character varying NOT NULL, "user_id" uuid NOT NULL, CONSTRAINT "PK_e113335f11c926da929a625f118" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "blog-comments" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "deleted_at" TIMESTAMP WITH TIME ZONE, "comment" character varying NOT NULL, "user_id" uuid NOT NULL, "blog_id" uuid NOT NULL, "is_hide" boolean NOT NULL DEFAULT false, CONSTRAINT "PK_1370a7e4456fd6a6f91376117c3" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "blog-categories" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "deleted_at" TIMESTAMP WITH TIME ZONE, "value" character varying NOT NULL, "vn_label" character varying NOT NULL, "en_label" character varying NOT NULL, "is_hide" boolean NOT NULL DEFAULT true, CONSTRAINT "PK_ff7db0505d0085cc83c355d8c57" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "insurances_objective_of_insurance_insurance-objectives" ("insurancesId" uuid NOT NULL, "insuranceObjectivesId" uuid NOT NULL, CONSTRAINT "PK_5c83f34957bdaaa3c2eca0ddfd6" PRIMARY KEY ("insurancesId", "insuranceObjectivesId"))`,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_c562b5ee7404ad3dce68013d80" ON "insurances_objective_of_insurance_insurance-objectives" ("insurancesId") `,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_d4b99f9890bea6609a676210c3" ON "insurances_objective_of_insurance_insurance-objectives" ("insuranceObjectivesId") `,
    );
    await queryRunner.query(`ALTER TABLE "users" DROP COLUMN "createdAt"`);
    await queryRunner.query(`ALTER TABLE "users" DROP COLUMN "updatedAt"`);
    await queryRunner.query(`ALTER TABLE "users" DROP COLUMN "deletedAt"`);
    await queryRunner.query(`ALTER TABLE "ratings" DROP COLUMN "createdAt"`);
    await queryRunner.query(`ALTER TABLE "ratings" DROP COLUMN "updatedAt"`);
    await queryRunner.query(`ALTER TABLE "ratings" DROP COLUMN "deletedAt"`);
    await queryRunner.query(
      `ALTER TABLE "insurance-objectives" DROP COLUMN "createdAt"`,
    );
    await queryRunner.query(
      `ALTER TABLE "insurance-objectives" DROP COLUMN "updatedAt"`,
    );
    await queryRunner.query(
      `ALTER TABLE "insurance-objectives" DROP COLUMN "deletedAt"`,
    );
    await queryRunner.query(
      `ALTER TABLE "insurance-objectives" DROP COLUMN "insuranceId"`,
    );
    await queryRunner.query(
      `ALTER TABLE "additional-benefits" DROP COLUMN "createdAt"`,
    );
    await queryRunner.query(
      `ALTER TABLE "additional-benefits" DROP COLUMN "updatedAt"`,
    );
    await queryRunner.query(
      `ALTER TABLE "additional-benefits" DROP COLUMN "deletedAt"`,
    );
    await queryRunner.query(
      `ALTER TABLE "customer-orientation" DROP COLUMN "createdAt"`,
    );
    await queryRunner.query(
      `ALTER TABLE "customer-orientation" DROP COLUMN "updatedAt"`,
    );
    await queryRunner.query(
      `ALTER TABLE "customer-orientation" DROP COLUMN "deletedAt"`,
    );
    await queryRunner.query(`ALTER TABLE "terms" DROP COLUMN "createdAt"`);
    await queryRunner.query(`ALTER TABLE "terms" DROP COLUMN "updatedAt"`);
    await queryRunner.query(`ALTER TABLE "terms" DROP COLUMN "deletedAt"`);
    await queryRunner.query(`ALTER TABLE "insurances" DROP COLUMN "createdAt"`);
    await queryRunner.query(`ALTER TABLE "insurances" DROP COLUMN "updatedAt"`);
    await queryRunner.query(`ALTER TABLE "insurances" DROP COLUMN "deletedAt"`);
    await queryRunner.query(`ALTER TABLE "companies" DROP COLUMN "createdAt"`);
    await queryRunner.query(`ALTER TABLE "companies" DROP COLUMN "updatedAt"`);
    await queryRunner.query(`ALTER TABLE "companies" DROP COLUMN "deletedAt"`);
    await queryRunner.query(`ALTER TABLE "otp" DROP COLUMN "createdAt"`);
    await queryRunner.query(`ALTER TABLE "otp" DROP COLUMN "updatedAt"`);
    await queryRunner.query(`ALTER TABLE "otp" DROP COLUMN "deletedAt"`);
    await queryRunner.query(
      `ALTER TABLE "users" ADD "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()`,
    );
    await queryRunner.query(
      `ALTER TABLE "users" ADD "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()`,
    );
    await queryRunner.query(
      `ALTER TABLE "users" ADD "deleted_at" TIMESTAMP WITH TIME ZONE`,
    );
    await queryRunner.query(
      `ALTER TABLE "users" ADD "gender" character varying`,
    );
    await queryRunner.query(
      `ALTER TABLE "users" ADD "avatar_profile_url" character varying`,
    );
    await queryRunner.query(
      `ALTER TABLE "ratings" ADD "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()`,
    );
    await queryRunner.query(
      `ALTER TABLE "ratings" ADD "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()`,
    );
    await queryRunner.query(
      `ALTER TABLE "ratings" ADD "deleted_at" TIMESTAMP WITH TIME ZONE`,
    );
    await queryRunner.query(
      `ALTER TABLE "ratings" ADD "is_verified_by_admin" boolean NOT NULL DEFAULT false`,
    );
    await queryRunner.query(
      `ALTER TABLE "ratings" ADD "is_hide" boolean NOT NULL DEFAULT false`,
    );
    await queryRunner.query(
      `ALTER TABLE "insurance-objectives" ADD "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()`,
    );
    await queryRunner.query(
      `ALTER TABLE "insurance-objectives" ADD "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()`,
    );
    await queryRunner.query(
      `ALTER TABLE "insurance-objectives" ADD "deleted_at" TIMESTAMP WITH TIME ZONE`,
    );
    await queryRunner.query(
      `ALTER TABLE "additional-benefits" ADD "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()`,
    );
    await queryRunner.query(
      `ALTER TABLE "additional-benefits" ADD "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()`,
    );
    await queryRunner.query(
      `ALTER TABLE "additional-benefits" ADD "deleted_at" TIMESTAMP WITH TIME ZONE`,
    );
    await queryRunner.query(
      `ALTER TABLE "customer-orientation" ADD "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()`,
    );
    await queryRunner.query(
      `ALTER TABLE "customer-orientation" ADD "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()`,
    );
    await queryRunner.query(
      `ALTER TABLE "customer-orientation" ADD "deleted_at" TIMESTAMP WITH TIME ZONE`,
    );
    await queryRunner.query(
      `ALTER TABLE "terms" ADD "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()`,
    );
    await queryRunner.query(
      `ALTER TABLE "terms" ADD "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()`,
    );
    await queryRunner.query(
      `ALTER TABLE "terms" ADD "deleted_at" TIMESTAMP WITH TIME ZONE`,
    );
    await queryRunner.query(
      `ALTER TABLE "insurances" ADD "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()`,
    );
    await queryRunner.query(
      `ALTER TABLE "insurances" ADD "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()`,
    );
    await queryRunner.query(
      `ALTER TABLE "insurances" ADD "deleted_at" TIMESTAMP WITH TIME ZONE`,
    );
    await queryRunner.query(
      `ALTER TABLE "companies" ADD "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()`,
    );
    await queryRunner.query(
      `ALTER TABLE "companies" ADD "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()`,
    );
    await queryRunner.query(
      `ALTER TABLE "companies" ADD "deleted_at" TIMESTAMP WITH TIME ZONE`,
    );
    await queryRunner.query(
      `ALTER TABLE "otp" ADD "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()`,
    );
    await queryRunner.query(
      `ALTER TABLE "otp" ADD "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()`,
    );
    await queryRunner.query(
      `ALTER TABLE "otp" ADD "deleted_at" TIMESTAMP WITH TIME ZONE`,
    );
    await queryRunner.query(
      `ALTER TABLE "ratings" ADD CONSTRAINT "FK_7c17613c916717c7e99f51108ed" FOREIGN KEY ("insuranceId") REFERENCES "insurances"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "blogs" ADD CONSTRAINT "FK_57d7c984ba4a3fa3b4ea2fb5553" FOREIGN KEY ("user_id") REFERENCES "users"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "blog-comments" ADD CONSTRAINT "FK_cf2e8e3f2dd46393594db83000d" FOREIGN KEY ("user_id") REFERENCES "users"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "blog-comments" ADD CONSTRAINT "FK_80820ceee271ae122011dcd0066" FOREIGN KEY ("blog_id") REFERENCES "blogs"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "insurances_objective_of_insurance_insurance-objectives" ADD CONSTRAINT "FK_c562b5ee7404ad3dce68013d802" FOREIGN KEY ("insurancesId") REFERENCES "insurances"("id") ON DELETE CASCADE ON UPDATE CASCADE`,
    );
    await queryRunner.query(
      `ALTER TABLE "insurances_objective_of_insurance_insurance-objectives" ADD CONSTRAINT "FK_d4b99f9890bea6609a676210c3a" FOREIGN KEY ("insuranceObjectivesId") REFERENCES "insurance-objectives"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "insurances_objective_of_insurance_insurance-objectives" DROP CONSTRAINT "FK_d4b99f9890bea6609a676210c3a"`,
    );
    await queryRunner.query(
      `ALTER TABLE "insurances_objective_of_insurance_insurance-objectives" DROP CONSTRAINT "FK_c562b5ee7404ad3dce68013d802"`,
    );
    await queryRunner.query(
      `ALTER TABLE "blog-comments" DROP CONSTRAINT "FK_80820ceee271ae122011dcd0066"`,
    );
    await queryRunner.query(
      `ALTER TABLE "blog-comments" DROP CONSTRAINT "FK_cf2e8e3f2dd46393594db83000d"`,
    );
    await queryRunner.query(
      `ALTER TABLE "blogs" DROP CONSTRAINT "FK_57d7c984ba4a3fa3b4ea2fb5553"`,
    );
    await queryRunner.query(
      `ALTER TABLE "ratings" DROP CONSTRAINT "FK_7c17613c916717c7e99f51108ed"`,
    );
    await queryRunner.query(`ALTER TABLE "otp" DROP COLUMN "deleted_at"`);
    await queryRunner.query(`ALTER TABLE "otp" DROP COLUMN "updated_at"`);
    await queryRunner.query(`ALTER TABLE "otp" DROP COLUMN "created_at"`);
    await queryRunner.query(`ALTER TABLE "companies" DROP COLUMN "deleted_at"`);
    await queryRunner.query(`ALTER TABLE "companies" DROP COLUMN "updated_at"`);
    await queryRunner.query(`ALTER TABLE "companies" DROP COLUMN "created_at"`);
    await queryRunner.query(
      `ALTER TABLE "insurances" DROP COLUMN "deleted_at"`,
    );
    await queryRunner.query(
      `ALTER TABLE "insurances" DROP COLUMN "updated_at"`,
    );
    await queryRunner.query(
      `ALTER TABLE "insurances" DROP COLUMN "created_at"`,
    );
    await queryRunner.query(`ALTER TABLE "terms" DROP COLUMN "deleted_at"`);
    await queryRunner.query(`ALTER TABLE "terms" DROP COLUMN "updated_at"`);
    await queryRunner.query(`ALTER TABLE "terms" DROP COLUMN "created_at"`);
    await queryRunner.query(
      `ALTER TABLE "customer-orientation" DROP COLUMN "deleted_at"`,
    );
    await queryRunner.query(
      `ALTER TABLE "customer-orientation" DROP COLUMN "updated_at"`,
    );
    await queryRunner.query(
      `ALTER TABLE "customer-orientation" DROP COLUMN "created_at"`,
    );
    await queryRunner.query(
      `ALTER TABLE "additional-benefits" DROP COLUMN "deleted_at"`,
    );
    await queryRunner.query(
      `ALTER TABLE "additional-benefits" DROP COLUMN "updated_at"`,
    );
    await queryRunner.query(
      `ALTER TABLE "additional-benefits" DROP COLUMN "created_at"`,
    );
    await queryRunner.query(
      `ALTER TABLE "insurance-objectives" DROP COLUMN "deleted_at"`,
    );
    await queryRunner.query(
      `ALTER TABLE "insurance-objectives" DROP COLUMN "updated_at"`,
    );
    await queryRunner.query(
      `ALTER TABLE "insurance-objectives" DROP COLUMN "created_at"`,
    );
    await queryRunner.query(`ALTER TABLE "ratings" DROP COLUMN "is_hide"`);
    await queryRunner.query(
      `ALTER TABLE "ratings" DROP COLUMN "is_verified_by_admin"`,
    );
    await queryRunner.query(`ALTER TABLE "ratings" DROP COLUMN "deleted_at"`);
    await queryRunner.query(`ALTER TABLE "ratings" DROP COLUMN "updated_at"`);
    await queryRunner.query(`ALTER TABLE "ratings" DROP COLUMN "created_at"`);
    await queryRunner.query(
      `ALTER TABLE "users" DROP COLUMN "avatar_profile_url"`,
    );
    await queryRunner.query(`ALTER TABLE "users" DROP COLUMN "gender"`);
    await queryRunner.query(`ALTER TABLE "users" DROP COLUMN "deleted_at"`);
    await queryRunner.query(`ALTER TABLE "users" DROP COLUMN "updated_at"`);
    await queryRunner.query(`ALTER TABLE "users" DROP COLUMN "created_at"`);
    await queryRunner.query(
      `ALTER TABLE "otp" ADD "deletedAt" TIMESTAMP WITH TIME ZONE`,
    );
    await queryRunner.query(
      `ALTER TABLE "otp" ADD "updatedAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()`,
    );
    await queryRunner.query(
      `ALTER TABLE "otp" ADD "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()`,
    );
    await queryRunner.query(
      `ALTER TABLE "companies" ADD "deletedAt" TIMESTAMP WITH TIME ZONE`,
    );
    await queryRunner.query(
      `ALTER TABLE "companies" ADD "updatedAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()`,
    );
    await queryRunner.query(
      `ALTER TABLE "companies" ADD "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()`,
    );
    await queryRunner.query(
      `ALTER TABLE "insurances" ADD "deletedAt" TIMESTAMP WITH TIME ZONE`,
    );
    await queryRunner.query(
      `ALTER TABLE "insurances" ADD "updatedAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()`,
    );
    await queryRunner.query(
      `ALTER TABLE "insurances" ADD "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()`,
    );
    await queryRunner.query(
      `ALTER TABLE "terms" ADD "deletedAt" TIMESTAMP WITH TIME ZONE`,
    );
    await queryRunner.query(
      `ALTER TABLE "terms" ADD "updatedAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()`,
    );
    await queryRunner.query(
      `ALTER TABLE "terms" ADD "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()`,
    );
    await queryRunner.query(
      `ALTER TABLE "customer-orientation" ADD "deletedAt" TIMESTAMP WITH TIME ZONE`,
    );
    await queryRunner.query(
      `ALTER TABLE "customer-orientation" ADD "updatedAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()`,
    );
    await queryRunner.query(
      `ALTER TABLE "customer-orientation" ADD "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()`,
    );
    await queryRunner.query(
      `ALTER TABLE "additional-benefits" ADD "deletedAt" TIMESTAMP WITH TIME ZONE`,
    );
    await queryRunner.query(
      `ALTER TABLE "additional-benefits" ADD "updatedAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()`,
    );
    await queryRunner.query(
      `ALTER TABLE "additional-benefits" ADD "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()`,
    );
    await queryRunner.query(
      `ALTER TABLE "insurance-objectives" ADD "insuranceId" uuid`,
    );
    await queryRunner.query(
      `ALTER TABLE "insurance-objectives" ADD "deletedAt" TIMESTAMP WITH TIME ZONE`,
    );
    await queryRunner.query(
      `ALTER TABLE "insurance-objectives" ADD "updatedAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()`,
    );
    await queryRunner.query(
      `ALTER TABLE "insurance-objectives" ADD "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()`,
    );
    await queryRunner.query(
      `ALTER TABLE "ratings" ADD "deletedAt" TIMESTAMP WITH TIME ZONE`,
    );
    await queryRunner.query(
      `ALTER TABLE "ratings" ADD "updatedAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()`,
    );
    await queryRunner.query(
      `ALTER TABLE "ratings" ADD "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()`,
    );
    await queryRunner.query(
      `ALTER TABLE "users" ADD "deletedAt" TIMESTAMP WITH TIME ZONE`,
    );
    await queryRunner.query(
      `ALTER TABLE "users" ADD "updatedAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()`,
    );
    await queryRunner.query(
      `ALTER TABLE "users" ADD "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()`,
    );
    await queryRunner.query(
      `DROP INDEX "public"."IDX_d4b99f9890bea6609a676210c3"`,
    );
    await queryRunner.query(
      `DROP INDEX "public"."IDX_c562b5ee7404ad3dce68013d80"`,
    );
    await queryRunner.query(
      `DROP TABLE "insurances_objective_of_insurance_insurance-objectives"`,
    );
    await queryRunner.query(`DROP TABLE "blog-categories"`);
    await queryRunner.query(`DROP TABLE "blog-comments"`);
    await queryRunner.query(`DROP TABLE "blogs"`);
    await queryRunner.query(
      `ALTER TABLE "insurance-objectives" ADD CONSTRAINT "FK_0a08d3edff248918fc65ee86c2d" FOREIGN KEY ("insuranceId") REFERENCES "insurances"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "ratings" ADD CONSTRAINT "FK_7c17613c916717c7e99f51108ed" FOREIGN KEY ("insuranceId") REFERENCES "insurances"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }
}
