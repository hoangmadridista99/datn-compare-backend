import { MigrationInterface, QueryRunner } from 'typeorm';

export class UpdateMigration1682416277196 implements MigrationInterface {
  name = 'UpdateMigration1682416277196';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "users" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "deletedAt" TIMESTAMP WITH TIME ZONE, "password" character varying NOT NULL, "first_name" character varying NOT NULL, "last_name" character varying NOT NULL, "role" "public"."users_role_enum" NOT NULL DEFAULT 'user', "phone" character varying NOT NULL, "date_of_birth" character varying NOT NULL, "email" character varying, "address" character varying, "company_name" character varying, "account" character varying, "vendor_status" character varying, "company_other" character varying, "company_id" uuid, CONSTRAINT "PK_a3ffb1c0c8416b9fc6f907b7433" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "ratings" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "deletedAt" TIMESTAMP WITH TIME ZONE, "score" integer NOT NULL, "comment" character varying NOT NULL, "insuranceId" uuid, "userId" uuid, CONSTRAINT "PK_0f31425b073219379545ad68ed9" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "insurance-objectives" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "deletedAt" TIMESTAMP WITH TIME ZONE, "objective_type" character varying NOT NULL, "insuranceId" uuid, CONSTRAINT "PK_d6d1ad9617c87c3d17e3f115b2c" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "additional-benefits" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "deletedAt" TIMESTAMP WITH TIME ZONE, "death_or_disability" text NOT NULL, "serious_illnesses" text NOT NULL, "health_care" text NOT NULL, "investment_benefit" text NOT NULL, "increasing_value_bonus" text NOT NULL, "for_child" text NOT NULL, "flexible_and_diverse" text NOT NULL, "termination_benefits" text NOT NULL, "expiration_benefits" text NOT NULL, "fee_exemption" text NOT NULL, CONSTRAINT "PK_aeb238f62c3f8723672e01a3983" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "customer-orientation" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "deletedAt" TIMESTAMP WITH TIME ZONE, "acceptance_rate" text NOT NULL, "completion_time_deal" text NOT NULL, "end_of_process" text NOT NULL, "withdrawal_time" text NOT NULL, "reception_and_processing_time" text NOT NULL, CONSTRAINT "PK_4e9b744106d8588e235e235495f" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "terms" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "deletedAt" TIMESTAMP WITH TIME ZONE, "age_eligibility" text NOT NULL, "deadline_for_deal" text NOT NULL, "insurance_minimum_fee" text NOT NULL, "total_minimum_insured" text NOT NULL, "deadline_for_payment" text NOT NULL, "insured_person" text NOT NULL, "profession" text NOT NULL, "age_of_contract_termination" text NOT NULL, "termination_conditions" text NOT NULL, "total_sum_insured" text NOT NULL, "monthly_fee" text NOT NULL, CONSTRAINT "PK_33b6fe77d6ace7ff43cc8a65958" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "insurances" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "deletedAt" TIMESTAMP WITH TIME ZONE, "name" character varying NOT NULL, "company_id" uuid, "description_insurance" character varying NOT NULL, "insurance_type" character varying NOT NULL, "rating_scores" double precision NOT NULL DEFAULT '0', "total_rating" integer NOT NULL DEFAULT '0', "key_benefits" character varying NOT NULL, "benefits_illustration_table" character varying, "documentation_url" character varying, "additionalBenefitsId" uuid, "customerOrientationId" uuid, "termsId" uuid, CONSTRAINT "REL_3484182277fc4210759eb50dac" UNIQUE ("additionalBenefitsId"), CONSTRAINT "REL_b04cc091d716f7ee69f64e6fd0" UNIQUE ("customerOrientationId"), CONSTRAINT "REL_12fbc0217439ba63cc3e9bc835" UNIQUE ("termsId"), CONSTRAINT "PK_1a09d6f8e21d5eba4ad19d9ec0b" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "companies" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "deletedAt" TIMESTAMP WITH TIME ZONE, "logo" character varying NOT NULL, "long_name" character varying NOT NULL, "short_name" character varying NOT NULL, "homepage" character varying NOT NULL, CONSTRAINT "PK_d4bc3e82a314fa9e29f652c2c22" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "otp" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "deletedAt" TIMESTAMP WITH TIME ZONE, "destination" character varying NOT NULL, "otp_type" "public"."otp_otp_type_enum" NOT NULL, "destination_type" "public"."otp_destination_type_enum" NOT NULL, "code" character varying NOT NULL, "expired_at" TIMESTAMP WITH TIME ZONE NOT NULL, "otp_status" character varying NOT NULL DEFAULT 'pending', "block_expired_at" TIMESTAMP WITH TIME ZONE, CONSTRAINT "PK_32556d9d7b22031d7d0e1fd6723" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `ALTER TABLE "users" ADD CONSTRAINT "FK_7ae6334059289559722437bcc1c" FOREIGN KEY ("company_id") REFERENCES "companies"("id") ON DELETE SET NULL ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "ratings" ADD CONSTRAINT "FK_7c17613c916717c7e99f51108ed" FOREIGN KEY ("insuranceId") REFERENCES "insurances"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "ratings" ADD CONSTRAINT "FK_4d0b0e3a4c4af854d225154ba40" FOREIGN KEY ("userId") REFERENCES "users"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "insurance-objectives" ADD CONSTRAINT "FK_0a08d3edff248918fc65ee86c2d" FOREIGN KEY ("insuranceId") REFERENCES "insurances"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "insurances" ADD CONSTRAINT "FK_5785872e5927a7a24ec75a21f78" FOREIGN KEY ("company_id") REFERENCES "companies"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "insurances" ADD CONSTRAINT "FK_3484182277fc4210759eb50dace" FOREIGN KEY ("additionalBenefitsId") REFERENCES "additional-benefits"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "insurances" ADD CONSTRAINT "FK_b04cc091d716f7ee69f64e6fd0a" FOREIGN KEY ("customerOrientationId") REFERENCES "customer-orientation"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "insurances" ADD CONSTRAINT "FK_12fbc0217439ba63cc3e9bc8358" FOREIGN KEY ("termsId") REFERENCES "terms"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "insurances" DROP CONSTRAINT "FK_12fbc0217439ba63cc3e9bc8358"`,
    );
    await queryRunner.query(
      `ALTER TABLE "insurances" DROP CONSTRAINT "FK_b04cc091d716f7ee69f64e6fd0a"`,
    );
    await queryRunner.query(
      `ALTER TABLE "insurances" DROP CONSTRAINT "FK_3484182277fc4210759eb50dace"`,
    );
    await queryRunner.query(
      `ALTER TABLE "insurances" DROP CONSTRAINT "FK_5785872e5927a7a24ec75a21f78"`,
    );
    await queryRunner.query(
      `ALTER TABLE "insurance-objectives" DROP CONSTRAINT "FK_0a08d3edff248918fc65ee86c2d"`,
    );
    await queryRunner.query(
      `ALTER TABLE "ratings" DROP CONSTRAINT "FK_4d0b0e3a4c4af854d225154ba40"`,
    );
    await queryRunner.query(
      `ALTER TABLE "ratings" DROP CONSTRAINT "FK_7c17613c916717c7e99f51108ed"`,
    );
    await queryRunner.query(
      `ALTER TABLE "users" DROP CONSTRAINT "FK_7ae6334059289559722437bcc1c"`,
    );
    await queryRunner.query(`DROP TABLE "otp"`);
    await queryRunner.query(`DROP TABLE "companies"`);
    await queryRunner.query(`DROP TABLE "insurances"`);
    await queryRunner.query(`DROP TABLE "terms"`);
    await queryRunner.query(`DROP TABLE "customer-orientation"`);
    await queryRunner.query(`DROP TABLE "additional-benefits"`);
    await queryRunner.query(`DROP TABLE "insurance-objectives"`);
    await queryRunner.query(`DROP TABLE "ratings"`);
    await queryRunner.query(`DROP TABLE "users"`);
  }
}
