import { MigrationInterface, QueryRunner } from 'typeorm';

export class FixInsuranceTemp1682507070950 implements MigrationInterface {
  name = 'FixInsuranceTemp1682507070950';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "terms" DROP COLUMN "total_minimum_insured"`,
    );
    await queryRunner.query(
      `ALTER TABLE "insurance-objectives" ADD "label" character varying`,
    );
    await queryRunner.query(`ALTER TABLE "insurances" ADD "userId" uuid`);
    await queryRunner.query(
      `ALTER TABLE "insurances" ADD CONSTRAINT "FK_e120a9360b9fe60b62301c38ba7" FOREIGN KEY ("userId") REFERENCES "users"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "insurances" DROP CONSTRAINT "FK_e120a9360b9fe60b62301c38ba7"`,
    );
    await queryRunner.query(`ALTER TABLE "insurances" DROP COLUMN "userId"`);
    await queryRunner.query(
      `ALTER TABLE "insurance-objectives" DROP COLUMN "label"`,
    );
    await queryRunner.query(
      `ALTER TABLE "terms" ADD "total_minimum_insured" jsonb NOT NULL`,
    );
  }
}
