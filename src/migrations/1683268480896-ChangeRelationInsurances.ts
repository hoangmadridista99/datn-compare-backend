import { MigrationInterface, QueryRunner } from "typeorm";

export class ChangeRelationInsurances1683268480896 implements MigrationInterface {
    name = 'ChangeRelationInsurances1683268480896'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "insurance-objectives" DROP CONSTRAINT "FK_0a08d3edff248918fc65ee86c2d"`);
        await queryRunner.query(`CREATE TABLE "insurances_objective_of_insurance_insurance-objectives" ("insurancesId" uuid NOT NULL, "insuranceObjectivesId" uuid NOT NULL, CONSTRAINT "PK_5c83f34957bdaaa3c2eca0ddfd6" PRIMARY KEY ("insurancesId", "insuranceObjectivesId"))`);
        await queryRunner.query(`CREATE INDEX "IDX_c562b5ee7404ad3dce68013d80" ON "insurances_objective_of_insurance_insurance-objectives" ("insurancesId") `);
        await queryRunner.query(`CREATE INDEX "IDX_d4b99f9890bea6609a676210c3" ON "insurances_objective_of_insurance_insurance-objectives" ("insuranceObjectivesId") `);
        await queryRunner.query(`ALTER TABLE "insurance-objectives" DROP COLUMN "insuranceId"`);
        await queryRunner.query(`ALTER TABLE "insurances_objective_of_insurance_insurance-objectives" ADD CONSTRAINT "FK_c562b5ee7404ad3dce68013d802" FOREIGN KEY ("insurancesId") REFERENCES "insurances"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
        await queryRunner.query(`ALTER TABLE "insurances_objective_of_insurance_insurance-objectives" ADD CONSTRAINT "FK_d4b99f9890bea6609a676210c3a" FOREIGN KEY ("insuranceObjectivesId") REFERENCES "insurance-objectives"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "insurances_objective_of_insurance_insurance-objectives" DROP CONSTRAINT "FK_d4b99f9890bea6609a676210c3a"`);
        await queryRunner.query(`ALTER TABLE "insurances_objective_of_insurance_insurance-objectives" DROP CONSTRAINT "FK_c562b5ee7404ad3dce68013d802"`);
        await queryRunner.query(`ALTER TABLE "insurance-objectives" ADD "insuranceId" uuid`);
        await queryRunner.query(`DROP INDEX "public"."IDX_d4b99f9890bea6609a676210c3"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_c562b5ee7404ad3dce68013d80"`);
        await queryRunner.query(`DROP TABLE "insurances_objective_of_insurance_insurance-objectives"`);
        await queryRunner.query(`ALTER TABLE "insurance-objectives" ADD CONSTRAINT "FK_0a08d3edff248918fc65ee86c2d" FOREIGN KEY ("insuranceId") REFERENCES "insurances"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

}
