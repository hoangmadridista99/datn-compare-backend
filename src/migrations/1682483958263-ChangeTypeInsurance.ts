import { MigrationInterface, QueryRunner } from 'typeorm';

export class ChangeTypeInsurance1682483958263 implements MigrationInterface {
  name = 'ChangeTypeInsurance1682483958263';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "additional-benefits" DROP COLUMN "death_or_disability"`,
    );
    await queryRunner.query(
      `ALTER TABLE "additional-benefits" ADD "death_or_disability" jsonb NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "additional-benefits" DROP COLUMN "serious_illnesses"`,
    );
    await queryRunner.query(
      `ALTER TABLE "additional-benefits" ADD "serious_illnesses" jsonb NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "additional-benefits" DROP COLUMN "health_care"`,
    );
    await queryRunner.query(
      `ALTER TABLE "additional-benefits" ADD "health_care" jsonb NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "additional-benefits" DROP COLUMN "investment_benefit"`,
    );
    await queryRunner.query(
      `ALTER TABLE "additional-benefits" ADD "investment_benefit" jsonb NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "additional-benefits" DROP COLUMN "increasing_value_bonus"`,
    );
    await queryRunner.query(
      `ALTER TABLE "additional-benefits" ADD "increasing_value_bonus" jsonb NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "additional-benefits" DROP COLUMN "for_child"`,
    );
    await queryRunner.query(
      `ALTER TABLE "additional-benefits" ADD "for_child" jsonb NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "additional-benefits" DROP COLUMN "flexible_and_diverse"`,
    );
    await queryRunner.query(
      `ALTER TABLE "additional-benefits" ADD "flexible_and_diverse" jsonb NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "additional-benefits" DROP COLUMN "termination_benefits"`,
    );
    await queryRunner.query(
      `ALTER TABLE "additional-benefits" ADD "termination_benefits" jsonb NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "additional-benefits" DROP COLUMN "expiration_benefits"`,
    );
    await queryRunner.query(
      `ALTER TABLE "additional-benefits" ADD "expiration_benefits" jsonb NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "additional-benefits" DROP COLUMN "fee_exemption"`,
    );
    await queryRunner.query(
      `ALTER TABLE "additional-benefits" ADD "fee_exemption" jsonb NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "customer-orientation" DROP COLUMN "acceptance_rate"`,
    );
    await queryRunner.query(
      `ALTER TABLE "customer-orientation" ADD "acceptance_rate" jsonb NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "customer-orientation" DROP COLUMN "completion_time_deal"`,
    );
    await queryRunner.query(
      `ALTER TABLE "customer-orientation" ADD "completion_time_deal" jsonb NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "customer-orientation" DROP COLUMN "end_of_process"`,
    );
    await queryRunner.query(
      `ALTER TABLE "customer-orientation" ADD "end_of_process" jsonb NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "customer-orientation" DROP COLUMN "withdrawal_time"`,
    );
    await queryRunner.query(
      `ALTER TABLE "customer-orientation" ADD "withdrawal_time" jsonb NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "customer-orientation" DROP COLUMN "reception_and_processing_time"`,
    );
    await queryRunner.query(
      `ALTER TABLE "customer-orientation" ADD "reception_and_processing_time" jsonb NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "terms" DROP COLUMN "age_eligibility"`,
    );
    await queryRunner.query(
      `ALTER TABLE "terms" ADD "age_eligibility" jsonb NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "terms" DROP COLUMN "deadline_for_deal"`,
    );
    await queryRunner.query(
      `ALTER TABLE "terms" ADD "deadline_for_deal" jsonb NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "terms" DROP COLUMN "insurance_minimum_fee"`,
    );
    await queryRunner.query(
      `ALTER TABLE "terms" ADD "insurance_minimum_fee" jsonb NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "terms" DROP COLUMN "total_minimum_insured"`,
    );
    await queryRunner.query(
      `ALTER TABLE "terms" ADD "total_minimum_insured" jsonb NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "terms" DROP COLUMN "deadline_for_payment"`,
    );
    await queryRunner.query(
      `ALTER TABLE "terms" ADD "deadline_for_payment" jsonb NOT NULL`,
    );
    await queryRunner.query(`ALTER TABLE "terms" DROP COLUMN "insured_person"`);
    await queryRunner.query(
      `ALTER TABLE "terms" ADD "insured_person" jsonb NOT NULL`,
    );
    await queryRunner.query(`ALTER TABLE "terms" DROP COLUMN "profession"`);
    await queryRunner.query(
      `ALTER TABLE "terms" ADD "profession" jsonb NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "terms" DROP COLUMN "age_of_contract_termination"`,
    );
    await queryRunner.query(
      `ALTER TABLE "terms" ADD "age_of_contract_termination" jsonb NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "terms" DROP COLUMN "termination_conditions"`,
    );
    await queryRunner.query(
      `ALTER TABLE "terms" ADD "termination_conditions" jsonb NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "terms" DROP COLUMN "total_sum_insured"`,
    );
    await queryRunner.query(
      `ALTER TABLE "terms" ADD "total_sum_insured" jsonb NOT NULL`,
    );
    await queryRunner.query(`ALTER TABLE "terms" DROP COLUMN "monthly_fee"`);
    await queryRunner.query(
      `ALTER TABLE "terms" ADD "monthly_fee" jsonb NOT NULL`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "terms" DROP COLUMN "monthly_fee"`);
    await queryRunner.query(
      `ALTER TABLE "terms" ADD "monthly_fee" text NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "terms" DROP COLUMN "total_sum_insured"`,
    );
    await queryRunner.query(
      `ALTER TABLE "terms" ADD "total_sum_insured" text NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "terms" DROP COLUMN "termination_conditions"`,
    );
    await queryRunner.query(
      `ALTER TABLE "terms" ADD "termination_conditions" text NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "terms" DROP COLUMN "age_of_contract_termination"`,
    );
    await queryRunner.query(
      `ALTER TABLE "terms" ADD "age_of_contract_termination" text NOT NULL`,
    );
    await queryRunner.query(`ALTER TABLE "terms" DROP COLUMN "profession"`);
    await queryRunner.query(
      `ALTER TABLE "terms" ADD "profession" text NOT NULL`,
    );
    await queryRunner.query(`ALTER TABLE "terms" DROP COLUMN "insured_person"`);
    await queryRunner.query(
      `ALTER TABLE "terms" ADD "insured_person" text NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "terms" DROP COLUMN "deadline_for_payment"`,
    );
    await queryRunner.query(
      `ALTER TABLE "terms" ADD "deadline_for_payment" text NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "terms" DROP COLUMN "total_minimum_insured"`,
    );
    await queryRunner.query(
      `ALTER TABLE "terms" ADD "total_minimum_insured" text NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "terms" DROP COLUMN "insurance_minimum_fee"`,
    );
    await queryRunner.query(
      `ALTER TABLE "terms" ADD "insurance_minimum_fee" text NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "terms" DROP COLUMN "deadline_for_deal"`,
    );
    await queryRunner.query(
      `ALTER TABLE "terms" ADD "deadline_for_deal" text NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "terms" DROP COLUMN "age_eligibility"`,
    );
    await queryRunner.query(
      `ALTER TABLE "terms" ADD "age_eligibility" text NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "customer-orientation" DROP COLUMN "reception_and_processing_time"`,
    );
    await queryRunner.query(
      `ALTER TABLE "customer-orientation" ADD "reception_and_processing_time" text NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "customer-orientation" DROP COLUMN "withdrawal_time"`,
    );
    await queryRunner.query(
      `ALTER TABLE "customer-orientation" ADD "withdrawal_time" text NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "customer-orientation" DROP COLUMN "end_of_process"`,
    );
    await queryRunner.query(
      `ALTER TABLE "customer-orientation" ADD "end_of_process" text NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "customer-orientation" DROP COLUMN "completion_time_deal"`,
    );
    await queryRunner.query(
      `ALTER TABLE "customer-orientation" ADD "completion_time_deal" text NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "customer-orientation" DROP COLUMN "acceptance_rate"`,
    );
    await queryRunner.query(
      `ALTER TABLE "customer-orientation" ADD "acceptance_rate" text NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "additional-benefits" DROP COLUMN "fee_exemption"`,
    );
    await queryRunner.query(
      `ALTER TABLE "additional-benefits" ADD "fee_exemption" text NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "additional-benefits" DROP COLUMN "expiration_benefits"`,
    );
    await queryRunner.query(
      `ALTER TABLE "additional-benefits" ADD "expiration_benefits" text NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "additional-benefits" DROP COLUMN "termination_benefits"`,
    );
    await queryRunner.query(
      `ALTER TABLE "additional-benefits" ADD "termination_benefits" text NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "additional-benefits" DROP COLUMN "flexible_and_diverse"`,
    );
    await queryRunner.query(
      `ALTER TABLE "additional-benefits" ADD "flexible_and_diverse" text NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "additional-benefits" DROP COLUMN "for_child"`,
    );
    await queryRunner.query(
      `ALTER TABLE "additional-benefits" ADD "for_child" text NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "additional-benefits" DROP COLUMN "increasing_value_bonus"`,
    );
    await queryRunner.query(
      `ALTER TABLE "additional-benefits" ADD "increasing_value_bonus" text NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "additional-benefits" DROP COLUMN "investment_benefit"`,
    );
    await queryRunner.query(
      `ALTER TABLE "additional-benefits" ADD "investment_benefit" text NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "additional-benefits" DROP COLUMN "health_care"`,
    );
    await queryRunner.query(
      `ALTER TABLE "additional-benefits" ADD "health_care" text NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "additional-benefits" DROP COLUMN "serious_illnesses"`,
    );
    await queryRunner.query(
      `ALTER TABLE "additional-benefits" ADD "serious_illnesses" text NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "additional-benefits" DROP COLUMN "death_or_disability"`,
    );
    await queryRunner.query(
      `ALTER TABLE "additional-benefits" ADD "death_or_disability" text NOT NULL`,
    );
  }
}
