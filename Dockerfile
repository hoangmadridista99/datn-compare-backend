FROM node:18.12.1

WORKDIR /app

COPY ["package.json", "yarn.lock", "./"]

RUN npm install @nestjs/cli

RUN npm install

COPY . .

RUN npm run build

CMD [ "npm", "start" ]
